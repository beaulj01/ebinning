import sys
from Bio import Entrez
from itertools import groupby
import urllib2

accs      = ["CP011313"    , \
			 "CP009114"    , \
			 "CP010361"    , \
			 "CP010392"    , \
			 "JWRM01000001", \
			 "JWRL01000009", \
			 "CP011980"    , \
			 "CP014755"    , \
			 "CP009208"    , \
			 "CP011976"    , \
			 "AQPG01000012", \
			 "LN824133"    , \
			 "CP011985"    , \
			 "CP011989"    , \
			 "CP008827"    , \
			 "CP007727"    , \
			 "CP008797"    , \
			 "CP007731"    , \
			 "CP009863"    , \
			 "CP009872"    , \
			 "CP009876"    , \
			 "CP009775"    , \
			 "CP009771"    , \
			 "AJZX01000004", \
			 "CP008831"]

def fasta_iter( fasta_name ):
	fh = open(fasta_name)
	# ditch the boolean (x[0]) and just keep the header or sequence since
	# we know they alternate.
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		# drop the ">"
		header = header.next()[1:].strip()
		# join all sequence lines to one.
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

def pull_fasta_get_comp( accs ):
	# Download all the fastas from this NCBI page
	db           = "nuccore"
	Entrez.email = "john.beaulaurier@mssm.edu"
	batchSize    = 100
	retmax       = 10**9

	# all_accs = list(np.loadtxt("accessions.txt", dtype="str"))
	all_accs   = accs
	fasta_name = "tmp.fasta"
	f_comps    = open("comp_vectors.kp_strains.out", "wb")
	f_out      = open("dists.kp_strains.out", "wb")
	try:
		for k,start in enumerate(range( 0,len(all_accs),batchSize )):
			#first get GI for query accesions
			batch_accs = all_accs[start:(start+batchSize)]
			sys.stderr.write( "Fetching %s entries from GenBank: %s\n" % (len(batch_accs), ", ".join(batch_accs[:10])))
			query  = " ".join(batch_accs)
			handle = Entrez.esearch( db=db,term=query,retmax=retmax )
			giList = Entrez.read(handle)['IdList']
			sys.stderr.write( "Found %s GI: %s\n" % (len(giList), ", ".join(giList[:10])))
			#post NCBI query
			search_handle     = Entrez.epost(db=db, id=",".join(giList))
			search_results    = Entrez.read(search_handle)
			webenv,query_key  = search_results["WebEnv"], search_results["QueryKey"]
			#fetch entries in batch
			# handle = Entrez.efetch(db=db, rettype="fasta", retstart=start, retmax=batchSize, webenv=webenv, query_key=query_key)
			handle = Entrez.efetch(db=db, rettype="fasta", webenv=webenv, query_key=query_key)
			# sys.stdout.write(handle.read())
			f = open(fasta_name, "wb")
			f.write(handle.read())
			f.close()
	except urllib2.HTTPError as err:
		if err.code==404:
			print "**** HTTPError 404! ****"
			error_urls.append(err.geturl())
			errors_n += 1
		else:
			raise

pull_fasta_get_comp( accs )