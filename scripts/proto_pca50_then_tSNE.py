import os,sys
import numpy as np
from itertools import product,groupby
from collections import Counter
import math
import subprocess

contig_fasta_fn =     sys.argv[1]
minContigSize   = int(sys.argv[2])
class Opts:
	def __init__(self):
		self.comp_kmer = 5

def run_OS_command( CMD ):
	# p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	p         = subprocess.Popen(CMD, shell=True)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts != 0:
		print "Failed command: %s" % CMD
	return sts, stdOutErr

def chunks( l, n ):
	"""
	Yield successive n-sized chunks from l.
	"""
	for i in xrange(0, len(l), n):
		yield l[i:i+n]

def fasta_iter( fasta_name ):
	"""
	Given a fasta file, yield tuples of (header, sequence).
	"""
	fh = open(fasta_name)
	# ditch the boolean (x[0]) and just keep the header or sequence since
	# we know they alternate.
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		# drop the ">"
		header = header.next()[1:].strip()
		# join all sequence lines to one.
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

def rev_comp_motif( motif ):
	COMP = {"A":"T", \
			"T":"A", \
			"C":"G", \
			"G":"C", \
			"W":"S", \
			"S":"W", \
			"M":"K", \
			"K":"M", \
			"R":"Y", \
			"Y":"R", \
			"B":"V", \
			"D":"H", \
			"H":"D", \
			"V":"B", \
			"N":"N", \
			"X":"X", \
			"*":"*"}
	rc_motif = []
	for char in motif[::-1]:
		rc_motif.append( COMP[char] )
	return "".join(rc_motif)

def kmer_freq ( mode, read_str, opts ):
	k = opts.comp_kmer
	kmers = []
	for seq in product("ATGC",repeat=k):
		kmers.append( "".join(seq) )

	kmer_counts = Counter()
	for j in range( len(read_str)-(k-1) ):
		motif    = read_str[j:j+k]
		kmer_counts[motif] += 1

	# Combine forward and reverse complement motifs into one count
	combined_kmer = Counter()
	for kmer in kmers:
		kmer_rc = rev_comp_motif(kmer)
		if not combined_kmer.get(kmer_rc):
			combined_kmer[kmer] = kmer_counts[kmer] + kmer_counts[kmer_rc] + 1

	return combined_kmer

opts              = Opts()
contig_comp_fn    = "contigs.comp"
contig_names_fn   = "contigs.names"
contig_labels_fn  = "contigs.labels"
contig_lengths_fn = "contigs.lengths"

def calc_contig_comps( contigs_fasta ):
	contigs_comp_strings = {}
	contigs_lens         = {}
	contigs_order        = []
	for i,(name,contig_seq) in enumerate(fasta_iter(contigs_fasta)):
		# if i==200: break


		name         = name.split("|")[0]
		contigs_order.append(name)
		contig_comp  = []
		contig_kmers = kmer_freq( "cmp", contig_seq, opts )
		for kmer,count in contig_kmers.iteritems():
			kmer_normed_comp = math.log(float(count) / sum(contig_kmers.values()))
			contig_comp.append(kmer_normed_comp)
		contig_comp_str = "\t".join(map(lambda x: str(round(x,6)), contig_comp))
		contigs_comp_strings[name] = contig_comp_str
		contigs_lens[name]         = len(contig_seq)
	return contigs_comp_strings, contigs_lens, contigs_order

print "Calculating whole contig compositions..."
contigs_comp_strings, contigs_lens, contigs_order = calc_contig_comps( contig_fasta_fn )

names = np.loadtxt("../contigs.names", dtype="str")
labs  = np.loadtxt("../contigs.labels", dtype="str")
labels_map = dict( zip(names,labs) )

f      = open(contig_comp_fn, "w")
f_name = open(contig_names_fn, "w")
f_len  = open(contig_lengths_fn, "w")
f_lab  = open(contig_labels_fn, "w")
f_lab_vb   = open(contig_labels_fn+".vizbin", "w")
f_fasta_vb = open("polished_assembly.vizbin.fasta", "w")
f_lab_vb.write("label\n")
for j,contig in enumerate(contigs_order):
	if contigs_lens[contig] >= minContigSize:
		f.write(     "%s\n" % contigs_comp_strings[contig])
		f_name.write("%s\n" % contig)
		f_len.write( "%s\n" % contigs_lens[contig])
		if labels_map.get(contig) and labels_map.get(contig)!="unknown":
			f_lab.write(   "%s\n" % labels_map[contig])
			f_lab_vb.write("%s\n" % labels_map[contig])
			for i,(name,contig_seq) in enumerate(fasta_iter(contig_fasta_fn)):
				if name.split("|")[0]==contig:
					f_fasta_vb.write(">%s\n" % name)
					f_fasta_vb.write("%s\n" % contig_seq)
		else:
			f_lab.write("unknown\n")
			# f_lab_vb.write("unknown\n")
f.close()
f_name.close()
f_len.close()
f_lab.close()
f_lab_vb.close()
f_fasta_vb.close()

print "Digesting the large contigs and calculating the digest compositions..."
names             = np.loadtxt(contig_names_fn, dtype="str")
labels            = np.loadtxt(contig_labels_fn, dtype="str")
lengths           = np.loadtxt(contig_lengths_fn, dtype="int")

max_size          = 50000
ref_comp_fn       = "contigs.frag_comp"
ref_names_fn      = "contigs.frag_names"
ref_labels_fn     = "contigs.frag_labels"
ref_lengths_fn    = "contigs.frag_lengths"
f_ref_comp        = open(ref_comp_fn,    "w")
f_ref_names       = open(ref_names_fn,   "w")
f_ref_labels      = open(ref_labels_fn,  "w")
f_ref_lengths     = open(ref_lengths_fn, "w")
for i,length in enumerate(lengths):
	if length>max_size:
		name  = names[i]
		label = labels[i]
		for j,(header,contig_seq) in enumerate(fasta_iter(contig_fasta_fn)):
				if header.split("|")[0] == name:
					seq_chunks = list(chunks(contig_seq, max_size))
					# Omit the chunk containing the remainder sequence
					seq_chunks = seq_chunks[:-1]
					for seq_chunk in seq_chunks:
						contig_comp  = []
						contig_kmers = kmer_freq( "cmp", seq_chunk, opts )
						for kmer,count in contig_kmers.iteritems():
							kmer_normed_comp = math.log(float(count) / sum(contig_kmers.values()))
							# kmer_normed_comp = float(count) / sum(contig_kmers.values())
							contig_comp.append(kmer_normed_comp)
						contig_comp_str = "\t".join(map(lambda x: str(round(x,6)), contig_comp))
						f_ref_comp.write("%s\n" %    contig_comp_str)
						f_ref_names.write("%s\n" %   name)
						f_ref_labels.write("%s\n" %  label)
						f_ref_lengths.write("%s\n" % len(seq_chunk))
f_ref_comp.close()
f_ref_names.close()
f_ref_labels.close()
f_ref_lengths.close()

def append_file( orig, new ):
	cat_CMD        = "cat %s %s > tmp.appended" % (orig, new)
	sts, stdOutErr = run_OS_command( cat_CMD )
	newname        = orig + ".wDigest"
	os.rename("tmp.appended", newname)
	os.remove(new)
	
append_file( contig_comp_fn,    ref_comp_fn )
append_file( contig_names_fn,   ref_names_fn )
append_file( contig_labels_fn,  ref_labels_fn )
append_file( contig_lengths_fn, ref_lengths_fn )

SNE_CMD = "python ~/gitRepo/eBinning/src/bhtsne.py -v -i contigs.comp -l contigs.labels -z contigs.lengths -o contigs.comp.noPCA50.2D -s contigs.comp.noPCA50.scatter.png -n contigs_composition_noPCA50 -p 30"
print "Running %s" % SNE_CMD
sts, stdOutErr = run_OS_command( SNE_CMD )

SNE_CMD = "python ~/gitRepo/eBinning/src/bhtsne.py -v -i contigs.comp.wDigest -l contigs.labels.wDigest -z contigs.lengths.wDigest -o contigs.comp.noPCA50.wDigest.2D -s contigs.comp.noPCA50.wDigest.scatter.png -n contigs_composition_noPCA50_wDigest -p 30"
print "Running %s" % SNE_CMD
sts, stdOutErr = run_OS_command( SNE_CMD )

PCA_CMD = "Rscript ~/gitRepo/eBinning/src/run_iPCA.r contigs.comp contigs.comp.50D 50 1e-8"
print "Running %s" % PCA_CMD
sts, stdOutErr = run_OS_command( PCA_CMD )

PCA_CMD = "Rscript ~/gitRepo/eBinning/src/run_iPCA.r contigs.comp.wDigest contigs.comp.wDigest.50D 50 1e-8"
print "Running %s" % PCA_CMD
sts, stdOutErr = run_OS_command( PCA_CMD )

SNE_CMD = "python ~/gitRepo/eBinning/src/bhtsne.py -v -i contigs.comp.50D -l contigs.labels -z contigs.lengths -o contigs.comp.wPCA50.2D -s contigs.comp.wPCA50.scatter.png -n contigs_composition_wPCA50 -p 30"
print "Running %s" % SNE_CMD
sts, stdOutErr = run_OS_command( SNE_CMD )

SNE_CMD = "python ~/gitRepo/eBinning/src/bhtsne.py -v -i contigs.comp.wDigest.50D -l contigs.labels.wDigest -z contigs.lengths.wDigest -o contigs.comp.wPCA50.wDigest.2D -s contigs.comp.wPCA50.wDigest.scatter.png -n contigs_composition_wPCA50_wDigest -p 30"
print "Running %s" % SNE_CMD
sts, stdOutErr = run_OS_command( SNE_CMD )