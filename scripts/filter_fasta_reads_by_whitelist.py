import os,sys
import numpy as np
from itertools import groupby
import re

fastas_fofn = sys.argv[1]
whitelist   = set(np.loadtxt(sys.argv[2], dtype="str"))

def fasta_iter( fasta_name ):
	"""
	Given a fasta file, yield tuples of (header, sequence).
	"""
	fh = open(fasta_name)
	# ditch the boolean (x[0]) and just keep the header or sequence since
	# we know they alternate.
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		# drop the ">"
		header = header.next()[1:].strip()
		# join all sequence lines to one.
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

out_fasta   = "selected_reads.fasta"
f           = open(out_fasta, "w")
fasta_width = 70
for line in open(fastas_fofn).xreadlines():
	line = line.strip()
	fn   = line
	for name,seq in fasta_iter(fn):
		if name in whitelist:
			f.write(">%s\n" % name)
			# Add newline every <fasta_width> bases in the sequence
			seq = re.sub("(.{%s})" % fasta_width, "\\1\n", seq, 0, re.DOTALL)
			f.write("%s\n" % seq)
f.close()