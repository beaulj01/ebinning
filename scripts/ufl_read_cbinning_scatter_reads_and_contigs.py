import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import scipy
from scipy.stats import gaussian_kde
import sklearn.cluster as cluster
import pub_figs
import random
from pbcore.io.BasH5IO import BasH5Reader

data     = np.loadtxt("both.comp.2D", dtype="float")
labels   = np.loadtxt("both.labels",  dtype="str")
names    = np.loadtxt("both.names",   dtype="str")
sizes    = np.loadtxt("both.lengths", dtype="int")
unmapped = "unmappedSubreads.fasta"
plot_fn  = "both.comp.scatter.rand.png"
title    = "read-level cBinning"
# box_dims = [int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4]),]
# pre      = sys.argv[5]

# label_set = list(set(labels))
# label_set.sort()
label_set = ["105_contig", "439_contig", "unknown_contig", "read"]
colors    = plt.get_cmap('spectral')(np.linspace(0.2, 0.8, len(label_set)))
shapes    = ["v", "^", "s", "o"]
fig       = plt.figure(figsize=[15,12])
ax        = fig.add_axes([0.1, 0.1, 0.7, 0.7])
sizes[sizes<50000] = 50000
scaled_sizes = sizes**1.5 / max(sizes**1.5) * 2000
# ax.axis("off")
res       = []
for k,target_lab in enumerate(label_set):
	idxs             = [j for j,label in enumerate(labels) if label==target_lab]
	scaled_sizes_idx = np.array(scaled_sizes)[idxs]
	X                = data[idxs,0]
	Y                = data[idxs,1]
	color            = colors[k]
	for i,x in enumerate(X):
		res.append( (x, Y[i], target_lab, color, scaled_sizes_idx[i], shapes[k%len(shapes)]) ) 

# res          = random.sample(res, 5000)
res          = res[:10000]
res          = res[::-1]
# np.random.shuffle(res) 
plotted_labs = set()
legend_plots = []
legend_labs  = []
for (x,y, target_lab, color, size, shape) in res:
	plot = ax.scatter(x,y, marker=shape, s=size , edgecolors="None", label=target_lab, facecolors=color, alpha=0.3)
	if target_lab not in plotted_labs:
		plotted_labs.add(target_lab)
		legend_plots.append(plot)
		legend_labs.append(target_lab)
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
leg_tup = zip(legend_labs,legend_plots)
# leg_tup.sort(key=lambda x: x[0])
legend_labs, legend_plots = zip(*leg_tup)
leg = ax.legend(legend_plots, legend_labs, loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':18}, frameon=False)
for i in range(len(legend_labs)):
	leg.legendHandles[i]._sizes = [50]
plt.savefig(plot_fn)

sys.exit()

minx = min(data[:,0])
maxx = max(data[:,0])
miny = min(data[:,1])
maxy = max(data[:,1])
box_xmin = box_dims[0]
box_xmax = box_dims[1]
box_ymin = box_dims[2]
box_ymax = box_dims[3]
ax.set_xlim([min(minx-1, box_xmin-1), max(maxx+1, box_xmax+1)])
ax.set_ylim([min(miny-1, box_ymin-1), max(maxy+1, box_ymax+1)])
ax.plot([box_xmin,box_xmax,box_xmax,box_xmin,box_xmin], [box_ymax,box_ymax,box_ymin,box_ymin,box_ymax], linestyle="--", color="k", linewidth=3)

box_readnames = set()
for i in range(data.shape[0]):
	x = data[i,0]
	y = data[i,1]
	if y>box_ymin and y<box_ymax and x>box_xmin and x<box_xmax:
		box_readnames.add(names[i])

print "Found %s reads in box..." % len(box_readnames)
f = open("box_reads_%s.fasta" % pre, "w")
for line in open("bas.h5.fofn", "r").xreadlines():
	line     = line.strip()
	baxh5    = line.split()[0]
	reader   = BasH5Reader(baxh5)
	zmws     = [z for z in reader if z.zmwName in box_readnames]
	for z in zmws:
		for sub in z.subreads:
			f.write(">%s\n" % sub.readName)
			f.write("%s\n" % sub.basecalls())
f.close()