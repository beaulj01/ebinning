import os,sys
import collections

marc26 = {"Terriglobus roseus": "DSM 18391", \
		  "Corynebacterium glutamicum": "ATCC 13032", \
		  "Nocardiopsis dassonvillei": "DSM 43111", \
		  "Olsenella uli": "DSM 7084", \
		  "Segniliparus rotundus": "DSM 44985", \
		  "Echinicola vietnamensis": "DSM 17526", \
		  "Meiothermus silvanus": "DSM 9946", \
		  "Clostridium perfringens": "ATCC 13124", \
		  "Ruminiclostridium thermocellum": "ATCC 27405", \
		  "Desulfosporosinus acidiphilus": "SJ4 DSM 22704", \
		  "Desulfosporosinus meridiei": "DSM 13257", \
		  "Desulfotomaculum gibsoniae": "DSM 7213", \
		  "Streptococcus pyogenes": "M1 GAS SF370", \
		  "Thermobacillus composti": "KWC4, DSM 18247", \
		  "Escherichia coli": "K-12 MG1655", \
		  "Frateuria aurantia": "DSM 6220", \
		  "Hirschia baltica": "ATCC 49814", \
		  "Pseudomonas stutzeri": "RCH2", \
		  "Salmonella bongori": "NCTC 12419", \
		  "Salmonella enterica": "subsp. arizonae serovar RSK2980", \
		  "Spirochaeta smaragdinae": "DSM 11293", \
		  "Fervidobacterium pennivorans": "DSM 9078", \
		  "Coraliomargarita akajimensis": "DSM 45221", \
		  "Halovivax ruber": "XH-70", \
		  "Natronobacterium gregoryi": "SP2", \
		  "Natronococcus occultus": "DSM 3396"}

species = set(marc26.keys())
strains = set(map(lambda x: x+" "+marc26[x], species))

report_fn    = sys.argv[1]
found        = 0
classified_n = 0
for line in open(report_fn, "rb").xreadlines():
	line = line.strip("\n")
	if line.split()[-1]=="unclassified":
		pass
	elif line.split()[-1]=="root":
		root_n = int(line.split()[1])
	call = line.split("\t")[-1].strip()
	# if len(call.split(" "))>2 and call in strains:
	# 	print line
	if len(call.split(" "))==2 and call in species:
		print line
		found += 1
		classified_n += float(line.split()[1])
		species.remove(call)
print "Found: %s" % found
print "Accounting for %.2f%% of all classifications" % (100*classified_n/root_n)

print ""
print "Not found: %s" % len(species)
for s in list(species):
	print s
