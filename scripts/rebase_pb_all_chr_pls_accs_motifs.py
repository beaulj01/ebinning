import os,sys
import numpy as np
from itertools import groupby
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Bio import Entrez
import subprocess
import re
import string
import urllib2
from bs4 import BeautifulSoup
from collections import defaultdict,Counter
import pub_figs

def run_OS_command( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts != 0:
		print "Failed command: %s" % CMD
	return sts, stdOutErr

def fasta_iter( fasta_name ):
	fh = open(fasta_name)
	# ditch the boolean (x[0]) and just keep the header or sequence since
	# we know they alternate.
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		# drop the ">"
		header = header.next()[1:].strip()
		# join all sequence lines to one.
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

# Read all Rebase PacBio organisms
pb_org_accs = defaultdict(list)
response    = urllib2.urlopen("http://rebase.neb.com/cgi-bin/pacbiolist?0")
html        = response.read()
soup        = BeautifulSoup(html, "html.parser")

tables          = soup.find_all("table")
org_n           = 0
acc_org_map     = {}
acc_urls        = {}


m6A_color     = "#00BBBB"
m4C_color     = "orange"
m5C_color     = "#CC00CC"
unknown_color = "#BBBB00"

OK_colors = [m6A_color]
# OK_colors = [m6A_color,m4C_color]
# OK_colors = [m6A_color,m4C_color,m5C_color,unknown_color]

f             = open("pb_rebase_seqs_and_motifs.6mA_only.out","wb")
# f             = open("pb_rebase_seqs_and_motifs.6mA_4mC_only.out","wb")
# f             = open("pb_rebase_seqs_and_motifs.all_mods.out","wb")

all_fastas_fn = "all_plasmids.2.fasta"
if os.path.exists(all_fastas_fn):
	os.remove(all_fastas_fn)

errors_n   = 0
error_urls = []
for table in tables:
	if table.attrs.get("bgcolor")=="beige":
		all_as = table.find_all("a")
		for j,a in enumerate(all_as):
			if a.attrs.get("href").find("pacbioget")>-1:
				org     = a.text
				org_url = "http://rebase.neb.com"+a.attrs["href"]
				org_n  +=1

				# if org_n < 650: continue

				response = urllib2.urlopen(org_url)
				html     = response.read()
				soup     = BeautifulSoup(html, "html.parser")

				######################
				# Get organism motifs
				######################
				motifs = set()
				
				# First get all the known motifs w/ known MTases
				known_motifs_table = list(soup.find_all("table"))[7]
				trs = known_motifs_table.td.find_all("tr")
				for tr in trs[2:]: #####
					tds       = tr.find_all("td")
					
					all_fonts = tds[5].find_all("font")
					m6A       = False
					for font in all_fonts:
						if font.attrs.get('size')=="3":
							if font.attrs.get("color") in OK_colors:
								m6A = True
					for font in all_fonts:
						if font.attrs.get('size')!="3" and m6A:
							motifs.add(font.text.strip())


				# Next get all the motifs with unmatch MTases
				unknown_motifs_table = list(soup.find_all("table"))[10]
				trs = unknown_motifs_table.td.find_all("tr")
				for tr in trs[2:]: #####
					tds       = tr.find_all("td")
					all_fonts = tds[0].find_all("font")
					m6A       = False
					for font in all_fonts:
						if font.attrs.get('size')=="3":
							if font.attrs.get("color") in OK_colors:
								m6A = True
					for font in all_fonts:
						if font.attrs.get('size')!="3" and m6A:
							motifs.add(font.text.strip())

				motifs = ";".join(motifs)

				######################
				# Get organism accession codes
				######################

				# The first table entry contains the organism info
				center      = soup.body.center
				chr_table   = list(center.children)[6].find("table")
				acc_section = list(chr_table.children)[0]
				chr_box     = list(acc_section.children)[0]
				all_trs     = chr_box.find_all("tr")
				acc_tr      = all_trs[2]
				all_as      = acc_tr.find_all("a")
				accs        = []
				for a in all_as:
					if a.attrs.get("href").find("seqsget")>-1:
						acc              = a.string.strip()
						accs.append(acc)
						pb_org_accs[org].append(acc)
						acc_org_map[acc] = org
						acc_url          = "http://rebase.neb.com/"+a.attrs.get("href")

						response = urllib2.urlopen(acc_url)
						html     = response.read()
						
						# Find the NCBI Nucleotide page for this accession code
						nuc_url = "https://www.ncbi.nlm.nih.gov/nuccore/"+acc
						acc_urls[acc] = nuc_url

				bail = False
				for acc in accs:
					url = acc_urls[acc]
					
					try:
						response = urllib2.urlopen(url)
					except urllib2.HTTPError as err:
						print "**** HTTPError! ****", org, url
						bail = True
						errors_n += 1
						error_urls.append(err.geturl())
						break
					
					html     = response.read()
					soup     = BeautifulSoup(html, "html.parser")	

					# Try to classify as plasmid or chromosome based on Nucleotide page
					title = soup.head.title.string
					if title.find("lasmid")>-1:
						tp = "plasmid"
					else:
						tp = "chr"

					org    = acc_org_map[acc]
					print org_n, acc, tp, org, motifs
					f.write("%s\t%s\t%s\t%s\t%s\n" % (org_n, acc, tp, org, motifs))

				# if bail:
				# 	continue

				# # Download all the fastas from this NCBI page
				# db           = "nuccore"
				# Entrez.email = "john.beaulaurier@mssm.edu"
				# batchSize    = 100
				# retmax       = 10**9

				# all_accs = accs
				# tmp_fasta_name = "tmp.fasta"
				# try:
				# 	for k,start in enumerate(range( 0,len(all_accs),batchSize )):
				# 		#first get GI for query accesions
				# 		batch_accs = all_accs[start:(start+batchSize)]
				# 		# sys.stderr.write( "Fetching %s entries from GenBank: %s\n" % (len(batch_accs), ", ".join(batch_accs[:10])))
				# 		query  = " ".join(batch_accs)
				# 		handle = Entrez.esearch( db=db,term=query,retmax=retmax )
				# 		giList = Entrez.read(handle)['IdList']
				# 		# sys.stderr.write( "Found %s GI: %s\n" % (len(giList), ", ".join(giList[:10])))
				# 		#post NCBI query
				# 		search_handle     = Entrez.epost(db=db, id=",".join(giList))
				# 		search_results    = Entrez.read(search_handle)
				# 		webenv,query_key  = search_results["WebEnv"], search_results["QueryKey"]
				# 		#fetch entries in batch
				# 		# handle = Entrez.efetch(db=db, rettype="fasta", retstart=start, retmax=batchSize, webenv=webenv, query_key=query_key)
				# 		handle = Entrez.efetch(db=db, rettype="fasta", webenv=webenv, query_key=query_key)
				# 		# sys.stdout.write(handle.read())
				# 		fa = open(tmp_fasta_name, "wb")
				# 		fa.write(handle.read())
				# 		fa.close()
				# except urllib2.HTTPError as err:
				# 	print "**** HTTPError! ****", all_accs
				# 	error_urls.append(err.geturl())
				# 	errors_n += 1
				# 	continue

				# # Output all the fasta sequences for the plasmids
				# if org_n==1:
				# 	mv_CMD  = "mv %s %s" % (tmp_fasta_name, all_fastas_fn)
				# 	sts,stdOutErr = run_OS_command( mv_CMD )
				# else:
				# 	cat_CMD = "cat %s %s > tmp.combo.fasta" % (all_fastas_fn, tmp_fasta_name)
				# 	sts,stdOutErr = run_OS_command( cat_CMD )

				# 	mv_CMD  = "mv tmp.combo.fasta %s" % all_fastas_fn
				# 	sts,stdOutErr = run_OS_command( mv_CMD )

f.close()
os.remove(tmp_fasta_name)

