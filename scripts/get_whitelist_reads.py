import os,sys
from pbcore.io.align.CmpH5IO import CmpH5Reader
import numpy as np

cmph5  = "aligned_reads.cmp.h5"
reader = CmpH5Reader(cmph5)

contig_names = np.loadtxt(sys.argv[1], dtype="str")
contig_names = set(map(lambda x: x.split("|")[0], contig_names))
whitelist_fn = "whitelist.txt"
f            = open(whitelist_fn, "w")
f_stats      = open("whitelist_stats.txt", "w")
lengths      = []
for r in reader:
	contig = r.referenceInfo[3]
	if contig in contig_names:
		mol = "/".join(r.readName.split("/")[:-1])
		f.write(mol+"\n")
		length = r.rEnd - r.rStart
		lengths.append(length)
		f_stats.write("N mols = %s\n" % len(lengths))
		f_stats.write("Mean length = %s\n" % np.mean(lengths))
		f_stats.write("N bases = %s\n" % np.sum(lengths))
f.close()
f_stats.close()