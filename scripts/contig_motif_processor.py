import os,sys
import numpy as np
import subprocess
import glob
import math

class motifs_scanner:
	def __init__( self, ipds, kmers, ipds_N, control_ipds, control_ipds_N, minMotifIPD ):
		self.ipds           = ipds
		self.kmers          = kmers
		self.ipds_N         = ipds_N
		self.control_ipds   = control_ipds
		self.control_ipds_N = control_ipds_N
		self.minMotifIPD    = minMotifIPD

	def scan_motifs_in_matrix_chunk( self ):
		keeper_motifs = set()
		
		contig_motifs        = {}
		motif_stats          = {}
		contig_stats[contig] = {}
		num_motifs = ipds.shape[1]
		for motif_idx in range(num_motifs):
			motif              = self.kmers[motif_idx]
			case_read_means    = self.ipds[:,motif_idx]
			case_read_Ns       = self.ipds_N[:,motif_idx]
			control_read_means = self.control_ipds[:,motif_idx]
			control_read_Ns    = self.control_ipds_N[:,motif_idx]
			if case_read_Ns.sum()>=20 and control_read_Ns.sum()>=20:
				case_mean            = np.dot(case_read_means, case_read_Ns) / case_read_Ns.sum()
				control_mean         = np.dot(control_read_means, control_read_Ns) / control_read_Ns.sum()
				score                = case_mean - control_mean
				contig_motifs[motif] = score
				motif_stats[motif]   = "%.3f\t%.3f\t%s\t%s" % ( case_mean, control_mean, case_read_Ns.sum(), control_read_Ns.sum() )
			else:
				score = 0.0
			contig_stats[contig][motif] = score
		sorted_contig_motifs = sorted(contig_motifs.items(), key=operator.itemgetter(1), reverse=True)
		for m in sorted_contig_motifs[:min(len(sorted_contig_motifs), 1000)]:
			count    = motif_stats[m[0]].split("\t")[2]
		
		# Keep only the shortest version of the high scoring motifs (reduces redundancy)
		highscore_motifs = dict( [(motif,SCp) for motif,SCp in contig_motifs.items() if SCp>=self.minMotifIPD] )
		if len(highscore_motifs)>0:
			shortest_contiguous = min([len(m.split("-")[0]) for m in highscore_motifs.keys()])
			shortest_motifs     = [m for m in highscore_motifs.keys() if len(m.split("-")[0])==shortest_contiguous]
			to_del = set()
			for shorty in shortest_motifs:
				shorty_str =     shorty.split("-")[0]
				shorty_idx = int(shorty.split("-")[1])
				for motif in highscore_motifs.keys():
					if motif!=shorty:
						motif_str =     motif.split("-")[0]
						motif_idx = int(motif.split("-")[1])
						match = re.search(shorty_str, motif_str)
						if match != None:
							if (shorty_idx + match.start()) == motif_idx:
								to_del.add( motif )
			for motif in highscore_motifs.keys():
				if motif not in to_del:
					count = int(motif_stats[motif].split("\t")[2])
					print "   %s:\t%.3f\t%s" % (motif, highscore_motifs[motif], count)
					keeper_motifs.add(motif)
		return keeper_motifs