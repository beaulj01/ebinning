#!/bin/bash
module use /hpc/packages/minerva-mothra/modulefiles
module load python samtools mpc

ref=$1
read_prefix=$2

########################################################################
# Simulate short reads from this reference
########################################################################
Nreads=1500000
r_len=100
ins_size=250
ins_dev=50
# ln -s ~/projects/software/GemSIM_v1.6
GemSIM_v1.6/GemReads.py -r $ref -n $Nreads -l $r_len -m GemSIM_v1.6/models/ill100v5_p.gzip -c -q 64 -o $read_prefix -p -s $ins_dev -u $ins_size
# # echo "GemSIM_v1.6/GemReads.py -r $ref -n $Nreads -l $r_len -m GemSIM_v1.6/models/ill100v5_p.gzip -c -q 64 -o $read_prefix -p -s $ins_dev -u $ins_size" > run.bsub
# # bsub -m mothra -P acc_fangg03a -J sim -oo myjob.log -eo myjob${i}.err -q alloc -W 12:00 -u john.beaulaurier@gmail.com -M 1000000 < run.bsub

########################################################################
# Do assembly of metagenomic short reads
########################################################################
R1="${read_prefix}_fir.fastq"
R2="${read_prefix}_sec.fastq"
# python ~/gitRepo/eBinning/scripts/rename_GemSim_reads.py $R1 R1 > ${R1}.tmp
# python ~/gitRepo/eBinning/scripts/rename_GemSim_reads.py $R2 R2 > ${R2}.tmp
# mv ${R1}.tmp $R1
# mv ${R2}.tmp $R2

mkdir velvet
cd velvet
ln -s ../$R1
ln -s ../$R2
~/projects/software/velvet_1.2.10/velveth velveth_k31 31 -fastq -shortPaired -separate $R1 $R2
~/projects/software/velvet_1.2.10/velvetg velveth_k31 -ins_length $ins_size -exp_cov auto -cov_cutoff auto -min_contig_lgth 100