import os,sys
from pbcore.io.BasH5IO import BasH5Reader
import numpy as np
from collections import Counter

movie_map_fn = "movie_mapping.txt"
input_fn     = np.loadtxt("bax.fofn", dtype="str")

read_infos = ["J99_150x_26695_50x_read_info.txt", \
			  "J99_150x_26695_100x_read_info.txt", \
			  "J99_150x_26695_150x_read_info.txt"]

genome_sizes = {"J99" :   1698366, \
		   		"26695" : 1590068}

for f in read_infos:
	print f
	n_reads   = Counter()
	n_bases   = Counter()
	rlengths  = {}
	movie_map = {}
	for line in open(movie_map_fn, "r").xreadlines():
		line  = line.strip()
		spec  = line.split(":")[0]
		mov   = line.split(":")[1]
		movie_map[mov] = spec
		rlengths[spec] = []

	for line in open(f, "r").xreadlines():
		line   =     line.strip()
		read   =     line.split("\t")[0]
		spec   =     line.split("\t")[1]
		length = int(line.split("\t")[2])
		n_reads[spec] += 1
		n_bases[spec] += length
		rlengths[spec].append(length)

	for spec in n_reads.keys():
		print "%s\t%s\t%s\t%.2f\t%.2f" % (spec, n_bases[spec], n_reads[spec], np.mean(rlengths[spec]), float(n_bases[spec])/genome_sizes[spec])
