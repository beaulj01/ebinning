import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import subprocess
import numpy as np
from collections import defaultdict

def run_OS( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts != 0:
		raise Exception("Failed command: %s" % CMD)
	return sts, stdOutErr

contigs_fns      = ["/hpc/users/beaulj01/projects/ebinning/pacbio20/HGAP_summaries/hmp_all_sets_baseline/data/polished_assembly.fasta", \
					"/hpc/users/beaulj01/projects/ebinning/pacbio20/HGAP_summaries/hmp_sets5_6_baseline/data/polished_assembly.fasta",  \
					"/hpc/users/beaulj01/projects/ebinning/pacbio20/HGAP_summaries/hmp_sets5_6_log/data/polished_assembly.fasta"]

genome_sizes     = {"A_baumannii_ATCC_17978" :               3976747, \
		   			"A_odontolyticus_ATCC_17982_Scfld021" :  2391230, \
		   			"B_cereus_ATCC_10987" :                  5224283, \
		   			"B_vulgatus_ATCC_8482" :                 5163189, \
		   			"C_beijerinckii_NCIMB_8052" :            6000632, \
		   			"D_radiodurans_R1" :                     3060986, \
		   			"E_coli_str_K-12_substr_MG1655" :        4641652, \
		   			"E_faecalis_OG1RF" :                     2739625, \
		   			"H_pylori_26695" :                       1667867, \
		   			"L_gasseri_ATCC_33323" :                 1894360, \
		   			"L_monocytogenes_EGD-e" :                2944528, \
		   			"N_meningitidis_MC58" :                  2272360, \
		   			"P_acnes_KPA171202" :                    2560265, \
		   			"P_aeruginosa_PAO1" :                    6264404, \
		   			"R_sphaeroides_2.4.1" :                  4131542, \
		   			"S_agalactiae_2603V_R" :                 2160267, \
		   			"S_aureus_subsp_aureus_USA300_TCH1516" : 2872915, \
		   			"S_epidermidis_ATCC_12228" :             2499279, \
		   			"S_mutans_UA159" :                       2032925, \
		   			"S_pneumoniae_TIGR4" :                   2160842}

genomes          = ["S_mutans_UA159", \
		   			"L_monocytogenes_EGD-e", \
		   			"S_epidermidis_ATCC_12228", \
		   			"L_gasseri_ATCC_33323", \
		   			"P_acnes_KPA171202", \
		   			"S_aureus_subsp_aureus_USA300_TCH1516", \
		   			"N_meningitidis_MC58", \
		   			"P_aeruginosa_PAO1", \
					"H_pylori_26695", \
					"D_radiodurans_R1", \
		   			"A_odontolyticus_ATCC_17982_Scfld021", \
		   			"B_vulgatus_ATCC_8482", \
		   			"E_faecalis_OG1RF", \
		   			"E_coli_str_K-12_substr_MG1655", \
		   			"S_agalactiae_2603V_R", \
					"A_baumannii_ATCC_17978", \
					"R_sphaeroides_2.4.1", \
		   			"S_pneumoniae_TIGR4", \
		   			"C_beijerinckii_NCIMB_8052", \
		   			"B_cereus_ATCC_10987"]
		   		

genomes_str      = [r"${S. mutans}$ UA159", \
		   			r"${L. monocytogenes}$ EGD-e", \
		   			r"${S. epidermidis}$ ATCC 12228", \
		   			r"${L. gasseri}$ ATCC 33323", \
		   			r"${P. acnes}$ KPA171202", \
		   			r"${S. aureus}$ subsp aureus USA300 TCH1516", \
		   			r"${N. meningitidis}$ MC58", \
		   			r"${P. aeruginosa}$ PAO1", \
					r"${H. pylori}$ 26695", \
					r"${D. radiodurans}$ R1", \
		   			r"${A. odontolyticus}$ ATCC 17982 Scfld021", \
		   			r"${B. vulgatus}$ ATCC 8482", \
		   			r"${E. faecalis}$ OG1RF", \
		   			r"${E. coli}$ str K-12 substr MG1655", \
		   			r"${S. agalactiae}$ 2603V R", \
					r"${A. baumannii}$ ATCC 17978", \
					r"${R. sphaeroides}$ 2.4.1", \
		   			r"${S. pneumoniae}$ TIGR4", \
		   			r"${C. beijerinckii}$ NCIMB 8052", \
		   			r"${B. cereus}$ ATCC 10987"]		   			

covs   = defaultdict(list)
for k,contigs_fn in enumerate(contigs_fns):
	labels = []
	for j,spec in enumerate(genomes):
		nucmer_CMD     = "nucmer -p %s.%s ~/projects/data_repo/refs/%s.fasta %s" % (spec, k, spec,contigs_fn)
		showcoords_CMD = "show-coords -H -r -l -L 1000 %s.%s.delta" % (spec,k)

		print nucmer_CMD
		# sts, stdOutErr = run_OS(nucmer_CMD)
		sts, stdOutErr = run_OS(showcoords_CMD)
		print showcoords_CMD

		coords_out  = stdOutErr[0].split("\n")
		
		out         = []
		chroms_set  = set()
		chroms      = {}
		for i,line in enumerate(coords_out):
			
			if len(line)>0:
				ref_start   =   int(line.split()[0])
				ref_stop    =   int(line.split()[1])
				q_align_len =   int(line.split()[7])
				ident       = float(line.split()[9])
				q_len       =   int(line.split()[12])
				r_name      =     line.split()[-2]
				q_name      =     line.split()[-1]
				# Deal with species that have 2 (and only 2) chromosomes
				if r_name not in chroms.keys():
					chroms_set.add(r_name)
					chroms[r_name] = len(chroms_set)
				if chroms[r_name] == 2:
					ref_start += 100000000
					ref_stop  += 100000000

				if ident>99:
					out.append( (ref_start, ref_stop, q_len, r_name, q_name) )
		
		out.sort(key=lambda x: x[2], reverse=True)
		keeping_contigs   = set()
		covered_pos       = set()
		n_aligned_contigs = len(set(map(lambda x: x[4], out)))
		for entry in out:
			# if entry[4] in keeping_contigs:
			# 	continue
			keeping_contigs.add(entry[4])
			# if len(keeping_contigs)>10:
			# 	break
			covered_pos.update( range(entry[0], entry[1]+1) )
			contig_size    = entry[2]
			contig_cov_pct = 100*float(contig_size)/genome_sizes[spec]
			print spec, contig_size, entry[0], entry[1], contig_cov_pct

		# print contigs_fn, spec, len(covered_pos), genome_sizes[spec], 100*float(len(covered_pos))/genome_sizes[spec]
		covs[k].append( 100*float(len(covered_pos))/genome_sizes[spec] )
		labels.append(genomes_str[j])

N     = len(labels)
covs1 = covs[0]

ind   = np.arange(N) # the x locations for the groups
width = 0.2         # the width of the bars

# plt.figure(figsize=[15,8])
fig, ax = plt.subplots(figsize=[15,8])
rects1  = ax.bar(ind - 0.5*width, covs1, width, linewidth=0, color='0.8')

covs2   = covs[1]
rects2  = ax.bar(ind + 0.5*width, covs2, width, linewidth=0, color='r')

covs3   = covs[2]
rects3  = ax.bar(ind + 1.5*width, covs3, width, linewidth=0, color='orange')

ax.set_ylabel('Percent genome covered')
# ax.set_title('Scores by group and gender')
ax.set_xticks(ind + width)
ax.set_xticklabels(labels, rotation=45, ha="right")
ax.set_xlim([-1,20])
ax.set_ylim([0,100])
# ax.legend((rects1[0], rects2[0], rects3[0]), ('all_sets_baseline', 'sets_5_6_baseline', 'sets_5_6_log'))
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
ax.legend((rects1[0], rects2[0], rects3[0]), ('49 SMRT cells (baseline)', '15 SMRT cells (baseline)', '15 SMRT cells (log-abundance)'), loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':14})
bottom_adjust=0.40
left_adjust=0.05
right_adjust=0.70
hspace=0.5
wspace=0.2 
plt.gcf().subplots_adjust(bottom=bottom_adjust, left=left_adjust, right=right_adjust, hspace=hspace, wspace=wspace)
plt.savefig("assembly_summaries.png")