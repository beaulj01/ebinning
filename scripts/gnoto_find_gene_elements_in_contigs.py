import os,sys
from Bio import SeqIO
import glob

org_fns = glob.glob("*.contigs.gbk")

for org_fn in org_fns:
	org = org_fn.split(".")[0]
	for record in SeqIO.parse(open(org_fn,"r"), "genbank"):
		record_contig = record.annotations["accessions"][0].split("|")[0]

		for feature in record.features:
			if feature.type=="CDS":
				product = feature.qualifiers["product"][0]

				# if product.find("hypothetical protein")>-1:
				#       continue

				start   = feature.location.nofuzzy_start
				end     = feature.location.nofuzzy_end

				if product.find("onjugative transpos")>-1 and len(record.seq)<200000:
					print "%s\t%s\t%s bp\t%s\t%s\t%s" % (org, record_contig, len(record.seq), start, end, product[:50])