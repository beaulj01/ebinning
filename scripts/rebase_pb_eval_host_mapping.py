import os,sys
import numpy as np
from itertools import groupby
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
from collections import defaultdict,Counter
import random
import pub_figs
import time
import math

input_fn   = "pb_rebase_seqs_and_motifs.6mA_only.out.pls_wCounts"
results_fn = "host_mapping.6mA_only.out"

plot_fn    = "host_mapping_eval.6mA_only.png"

data       = []
for i,line in enumerate(open(input_fn).xreadlines()):
	line   = line.strip()
	vals   = line.split("\t")
	org_id = int(vals[0])
	acc    =     vals[1]
	length = int(vals[3])
	org    =     vals[4]
	if len(vals)==7:
		motifs =     vals[5]
		counts =     vals[6]
	else:
		motifs =     ""
		counts =     ""
	
	if org.find("species")>-1:
		spec   = " ".join(org.split(" ")[:3])
	else:
		spec   = " ".join(org.split(" ")[:2])

	entry = [int(org_id), acc, length, org, spec, motifs, counts]
	data.append(entry)

data = np.matrix(data)

# Identify which rows are orgs with >1 strain present
specs_counter = Counter()
counted_orgs  = set()
org_id_vector = np.squeeze(np.asarray(data[:,0]))
specs_vector  = np.squeeze(np.asarray(data[:,4]))
for i,spec in enumerate(specs_vector):
	# Only count the first entry for each organism (don't repeat for chrs and plasmids)
	org_id = org_id_vector[i]
	if org_id not in counted_orgs and spec!="Clostridium pasteurianum":
		specs_counter[spec] +=1
		counted_orgs.add(org_id)
multistrain_rows = [i for i,spec in enumerate(specs_vector) if specs_counter[spec]>1]

n_orgs             = [20,40,60]
m_iters            = 1000
strain_overlap_pct = [0,5,10]
# max_strain_overlap = 3
f                  = open(results_fn, "w")
results            = {}
# for s in range(max_strain_overlap+1):
for k,s in enumerate(strain_overlap_pct):
	results[k] = []
	for n in n_orgs:
		n_strains = int((float(s)/100 * n))
		timeouts  = 0
		iter_vals = []
		for m in range(m_iters):
			# print "  %s %s %s/%s" % (n_strains, n, m, m_iters)
			skip          = False
			orgs_set      = set()
			specs_set     = set()
			multispec_set = set()
			rep_specs     = set()
			rows          = set()
			
			# Build vector (size=n-2n_strains) of orgs with NO strain repeats: part1
			n_rows  = data.shape[0]
			while len(orgs_set) < (n - (2*n_strains)):
				row_id = random.randint(0, (n_rows-1))
				org    = data[row_id,3]
				spec   = data[row_id,4]
				if org not in orgs_set and spec not in specs_set:
					orgs_set.add(org)
					specs_set.add(spec)
					rows.add(row_id)
				
			# Add n_strains places with orgs that have multiple strains present for sampling: part2
			while len(orgs_set) < (n - n_strains):
				if n_strains==0:
					row_id = random.randint(0, (n_rows-1))
				else:
					row_id = random.choice(multistrain_rows)
				org    = data[row_id,3]
				spec   = data[row_id,4]
				if org not in orgs_set and specs_counter[spec]>1 and spec not in multispec_set:
					orgs_set.add(org)
					specs_set.add(spec)
					multispec_set.add(spec)
					rows.add(row_id)

			# Fill in remaining n_strains places in vector with strain repeats: part3
			timeout = time.time() + 0.3
			while len(orgs_set) < n:
				if n_strains==0:	
					row_id = random.randint(0, (n_rows-1))
				else:
					row_id = random.choice(multistrain_rows)
				org    = data[row_id,3]
				spec   = data[row_id,4]
				# if org not in orgs_set and spec in specs_set and spec not in rep_specs:
				if org not in orgs_set and spec in specs_set and spec not in rep_specs:
					orgs_set.add(org)
					specs_set.add(spec)
					rep_specs.add(spec)
					rows.add(row_id)
				if time.time() > timeout:
					timeouts += 1
					skip      = True
					break
			if skip:
				print "BREAK"
				print "multispec_set:", multispec_set
				continue

			# DEBUG MODE 1: Look at all 232 plasmids for the 151 organisms (strains)
			# rows = range(data.shape[0])

			# DEBUG MODE 2: Look at one plasmid for each of the 151 organisms (strains)
			# This means finding the first row idx for each org
			# rows = range(data.shape[0])
			# orgs = list(np.squeeze(np.asarray(data[rows,3])))
			# rows = []
			# uniq_orgs = list(set(orgs))
			# uniq_orgs.sort()
			# for org in uniq_orgs:
			# 	rows.append(orgs.index(org))

			# DEBUG MODE 3: Pick one representative organism (strain) for each of the 69 species
			# rows  = range(data.shape[0])
			# orgs  = list(np.squeeze(np.asarray(data[rows,3])))
			# specs = list(np.squeeze(np.asarray(data[rows,4])))
			# rows = []
			# uniq_specs = list(set(specs))
			# uniq_specs.sort()
			# for spec in uniq_specs:
			# 	rows.append(specs.index(spec))

			rows = np.array(list(rows))
			rows_real_motifs = []
			# Count number of unique methylomes that have > 0 motif counts
			for row_id in rows:
				row_real_motifs = []
				for i,motif in enumerate(data[row_id,5].split(";")):
					if data[row_id,6]!="":
						motif_sites = int(data[row_id,6].split(";")[i])
					else:
						motif_sites = 0

					if motif_sites>0:
						row_real_motifs.append(motif)
					else:
						pass

				rows_real_motifs.append(row_real_motifs)

			list_of_motif_sets = map(frozenset, rows_real_motifs)
			
			uniq = 0
			for i,row_motifs in enumerate(rows_real_motifs):
				match_ids = [i for i,motifs_set in enumerate(list_of_motif_sets) if motifs_set==set(row_motifs)]
				if len(match_ids)>1:
					# Redundant motif set
					pass
				else:
					# Unique motif set
					uniq += 1

			frac_uniq = float(uniq) / len(rows_real_motifs)
			iter_vals.append(frac_uniq)
		
		print n, n_strains, np.mean(iter_vals)
		results[k].append( (n, iter_vals) )
		f.write("%s\t%s\t%.4f\n" % (n, n_strains, np.mean(iter_vals)))
f.close()

font_size      = 25
adjust         = 0.15
fontProperties = pub_figs.setup_math_fonts(font_size)

plt.figure(1, figsize=(12,10))
ax     = plt.axes([0.17, 0.3, 0.5, 0.6])
# colors = plt.get_cmap('spectral')(np.linspace(0, 0.9, len(strain_overlap_pct)))
colors = ["k", "b", "r"]
for k,s in enumerate(strain_overlap_pct):
	X      = map(lambda x: x[0], results[k])
	Y      = map(lambda x: np.mean(x[1]), results[k])
	stderr = map(lambda x: np.std(x[1])/math.sqrt(len(x[1])), results[k])
	lab    = "%s" % s
	ax.errorbar(X, Y, yerr=stderr, linestyle="-", color=colors[k], linewidth=3, label=lab+"\%")
	ax.hold(True)

ax.set_ylim([0.73,0.90])
ax.set_xlim([(n_orgs[0]-3), (n_orgs[-1]+3)])
ax.set_ylabel("Fraction plasmids with\nunique methylomes")
ax.set_xlabel("Number organisms in sample")
pub_figs.remove_top_right_axes(ax)
ax.tick_params(axis='both', direction='out')
ax.set_xticks(n_orgs)
ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
ax.set_yticks(np.arange(75,91,5)/float(100))
ax.grid()
# ax.set_yticklabels(map(lambda x: str(x), range(0,80,20)))
leg = ax.legend(title="\% species with\n2 strains in sample", prop={'size':font_size}, frameon=False)
# get handles, remove the errorbars, use them in the legend
handles, labels = ax.get_legend_handles_labels()
handles = [h[0] for h in handles]
leg = ax.legend(handles, labels, numpoints=1,title="\% species with\n2 strains in sample", prop={'size':font_size}, frameon=False, loc=3)
plt.setp(leg.get_title(), multialignment='center')
pub_figs.setup_math_fonts(font_size)
plt.savefig(plot_fn, dpi=300)