import os,sys
import numpy as np
import scipy.stats as stats

fn = sys.argv[1]
m         = np.loadtxt(fn, dtype="float")
m_std     = stats.mstats.zscore(m, axis=0)
out_fn    = fn + ".zscores"
np.savetxt(out_fn, m_std, fmt='%.3f', delimiter="\t")