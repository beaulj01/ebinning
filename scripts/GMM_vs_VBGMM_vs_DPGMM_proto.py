import itertools
import os,sys
import numpy as np
from scipy import linalg
import matplotlib as mpl; mpl.use('Agg')
import matplotlib.pyplot as plt

from sklearn import mixture

X            = np.loadtxt(sys.argv[1])
readnames    = np.loadtxt(sys.argv[2], dtype="str")
n_clusters   = int(sys.argv[3])
n_iterations = 4
epsilon      = 1e-3

FIG_SIZE = [15,15]

print "Fit a mixture of Gaussians with variation-Bayesian"
vbgmm = mixture.VBGMM(n_components=n_clusters, 
                      covariance_type='full', 
                      n_iter=n_iterations, 
                      thresh=epsilon, 
                      alpha=100)
vbgmm.fit(X)

print "Plotting..."
colors = plt.get_cmap('Accent')(np.linspace(0, 1.0, n_clusters))
plt.figure(figsize=FIG_SIZE)
splot = plt.subplot(1, 1, 1)
Y_    = vbgmm.predict(X)
print "Converged to %s cluster components" % len(set(Y_))
k     = 0
cluster_ID_map = {}
for j, (mean, covar, color) in enumerate(zip(vbgmm.means_, vbgmm._get_covars(), colors)):
    v, w = linalg.eigh(covar)
    u    = w[0] / linalg.norm(w[0])
    # as the DP will not use every component it has access to
    # unless it needs it, we shouldn't plot the redundant
    # components.
    if not np.any(Y_ == j):
        continue
    k += 1
    cluster_ID_map[j] = k
    plt.scatter(X[Y_ == j, 0], X[Y_ == j, 1], .8, color=color, label=str(k))

    # Plot an ellipse to show the Gaussian component
    angle = np.arctan(u[1] / u[0])
    angle = 180 * angle / np.pi  # convert to degrees
    ell   = mpl.patches.Ellipse(mean, v[0], v[1], 180 + angle, color=color, fill=False, linestyle="dashed", linewidth=2)
    ell.set_clip_box(splot.bbox)
    ell.set_alpha(1)
    splot.add_artist(ell)
    plt.text(mean[0], mean[1], k, weight="bold", color="k", fontsize=14, style="italic", horizontalalignment="center", verticalalignment="center")

plt.xticks(())
plt.yticks(())
plt.legend(prop={'size':6})
plt.title("VBGMM")
plt.savefig("VBGMM.png")

all_clusters_dir = "clusters"
if not os.path.exists(all_clusters_dir): os.mkdir(all_clusters_dir)
orig_dir = os.getcwd()
os.chdir(all_clusters_dir)

for cluster_ID in set(cluster_ID_map.values()):
    dirname = "k_%s" % cluster_ID
    if not os.path.exists(dirname): os.mkdir(dirname)

assigns_fn = sys.argv[2]+".cluster_ID"
print "Writing read-cluster assignments to %s" % assigns_fn
assigns    = open(assigns_fn, "w")
for i,read in enumerate(readnames):
    assigns.write("%s\t%s\n" % (read, cluster_ID_map[Y_[i]]))
    clust_dir = "k_%s" % cluster_ID_map[Y_[i]]
    clust_fn  = "k_%s.readnames" % cluster_ID_map[Y_[i]]
    f = open(os.path.join(clust_dir, clust_fn), "ab+")
    f.write(read+"\n")
    f.close()
assigns.close()
os.chdir(orig_dir)