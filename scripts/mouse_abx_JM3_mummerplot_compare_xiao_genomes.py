import os,sys
import glob
import subprocess
import multiprocessing

procs = 12

def launch_pool( procs, funct, args ):
	p    = multiprocessing.Pool(processes=procs)
	try:
		results = p.map(funct, args)
		p.close()
		p.join()
	except KeyboardInterrupt:
		p.terminate()
	return results

def launch_mummer(tup):
	i    = tup[0]
	xiao = tup[1]
	xi = xiao.split("/")[1].split(".")[0]
	for box_id in range(1,10):
		compare   = "%s_vs_box%s" % (xi, box_id)
		print i,box_id, compare
		box_fasta = "box%s_gt100kb.fasta" % box_id
		CMD1 = "nucmer -p %s %s %s" % (compare, xiao, box_fasta)
		CMD2 = "delta-filter -m %s.delta > %s.delta.m" % (compare, compare)
		sts,stdOutErr = run_OS_command(CMD1)
		sts,stdOutErr = run_OS_command(CMD2)
		# if os.path.getsize("%s.delta.m" % compare)>50000:
		if os.path.getsize("%s.delta.m" % compare)>300:
			CMD3 = "./my_mummerplot -color -layout %s.delta.m --png -t %s -p %s" % (compare, compare, compare)
			sts,stdOutErr = run_OS_command(CMD3)

		# to_rm = glob.glob("*.filter")
		# for f in to_rm:
		# 	os.remove(f)
		# to_rm = glob.glob("*.*plot")
		# for f in to_rm:
		# 	os.remove(f)
		# to_rm = glob.glob("*.gp")
		# for f in to_rm:
		# 	os.remove(f)
		
def run_OS_command( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts != 0:
		print "Failed command: %s" % CMD
	return sts, stdOutErr

xiaos = glob.glob(os.path.join("MGS", "*.fasta"))
args  = [(i,xiao) for i,xiao in enumerate(xiaos)]

args.sort(key=lambda x: int(x[1].split("_")[1].split(".")[0]))
results = launch_pool(procs, launch_mummer, args)

# to_rm = glob.glob("*.delta*")
# for f in to_rm:
# 	os.remove(f)