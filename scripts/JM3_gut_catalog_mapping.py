import os,sys
import numpy as np

taxa_assigns = np.loadtxt("3300005806.a.phylodist.txt",    dtype="str", delimiter="\t")
taxa_assigns = dict([entry[0], entry] for entry in taxa_assigns[:,:])
# domain;phylum;class;order;family;genus;species;taxon_name

gene_prods   = np.loadtxt("3300005806.a.gene_product.txt", dtype="str", delimiter="\t")
gene_prods   = dict([(gene,prod) for gene,prod in gene_prods[:,:2]])

nucmer_fn    = "nucmer.out"
for i,line in enumerate(open(nucmer_fn, "rb").xreadlines()):
	if i>4:
		line = line.strip()
		start1_idx    = 0
		end1_idx      = 1
		start2_idx    = 3
		end2_idx      = 4
		len1_idx      = 6
		len2_idx      = 7
		ident_idx     = 9
		lenR_idx      = 11
		lenQ_idx      = 12
		covR_idx      = 14
		covQ_idx      = 15
		strandR_idx   = 17
		strandQ_idx   = 18
		refname_idx   = 19
		queryname_idx = 20
		align     = line.split()
		phylodist = taxa_assigns.get(align[queryname_idx]+"1")
		if phylodist!=None:
			full_taxonomy = phylodist[-1]
			homology      = phylodist[-2]
			genus         = ";".join(full_taxonomy.split(";")[:6])
			species       = ";".join(full_taxonomy.split(";")[:7])
			print "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s" % (align[refname_idx], \
											  	  align[lenR_idx], \
											  	  align[start1_idx], \
											  	  align[end1_idx], \
											  	  align[queryname_idx], \
											  	  align[lenQ_idx], \
											  	  align[ident_idx], \
											  	  align[len1_idx], \
											  	  homology, \
											  	  species, \
											  	  gene_prods.get((align[queryname_idx]+"1")))
