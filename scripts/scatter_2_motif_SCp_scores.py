import os,sys
import matplotlib.pyplot as plt

ebarcodes_fn = sys.argv[1]

X = []
Y = []
label = []
cols  = []
col = {"C227_contig_0":"r", "J109_contig_0":"g", "ambiguous":"b"}
for i,line in enumerate(open(ebarcodes_fn).xreadlines()):
	if i==0: continue
	line = line.strip()
	ACCACC = float(line.split("\t")[2])
	X.append(ACCACC)
	CTGCAG = float(line.split("\t")[3])
	Y.append(CTGCAG)
	label.append(line.split("\t")[0])
	cols.append(col[line.split("\t")[0]])
fig = plt.figure(figsize=[10,10])
ax1  = fig.add_subplot(1,1,1)

ax1.scatter(X,Y,color=cols)
ax1.set_xlabel("ACCACC SCp")
ax1.set_ylabel("CTGCAG SCp")
# ax1.legend(loc=3)
plt.savefig("scatter_ACCACC_CTGCAG.png")