import os,sys
import numpy as np

minlength = 5000

cov_table = np.loadtxt("concoct_inputtableR.tsv", dtype="str", skiprows=1)
motifs    = np.loadtxt("ordered_motifs.txt",      dtype="str")
contigs   = np.loadtxt("contigs.names",           dtype="str")
scores    = np.loadtxt("contigs.SCp",          dtype="float")
lengths   = np.loadtxt("contigs.lengths",         dtype="int")

scores_norm = np.zeros(scores.shape)
for j in range(scores.shape[1]):
	motif_ipds = scores[:,j]
	
	motif_ipds = np.clip(motif_ipds, -1, 2)
	
	span      = max(motif_ipds) - min(motif_ipds)
	for i in range(scores.shape[0]):
		# I'll normalize and multiply by 100 to make it look like coverage
		scores_norm[i,j] = 10 * (motif_ipds[i] - min(motif_ipds)) / span
np.savetxt("contigs.SCp.norm", scores_norm, delimiter="\t", fmt="%.3f")
scores     = np.loadtxt("contigs.SCp.norm", dtype="float")

cov_header = open("concoct_inputtableR.tsv", "r").readlines()[0].strip()
mot_header = "\t".join(motifs)
print "%s\t%s" % (cov_header, mot_header)
for i,contig in enumerate(contigs):
	if lengths[i]>=minlength:
		find_in_table = cov_table[:,0]==contig
		cov_str       = "\t".join(cov_table[find_in_table,1:][0])
		SCp_str       = "\t".join(map(str,map(lambda x: round(x,3), scores[i,:])))
		print "%s\t%s\t%s" % (contig, cov_str, SCp_str)