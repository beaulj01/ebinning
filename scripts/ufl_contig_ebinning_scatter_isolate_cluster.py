import sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import random
import pub_figs
import textwrap
from itertools import groupby

label_dict      =  {"105" :     r"${B. dorei}$ strain 105", \
		   			"439" :     r"${B. dorei}$ strain 439", \
		   			"unknown" : r"Unknown organism"}

def fasta_iter( fasta_name ):
	"""
	Given a fasta file, yield tuples of (header, sequence).
	"""
	fh = open(fasta_name)
	# ditch the boolean (x[0]) and just keep the header or sequence since
	# we know they alternate.
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		# drop the ">"
		header = header.next()[1:].strip()
		# join all sequence lines to one.
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

def scatterplot(results, labels, plot_fn, title, names, extra, box_dims):
	label_set = list(set(labels))
	label_set.sort()
	colors    = plt.get_cmap('spectral')(np.linspace(0, 1.0, len(label_set)))
	fig       = plt.figure(figsize=[15,12])
	ax        = fig.add_axes([0.1, 0.1, 0.6, 0.6])
	# ax.axis("off")
	res       = []
	for k,target_lab in enumerate(label_set):
		idxs  = [j for j,label in enumerate(labels) if label==target_lab]
		X     = results[idxs,0]
		Y     = results[idxs,1]
		color = colors[k]
		for i,x in enumerate(X):
			res.append( (x, Y[i], target_lab, color) ) 
	
	np.random.shuffle(res) 
	plotted_labs = set()
	legend_plots = []
	legend_labs  = []
	for (x,y, target_lab, color) in res:
		plot = ax.scatter(x,y, marker="o", s=15 , edgecolors="None", label=label_dict[target_lab], facecolors=color, alpha=0.8)
		if label_dict[target_lab] not in plotted_labs:
			plotted_labs.add(label_dict[target_lab])
			legend_plots.append(plot)
			legend_labs.append(label_dict[target_lab])
	box = ax.get_position()
	ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
	leg_tup = zip(legend_labs,legend_plots)
	leg_tup.sort(key=lambda x: x[0])
	legend_labs, legend_plots = zip(*leg_tup)
	leg = ax.legend(legend_plots, legend_labs, loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':18}, frameon=False)
	for i in range(len(legend_labs)):
		leg.legendHandles[i]._sizes = [200]
	minx = min(map(lambda x: x[0], res))
	maxx = max(map(lambda x: x[0], res))
	miny = min(map(lambda x: x[1], res))
	maxy = max(map(lambda x: x[1], res))

	box_xmin = box_dims[0]
	box_xmax = box_dims[1]
	box_ymin = box_dims[2]
	box_ymax = box_dims[3]
	ax.set_xlim([min(minx-1, box_xmin-1), max(maxx+1, box_xmax+1)])
	ax.set_ylim([min(miny-1, box_ymin-1), max(maxy+1, box_ymax+1)])
	ax.plot([box_xmin,box_xmax,box_xmax,box_xmin,box_xmin], [box_ymax,box_ymax,box_ymin,box_ymin,box_ymax], linestyle="--", color="k", linewidth=3)
	plt.savefig(plot_fn)

	box_contig_names = set()
	for i in range(results.shape[0]):
		x = results[i,0]
		y = results[i,1]
		if y>box_ymin and y<box_ymax and x>box_xmin and x<box_xmax:
			box_contig_names.add(names[i])

	for name,seq in fasta_iter("polished_assembly.fasta"):
		if name.split("|")[0] in box_contig_names:
			print ">%s" % name
			# print textwrap.fill(seq, 70)
			print "%s\n" % seq

results = np.loadtxt("contigs.SCp.digest.3.2D", dtype="float")
labels  = np.loadtxt("contigs.labels.digest",   dtype="str")
sizes   = np.loadtxt("contigs.lengths.digest",  dtype="int")
names   = np.loadtxt("contigs.names.digest",    dtype="str")

results = results[sizes!=50000]
labels  = labels[sizes!=50000]
sizes   = sizes[sizes!=50000]
names   = names[sizes!=50000]

extra   = np.array([]) 
plot_fn = "pub.contigs.SCp.png"
title   = "contig-level eBinning"
box_dims = [int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4]),]

scatterplot(results, labels, plot_fn, title, names, extra, box_dims)