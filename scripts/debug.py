import os,sys
import numpy as np
import glob
import warnings

# ipd_fns   = glob.glob("tmp/chunk*/unitig_*_ipds.tmp")
# ipd_N_fns = glob.glob("tmp/chunk*/unitig_*_ipds_N.tmp")

ipd_fns   = glob.glob("tmp/unitig_*_ipds.tmp")
ipd_N_fns = glob.glob("tmp/unitig_*_ipds_N.tmp")
ipd_fns.sort()
ipd_N_fns.sort()

bad_contigs = set()
for k in range(len(ipd_fns)):
	# Iterate over each unitig
	ipd_fn   = ipd_fns[k]
	ipd_N_fn = ipd_N_fns[k]
	ipds     = np.loadtxt(ipd_fn,   dtype="float")
	ipds_N   = np.loadtxt(ipd_N_fn, dtype="int")
	if ipds.shape != ipds_N.shape:
		print ipd_N_fns[k], ipds_N.shape, ipd_fns[k], ipds.shape
		raise Exception("WTF the two are not the same shape!")

	if len(ipds_N.shape)==1:
		ipds_N = ipds_N.reshape(1,ipds_N.shape[0])
		ipds   = ipds.reshape(1,ipds.shape[0])
	for i in range(ipds_N.shape[0]):
		# Iterate over rows of each matrix
		ipd_row   = ipds[i,:]
		ipd_N_row = ipds_N[i,:]

		# print ipd_row[ipd_N_row==0]

		if ipd_row[ipd_N_row==0].sum() > 0:
			# Do the motif zero counts not match the reported IPDs?
			# print i, ipd_N_fns[k], ipds_N.shape
			# print i, ipd_fns[k], ipds.shape
			# print ipds_N[i,:]
			# print ipds[i,:]
			bad_contigs.add(ipd_N_fns[k].split("/")[1].split("_read_")[0])
			# print ""

print "BAD:", bad_contigs
