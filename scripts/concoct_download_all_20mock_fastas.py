import sys
from Bio import Entrez
from itertools import groupby
import urllib2
sys.path.append("/hpc/users/beaulj01/gitRepo/eBinning/src")
import read_scanner
from collections import Counter

# Not including the 5 E. coli strains
accs      = ["NC_009614.1", \
			 "NC_004663.1", \
			 "NC_016776.1", \
			 "NC_014933.1", \
			 "NC_015164.1", \
			 "NC_017179.1", \
			 "NC_021042.1", \
			 "NC_015977.1", \
			 "NC_014833.1", \
			 "NC_012781.1", \
			 "NC_014656.1", \
			 "NC_018221.1", \
			 "NC_010655.1", \
			 "NC_008497.1", \
			 "NC_018937.1", \
			 "NC_000913.3"]

def pull_fasta_get_comp( accs ):
	# Download all the fastas from this NCBI page
	db           = "nuccore"
	Entrez.email = "john.beaulaurier@mssm.edu"
	batchSize    = 100
	retmax       = 10**9

	# all_accs = list(np.loadtxt("accessions.txt", dtype="str"))
	all_accs   = accs
	fasta_name = "tmp.fasta"
	try:
		for k,start in enumerate(range( 0,len(all_accs),batchSize )):
			#first get GI for query accesions
			batch_accs = all_accs[start:(start+batchSize)]
			sys.stderr.write( "Fetching %s entries from GenBank: %s\n" % (len(batch_accs), ", ".join(batch_accs[:10])))
			query  = " ".join(batch_accs)
			handle = Entrez.esearch( db=db,term=query,retmax=retmax )
			giList = Entrez.read(handle)['IdList']
			sys.stderr.write( "Found %s GI: %s\n" % (len(giList), ", ".join(giList[:10])))
			#post NCBI query
			search_handle     = Entrez.epost(db=db, id=",".join(giList))
			search_results    = Entrez.read(search_handle)
			webenv,query_key  = search_results["WebEnv"], search_results["QueryKey"]
			#fetch entries in batch
			# handle = Entrez.efetch(db=db, rettype="fasta", retstart=start, retmax=batchSize, webenv=webenv, query_key=query_key)
			handle = Entrez.efetch(db=db, rettype="fasta", webenv=webenv, query_key=query_key)
			# sys.stdout.write(handle.read())
			f = open(fasta_name, "wb")
			f.write(handle.read())
			f.close()
	except urllib2.HTTPError as err:
		if err.code==404:
			print "**** HTTPError 404! ****"
			error_urls.append(err.geturl())
			errors_n += 1
		else:
			raise

pull_fasta_get_comp( accs )