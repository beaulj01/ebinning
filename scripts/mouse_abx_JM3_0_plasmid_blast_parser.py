import os,sys

min_frac  = 0.6
min_ident = 80

for line in open(sys.argv[1], "rb").xreadlines():
	line     = line.strip()
	ident    =    float(line.split("\t")[2])
	a_length = float(line.split("\t")[3])
	r_len    = int(line.split("\t")[7])
	frac     = float(a_length) / r_len
	if ident>=min_ident and frac>=min_frac:
		print line