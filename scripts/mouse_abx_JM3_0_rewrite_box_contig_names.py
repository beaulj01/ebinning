import os,sys
from Bio import SeqIO

for i in range(1,10):
	fn      = "box%s.fasta" % i
	renamed = []
	for Seq in SeqIO.parse(fn, "fasta"):
		Seq.id          = "box%s_%s" % (i,Seq.id.split("|")[0])
		Seq.description = "box%s_%s" % (i,Seq.description.split("|")[0])
		renamed.append(Seq)

	out_fn = fn.replace(".fasta", ".renamed.fasta")
	SeqIO.write(renamed, out_fn, "fasta")