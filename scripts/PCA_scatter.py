import matplotlib.pyplot as plt

fn = "PCA_transformed_data_gt500.csv"

strains = set()
for i,line in enumerate(open(fn).xreadlines()):
	if i==0:continue
	line = line.strip("\n")
	strains.add(line.split(",")[0])

colors = {}
cols_list = ["r", "b", "g", "m", "c", "k", "y", "0.75"]
for i,strain in enumerate(strains):
	colors[strain] = cols_list[i]

fig = plt.figure(figsize=[20,20])
ax1  = fig.add_subplot(2,2,1)
# ax2  = fig.add_subplot(2,2,2)
# ax3  = fig.add_subplot(2,2,3)

for strain in strains:
	dim1 = []
	dim2 = []
	dim3 = []
	dim4 = []
	for i,line in enumerate(open(fn).xreadlines()):
		if i==0:continue
		line = line.strip("\n")
		lab  = line.split(",")[0]
		if lab==strain:
			dim1.append(float(line.split(",")[1]))
			dim2.append(float(line.split(",")[2]))
			# dim3.append(float(line.split(",")[3]))
			# dim4.append(float(line.split(",")[4]))

	ax1.scatter(dim1,dim2,alpha=0.5,color=colors[strain], label=strain)
	# ax2.scatter(dim2,dim3,alpha=0.5,color=colors[strain], label=strain)
	# ax3.scatter(dim3,dim4,alpha=0.5,color=colors[strain], label=strain)

ax1.set_xlabel("dim1")
# ax2.set_xlabel("dim2")
# ax3.set_xlabel("dim3")
ax1.set_ylabel("dim2")
# ax2.set_ylabel("dim3")
# ax3.set_ylabel("dim4")

plt.legend(loc=3)
plt.savefig("PCA_scatter.png")