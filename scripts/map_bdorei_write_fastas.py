import os,sys
import urllib2
from BeautifulSoup import BeautifulSoup
from Bio import SeqIO
import glob
from itertools import groupby

assembly_summary = "/hpc/users/beaulj01/projects/software/kraken/kraken-0.10.5-beta/bdorei_add/bdorei_assembly_summary.txt"

for i,line in enumerate(open(assembly_summary, "r").xreadlines()):
	line     = line.strip()
	cols     = line.split("\t")
	GCF      = cols[0]
	taxid    = cols[5]
	name     = cols[7]
	strain   = cols[8]
	ftp_path = cols[19]
	fn       = glob.glob(GCF+"*.fasta")[0]
	f = open(fn.replace(".fasta", ".taxid.fasta"),"w")
	for j,seq_record in enumerate(SeqIO.parse(fn, "fasta")):
		seq_record.id += "|kraken:taxid|%s" % taxid
		# print seq_record.id
		f.write(seq_record.format("fasta"))
	f.close()