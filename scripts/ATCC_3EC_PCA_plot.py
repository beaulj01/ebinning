import os,sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import random


# pca_fn      = "reads.SMp.20kb.no0s.PCA"
# pca_fn      = "reads.SMp.15kb.no0s.PCA"
# pca_fn      = "reads.SMp.10kb.no0s.PCA"
pca_fn      = "reads.SMp.5kb.no0s.PCA"
# pca_fn      = "reads.raw.SMp.20kb.no0s.PCA"

SMp_coords  = np.loadtxt(pca_fn, dtype="float")

fig    = plt.figure(figsize=[12,12])
ax     = fig.add_subplot()
xmin   = -1
xmax   = 6
ymin   = -4
ymax   = 3
extent = [xmin, xmax, ymin, ymax]
H, xedges, yedges = np.histogram2d(SMp_coords[:,0], SMp_coords[:,1], bins=25, range=[extent[:2], extent[2:]])
H = np.rot90(H)
H = np.flipud(H)
Hmasked = np.ma.masked_where(H==0,H) # Mask pixels with a value of zero
plt.hot()                                    # set 'hot' as default colour map

im = plt.imshow(H, interpolation='bilinear', # creates background image
                origin='lower', cmap=cm.Blues, 
                extent=extent)
# plt.scatter(SMp_coords[:,0], SMp_coords[:,1])
levels            = [50,70,90,110]
cols              = ["k","0.4","0.6","0.8"]
cset              = plt.contour(H, levels, origin="lower",colors=cols,linewidths=1.4,extent=extent)
# plt.clabel(cset, inline=1, fontsize=10, fmt="%1.0i")
# for c in cset.collections:
# 	c.set_linestyle("solid")

plt.savefig(pca_fn+".png")