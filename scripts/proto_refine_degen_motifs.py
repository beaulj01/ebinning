import os,sys
import re
import numpy as np
from Bio import motifs as BioMotifs
from Bio.Seq import Seq
from collections import defaultdict

keeper_motifs = set(["AGACNNNNNNTATG-2", \
"AGTANNNNNNGTGG-0", \
"ATGCNNNNNNTCCG-0", \
"CATANNNNNAATC-3", \
"CATANNNNNATTC-3", \
"CATANNNNNCATC-3", \
"CATANNNNNCCTC-3", \
"CATANNNNNCTTC-3", \
"CATANNNNNGATC-3", \
"CATANNNNNGCTC-3", \
"CATANNNNNNATC-3", \
"CATANNNNNNCTC-3", \
"CATANNNNNNGTC-3", \
"CATANNNNNNTTC-3", \
"CATANNNNNTATC-3", \
"CATANNNNNTCTC-3", \
"CATANNNNNTGTC-3", \
"CATANNNNNTTTC-3", \
"CATG-1", \
"CGGANNNNNTGCA-3", \
"CGTANNNNNAATC-3", \
"CGTANNNNNATTC-3", \
"CGTANNNNNNATC-3", \
"CGTANNNNNNCTC-3", \
"CGTANNNNNNGTC-3", \
"CGTANNNNNNTTC-3", \
"CTACAG-4", \
"CTATAG-4", \
"CTGTAG-4", \
"GAAANNNNNTATG-1", \
"GAACNNNNNTCAG-1", \
"GAAGA-4", \
"GAAGNNNNNTATG-1", \
"GAANNNNNNTACG-1", \
"GAANNNNNNTATG-1", \
"GAATNNNNNTACG-1", \
"GAATNNNNNTATG-1", \
"GAATTC-2", \
"GAGCNNNNNTATG-1", \
"GAGG-1", \
"GAGTNNNNNTATG-1", \
"GATANNNNNTACG-1", \
"GATCNNNNNTATG-1", \
"GATGNNNNNTACG-1", \
"GATNNNNNNTACG-1", \
"GATNNNNNNTATG-1", \
"GATTNNNNNTACG-1", \
"GATTNNNNNTATG-1", \
"GCACNNNNNTTCC-2", \
"GCAGA-4", \
"GCGGA-4", \
"GTAC-2", \
"TCAAGA-5", \
"TCAGGA-5", \
"TCATGA-5", \
"TCCTGA-5", \
"TCGA-3", \
"TCGAGA-5", \
"TCGGGA-5", \
"TCGTGA-5", \
"TCTAGA-5", \
"TCTGGA-5", \
"TCTTGA-5", \
"TGAGNNNNNNTATG-2", \
"TGCA-3"])

degen_subs = {"[AT]":"W",  \
			  "[CG]":"S",  \
			  "[AC]":"M",  \
			  "[GT]":"K",  \
			  "[AG]":"R",  \
			  "[CT]":"Y",  \
			  "[CGT]":"B", \
			  "[AGT]":"D", \
			  "[ACT]":"H", \
			  "[ACG]":"V", \
			  "[ACGT]":"N"}

degen_nucs = ["[AT]", \
			  "[CG]", \
			  "[AC]", \
			  "[GT]", \
			  "[AG]", \
			  "[CT]", \
			  "[CGT]", \
			  "[AGT]", \
			  "[ACT]", \
			  "[ACG]", \
			  "[ACGT]"]

degen_nucs.sort(key=lambda x: len(x), reverse=True)

#### SHORTEN MOTIFS ####
# shortest_contiguous = min([len(m.split("-")[0]) for m in highscore_motifs.keys()])
# # (1) Sort by keys; shortest motif to longest
# motifs_s = sorted(highscore_motifs, key=len)
# # (2) For each motif, check if it's contained in a longer version of other motifs
# for m in motifs_s:
# 	motif_str =     m.split("-")[0]
# 	motif_idx = int(m.split("-")[1])
# 	for remaining in list(keeper_motifs):
# 		remaining_str =     remaining.split("-")[0]
# 		remaining_idx = int(remaining.split("-")[1])
# 		match         = re.search(motif_str, remaining_str)
# 		if match != None and (motif_idx + match.start()) == remaining_idx and len(remaining_str) > len(motif_str):
# 			# 3. If True, remove the longer version
# 			keeper_motifs.remove(remaining)
########################

#### REFINE DEGEN MOTIFS ####
# keeper_motifs = list(keeper_motifs)
# n_Ns = []
# for m in keeper_motifs:
# 	match = re.search("N+",m)
# 	try:
# 		n_Ns.append(len(match.group()))
# 	except AttributeError:
# 		n_Ns.append(0)
# # Sort by number of N's in the motif
# keeper_motifs    = [x for (x,y) in sorted(zip(keeper_motifs,n_Ns), key=lambda x: x[1])]

# remaining_motifs = []
# for j,m in enumerate(keeper_motifs):
# 	# (1) For each motif, go through each nucleotide and search for degen companions
# 	motif_str =     m.split("-")[0]
# 	motif_idx = int(m.split("-")[1])
# 	for i,nuc in enumerate(m):
# 		# (2) Identify all possible degen companions for that nucleotide
# 		if nuc!="N":
# 			poss_subs = [sub for sub in degen_nucs if nuc in sub]
# 			# (3) Search remaining motifs for instances of ALL companion motifs
# 			for poss_sub in poss_subs:
# 				n_companions = len(poss_sub.strip("[").strip("]"))
# 				to_query = "%s%s%s" % (motif_str[:i], poss_sub, motif_str[(i+1):])
# 				regex    = re.compile(to_query)
# 				found_companions = []
# 				for target_motif in remaining_motifs:
# 					target_motif_str =     target_motif.split("-")[0]
# 					target_motif_idx = int(target_motif.split("-")[1])
# 					match = re.search(regex, target_motif)
# 					if match and target_motif_str!=motif_str and len(motif_str)==len(target_motif_str) and motif_idx==target_motif_idx:
# 						found_companions.append(target_motif)
				
# 				if len(found_companions)==(n_companions-1):
# 					print to_query, m, found_companions, "-->", "%s%s%s" % (motif_str[:i], degen_subs[poss_sub], motif_str[(i+1):])
# 					remaining_motifs.remove(m)
# 					for found_c in found_companions:
# 						remaining_motifs.remove(found_c)
# 					remaining_motifs.append(degen_subs[poss_sub])


# remaining_motifs.sort()
# for motif in remaining_motifs:
# 	print motif
# print len(remaining_motifs)

def wagner_fischer(word_1, word_2):
	n = len(word_1) + 1  # counting empty string 
	m = len(word_2) + 1  # counting empty string
 
	# initialize D matrix
	D = np.zeros(shape=(n, m), dtype=np.int)
	D[:,0] = range(n)
	D[0,:] = range(m)
 
	# B is the backtrack matrix. At each index, it contains a triple
	# of booleans, used as flags. if B(i,j) = (1, 1, 0) for example,
	# the distance computed in D(i,j) came from a deletion or a
	# substitution. This is used to compute backtracking later.
	B = np.zeros(shape=(n, m), dtype=[("del", 'b'), 
									  ("sub", 'b'),
									  ("ins", 'b')])
	B[1:,0] = (1, 0, 0) 
	B[0,1:] = (0, 0, 1)
 
	for i, l_1 in enumerate(word_1, start=1):
		for j, l_2 in enumerate(word_2, start=1):
			deletion = D[i-1,j] + 1
			insertion = D[i, j-1] + 1
			substitution = D[i-1,j-1] + (0 if l_1==l_2 else 2)
 
			mo = np.min([deletion, insertion, substitution])
 
			B[i,j] = (deletion==mo, substitution==mo, insertion==mo)
			D[i,j] = mo
	return D, B

def naive_backtrace(B_matrix):
	i, j = B_matrix.shape[0]-1, B_matrix.shape[1]-1
	backtrace_idxs = [(i, j)]
 
	while (i, j) != (0, 0):
		if B_matrix[i,j][1]:
			i, j = i-1, j-1
		elif B_matrix[i,j][0]:
			i, j = i-1, j
		elif B_matrix[i,j][2]:
			i, j = i, j-1
		backtrace_idxs.append((i,j))
 
	return backtrace_idxs

def align(word_1, word_2, bt):
	aligned_word_1 = []
	aligned_word_2 = []
	operations = []
 
	backtrace = bt[::-1]  # make it a forward trace
 
	for k in range(len(backtrace) - 1): 
		i_0, j_0 = backtrace[k]
		i_1, j_1 = backtrace[k+1]
 
		w_1_letter = None
		w_2_letter = None
		op = None
 
		if i_1 > i_0 and j_1 > j_0:  # either substitution or no-op
			if word_1[i_0] == word_2[j_0]:  # no-op, same symbol
				w_1_letter = word_1[i_0]
				w_2_letter = word_2[j_0]
				op = " "
			else:  # cost increased: substitution
				w_1_letter = word_1[i_0]
				w_2_letter = word_2[j_0]
				op = "s"
		elif i_0 == i_1:  # insertion
			w_1_letter = " "
			w_2_letter = word_2[j_0]
			op = "i"
		else: #  j_0 == j_1,  deletion
			w_1_letter = word_1[i_0]
			w_2_letter = " "
			op = "d"
 
		aligned_word_1.append(w_1_letter)
		aligned_word_2.append(w_2_letter)
		operations.append(op)
 
	return aligned_word_1, aligned_word_2, operations

refined_motifs = []
lens = map(lambda x: len(x), list(keeper_motifs))
for size in list(set(lens)):
	size_motifs = [m for m in list(keeper_motifs) if len(m)==size]
	idxs = set(map(lambda x: x.split("-")[1], size_motifs))
	for idx in list(idxs):
		idx_motifs = [m for m in size_motifs if m.split("-")[1]==idx]
		n_N_motifs = defaultdict(list)
		for m in idx_motifs:
			n_N = len([nuc for nuc in m if nuc=="N"])
			n_N_motifs[n_N].append(m)
		for n_N,N_motifs in n_N_motifs.iteritems():
			motif_set  = set()
			leftovers  = set()
			# Calc edit distance between last motif and the rest of motifs in set
			word1      = N_motifs[-1]
			for word2 in N_motifs[:-1]:
				D,B = wagner_fischer(word1, word2)
				bt  = naive_backtrace(B)
				alignment_table = align(word1, word2, bt)
				n_edits = len([entry for entry in alignment_table[2] if entry!=" "])
				# print word1, word2, n_edits
				n_Ns    = len([x for x in word1 if x=="N"])
				if (n_Ns==0 and n_edits<=1) or (n_Ns>0 and n_edits<=2):
					# Close enough edit distance to use in consensus calling
					motif_set.add(word1)
					motif_set.add(word2)
				else:
					# Not close enough edit distance, will keep motif separate
					leftovers.add(word2)
			if len(motif_set)==0:
				# No companion motif sets found for consensus. Cannot refine.
				refined_motifs+=N_motifs
			else:
				# Call consensus degenerate motif using found companion motifs
				for_consensus = [m.split("-")[0] for m in list(motif_set)]
				if n_N>0:
					# Degenerate caller cannot accept Ns, temporarily replace
					replaced = []
					for j,m in enumerate(for_consensus):
						mk = np.array([i for i,nuc in enumerate(m) if nuc=="N"])
						replaced.append( m.replace("N","T") )
					m = BioMotifs.create(replaced)
					x = list(m.degenerate_consensus)
					for pos in mk:
						x[pos] = "N"
					degen_motif = "".join(x) + "-%s" % idx
				else:
					# No need for N replacement
					m = BioMotifs.create(for_consensus)
					degen_motif = str(m.degenerate_consensus) + "-%s" % idx
				
				refined_motifs.append(degen_motif)

			refined_motifs += list(leftovers)

refined_motifs = list(set(refined_motifs))
for m in refined_motifs:
	print m
