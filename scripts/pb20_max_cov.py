import numpy as np

SLR_cov = {"A_baumannii" : 	10894845, \
	    "A_odontolyticus" : 3730113, \
	    "B_cereus" : 		26483461, \
	    "B_vulgatus" : 		3579005, \
	    "C_beijerinckii" : 	11135316, \
	    "D_radiodurans" : 	3983663, \
	    "E_faecalis" : 		11270120, \
	    "E_coli" : 			2133476693, \
	    "H_pylori" : 		26015813, \
	    "L_gasseri" : 		10760149, \
	    "L_monocytogenes" : 24364014, \
	    "N_meningitidis" : 	15910092, \
	    "P_acnes" : 		26717866, \
	    "P_aeruginosa" : 	170029436, \
	    "R_sphaeroides" : 	29901273, \
	    "S_aureus" : 		61148568, \
	    "S_epidermidis" : 	173408151, \
	    "S_agalactiae" : 	49104157, \
	    "S_mutans" : 		252711874, \
	    "S_pneumoniae" : 	23107608}

SMRT_cov = {"A_baumannii" : 458337257, \
	    "A_odontolyticus" : 390178052, \
	    "B_cereus" : 		101134305, \
	    "B_vulgatus" : 		744355850, \
	    "C_beijerinckii" : 	518725397, \
	    "D_radiodurans" : 	112809810, \
	    "E_faecalis" : 		388729626, \
	    "E_coli" : 			607875898, \
	    "H_pylori" : 		1637499690, \
	    "L_gasseri" : 		436941561, \
	    "L_monocytogenes" : 770815426, \
	    "N_meningitidis" : 	448123214, \
	    "P_acnes" : 		533794211, \
	    "P_aeruginosa" : 	1072961775, \
	    "R_sphaeroides" : 	363229532, \
	    "S_aureus" : 		616183706, \
	    "S_epidermidis" : 	444655487, \
	    "S_agalactiae" : 	277523113, \
	    "S_mutans" : 		506964576, \
	    "S_pneumoniae" : 	76800567}

for key in SLR_cov.keys():
	idx = np.argmax([SLR_cov[key], SMRT_cov[key]])
	print key, ["SLR", "SMRT"][idx]