import os,sys
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA as sklearnPCA
import numpy as np
import pandas as pd

orig_mat_fn = sys.argv[1]

X        = pd.read_csv(filepath_or_buffer=orig_mat_fn, header=None, sep='\t')
X_std    = StandardScaler().fit_transform(X)
cov_mat  = np.cov(X_std.T)

eig_vals, eig_vecs = np.linalg.eig(cov_mat)
for ev in eig_vecs:
    np.testing.assert_array_almost_equal(1.0, np.linalg.norm(ev))

# Make a list of (eigenvalue, eigenvector) tuples, sort from high to low
eig_pairs = [(np.abs(eig_vals[i]), eig_vecs[:,i]) for i in range(len(eig_vals))]
eig_pairs.sort(key=lambda x: x[0], reverse=True)

tot         = sum(eig_vals)
var_exp     = [(i / tot)*100 for i in sorted(eig_vals, reverse=True)]
cum_var_exp = np.cumsum(var_exp)
for j,pair in enumerate(eig_pairs):
	nPC = j+1
	if cum_var_exp[j]>=90:
		# print "PC-%s: %.4f" % (nPC, cum_var_exp[j])
		break

sklearn_pca = sklearnPCA(n_components=nPC)
dim_reduced = sklearn_pca.fit_transform(X_std)

outname = orig_mat_fn + ".90pctPCA"
np.savetxt(outname, dim_reduced, fmt="%.5f", delimiter="\t")