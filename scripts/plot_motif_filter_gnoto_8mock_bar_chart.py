import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})
import numpy as np
import pub_figs

font_size      = 25
adjust         = 0.15
fontProperties = pub_figs.setup_math_fonts(font_size)

true_motifs = {"AATCC-1":          ["btheta"],    \
			   "CCANNNNNNCAT-2":   ["btheta"],    \
			   "ATGNNNNNNTGG-0":   ["btheta"],    \
			   # "GGCANNNNNNNRTTT-3":["btheta"],    \
			   "GGCANNNNNNNATTT-3":["btheta"],    \
			   "GGCANNNNNNNGTTT-3":["btheta"],    \
			   # "AAAYNNNNNNNTGCC-2":["btheta"],    \
			   "AAACNNNNNNNTGCC-2":["btheta"],    \
			   "AAATNNNNNNNTGCC-2":["btheta"],    \
			   # "RGATCY-2":         ["btheta"],    \
			   "AGATCC-2":         ["btheta"],    \
			   "GGATCC-2":         ["btheta"],    \
			   "AGATCT-2":         ["btheta"],    \
			   "GGATCT-2":         ["btheta"],    \
			   # "CAYNNNNNRTG-1":    ["bvulgatus"], \
			   "CACNNNNNATG-1":    ["bvulgatus"], \
			   "CACNNNNNGTG-1":    ["bvulgatus"], \
			   "CATNNNNNATG-1":    ["bvulgatus"], \
			   "CATNNNNNGTG-1":    ["bvulgatus"], \
			   "GAAGNNNNNNNTCC-2": ["bvulgatus"], \
			   "GGANNNNNNNCTTC-2": ["bvulgatus"], \
			   # "GACNNNNNRGAC-1":   ["bcaccae"],   \
			   "GACNNNNNAGAC-1":   ["bcaccae"],   \
			   "GACNNNNNGGAC-1":   ["bcaccae"],   \
			   "CAGNNNNNGGA-1":    ["bcaccae", "bovatus"],   \
			   "GATGG-1":          ["bcaccae"],   \
			   "CCATC-2":          ["bcaccae"],   \
			   # "GACNNNNNNRTTG-1":  ["bcaccae"],   \
			   "GACNNNNNNATTG-1":  ["bcaccae"],   \
			   "GACNNNNNNGTTG-1":  ["bcaccae"],   \
			   # "CAAYNNNNNNGTC-2":  ["bcaccae"],   \
			   "CAACNNNNNNGTC-2":  ["bcaccae"],   \
			   "CAATNNNNNNGTC-2":  ["bcaccae"],   \
			   # "CGMAGG-3":         ["bcaccae"],   \
			   "CGAAGG-3":         ["bcaccae"],   \
			   "CGCAGG-3":         ["bcaccae"],   \
			   # "GATGNAG-5":        ["bovatus"],   \
			   "GATGAAG-5":        ["bovatus"],   \
			   "GATGCAG-5":        ["bovatus"],   \
			   "GATGGAG-5":        ["bovatus"],   \
			   "GATGTAG-5":        ["bovatus"],   \
			   "GATC-1":           ["bovatus", "ecoli"],   \
			   "TAANNNNNNCTTG-2":  ["bovatus"],   \
			   "CAAGNNNNNNTTA-2":  ["bovatus"],   \
			   # "WGATC-2":          ["cbolteae"],  \
			   "AGATC-2":          ["cbolteae"],  \
			   "TGATC-2":          ["cbolteae"],  \
			   "CTAAG-3":          ["cbolteae"],  \
			   # "GAYNNNNNNNTCGC-1": ["cbolteae"],  \
			   "GACNNNNNNNTCGC-1": ["cbolteae"],  \
			   "GATNNNNNNNTCGC-1": ["cbolteae"],  \
			   # "GCGANNNNNNNRTC-3": ["cbolteae"],  \
			   "GCGANNNNNNNATC-3": ["cbolteae"],  \
			   "GCGANNNNNNNGTC-3": ["cbolteae"],  \
			   # "SGAKC-2":          ["cbolteae"],  \
			   "CGAGC-2":          ["cbolteae"],  \
			   "CGATC-2":          ["cbolteae"],  \
			   "GGAGC-2":          ["cbolteae"],  \
			   "GGATC-2":          ["cbolteae"],  \
			   "CAGNNNNNCTG-1":    ["cbolteae"],  \
			   "GATC-1":           ["bovatus", "ecoli"],     \
			   "GCACNNNNNNGTT-2":  ["ecoli"],     \
			   "AACNNNNNNGTGC-1":  ["ecoli"],     \
			   "CAGGAG-4":         ["caero"]}

# called_motifs =     {"CCANNNNNGCAT-2":   "TV", \
# 					 "CAGGAG-4":         "TP", \
# 					 "GAGC-1":           "TV", \
# 					 "ATGNNNNNTTGG-0":   "TV", \
# 					 "GAGCA-1":          "TV", \
# 					 "CAGNNNNNGGAA-1":   "TV", \
# 					 "CCANNNNNTCAT-2":   "TV", \
# 					 "AGATCT-2":         "TP", \
# 					 "GGAG-2":           "TV", \
# 					 "ATGNNNNNNTGG-0":   "TP", \
# 					 "CACNNNNNATG-1":    "TP", \
# 					 "ACAGNNNNNGGA-2":   "TV", \
# 					 "CAATNNNNNNGTC-2":  "TP", \
# 					 "CCATC-2":          "TP", \
# 					 "ATGANNNNNTGG-0":   "TV", \
# 					 "AACNNNNNNGTGC-1":  "TP", \
# 					 "CAGNNNNNGGA-1":    "TP", \
# 					 "CAGNNNNNGGAT-1":   "TV", \
# 					 "GATC-1":           "TP", \
# 					 "AGATCC-2":         "TP", \
# 					 "AGCGNNNNNNCGGG-0": "FP", \
# 					 "ATGNNNNNATGG-0":   "TV", \
# 					 "GCACNNNNNNGTT-2":  "TP", \
# 					 "TCAGNNNNNGGA-2":   "TV", \
# 					 "CACNNNNNGTG-1":    "TP", \
# 					 "AATCC-1":          "TP", \
# 					 "CCATNNNNNGTG-2":   "TV", \
# 					 "CCANNNNNNCAT-2":   "TP", \
# 					 "GGAGC-2":          "TP", \
# 					 "ATGNNNNNNTGGA-0":  "TV", \
# 					 "GAGCT-1":          "TV", \
# 					 "GCAGNNNNNGGA-2":   "TV", \
# 					 "GGATCT-2":         "TP", \
# 					 "CCGANNNNNNGGCG-3": "FP", \
# 					 "GATGG-1":          "TP"}

called_motifs = {"CAGNNNNNGGA-1": "TP", \
				 "CAGGAG-4":      "TP", \
				 "GATC-1":        "TP", \
				 "AGATCC-2":      "TP", \
				 "GGAGC-2":       "TP", \
				 "GAGC-1":        "TV", \
				 "CACNNNNNATG-1": "TP", \
				 "CCANNNNNNCAT-2":"TP", \
				 "AGATCT-2":      "TP", \
				 "GGATCT-2":      "TP", \
				 "ATGNNNNNNTGG-0":"TP", \
				 "CCATC-2":       "TP", \
				 "AATCC-1":       "TP", \
				 "GATGG-1":       "TP"}

print len(called_motifs.keys()), len(set(true_motifs.keys())), len(set(called_motifs.keys()) & set(true_motifs.keys()))
print len([ (motif,m) for motif,m in called_motifs.items() if m=="TP"])
print len([ (motif,m) for motif,m in called_motifs.items() if m=="TV"])
print len([ (motif,m) for motif,m in called_motifs.items() if m=="FP"])

n_filtered = len(called_motifs)
n_TP       = len([ (motif,m) for motif,m in called_motifs.items() if m=="TP"])
n_TV       = len([ (motif,m) for motif,m in called_motifs.items() if m=="TV"])
n_FP       = len([ (motif,m) for motif,m in called_motifs.items() if m=="FP"])
# make a square figure and axes
plt.figure(1, figsize=(8,8))
ax = plt.axes([0.2, 0.3, 0.5, 0.65])

tot_motifs = np.sum([n_TP, n_TV, n_FP])
vals  = ( 100*float(n_TP)/tot_motifs, 100*float(n_TV)/tot_motifs, 100*float(n_FP)/tot_motifs )
Ns    = [n_TP, n_TV, n_FP]
N     = len(vals)
ind   = np.arange(N)  # the x locations for the groups
width = 0.75       # the width of the bars

rects1  = ax.bar(ind + float(width)/2, vals, width, color='k')

def autolabel(rects, Ns):
    # attach some text labels
    for i,rect in enumerate(rects):
        height = rect.get_height()
        N = Ns[i]
        ax.text(rect.get_x() + rect.get_width()/2., height+2, 'N=%s' % int(N), ha='center', va='bottom')

autolabel(rects1, Ns)

# add some text for labels, title and axes ticks
ax.set_ylabel('$\%$ called motifs')
ax.set_xticks(ind + width)
ax.set_xticklabels(('True positives', 'True variants', 'False positives'), rotation=45, ha="right")
ax.xaxis.labelpad = 30
pub_figs.remove_top_right_axes(ax)

bar_fn = "filtered_motifs_bar.png"
plt.savefig(bar_fn)