import os,sys
import glob
import subprocess
import multiprocessing

procs = 6

def launch_pool( procs, funct, args ):
	p    = multiprocessing.Pool(processes=procs)
	try:
		results = p.map(funct, args)
		p.close()
		p.join()
	except KeyboardInterrupt:
		p.terminate()
	return results

def launch_mummer(ref):
	xi = ref.split("/")[1].split(".")[0]
	for box_id in range(1,10):
	# for box_id in [2,7]:
		compare   = "%s_vs_box%s" % (xi, box_id)
		print compare
		box_fasta = "box%s_gt100kb.fasta" % box_id
		CMD1 = "nucmer -p %s %s %s" % (compare, ref, box_fasta)
		CMD2 = "delta-filter -m %s.delta > %s.delta.m" % (compare, compare)
		sts,stdOutErr = run_OS_command(CMD1)
		sts,stdOutErr = run_OS_command(CMD2)
		if os.path.getsize("%s.delta.m" % compare)>300:
			CMD3 = "./my_mummerplot -color -layout %s.delta.m --png -t %s -p %s" % (compare, compare, compare)
			sts,stdOutErr = run_OS_command(CMD3)

		# to_rm = glob.glob("*.filter")
		# for f in to_rm:
		# 	os.remove(f)
		# to_rm = glob.glob("*.*plot")
		# for f in to_rm:
		# 	os.remove(f)
		# to_rm = glob.glob("*.gp")
		# for f in to_rm:
		# 	os.remove(f)

def run_OS_command( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts != 0:
		print "Failed command: %s" % CMD
	return sts, stdOutErr

omerods = glob.glob(os.path.join("mouse_omerod_catalog", "*.fna"))

results = launch_pool(procs, launch_mummer, omerods)

# for omerod in omerods:
# 	om = omerod.split("/")[1].split("_genomic")[0]
# 	for box_id in range(1,10):
# 		compare   = "%s_vs_box%s" % (om, box_id)
# 		box_fasta = "box%s_gt100kb.fasta" % box_id
# 		CMD1 = "nucmer %s %s" % (omerod, box_fasta)
# 		CMD2 = "delta-filter -m out.delta > out.delta.m"
# 		CMD3 = "./my_mummerplot -color -layout out.delta.m --png -t %s -p %s" % (compare, compare)
# 		sts,stdOutErr = run_OS_command(CMD1)
# 		sts,stdOutErr = run_OS_command(CMD2)
# 		sts,stdOutErr = run_OS_command(CMD3)

# 		to_rm = glob.glob("*.filter")
# 		for f in to_rm:
# 			os.remove(f)
# 		to_rm = glob.glob("*.*plot")
# 		for f in to_rm:
# 			os.remove(f)
# 		to_rm = glob.glob("*.gp")
# 		for f in to_rm:
# 			os.remove(f)