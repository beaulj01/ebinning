import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pub_figs
import numpy as np

font_size = 35
adjust    = 0.15

fontProperties = pub_figs.setup_math_fonts(font_size)

aligns_all = sys.argv[1]
aligns_filtered = sys.argv[2]

fig = pub_figs.init_fig(x=15,y=15)
ax  = fig.add_subplot(1,1,1)
width = 0.35
for k,align_fn in enumerate([aligns_filtered, aligns_all]):
	tups = []
	for i,line in enumerate(open(align_fn).xreadlines()):
		if i<5: continue
		line = line.strip()
		vals = line.split()
		rstart =   int(vals[0])
		rend   =   int(vals[1])
		qstart =   int(vals[3])
		qend   =   int(vals[4])
		rlen   =   int(vals[6])
		qlen   =   int(vals[7])
		idy    = float(vals[9])
		rcov   = float(vals[11])
		qcov   = float(vals[12])
		contig =       vals[17]
		if qcov<95: continue
		tup    = (qlen,rcov,rcov,qcov,idy,contig)
		tups.append(tup)

	tups.sort(key=lambda x: x[0], reverse=True)
	pct_ref_covered = 0
	X = []
	Y = []
	labs = []
	for j,tup in enumerate(tups):
		if j==20:
			break
		rcov   = tup[1]
		contig = tup[5]
		pct_ref_covered += rcov
		X.append(j)
		Y.append(pct_ref_covered)
		labs.append(j+1)

	ind = np.arange(j)
	if k==0:
		# rects1 = ax.bar(ind,       Y, width, color='0.7')
		line1 = ax.plot(ind, Y, "ro-", linewidth=7, markersize=15)
	elif k==1:
		# rects2 = ax.bar(ind+width, Y, width, color='k')
		line2 = ax.plot(ind, Y, "bo-", linewidth=7, markersize=15)

ax.set_xlabel("Assembled contig (20 largest)")
ax.set_ylabel("Percent of ref. covered")
# ax.set_xticks(ind+width)
ax.set_xticks(ind)
ax.set_xticklabels( labs, rotation=90)
ax.xaxis.labelpad = 30
ax.yaxis.labelpad = 30

# legend = ax.legend( (rects1[0], rects2[0]), ('Unfiltered', '$SM_{P}$ filtered'), loc=2 )
legend = ax.legend( (line1[0], line2[0]), ('$SM_{P}$ filtered', 'Unfiltered'), loc=2 )
legend.draw_frame(False)
pub_figs.change_font_size(ax, font_size=font_size, bottom_adjust=adjust, left_adjust=adjust)
pub_figs.remove_top_right_axes(ax)
fn = "UFL_contig_coverage"
fig.savefig(fn+".png")