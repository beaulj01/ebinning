import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.ticker import MultipleLocator
import subprocess
import numpy as np
from collections import defaultdict,Counter
import operator
import pub_figs
from pbcore.io.align.CmpH5IO import CmpH5Reader
import math

fontProperties = pub_figs.setup_math_fonts(24)

# combined_cmph5_fns = ["/hpc/users/beaulj01/projects/ebinning/2HP/combined_HGAP/J99_150x_and_26695_50x/gt5kb_only/genomesize_3.2/data/aligned_reads.cmp.h5",  \
# 					  "/hpc/users/beaulj01/projects/ebinning/2HP/combined_HGAP/J99_150x_and_26695_100x/gt5kb_only/genomesize_3.2/data/aligned_reads.cmp.h5", \
# 					  "/hpc/users/beaulj01/projects/ebinning/2HP/combined_HGAP/J99_150x_and_26695_150x/gt5kb_only/genomesize_3.2/data/aligned_reads.cmp.h5"]

combined_cmph5_fns = ["/hpc/users/beaulj01/projects/ebinning/2HP/combined_HGAP/J99_150x_and_26695_50x/gt5kb_only/data/aligned_reads.cmp.h5",  \
					  "/hpc/users/beaulj01/projects/ebinning/2HP/combined_HGAP/J99_150x_and_26695_100x/gt5kb_only/data/aligned_reads.cmp.h5", \
					  "/hpc/users/beaulj01/projects/ebinning/2HP/combined_HGAP/J99_150x_and_26695_150x/gt5kb_only/data/aligned_reads.cmp.h5"]

assembly_names  = ["J99_150x_and_26695_50x",  \
				   "J99_150x_and_26695_100x", \
				   "J99_150x_and_26695_150x"]

assembly_labels = {"J99_150x_and_26695_50x" : " 50x 26695, 150x J99", \
				   "J99_150x_and_26695_100x": "100x 26695, 150x J99", \
				   "J99_150x_and_26695_150x": "150x 26695, 150x J99"}

movies_spec = {"m140130_050607_42177R_c100623362550000001823111308061481_s1_p0" : "J99", \
			   "m140130_082505_42177R_c100623362550000001823111308061482_s1_p0" : "26695"}

fig1 = plt.figure(figsize=[15,12])
for i in range(3):
	ax       = fig1.add_subplot(3,3,i+7)
	ax.axis("off")
	
	ind     = np.arange(2)+0.2
	width   = 0.65
	HPJ99   = int(assembly_names[i].split("_")[1].strip("x"))
	HP26695 = int(assembly_names[i].split("_")[4].strip("x"))
	
	y        = np.array([0, 1])
	plot     = ax.scatter(y, y, c=y, cmap='cool')
	plt.cla()
	cbaxes   = fig1.add_axes([0.42, 0.52, 0.18, 0.03])
	cbar     = plt.colorbar(plot, cax=cbaxes, orientation="horizontal")
	cbar.set_ticks([0,1])
	# cbar.ax.set_xticklabels([r"100$\%$ 26695",r"100$\%$ J99"])#, rotation=45, ha="right")
	cbar.ax.set_xticklabels([r"26695",r"J99"])#, rotation=45, ha="right")

	ax       = fig1.add_subplot(3,3,i+1)
	rects1   = ax.bar(ind,   [HP26695,0], width, color=plt.get_cmap('cool')(0), linewidth=0)
	rects2   = ax.bar(ind+1, [HPJ99,0],   width, color=plt.get_cmap('cool')(0.99), linewidth=0)
	ax.set_xlim([0,2])
	plt.xticks([0.5,1.5], ["26695", "J99"], rotation=45, ha="right")
	plt.yticks([50,100,150], ["50x","100x","150x"])
	pub_figs.remove_top_right_axes(ax)
plt.subplots_adjust(wspace=1.3)
plt.savefig("HP_mixed_assemblies_proportions.png")

fig = plt.figure(figsize=[15,12])
ax  = fig.add_subplot(111) 
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.tick_params(labelcolor='w', top='off', bottom='off', left='off', right='off')


# First row
for i,cmph5 in enumerate(combined_cmph5_fns):
	ax     = fig.add_subplot(3,3,i+1)
	
	print assembly_names[i]
	
	contigs_counters = {}
	contig_lens      = {}
	seen_contigs     = set()
	reader           = CmpH5Reader(cmph5)

	for entry in reader.referenceInfoTable:
		contig_lens[entry[3]] = entry[4]

	for r in reader:
		movie     = r.movieName
		read_spec = movies_spec[movie]
		contig    = r.referenceName
		if contig not in seen_contigs:
			seen_contigs.add(contig)
			contigs_counters[contig] = Counter()
		contigs_counters[contig][read_spec] += 1

	sorted_contig_lens = sorted(contig_lens.items(), key=operator.itemgetter(1), reverse=True)

	x     = []
	y     = []
	color = []
	area  = []
	for j,(contig,length) in enumerate(sorted_contig_lens):
		tot_reads       = np.sum(contigs_counters[contig].values())
		reads_pct_J99   = float(contigs_counters[contig]["J99"])/tot_reads
		reads_pct_26695 = float(contigs_counters[contig]["26695"])/tot_reads
		print "%s  \t%.3f\t%.3f\t%s\t%s" % (contig, reads_pct_J99, reads_pct_26695, tot_reads, length)
		x.append(100*reads_pct_J99)
		y.append(j)
		color.append(plt.get_cmap('cool')(reads_pct_J99))
		area.append(math.sqrt(length))
	sct = ax.scatter(x, y, c=color, s=area, linewidths=2, edgecolor='w')
	sct.set_alpha(0.75)
	ax.set_xlim([-10,110])
	ax.set_ylim([-2,16])
	ax.set_xticks(range(0,105,25))
	ax.set_yticks(range(0,16,5))
	pub_figs.remove_top_right_axes(ax)
	ax.minorticks_on()
	ax.tick_params('both', length=10, width=2, which='major')
	ax.tick_params('both', length=5, width=1, which='minor')
	ax.tick_params(pad=10)
	if i==(len(assembly_names)-2):
		ax.set_xlabel(r"$\%$ J99 reads in contig")
	if i==0:
		ax.set_ylabel('Contig count')
	xticks = ax.xaxis.get_minor_ticks()
	yticks = ax.yaxis.get_minor_ticks()
	xticks[0].set_visible(False)
	xticks[-1].set_visible(False)
	yticks[0].set_visible(False)
	yticks[-1].set_visible(False)
	plt.grid()

# Second row
PCA      = "reads.SMp.gt5000.PCA"
labels   = "reads.raw.labels.gt5000"
names    = "reads.raw.names.gt5000"
combined_dirs    = ["/hpc/users/beaulj01/projects/ebinning/2HP/combined_HGAP/J99_150x_and_26695_50x/binning_bas", \
					"/hpc/users/beaulj01/projects/ebinning/2HP/combined_HGAP/J99_150x_and_26695_100x/binning_bas", \
					"/hpc/users/beaulj01/projects/ebinning/2HP/combined_HGAP/J99_150x_and_26695_150x/binning_bas"]
assembly_names  = ["J99_150x_and_26695_50x", \
				   "J99_150x_and_26695_100x", \
				   "J99_150x_and_26695_150x"]
assembly_labels = {"J99_150x_and_26695_30x" : " 30x 26695, 150x J99", \
				   "J99_150x_and_26695_50x" : " 50x 26695, 150x J99", \
				   "J99_150x_and_26695_100x": "100x 26695, 150x J99", \
				   "J99_150x_and_26695_150x": "150x 26695, 150x J99"}
label_dict = {"26695": r"${H. pylori}$ 26695", \
			  "J99":   r"${H. pylori}$ J99"}
plt.hot()
for d,dir_name in enumerate(combined_dirs):
	ax          = fig.add_subplot(3,3,d+4)
	
	pca_fn      = os.path.join(dir_name, PCA)
	read_labs   = os.path.join(dir_name, labels)
	read_names  = os.path.join(dir_name, names)
	SMp_coords  = np.loadtxt(pca_fn,     dtype="float")
	read_labels = np.loadtxt(read_labs,  dtype="str")
	read_names  = np.loadtxt(read_names, dtype="str")
	if d==0:
		m    = 0.8
		b    = -1.1
		xmin = 0
		xmax = 4
		ymin = -2
		ymax = 2
	elif d==1:
		m    = -0.8
		b    = -1.3
		xmin = -4
		xmax = 0
		ymin = -2
		ymax = 2
	elif d==2:
		m    = 0.8
		b    = -1.5
		xmin = 0
		xmax = 4
		ymin = -2
		ymax = 2
	extent            = [xmin, xmax, ymin, ymax]
	H, xedges, yedges = np.histogram2d(SMp_coords[:,0], SMp_coords[:,1], bins=25, range=[extent[:2], extent[2:]])
	H       = np.rot90(H)
	H       = np.flipud(H)
	Hmasked = np.ma.masked_where(H==0,H)         # Mask pixels with a value of zero
	im      = plt.imshow(H, interpolation='bilinear', origin='lower', cmap=cm.Greys, extent=extent) # Sets background image
	levels  = [200,100,50,25]
	cols    = ["k","0.4","0.6","0.8"]
	cset    = plt.contour(H, levels, origin="lower",colors=cols,linewidths=1.4,extent=extent)
	plt.clabel(cset, inline=1, fontsize=10, fmt="%1.0i")
	for c in cset.collections:
		c.set_linestyle("solid")
	x1 = np.array(range(xmin,xmax+1)) 
	y1 = eval("m*x1 + b")
	plt.plot(x1, y1, linestyle="--", color="k")
	if d==(len(assembly_names)-2):
		ax.set_xlabel("PC1")
	if d==0:
		ax.set_ylabel("PC2")
	ax.set_xticks(range(xmin,xmax+1,1))
	ax.set_yticks(range(ymin,ymax+1,1))
	ax.tick_params(pad=10)
	pub_figs.remove_top_right_axes(ax)

# Third row
iso_cmph5_base_dirs = ["/hpc/users/beaulj01/projects/ebinning/2HP/combined_HGAP/J99_150x_and_26695_50x/iso_HGAPs",  \
					   "/hpc/users/beaulj01/projects/ebinning/2HP/combined_HGAP/J99_150x_and_26695_100x/iso_HGAPs", \
					   "/hpc/users/beaulj01/projects/ebinning/2HP/combined_HGAP/J99_150x_and_26695_150x/iso_HGAPs"]

for i,iso_cmph5_base_dir in enumerate(iso_cmph5_base_dirs):
	ax     = fig.add_subplot(3,3,i+7)

	contigs_counters = {}
	contig_lens      = {}
	seen_contigs     = set()
	for strain in ["26695", "J99"]:
		cmph5 = os.path.join(iso_cmph5_base_dir, strain, "data", "aligned_reads.cmp.h5")
		print cmph5
		reader           = CmpH5Reader(cmph5)

		for entry in reader.referenceInfoTable:
			contig_lens[entry[3] + ".%s" % strain] = entry[4]

		for r in reader:
			movie     = r.movieName
			read_spec = movies_spec[movie]
			contig    = r.referenceName + ".%s" % strain
			if contig not in seen_contigs:
				seen_contigs.add(contig)
				contigs_counters[contig] = Counter()
			contigs_counters[contig][read_spec] += 1

	sorted_contig_lens = sorted(contig_lens.items(), key=operator.itemgetter(1), reverse=True)

	x     = []
	y     = []
	color = []
	area  = []
	for j,(contig,length) in enumerate(sorted_contig_lens):
		tot_reads       = np.sum(contigs_counters[contig].values())
		reads_pct_J99   = float(contigs_counters[contig]["J99"])/tot_reads
		reads_pct_26695 = float(contigs_counters[contig]["26695"])/tot_reads
		print "%s  \t%.3f\t%.3f\t%s\t%s" % (contig, reads_pct_J99, reads_pct_26695, tot_reads, length)
		x.append(100*reads_pct_J99)
		y.append(j)
		color.append(plt.get_cmap('cool')(reads_pct_J99))
		area.append(math.sqrt(length))
	sct = ax.scatter(x, y, c=color, s=area, linewidths=2, edgecolor='w')
	sct.set_alpha(0.75)
	ax.set_xlim([-10,110])
	ax.set_ylim([-2,16])
	ax.set_xticks(range(0,105,25))
	ax.set_yticks(range(0,16,5))
	pub_figs.remove_top_right_axes(ax)
	ax.minorticks_on()
	ax.tick_params('both', length=10, width=2, which='major')
	ax.tick_params('both', length=5, width=1, which='minor')
	ax.tick_params(pad=10)
	if i==(len(assembly_names)-2):
		ax.set_xlabel(r"$\%$ J99 reads in contig")
	if i==0:
		ax.set_ylabel('Contig count')
	xticks = ax.xaxis.get_minor_ticks()
	yticks = ax.yaxis.get_minor_ticks()
	xticks[0].set_visible(False)
	xticks[-1].set_visible(False)
	yticks[0].set_visible(False)
	yticks[-1].set_visible(False)
	plt.grid()

# plt.tight_layout()
fig.subplots_adjust(top=0.98, hspace=0.5)
plt.savefig("HP_mixed_assemblies_summary.png")