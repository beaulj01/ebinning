import os,sys
from Bio import SeqIO
import glob

# Check if labeled files already exist
to_rm = glob.glob("refs/EC_BAA_*.labeled.fasta")
for fn in to_rm:
	os.remove(fn)

fns = glob.glob("refs/EC_BAA_*.fasta")

for fn in fns:
	with open(fn.replace(".fasta", ".labeled.fasta"), "w") as output_handle:
		for j,entry in enumerate(SeqIO.parse(open(fn,"r"), "fasta")):
			contig_n = entry.id.split("|")[0].split("_")[1]
			entry.id = ""
			entry.id = "%s_contig_%s" % (fn.split(".")[0].split("/")[1], contig_n)
			SeqIO.write(entry, output_handle, "fasta")
