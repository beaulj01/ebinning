import os,sys
import subprocess

def run_OS( CMD ):
	print "Running %s" % CMD
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts != 0:
		print "Failed command: %s" % CMD
		for line in stdOutErr:
			if line!="":
				print line.strip()
	return sts, stdOutErr

print "########################"
print "# Composition"
print "########################"
cmd            = "python ~/gitRepo/eBinning/scripts/normalize_cols_proto.py contigs.comp.2D.digest"
sts, stdOutErr = run_OS(cmd)
cmd            = "wc -l contigs.labels | cut -d\" \" -f1"
sts, stdOutErr = run_OS(cmd)
num_contigs    = int(stdOutErr[0].split()[0].strip())
for suffix in ["", ".zscores", ".minmax"]:
	cmd = "head -%s contigs.comp.2D.digest%s > contigs.comp.2D.noDigest%s" % (num_contigs, suffix, suffix)
	sts, stdOutErr = run_OS(cmd)
	cmd = "python ~/gitRepo/eBinning/scripts/plot_cluster_silhouette_analysis.py contigs.comp.2D.noDigest%s contigs.labels" % suffix
	sts, stdOutErr = run_OS(cmd)
	print suffix, stdOutErr[0]

print ""
print "########################"
print "# Composition + Coverage"
print "########################"
cmd            = "paste contigs.comp.2D.digest contigs.cov.digest > contigs.cov_comp.3D.digest"
sts, stdOutErr = run_OS(cmd)
cmd            = "python ~/gitRepo/eBinning/scripts/normalize_cols_proto.py contigs.cov_comp.3D.digest"
sts, stdOutErr = run_OS(cmd)
cmd            = "wc -l contigs.labels | cut -d\" \" -f1"
sts, stdOutErr = run_OS(cmd)
num_contigs    = int(stdOutErr[0].split()[0].strip())
for suffix in ["", ".zscores", ".minmax"]:
	sts  = 1
	seed = 10
	while sts!=0:
		cmd = "python ~/gitRepo/eBinning/src/bhtsne.py -r %s -v -i contigs.cov_comp.3D.digest%s -l contigs.labels.digest -o contigs.cov_comp.2D.digest%s -s contigs.cov_comp.2D.digest%s.scatter.png -z contigs.lengths.digest -n contigs_cov_comp_digest%s -p 30" % (seed,suffix,suffix,suffix,suffix)
		sts, stdOutErr = run_OS(cmd)
		seed -= 1
	cmd = "head -%s contigs.cov_comp.2D.digest%s > contigs.cov_comp.2D.noDigest%s" % (num_contigs, suffix, suffix)
	sts, stdOutErr = run_OS(cmd)
	cmd = "python ~/gitRepo/eBinning/scripts/plot_cluster_silhouette_analysis.py contigs.cov_comp.2D.noDigest%s contigs.labels" % suffix
	sts, stdOutErr = run_OS(cmd)
	print suffix, stdOutErr[0]

print ""
print "########################"
print "# SCp"
print "########################"
cmd            = "python ~/gitRepo/eBinning/scripts/normalize_cols_proto.py contigs.SCp.2D.digest"
sts, stdOutErr = run_OS(cmd)
for suffix in ["", ".zscores", ".minmax"]:
	cmd = "head -%s contigs.SCp.2D.digest%s > contigs.SCp.2D.noDigest%s" % (num_contigs, suffix, suffix)
	sts, stdOutErr = run_OS(cmd)
	cmd = "python ~/gitRepo/eBinning/scripts/plot_cluster_silhouette_analysis.py contigs.SCp.2D.noDigest%s contigs.labels" % suffix
	sts, stdOutErr = run_OS(cmd)
	print suffix, stdOutErr[0]

print ""
print "##############################"
print "# Composition + SCp"
print "##############################"
cmd            = "paste contigs.comp.2D.digest contigs.SCp.2D.digest> contigs.comp_SCp.4D.digest"
sts, stdOutErr = run_OS(cmd)
cmd            = "python ~/gitRepo/eBinning/scripts/normalize_cols_proto.py contigs.comp_SCp.4D.digest"
sts, stdOutErr = run_OS(cmd)
for suffix in ["", ".zscores", ".minmax"]:
	sts  = 1
	seed = 10
	while sts!=0:
		seed 
		cmd = "python ~/gitRepo/eBinning/src/bhtsne.py -r %s -v -i contigs.comp_SCp.4D.digest%s -l contigs.labels.digest -o contigs.comp_SCp.2D.digest%s -s contigs.comp_SCp.2D.digest%s.scatter.png -z contigs.lengths.digest -n contigs_comp_SCp_digest%s -p 30" % (seed,suffix,suffix,suffix,suffix)
		sts, stdOutErr = run_OS(cmd)
		seed -= 1
	cmd = "head -%s contigs.comp_SCp.2D.digest%s > contigs.comp_SCp.2D.noDigest%s" % (num_contigs, suffix, suffix)
	sts, stdOutErr = run_OS(cmd)
	cmd = "python ~/gitRepo/eBinning/scripts/plot_cluster_silhouette_analysis.py contigs.comp_SCp.2D.noDigest%s contigs.labels" % suffix
	sts, stdOutErr = run_OS(cmd)
	print suffix, stdOutErr[0]

print ""
print "##############################"
print "# Composition + Coverage + SCp"
print "##############################"
cmd            = "paste contigs.comp.2D.digest contigs.cov.digest contigs.SCp.2D.digest> contigs.combined.5D.digest"
sts, stdOutErr = run_OS(cmd)
cmd            = "python ~/gitRepo/eBinning/scripts/normalize_cols_proto.py contigs.combined.5D.digest"
sts, stdOutErr = run_OS(cmd)
for suffix in ["", ".zscores", ".minmax"]:
	sts  = 1
	seed = 10
	while sts!=0:
		seed 
		cmd = "python ~/gitRepo/eBinning/src/bhtsne.py -r %s -v -i contigs.combined.5D.digest%s -l contigs.labels.digest -o contigs.combined.2D.digest%s -s contigs.combined.2D.digest%s.scatter.png -z contigs.lengths.digest -n contigs_combined_digest%s -p 30" % (seed,suffix,suffix,suffix,suffix)
		sts, stdOutErr = run_OS(cmd)
		seed -= 1
	cmd = "head -%s contigs.combined.2D.digest%s > contigs.combined.2D.noDigest%s" % (num_contigs, suffix, suffix)
	sts, stdOutErr = run_OS(cmd)
	cmd = "python ~/gitRepo/eBinning/scripts/plot_cluster_silhouette_analysis.py contigs.combined.2D.noDigest%s contigs.labels" % suffix
	sts, stdOutErr = run_OS(cmd)
	print suffix, stdOutErr[0]
