import os,sys
from collections import Counter
from itertools import product
from Bio import SeqIO
import numpy as np

names_fn   = "contigs.names"
labels_fn  = "contigs.labels"
lengths_fn = "contigs.lengths"
# comps_fn   = "contigs.comp"
comps_fn   = "contigs.4mers"
SCp_fn     = "contigs.SCp"
covs_fn    = "contigs.normcovs"

fasta   = "polished_assembly.renamed.fasta"

subcontigs_size = 50000

def chunks( l, n ):
	"""
	Yield successive n-sized chunks from l.
	"""
	for i in xrange(0, len(l), n):
		yield l[i:i+n]

names      = np.atleast_1d(np.loadtxt(names_fn,     dtype="str"))
labels     = np.atleast_1d(np.loadtxt(labels_fn,    dtype="str"))
lengths    = np.atleast_1d(np.loadtxt(lengths_fn,   dtype="int"))
SCp        = np.atleast_1d(np.loadtxt(SCp_fn,       dtype="float"))
covs       = np.atleast_1d(np.loadtxt(covs_fn,      dtype="float"))
comp       = np.atleast_1d(np.loadtxt(comps_fn,     dtype="float"))

n_contigs  = len(names)

f_comp    = open("digested_"+comps_fn,   "wb")
f_SCp     = open("digested_"+SCp_fn,     "wb")
f_names   = open("digested_"+names_fn,   "wb")
f_labels  = open("digested_"+labels_fn,  "wb")
f_covs    = open("digested_"+covs_fn,    "wb")
f_lengths = open("digested_"+lengths_fn, "wb")

for i,length in enumerate(lengths):
	if length>subcontigs_size:
		name            = names[i]
		label           = labels[i]
		cov             = covs[i]

		# Copy the original contig SCp and composition vectors; apply to fragments
		contig_SCp_str    = "\t".join(map(lambda x: str(x), SCp[i,:]))
		contig_comp_str   = "\t".join(map(lambda x: str(x), comp[i,:]))
		contig_covs_str   = "\t".join(map(lambda x: str(x), covs[i,:]))

		for Seq in SeqIO.parse(fasta, "fasta"):

			if Seq.id == name:

				seq_chunks = list(chunks(Seq.seq, subcontigs_size))
				# Omit the chunk containing the remainder sequence
				seq_chunks = seq_chunks[:-1]
				for seq_chunk in seq_chunks:
					f_comp.write(   "%s\n" %  contig_comp_str)
					f_SCp.write(    "%s\n" %  contig_SCp_str)
					f_covs.write(   "%s\n" %  contig_covs_str)
					f_names.write(  "%s\n" %  name)
					f_labels.write( "%s\n" %  label)
					f_lengths.write("%s\n" %  len(seq_chunk))

f_comp.close()
f_SCp.close()
f_names.close()
f_labels.close()
f_covs.close()
f_lengths.close()

def append_file( orig, new ):
	cat_CMD = "cat %s %s > tmp.appended" % (orig, new)
	os.system( cat_CMD )
	os.rename("tmp.appended", orig+".wdigest")
	os.remove(new)

append_file( comps_fn,   "digested_"+comps_fn )
append_file( SCp_fn,     "digested_"+SCp_fn )
append_file( names_fn,   "digested_"+names_fn )
append_file( labels_fn,  "digested_"+labels_fn )
append_file( covs_fn,    "digested_"+covs_fn )
append_file( lengths_fn, "digested_"+lengths_fn )

# def drop_appended_lines( fn, n_to_keep ):
# 	head_CMD = "head -%s %s > tmp.head" % (n_to_keep, fn)
# 	os.system( head_CMD )
# 	os.rename("tmp.head", fn)

# drop_appended_lines( comps_fn,   n_contigs )
# drop_appended_lines( SCp_fn,     n_contigs )
# drop_appended_lines( names_fn,   n_contigs )
# drop_appended_lines( labels_fn,  n_contigs )
# drop_appended_lines( covs_fn,    n_contigs )
# drop_appended_lines( lengths_fn, n_contigs )
