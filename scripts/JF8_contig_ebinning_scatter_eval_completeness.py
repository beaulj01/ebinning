import os,sys
import numpy as np
import subprocess
from collections import defaultdict,Counter
from Bio import SeqIO

def run_OS_command( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts != 0:
		print "Failed command: %s" % CMD
	return sts, stdOutErr

specs = ["B_caccae", \
		 "B_ovatus", \
		 "B_theta", \
		 "B_vulgatus", \
		 "C_aerofaciens", \
		 "C_bolteae", \
		 "E_coli", \
		 "R_gnavus"]

ref_lens = {"B_caccae": 4564814, \
			"B_ovatus": 6472489, \
			"B_theta": 6293399, \
			"B_vulgatus": 5163189, \
			"C_aerofaciens": 2439869, \
			"C_bolteae": 6557988, \
			"E_coli": 4641652, \
			"R_gnavus": 3501911, \
			}

for spec in specs:
	delta         = "%s.delta" % spec
	coords        = "%s.coords" % spec
	CMD           = "show-coords -I 99 -c %s > %s" % (delta, coords)
	sts,stdOutErr = run_OS_command(CMD)

	right_bases     = defaultdict(set)
	n_right_bases   = Counter()
	
	wrong_bases     = 0
	for i,line in enumerate(open(coords).xreadlines()):
		if i<5:
			continue
		line = line.strip()
		vals = line.split()
		
		ref_contig  = vals[14]
		contig      = vals[15]
		contig_spec = contig.split(".")[1]
		identity    = float(vals[9])

		if contig_spec==spec and identity>99:
			start     = min(int(vals[0]), int(vals[1]))
			stop      = max(int(vals[0]), int(vals[1]))
			for base in range(start, stop):
				right_bases[ref_contig].add(base)
			n_right_bases[ref_contig] = len(right_bases[ref_contig])
			# if spec=="C_aerofaciens" and contig=="unitig_414.C_aerofaciens":
			# 	print contig, ref_contig, n_right_bases[ref_contig]
		
	for contig in SeqIO.parse("contigs.box.%s.fasta" % spec, "fasta"):
		contig_spec = contig.id.split(".")[1]

		if contig_spec!=spec:
			contig_len  = len(contig.seq)
			wrong_bases+=contig_len
			# print spec, contig.id, contig_len

	print spec, sum(n_right_bases.values())/float(ref_lens[spec])*100, wrong_bases/float(ref_lens[spec])*100