import os,sys
import subprocess
import glob
import gzip

N_samples = int(sys.argv[1])
pair      = int(sys.argv[2])
fns       = glob.glob("ERR*_%s.fastq.gz" % pair)

def run_OS_command( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	return sts, stdOutErr

to_cat = []
to_rm  = []
fns    = fns[:(N_samples)]
for i,fn in enumerate(fns):
	print (i+1),fn
	
	gunzipped_fn = fn.replace(".gz", "")
	if os.path.exists(gunzipped_fn) and os.path.getsize(gunzipped_fn)>2000000000:
		# already gunzipped
		pass
	else:
		fu           = open(gunzipped_fn, "wb")
		f            = gzip.open(fn, 'rb')
		for line in f:
			fu.write(line)
		fu.close()
		f.close()
	
	to_cat.append(gunzipped_fn)

catted_fn     = "combined_N%s_%s.fastq" % (N_samples,pair)
cat_CMD       = "cat %s > %s" % (" ".join(to_cat), catted_fn)
print cat_CMD
sts,stdOutErr = run_OS_command(cat_CMD)

# for fn in to_rm:
# 	os.remove(gunzipped_fn)