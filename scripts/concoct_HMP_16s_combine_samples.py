import os,sys
from itertools import groupby
import glob

def fasta_iter( fasta_name ):
	fh = open(fasta_name)
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		header = header.next()[1:].strip()
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

fasta_fns = glob.glob("SRS0*.fsa")

combined_fn = "hmp_16s_64_samples.fsa"
f           = open(combined_fn, "wb")
for fn in fasta_fns:
	sample = fn.split(".")[0]
	for name,seq in fasta_iter(fn):
		new_name = "%s %s" % (sample, name)
		f.write(">%s\n" % new_name)
		f.write("%s\n" % seq)
f.close()