import os,sys
import numpy as np
from Bio import SeqIO

gbk_dir = "/hpc/users/beaulj01/projects/ebinning/mouse_abx/JM3/0/10kb/contig_binning/rerun_motif_discov/debug/box_assemblies/RAST_gbks"
blast   = np.loadtxt("/hpc/users/beaulj01/projects/ebinning/mouse_abx/JM3/0/10kb/contig_binning/rerun_motif_discov/debug/box_assemblies/blast/all_vs_all_boxes.out", dtype="str", delimiter="\t")

# BLAST columns
qseqid   = 0
sseqid   = 1
pident   = 2
length   = 3
qlen     = 4
qstart   = 5
qend     = 6
slen     = 7
sstart   = 8
send     = 9
mismatch = 10
gapopen  = 11
evalue   = 12
bitscore = 13

def retrieve_hit(blast, q_box_id, q_contig, start, end):
	min_acc = 99.5

	target_str = "box%s_%s" % (q_box_id, q_contig)
	contig_idx = blast[:,0]==target_str
	hits       = blast[contig_idx,:]

	hit_contigs = set()
	for i in range(hits.shape[0]):
		if hits[i,1]!=target_str:
			if float(hits[i,2])>=min_acc:
				if int(hits[i,5])<=start and int(hits[i,6])>=end:
					hit_str = "%s_%sbp" % (hits[i,1], hits[i,7])
					hit_contigs.add(hit_str)
	return hits[0,4], hit_contigs

for box_id in range(1,10):
	gbk_fn = "box%s_gt100kb.gbk" % box_id
	for record in SeqIO.parse(open(os.path.join(gbk_dir, gbk_fn),"r"), "genbank"):
		record_contig = record.annotations["accessions"][0].split("|")[0]
		
		# if not (box_id==7 and record_contig=="unitig_85"):
		# 	continue

		for feature in record.features:
			if feature.type=="CDS":
				product = feature.qualifiers["product"][0]
				
				# if product.find("hypothetical protein")>-1:
				# 	continue
				
				start   = feature.location.nofuzzy_start
				end     = feature.location.nofuzzy_end
				
				q_len,hit_contigs = retrieve_hit(blast, box_id, record_contig, start, end)

				print "box%s\t%s\t%s\t%s\t%s\t%s\t%s" % (box_id, record_contig, q_len, start, end, product[:50], "\t".join(list(hit_contigs)))
