import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os,sys
import math
from collections import Counter

baseline_readlist_fn  = sys.argv[1]

genome_sizes = {"J99" :   1698366, \
		   		"26695" : 1590068}

target_bases          = {}
target_bases["J99"]   = genome_sizes["J99"]*150
target_bases["26695"] = {}
fs                    = {}
for desired_cov in [30, 50, 100, 150]:
	target_bases["26695"][desired_cov] = genome_sizes["26695"]*desired_cov
	
	fn              = "J99_150x_26695_%sx_read_info.txt" % desired_cov
	fs[desired_cov] = open(fn, "w")
	
	base_counter    = Counter()
	for i,line in enumerate(open(baseline_readlist_fn, "r").xreadlines()):
		s     =     line.split("\t")[1]
		bases = int(line.split("\t")[2])
		key = "%s_%s" % (s,desired_cov)
		if s=="J99":
			if base_counter[s]<target_bases[s]:
				base_counter[s] += bases
				fs[desired_cov].write(line)
		elif s=="26695":
			if base_counter[key]<target_bases[s][desired_cov]:
				base_counter[key] += bases
				fs[desired_cov].write(line)
	fs[desired_cov].close()