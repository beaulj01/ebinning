import os,sys
import numpy as np
from itertools import groupby

ebarcodes_fn = sys.argv[1]

motifs = set()
for line in open(ebarcodes_fn).xreadlines():
	motif = line.split("\t")[2]
	motifs.add(motif)
motifs = list(motifs)

# m131221_204519_42177R_c100605712550000001823114606171452_s1_p0/48497    ACCACC-3        4       1.30155483388
# m131221_204519_42177R_c100605712550000001823114606171452_s1_p0/42820    ACCACC-3        13      -0.814689303278
# m131221_204519_42177R_c100605712550000001823114606171452_s1_p0/12828    ACCACC-3        9       0.436177833513
# m131221_204519_42177R_c100605712550000001823114606171452_s1_p0/19863    ACCACC-3        14      1.47067611614
# m131221_204519_42177R_c100605712550000001823114606171452_s1_p0/31779    ACCACC-3        7       1.47904597799

read_iter = [ (line.split("\t")[0], line.strip().split("\t")[1:]) for line in open(ebarcodes_fn).xreadlines()]

header = "label"
for motif in motifs:
	header += "\t%s" % motif
print header

for read,group in groupby(read_iter, lambda x: x[0]):
	motif_SMp = {}
	for motif_entry in group:
		vals   = motif_entry[1]
		label  =       vals[0]
		motif  =       vals[1]
		count  =   int(vals[2])
		SMp    = float(vals[3])
		if motif in motifs:
			motif_SMp[motif] = SMp
	entry    = label
	has_vals = False
	for motif in motifs:
		try:
			entry += "\t%s" % motif_SMp[motif]
			has_vals = True
		except KeyError:
			# pass
			entry += "\t%s" % 0
	if has_vals:
		print entry
