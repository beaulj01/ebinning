import os,sys
from itertools import groupby

def fasta_iter( fasta_name ):
	"""
	Given a fasta file, yield tuples of (header, sequence).
	"""
	fh = open(fasta_name)
	# ditch the boolean (x[0]) and just keep the header or sequence since
	# we know they alternate.
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		# drop the ">"
		header = header.next()[1:].strip()
		# join all sequence lines to one.
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

assembly_fn = sys.argv[1]
PID         = sys.argv[2]

cov_csv_fn = "/hpc/users/beaulj01/projects/smrtportal_jobs_021/0%s/results/polished_coverage_vs_quality.csv" % PID
cov_dict   = {}
qv_dict    = {}
for i,line in enumerate(open(cov_csv_fn, "r").xreadlines()):
	if i==0: continue
	line   = line.strip()
	contig = line.split(",")[0]
	cov    = float(line.split(",")[1])
	qv     = float(line.split(",")[2])
	cov_dict[contig] = cov
	qv_dict[contig]  = qv

output = []
for i,(name,contig_seq) in enumerate(fasta_iter(assembly_fn)):
	contig     = name.split("|")[0]
	contig_len = len(contig_seq)
	output.append( (contig, contig_len, cov_dict[contig], qv_dict[contig]) )	

output_s = sorted(output, key=lambda x: x[1])
for entry in output_s:
	print "%s\t%s\t%.1f\t%.1f" % (entry[0], entry[1], entry[2], entry[3])
