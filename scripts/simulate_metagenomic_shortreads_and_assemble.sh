#!/bin/bash
module use /hpc/packages/minerva-mothra/modulefiles
module unload smrtanalysis
module load python samtools mpc

# Need to start in a directory with a directory called 'refs' that contains the reference
# fasta files that you'd like to use to simulate the short reads. The filenames should
# descriptive, since these will be propogated to all the simulated reads.

########################################################################
# Create versions of each reference that only contain the main chromosome
########################################################################
basedir=`pwd`
# echo "Preprocessing reference fastas; rename and remove plasmid entries"
ref_dir="refs"
ref1="C227.fasta"
ref1_abund=0.25
ref2="J109.fasta"
ref2_abund=0.75
# cd $ref_dir
# module load python
# python ~/gitRepo/eBinning/scripts/preprocess_assemblies_for_GemSim.py
ref_combo="combined.fasta"
# cat $ref1 $ref2 > $ref_combo
# cd ../
# echo "Done."


########################################################################
# Simulate short reads from these new 1-contig references
########################################################################
short_dir="short_reads"
# mkdir $short_dir
cd $short_dir
# abund_fn="abundance.txt"
# echo "Writing ${abund_fn}..."
# echo -e "$ref1\t$ref1_abund" >  $abund_fn
# echo -e "$ref2\t$ref2_abund" >> $abund_fn
# echo "Done."
# Nreads=10000000
# r_len=100
ins_size=250
# ln -s ~/projects/software/GemSIM_v1.6
ln -s ../refs
# GemSIM_v1.6/GemReads.py -R $ref_dir -a $abund_fn -n $Nreads -l $r_len -m GemSIM_v1.6/models/ill100v5_p.gzip -c -q 64 -o metashort -p -s 30 -u $ins_size
# # echo "GemSIM_v1.6/GemReads.py -R $ref_dir -a $abund_fn -n $Nreads -l $r_len -m GemSIM_v1.6/models/ill100v5_p.gzip -c -q 64 -o metashort -p -s 30 -u $ins_size" > run.bsub
# # bsub -m mothra -P acc_fangg03a -J sim -oo myjob.log -eo myjob${i}.err -q alloc -W 12:00 -u john.beaulaurier@gmail.com -M 1000000 < run.bsub


########################################################################
# Do assembly of metagenomic short reads
########################################################################
R1="metashort_fir.fastq"
R2="metashort_sec.fastq"
# python ~/gitRepo/eBinning/scripts/rename_GemSim_reads.py $R1 R1 > ${R1}.tmp
# python ~/gitRepo/eBinning/scripts/rename_GemSim_reads.py $R2 R2 > ${R2}.tmp
# mv ${R1}.tmp $R1
# mv ${R2}.tmp $R2

mkdir velvet
cd velvet
ln -s ../$R1
ln -s ../$R2
~/projects/software/velvet_1.2.10/velveth velveth_k31 31 -fastq -shortPaired -separate $R1 $R2
~/projects/software/velvet_1.2.10/velvetg velveth_k31 -ins_length $ins_size -exp_cov auto -cov_cutoff auto -min_contig_lgth 100
~/projects/software/MetaVelvet-1.2.02/meta-velvetg velveth_k31/ -ins_length $ins_size | tee metavelvet.out