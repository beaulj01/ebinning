import os,sys
from itertools import groupby
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt

bed_mat = np.loadtxt(sys.argv[1], dtype="str")
contigs = bed_mat[:,0]
cov     = np.array(map(lambda x: int(x), bed_mat[:,2]))
covs    = []
lengths = []
for contig in set(bed_mat[:,0]):
	idx      = contigs==contig
	length   = len(cov[idx])
	mean_cov = cov[idx].mean()
	print contig, length, mean_cov
	lengths.append(length)
	covs.append(mean_cov)

fig       = plt.figure(figsize=[8,8])
ax        = fig.add_subplot(111)
ax.scatter(lengths, covs, edgecolors="k", facecolors="none", alpha=0.7)
ax.set_xlabel("Contig length")
ax.set_ylabel("Mean coverage")
plot_fn   = "contig_len_vs_cov.png"
plt.savefig(plot_fn)
