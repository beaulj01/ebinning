import os,sys
import numpy as np
from itertools import groupby

def fasta_iter( fasta_name ):
	"""
	Given a fasta file, yield tuples of (header, sequence).
	"""
	fh = open(fasta_name)
	# ditch the boolean (x[0]) and just keep the header or sequence since
	# we know they alternate.
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		# drop the ">"
		header = header.next()[1:].strip()
		# join all sequence lines to one.
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

fasta_fn  = sys.argv[1]
seq_names = set(np.loadtxt(sys.argv[2], dtype="str"))

for header,seq in fasta_iter(fasta_fn):
	if header.replace("|quiver","") in seq_names:
		print ">%s" % header
		print "%s"  % seq