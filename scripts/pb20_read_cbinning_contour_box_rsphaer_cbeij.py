import sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import random
import pub_figs
from collections import Counter
from pbcore.io.BasH5IO import BasH5Reader
import textwrap

font_s = 18
pub_figs.setup_math_fonts(font_size=font_s, font="Computer Modern Sans serif")

results = np.loadtxt("both.comp.2D", dtype="float")
names   = np.loadtxt("both.names",   dtype="str")
title   = "combined cBinning"

fig     = plt.figure(figsize=[13,8])
ax      = fig.add_axes([0.1, 0.1, 0.8, 0.8])
# ax.axis("off")
pub_figs.remove_top_right_axes( ax )
extent            = [-41,38,-36,40]
bins              = 50
H, xedges, yedges = np.histogram2d(results[:,0], results[:,1], bins=bins, range=[extent[:2], extent[2:]])
H = np.rot90(H)
H = np.flipud(H)
Hmasked = np.ma.masked_where(H==0,H)         # Mask pixels with a value of zero
plt.hot()                                    # set 'hot' as default colour map
im = plt.imshow(H, interpolation='bilinear', # creates background image
                origin='lower', cmap=cm.Greys, 
                extent=extent)
# plt.subplots_adjust(bottom=0.15, left=0.15)
levels            = [40,30,25,20]
cols              = ["k","0.4","0.6","0.8"]
cset              = plt.contour(H, levels[::-1], origin="lower",colors=cols[::-1],linewidths=1.4,extent=extent)
for c in cset.collections:
	c.set_linestyle("solid")
minx = min(results[:,0])
maxx = max(results[:,0])
miny = min(results[:,1])
maxy = max(results[:,1])
ax.tick_params(pad=10, labelsize=font_s)


# box_dims = [-9.5, -6.5, -20.5, -23.5]
# box_xmin = box_dims[0]
# box_xmax = box_dims[1]
# box_ymin = box_dims[2]
# box_ymax = box_dims[3]
# ax.plot([box_xmin,box_xmax,box_xmax,box_xmin,box_xmin], [box_ymax,box_ymax,box_ymin,box_ymin,box_ymax], linestyle="--", color="k", linewidth=2, dashes=(3,2))

# box_dims = [-6.0, 3, -21.5, -15.0]
# box_xmin = box_dims[0]
# box_xmax = box_dims[1]
# box_ymin = box_dims[2]
# box_ymax = box_dims[3]
# ax.plot([box_xmin,box_xmax,box_xmax,box_xmin,box_xmin], [box_ymax,box_ymax,box_ymin,box_ymin,box_ymax], linestyle="--", color="k", linewidth=2, dashes=(3,2))
# ax.set_xlim([min(minx-1, box_xmin-1), max(maxx+1, box_xmax+1)])
# ax.set_ylim([min(miny-1, box_ymin-1), max(maxy+1, box_ymax+1)])
plt.savefig("pub.contours.reads.comp.png", dpi=300)