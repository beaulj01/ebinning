import os,sys
from Bio import SeqIO
import glob

fn = sys.argv[1]


with open(fn.replace(".fasta", ".labeled.fasta"), "w") as output_handle:
	for j,entry in enumerate(SeqIO.parse(open(fn,"r"), "fasta")):
		entry.id          = entry.id.split("|")[0]
		entry.description = entry.id.split("|")[0]
		SeqIO.write(entry, output_handle, "fasta")