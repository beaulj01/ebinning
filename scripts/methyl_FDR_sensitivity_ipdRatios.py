import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib
from matplotlib import pyplot as plt
import pickle
import bisect
import numpy as np
from collections import defaultdict, OrderedDict, Counter
import math
import operator
import pub_figs

class PowerCurves:
	def __init__( self, bas_exps, bas_controls, cmp_exps, cmp_controls, FDR_thresh_methyl, logger ):
		self.bas_exps          = bas_exps
		self.bas_controls      = bas_controls
		self.cmp_exps          = cmp_exps
		self.cmp_controls      = cmp_controls
		self.FDR_thresh_methyl = FDR_thresh_methyl
		self.logger            = logger

	def run( self, plot=True ):
		cov         = 5
		hit_counter = Counter()
		
		results = defaultdict(list)
		stats_to_return = {}
		for mode in ["unaligned","aligned"]:
			if mode=="unaligned":
				control_pass_cov = [control for control in self.bas_controls if control[1]>=cov]
				exp_pass_cov     = [exp     for exp     in self.bas_exps     if exp[1]>=cov]
			else:
				control_pass_cov = [control for control in self.cmp_controls if control[1]>=cov]
				exp_pass_cov     = [exp     for exp     in self.cmp_exps     if exp[1]>=cov]


			# Get native scores
			controls_vals   = [entry[2] for entry in control_pass_cov]
			exps_vals       = [entry[2] for entry in exp_pass_cov]

			controls_sorted = sorted(controls_vals)
			exps_sorted     = sorted(exps_vals)
			FDRs = []
			already_printed = False
			for i,val in enumerate(exps_sorted):
				hit_counter[mode] += 1

				# Find number of p-values larger than this value
				N_called_exps = len(exps_sorted) - i
				N_exps        = len(exps_sorted)
				exp_frac      = float(N_called_exps) / N_exps

				# Find closest entry in sorted control p-value list
				N_called_controls = len(controls_sorted) - bisect.bisect_left(controls_sorted, val)
				N_controls        = len(controls_sorted)
				FPR               = float(N_called_controls) / N_controls

				FDR  = FPR / exp_frac
				FDRs.append(FDR)
				sens = float(N_called_exps) / len(exp_pass_cov)
				if FDR < self.FDR_thresh_methyl and i%100==0:			
					self.logger.debug("methyl: cov>=%s, i=%s, val=%s, FDR=%.4f, sensitivity=%.4f, FPR=%.4f, N_called_control=%s, exp_frac=%.4f, N_called_exps=%s" % (cov, \
																														   i, \
																														   val, \
																														   FDR, \
																														   sens, \
																														   FPR, \
																														   N_called_controls, \
																														   exp_frac, \
																														   N_called_exps))
				results[mode].append( (FPR, sens, val, FDR) )
				if FDR < self.FDR_thresh_methyl and not already_printed:
					stats = "methyl: mode=%s item/sorted_total=%s/%s FDR=%.4f pct_called=%.4f ipdRatio>=%s" % (mode, hit_counter[mode], len(exps_sorted), FDR, sens, val)
					self.logger.info(stats)
					string = "%s/%s" % (hit_counter[mode], len(exps_sorted))
					stats_to_return[mode] = {"mode":mode,  \
											 "item/sorted_total":string, \
											 "FDR":FDR, \
											 "pct_called":sens, \
											 "ipdRatio":val}
					already_printed       = True
				
		if plot:
			self.plot( results, hit_counter, cov )
		return stats_to_return

	def plot( self, results, hit_counter, cov ):
		font_size = 50
		adjust    = 0.15

		fontProperties = pub_figs.setup_math_fonts(font_size)

		fig = pub_figs.init_fig(x=15,y=15)

		ax       = fig.add_subplot(111)
		sq_axes  = pub_figs.make_square_axes( ax.axis() )
		ax.axis(sq_axes) 
		colors   = ["b", "r", "k", "c"]
		ordered_results = sorted(results.iteritems(), key=operator.itemgetter(0))
		for j,(mode, vals) in enumerate(ordered_results):
			X   = map(lambda x: x[0], vals)
			Y   = map(lambda x: x[1], vals)
			# gte = u"\u2265"
			lab = mode
			ax.plot(X, Y, color=colors[j], label=lab, linewidth=4)
		ax.set_xlim([0, 1])
		ax.set_ylim([0, 1])
		tit = "eBarcode mode (num sites > %s)" % cov
		legend = plt.legend(loc=3, title=tit, fontsize=font_size, labelspacing=0.1, borderaxespad=0.03)
		legend.draw_frame(False)
		locs, labels = plt.xticks()
		# plt.setp(labels, rotation="vertical")
		ax.set_xlabel("False positive rate")
		ax.set_ylabel("Sensitivity")
		ax.get_xaxis().set_tick_params(length=20)
		ax.get_yaxis().set_tick_params(length=20)
		ax.tick_params(axis='x', pad=15)
		ax.tick_params(axis='y', pad=15)
		pub_figs.change_font_size(ax, font_size=font_size, bottom_adjust=adjust, left_adjust=adjust*1.3)
		pub_figs.remove_top_right_axes(ax)
		fn = "FPR-sensitivity_methyl_cov%s" % cov
		if os.path.exists(fn+".png"): os.remove(fn+".png")
		plt.savefig(fn)

		# fig      = pub_figs.init_fig()
		# ax       = fig.add_subplot(111)
		# sq_axes  = pub_figs.make_square_axes( ax.axis() )
		# ax.axis(sq_axes) 
		# colors   = ["b", "r", "k", "c"]
		# ordered_results = sorted(results.iteritems(), key=operator.itemgetter(0))
		# for j,(cov, vals) in enumerate(ordered_results):
		# 	X   = map(lambda x: x[0], vals)
		# 	Y   = map(lambda x: x[1], vals)
		# 	ax.plot(X, Y, color=colors[j], label=lab, linewidth=5)
		# ax.set_xlim([0, 0.05])
		# ax.set_ylim([0.9, 1])
		# locs, labels = plt.xticks()
		# plt.setp(labels, rotation="vertical")
		# pub_figs.change_font_size(ax, font_size=font_size*1.1, bottom_adjust=adjust*2, left_adjust=adjust*2)
		# pub_figs.remove_top_right_axes(ax)
		# ax.get_xaxis().set_tick_params(length=20)
		# ax.get_yaxis().set_tick_params(length=20)
		# ax.tick_params(axis='x', pad=15)
		# ax.tick_params(axis='y', pad=15)
		# ax.xaxis.grid(color='gray', linestyle='dashed')
		# ax.yaxis.grid(color='gray', linestyle='dashed')
		# ax.set_axisbelow(True)
		# fn = "FPR-sensitivity_methyl_zoom"
		# if os.path.exists(fn+".png"): os.remove(fn+".png")
		# plt.savefig(fn)

if __name__ == "__main__":
	app = PowerCurves( bas_exps, bas_controls, cmp_exps, cmp_controls, FDR_thresh_methyl, logger )
	sys.exit( app.run() )