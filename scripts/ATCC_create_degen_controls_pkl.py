import os,sys
import pickle
import numpy as np
import re

controls_fn = "/hpc/users/beaulj01/projects/ebinning/concoct_integration/bigsix_atcc/10kb_bas_binning/2215_and_2440/k12_align/cmp_binning/control_4567_20K_wgaCdiff/tmp/control_means.pkl"
motifs      = np.loadtxt("motifs.txt", dtype="str")

def sub_bases( motif ):
	"""
	Return all possible specifications of a motif with degenerate bases.
	"""
	subs = {"W":"[AT]",  \
			"S":"[CG]",  \
			"M":"[AC]",  \
			"K":"[GT]",  \
			"R":"[AG]",  \
			"Y":"[CT]",  \
			"B":"[CGT]", \
			"D":"[AGT]", \
			"H":"[ACT]", \
			"V":"[ACG]", \
			"N":"[ACGT]"}
	for symbol,sub in subs.iteritems():
		if motif.find(symbol) > -1:
			motif = motif.replace(symbol, sub)
	return motif

controls    = pickle.load(open(controls_fn, "r"))
spec_motifs = ",".join(controls.keys())
new_control = {}
for motif in motifs:
	motif_re = sub_bases(motif)
	
	matches_iter = re.finditer(","+motif_re+",", spec_motifs)
	matches_list = []
	spec_vals    = []
	for match in matches_iter:
		spec_motif = match.group().strip(",")
		spec_vals.append(controls[spec_motif])

		print motif, spec_motif, controls[spec_motif]
	
	new_control[motif] = np.mean(spec_vals)

fn = controls_fn+".wdegen"
pickle.dump(new_control, open(fn, "wb"))