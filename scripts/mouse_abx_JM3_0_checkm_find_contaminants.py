import os,sys
import numpy as np
import glob
from Bio import SeqIO

gene_map = np.loadtxt("/hpc/users/beaulj01/projects/ebinning/mouse_abx/JM3/0/10kb/contig_binning/rerun_motif_discov/debug/box_assemblies/checkm/marker_gene.contig.map", dtype="str", skiprows=1, delimiter="\t")
fastas   = glob.glob("boxes/box*")

for fn in fastas:
	box            = os.path.basename(fn).split(".")[0]
	box_map        = gene_map[gene_map[:,0]==box]
	mapped_contigs = set()
	
	for mapping in box_map[:,2]:
		hits = mapping.split("&&")
		for hit in hits:
			contig = "_".join(hit.split("_")[:2])
			mapped_contigs.add(contig)
	
	tot_bases    = 0
	contam_bases = 0
	for seq_record in SeqIO.parse(fn, "fasta"):
		tot_bases += len(seq_record.seq)
		if seq_record.id not in mapped_contigs:
			contam_bases += len(seq_record.seq)
			print box, seq_record.id, len(seq_record.seq)

	print "BOX: %s" % box
	print "Contaminant bases pct: %.2f" % (100*float(contam_bases) / tot_bases)
	print ""