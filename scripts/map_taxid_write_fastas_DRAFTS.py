import os,sys
import urllib2
from Bio import SeqIO
import glob
from itertools import groupby
import re
import subprocess


# 13465 Mycobacterium_tuberculosis_M991_uid235624

def run_OS( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	return sts, stdOutErr

def del_fasta():
	to_rm  = glob.glob("*.fna")
	for bad in to_rm:
		os.remove(bad)

org_dirs = glob.glob("*")
orig     = os.getcwd()

dest_dir = "0_final_fasta"
os.chdir(dest_dir)
finished = set(map(lambda x: x.split(".taxid")[0], glob.glob("*.taxid.fna")))
os.chdir(orig)

for i,org_dir in enumerate(org_dirs):
	os.chdir(org_dir)
	print i, org_dir
	
	if org_dir=="0_final_fasta":
		os.chdir(orig)
		continue
	elif org_dir in finished:
		del_fasta()
		os.chdir(orig)
		continue
	
	del_fasta()
	
	gbk    = glob.glob("*.gbk")[0]
	f      = open(gbk, "r").read()
	m      = re.search(r"taxon:\w+", f)
	taxid  = m.group().split(":")[1]

	fastas_zs       = glob.glob("*contig.fna*tgz")
	for fastas_z in fastas_zs:
		tgz_CMD        = "tar -xf %s" % fastas_z
		sts, stdOutErr = run_OS(tgz_CMD)
	fastas         = glob.glob("*.fna")
	for fasta in fastas:
		if fasta.find(".taxid.")>-1:
			continue
		f = open(fasta.replace(".fna", ".taxid.fna"),"w")
		for j,seq_record in enumerate(SeqIO.parse(fasta, "fasta")):
			tax_str       = "kraken:taxid|%s|" % taxid
			seq_record.id = tax_str + seq_record.id
			f.write(seq_record.format("fasta"))
		f.close()

	# Only take the first 500 contig fasta entries (some can have tens of thousands)
	to_cat  = " ".join(glob.glob("*.taxid.fna")[:500])
	out     = os.path.join("..", "0_final_fasta", org_dir+".taxid.fna")
	cat_CMD = "cat %s > %s" % (to_cat, out)
	sts, stdOutErr = run_OS(cat_CMD)
	
	del_fasta()
	
	os.chdir(orig)
