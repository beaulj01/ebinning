import os,sys
from itertools import groupby

fn   =     sys.argv[1]
size = int(sys.argv[2])

def fasta_iter( fasta_name, max_s ):
		"""
		Given a fasta file, yield tuples of (header, sequence).
		"""
		fh = open(fasta_name)
		# ditch the boolean (x[0]) and just keep the header or sequence since
		# we know they alternate.
		faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
		for header in faiter:
			# drop the ">"
			header = header.next()[1:].strip()
			# join all sequence lines to one.
			seq = "".join(s.strip() for s in faiter.next())
			
			if len(seq) <= max_s:
				yield header, seq

for name,seq in fasta_iter(fn, size):
	print ">%s" % name
	print "%s" % seq