import os,sys
import glob
from collections import Counter
import shutil

def remove_plasmid_contigs( fn, orig_refs ):
	sizes  = Counter()
	contig = 0
	for line in open(fn).xreadlines():
		line = line.strip()
		if line[0]==">":
			contig += 1
		else:
			sizes[contig] += len(line)
	max_size = max(sizes.values())
	if max_size < 1000000:
		print "WARNING -- %s: largest contig only %sbp!" % (fn, max_size)

	shutil.copy(fn, orig_refs)

	new_fn = fn+".tmp"
	f      = open(new_fn, "w")
	contig = 0
	for line in open(fn).xreadlines():
		if line[0]==">":
			contig += 1
			if sizes[contig] == max_size:
				# This is the biggest contig
				f.write(line)
		elif sizes[contig] == max_size:
			f.write(line)
	f.close()
	os.rename(new_fn, fn)

def rename_contigs( fn ):
	new_fn = fn+".tmp"
	f      = open(new_fn, "w")
	contig = 0
	for line in open(fn).xreadlines():
		spec = fn.strip(".fasta")
		if line[0]==">":
			f.write(">%s_contig_%s\n" % (spec, contig))
			contig += 1
		else:
			f.write(line)
	f.close()
	os.rename(new_fn, fn)

fns = glob.glob("*.fasta")
print "Found:"
for fn in fns:
	print "  %s" % fn

orig_refs = "orig_refs"
if os.path.exists(orig_refs): shutil.rmtree(orig_refs)
os.mkdir(orig_refs)

for fn in fns:
	shutil.copy(fn, orig_refs)
	# remove_plasmid_contigs( fn, orig_refs )
	rename_contigs( fn )

