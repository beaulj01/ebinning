import os,sys
from collections import Counter
import numpy as np
from Bio import SeqIO
import operator

min_ident = 97.0
min_len   = 100
topn      = 3

min_contig_size = 100000

box_bps   = Counter()
hit_bps   = Counter()
hit_descs = {}
for box_id in range(1,10):
	
	# First get total bases in each bin
	box_fna = "box%s.fasta" % box_id
	for Seq in SeqIO.parse(box_fna, "fasta"):
		if len(Seq.seq)>=min_contig_size:
			box_bps[box_id] += len(Seq.seq)

	# Count bases with hits in blastn results
	out = "box%s.out" % box_id
	box_covered = {}
	for j,line in enumerate(open(out, "rb").xreadlines()):
		line = line.strip("\n")
		vals = line.split("\t")

		if float(vals[2])>=min_ident and int(vals[3])>=min_len:

			contig_name   = vals[0]
			contig_number = int(contig_name.split("_")[1].split("|")[0])
			hit_name      = vals[1]
			hit_desc      = vals[14]
			if hit_desc.find("Bacteroidales bacterium ")>-1:
				hit_desc = " ".join(hit_desc.split("_")[0].split(" ")[1:-1])
				hit_name = hit_desc

			if hit_name[:3]=="MGS":
				hit_name = hit_name.split("_Ga")[0]
				hit_desc = hit_desc.split("_Ga")[0]


			hit_descs[hit_name] = hit_desc

			contig_size = int(vals[4])
			if contig_size>=min_contig_size:

				try:
					x = box_covered[hit_name]
				except KeyError:
					box_covered[hit_name] = np.array([])

				qstart      = int(vals[5])
				qend        = int(vals[6])
				covered     = np.arange(qstart, qend+1) + (contig_number*1000000000000)
				
				box_covered[hit_name] = np.append(box_covered[hit_name], covered)
		
		# if j==10000:break

	# Total and sort results for each bin
	covd_uniq = {}
	for hit_name,covd in box_covered.iteritems():
		covd_uniq[hit_name] = len(set(box_covered[hit_name]))

	sorted_covd_uniq = sorted(covd_uniq.items(), key=operator.itemgetter(1), reverse=True)
	sorted_covd_uniq = sorted_covd_uniq[:topn]

	print "box%s top %s hits:" % (box_id, topn)
	for (hit_name,bps) in sorted_covd_uniq:
		print "    %.2f%%\t%s" % (100*float(bps)/box_bps[box_id], hit_descs[hit_name])
	print ""