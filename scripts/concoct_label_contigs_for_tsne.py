import os,sys
import numpy as np

contig_labels = "clustering_gt1000_s.csv"
contig_names  = "contigs.names"

labels = np.loadtxt(contig_labels, dtype="str", delimiter=",")
labels = dict(labels[:,:2])
print labels
f = open("contigs.spec_labels", "wb")
for name in open(contig_names, "rb").xreadlines():
	name = name.strip()
	try:
		lab = labels[name]
	except KeyError:
		lab = "Unlabeled"
	f.write("%s\n" % lab.replace(" ","_"))
f.close()