import os,sys

pent_only_dir = sys.argv[1]
SMp_dir       = sys.argv[2]

orig_dir = os.getcwd()

readnames_fn = "binning.readnames"
barcode_fn   = "tSNE.out.zscores"
labels_fn    = "binning.labels"

os.chdir(pent_only_dir)
pent_readnames = []
for i,line in enumerate(open(readnames_fn, "r").xreadlines()):
	read = line.strip("\n")
	pent_readnames.append(read)

pent_barcodes = {}
for j,line in enumerate(open(barcode_fn, "r").xreadlines()):
	barcode = line.strip("\n")
	pent_barcodes[pent_readnames[j]] = barcode

pent_labels = {}
for k,line in enumerate(open(labels_fn, "r").xreadlines()):
	label = line.strip("\n")
	pent_labels[pent_readnames[k]] = label
os.chdir(orig_dir)

readnames_fn = "binning.readnames"
barcode_fn   = "PCA.out.zscores"
labels_fn    = "binning.labels"

os.chdir(SMp_dir)
SMp_readnames = []
for i,line in enumerate(open(readnames_fn, "r").xreadlines()):
	read = line.strip("\n")
	SMp_readnames.append(read)

SMp_barcodes = {}
for j,line in enumerate(open(barcode_fn, "r").xreadlines()):
	barcode = line.strip("\n")
	SMp_barcodes[SMp_readnames[j]] = barcode

SMp_labels = {}
for k,line in enumerate(open(labels_fn, "r").xreadlines()):
	label = line.strip("\n")
	SMp_labels[SMp_readnames[k]] = label
os.chdir(orig_dir)

f_reads    = open("combined_binning.readnames", "w")
f_labels   = open("combined_binning.labels", "w")
f_barcodes = open("combined_binning.barcodes.zscore_4D", "w")
for read in pent_readnames:
	if read in pent_barcodes.keys() and read in SMp_barcodes.keys():
		f_reads.write("%s\n" % read)
		f_labels.write("%s\n" % pent_labels[read])
		f_barcodes.write("%s\t%s\n" % (pent_barcodes[read], SMp_barcodes[read]))
f_reads.close()
f_labels.close()
f_barcodes.close()
