import os,sys
import numpy as np
import glob

all_fn      = "read_species_mapping.txt"
if os.path.exists(all_fn): os.remove(all_fn)

orig_dir = os.getcwd()
ref_dirs = glob.glob("*")

f_all_reads = open(all_fn, "w")
for ref_dir in ref_dirs:
	print "...%s" % ref_dir
 	os.chdir(ref_dir)
	fn     = ref_dir+".reads"
	f      = open(fn, "w")
	sam_fn = "aligned_reads.sam"
	for line in open(sam_fn, "r").xreadlines():
		if line[0]=="@":
			continue
		else:
			mol   = line.split("\t")[0]
			mapQV = int(line.split("\t")[4])
			if mapQV==254:
				f.write("%s\n" % mol)
				f_all_reads.write("%s\t%s\n" % (mol, ref_dir))
	f.close()
	os.chdir(orig_dir)
f_all_reads.close()
