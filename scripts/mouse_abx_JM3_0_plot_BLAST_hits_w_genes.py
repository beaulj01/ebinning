import os,sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from itertools import izip
from collections import Counter
from matplotlib import gridspec
import re

def init_fig(x=8, y=8):
	FIG_SIZE = (x,y)
	return plt.figure(figsize=FIG_SIZE)

zoom_intervals = [ (689000,694000), \
				   (732000,755000), \
				   (1332000,1335000), \
				   (1582000,1582500), \
				   (1622000,1623000)]

# snps_JP26_fn = "1202_vs_1437/snp.out"
# snps_JP26    = []
# for i,line in enumerate(open(snps_JP26_fn, "rb").xreadlines()):
# 	if i>=5:
# 		line  = line.strip("\n")
# 		REGEX = r'\s\d+\s'
# 		m     = re.search(REGEX,line)
# 		p     = int(m.group()[1:-1])
# 		snps_JP26.append(p)

snps0_fn = "1202_vs_1215/snp.out"
snps0    = []
for i,line in enumerate(open(snps0_fn, "rb").xreadlines()):
	if i>=5:
		line  = line.strip("\n")
		REGEX = r'\s\d+\s'
		m     = re.search(REGEX,line)
		p     = int(m.group()[1:-1])
		snps0.append(p)

snps1_fn = "1202_vs_1216/snp.out"
snps1    = []
for i,line in enumerate(open(snps1_fn, "rb").xreadlines()):
	if i>=5:
		line  = line.strip("\n")
		REGEX = r'\s\d+\s'
		m     = re.search(REGEX,line)
		p     = int(m.group()[1:-1])
		snps1.append(p)

fig = init_fig(15,10)

gen_size       = 1681879
rows           = 6
cols           = 1
interval_len   = gen_size / rows
# snps_JP26      = np.array(snps_JP26)
snps0          = np.array(snps0)
snps1          = np.array(snps1)
plot_intervals = range(0, gen_size-1, interval_len)
j = 0
# for i,bottom_end in enumerate(plot_intervals):
for i,(bottom_end,top_end) in enumerate(zoom_intervals):
	# top_end = bottom_end + interval_len
	frame   = i+1
	ax      = fig.add_subplot(rows,cols,frame)
	
	# genome JP26 Cla
	# idx     = np.where((snps_JP26 >= bottom_end) & (snps_JP26 <=top_end))
	# h       = np.zeros(len(snps_JP26[idx]))+3
	# ax.scatter(snps_JP26[idx], h, s=100, edgecolor="None", alpha=0.2, color="k", label="HPXZ1437 JP26 Cla SNPs (N = %s)" % len(snps_JP26))

	# genome 0
	idx     = np.where((snps0 >= bottom_end) & (snps0 <=top_end))
	h       = np.zeros(len(snps0[idx]))+2
	ax.scatter(snps0[idx], h, s=100, edgecolor="None", alpha=0.2, color="r", label="HPXZ1215 Cla* -0 SNPs (N = %s)" % len(snps0))
	
	# genome 1
	idx     = np.where((snps1 >= bottom_end) & (snps1 <=top_end))
	h       = np.zeros(len(snps1[idx]))+1
	ax.scatter(snps1[idx], h, s=100, edgecolor="None", alpha=0.2, color="b", label="HPXZ1216 Cla* -1 SNPs (N = %s)" % len(snps1))

	ax.set_xlim([bottom_end, top_end])
	ax.set_yticks([])
	ax.set_ylim(-1,3.5)
	ax.get_xaxis().get_major_formatter().set_useOffset(False)

# 	alpha      = "ABCDEFGHIJKL"
# 	gene_names = {"A": "Inner membrane protein YihY, formerly thought to be RNase BN", \
# 				  "B": "4-diphosphocytidyl-2-C-methyl-D-erythritol kinase (EC 2.7.1.148)", \
# 				  "C": "tmRNA-binding protein SmpB", \
# 				  "D": "Ferric siderophore transport system, biopolymer transport protein ExbB", \
# 				  "E": "Biopolymer transport protein ExbD/TolR", \
# 				  "F": "LSU ribosomal protein L34p", \
# 				  "G": "Ribonuclease P protein component (EC 3.1.26.5)", \
# 				  "H": "Protein YidD", \
# 				  "I": "Inner membrane protein translocase component YidC, long form", \
# 				  "J": "secreted protein involved in flagellar motility", \
# 				  "K": "putative", \
# 				  "L": "Iron(III) dicitrate transport protein FecA"}

# 	arrow_h_f =  0.25
# 	arrow_h_r = -0.25
# 	if i==0:
# 		start = 689623
# 		end   = 690501
# 		ax.arrow(start, arrow_h_f, (end-start), 0, head_width=0.4, head_length=20, fc='k', ec='k')
# 		name  =  alpha[j]; j+=1
# 		ax.text(start+float(end-start)/2, arrow_h_f, name, fontsize=10, horizontalalignment='center', verticalalignment='bottom')
# 	elif i==1:
# 		start = 732439
# 		end   = 733245
# 		ax.arrow(start, arrow_h_f, (end-start), 0, head_width=0.4, head_length=20, fc='k', ec='k')
# 		name  =  alpha[j]; j+=1
# 		ax.text(start+float(end-start)/2, arrow_h_f+0.06, name, fontsize=10, horizontalalignment='center', verticalalignment='bottom')
# 		start = 733242
# 		end   = 733700
# 		ax.arrow(start, arrow_h_f, (end-start), 0, head_width=0.4, head_length=20, fc='k', ec='k')
# 		name  =  alpha[j]; j+=1
# 		ax.text(start+float(end-start)/2, arrow_h_f+0.06, name, fontsize=10, horizontalalignment='center', verticalalignment='bottom')
# 		start = 733703
# 		end   = 734155
# 		ax.arrow(start, arrow_h_f, (end-start), 0, head_width=0.4, head_length=20, fc='k', ec='k')
# 		name  =  alpha[j]; j+=1
# 		ax.text(start+float(end-start)/2, arrow_h_f+0.06, name, fontsize=10, horizontalalignment='center', verticalalignment='bottom')
# 		start = 734166
# 		end   = 734567
# 		ax.arrow(start, arrow_h_f, (end-start), 0, head_width=0.4, head_length=20, fc='k', ec='k')
# 		name  =  alpha[j]; j+=1
# 		ax.text(start+float(end-start)/2, arrow_h_f+0.06, name, fontsize=10, horizontalalignment='center', verticalalignment='bottom')
# 		start = 734639
# 		end   = 734773
# 		ax.arrow(start, arrow_h_f, (end-start), 0, head_width=0.4, head_length=20, fc='k', ec='k')
# 		name  =  alpha[j]; j+=1
# 		ax.text(start+float(end-start)/2, arrow_h_f+0.06, name, fontsize=10, horizontalalignment='center', verticalalignment='bottom')
# 		start = 734733
# 		end   = 735218
# 		ax.arrow(start, arrow_h_f, (end-start), 0, head_width=0.4, head_length=20, fc='k', ec='k')
# 		name  =  alpha[j]; j+=1
# 		ax.text(start+float(end-start)/2, arrow_h_f+0.06, name, fontsize=10, horizontalalignment='center', verticalalignment='bottom')
# 		start = 735205
# 		end   = 735558
# 		ax.arrow(start, arrow_h_f, (end-start), 0, head_width=0.4, head_length=20, fc='k', ec='k')
# 		name  =  alpha[j]; j+=1
# 		ax.text(start+float(end-start)/2, arrow_h_f+0.06, name, fontsize=10, horizontalalignment='center', verticalalignment='bottom')
# 		start = 735564
# 		end   = 737207
# 		ax.arrow(start, arrow_h_f, (end-start), 0, head_width=0.4, head_length=20, fc='k', ec='k')
# 		name  =  alpha[j]; j+=1
# 		ax.text(start+float(end-start)/2, arrow_h_f+0.06, name, fontsize=10, horizontalalignment='center', verticalalignment='bottom')
# 	elif i==2:
# 		start = 751581
# 		end   = 752168
# 		ax.arrow(end, arrow_h_r, (start-end), 0, head_width=0.4, head_length=20, fc='b', ec='b')
# 		name  =  alpha[j]; j+=1
# 		ax.text(start+float(end-start)/2, arrow_h_r+0.06, name, fontsize=10, horizontalalignment='center', verticalalignment='bottom')
# 		start = 752187
# 		end   = 752864
# 		ax.arrow(end, arrow_h_r, (start-end), 0, head_width=0.4, head_length=20, fc='b', ec='b')
# 		name  =  alpha[j]; j+=1
# 		ax.text(start+float(end-start)/2, arrow_h_r+0.06, name, fontsize=10, horizontalalignment='center', verticalalignment='bottom')
# 	elif i==3:
# 		start = 1620402
# 		end   = 1622705
# 		ax.arrow(end, arrow_h_r, (start-end), 0, head_width=0.4, head_length=20, fc='b', ec='b')
# 		name  =  alpha[j]; j+=1
# 		ax.text(start+float(end-start)/2, arrow_h_r+0.06, name, fontsize=10, horizontalalignment='center', verticalalignment='bottom')


# 	if frame==1:
# 		ax.legend(ncol=3, loc='lower left', bbox_to_anchor=(0.0, 1.1), frameon=False, fontsize=12, scatterpoints=1)
# 	elif frame==rows/2:
# 		ax.set_ylabel("SNP calls")
# 	elif frame==rows:
# 		ax.set_xlabel("Coordinates in HPXZ1202 (26695 original)")

# string = ""
# for i in range(len(alpha)):
# 	string += "%s : %s\n" % (alpha[i], gene_names[alpha[i]])

# ax.text(0.2, -0.5, string, fontsize=12, transform=ax.transAxes, horizontalalignment='left', verticalalignment='top')

fig.savefig("snps_positions_1202_vs_1215_1216_zoom_shared.png")