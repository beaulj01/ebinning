import os,sys
from itertools import groupby
import numpy as np
import re
from collections import Counter
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import pub_figs
import scipy.stats as stats

contig_lens    = np.loadtxt("contigs.lengths", dtype="int")
contig_motif_N = np.loadtxt("contigs.SCp_N",   dtype="int")
contig_labels  = np.loadtxt("contigs.labels",  dtype="str")
contig_motifs  = np.loadtxt("motifs.txt",      dtype="str")

cbolteae_motifs = set(["CAGGAG-4", \
		 			   "GATC-1", \
		 			   "AGATCC-2", \
		 			   "GGATCT-2", \
		 			   "AGATCT-2", \
		 			   "CATNNNNNGTG-1", \
		 			   "CAGNNNNNGGAA-1", \
		 			   "GAGC-1"])

contig_lens    = contig_lens[contig_labels=="C_bolteae"]
contig_motif_N = contig_motif_N[contig_labels=="C_bolteae"]
contig_labels  = contig_labels[contig_labels=="C_bolteae"]

interval                = 5000

range_true              = {}
range_tot               = {}
range_pcts              = {}

min_ns     = [4,2]
size_range = range(0, max(contig_lens)+1, interval)
N          = len(size_range)
ind        = np.arange(N) # the x locations for the groups
width      = 0.25          # the width of the bars

fig     = plt.figure(figsize=[15,12])
ax      = fig.add_axes([0.1, 0.2, 0.8, 0.6])
colors  = ['lightgray', 'dimgray']
for k,min_n in enumerate(min_ns):
	contigs_total           = Counter()
	contigs_with_all_motifs = Counter()
	for lower in range(0, max(contig_lens)+1, interval):
		idxs = (lower<=contig_lens) & (contig_lens<=(lower+interval))
		cbolteae_motif_idxs = []
		for j,motif in enumerate(contig_motifs):
			if motif in cbolteae_motifs:
				cbolteae_motif_idxs.append(j)
		counts = contig_motif_N[idxs,:]
		cb_counts = counts[:,cbolteae_motif_idxs]
		print cb_counts
		if len(cb_counts)>0:
			for contig_cnt in cb_counts:
				idx = contig_cnt>=min_n
				if len(contig_cnt[idx])==len(cbolteae_motifs):
					contigs_with_all_motifs[lower] += 1 
				contigs_total[lower] += 1
			pct = 100*float(contigs_with_all_motifs[lower]) / contigs_total[lower]
			range_true[lower] = contigs_with_all_motifs[lower]
			range_tot[lower]  = contigs_total[lower]
			range_pcts[lower] = pct
			print lower, (lower+interval), contigs_with_all_motifs[lower], contigs_total[lower], pct
		else:
			contigs_with_all_motifs[lower] = 0
			contigs_total[lower] = 0
			range_true[lower]    = 0
			range_tot[lower]     = 0
			range_pcts[lower]    = 0.0

	Y       = [range_true[x] for x in size_range]
	rects0  = ax.bar(ind+(k+1)*width, Y, width, color=colors[k], label='Contigs w/ >=%s IPD for each C. bolteae motif' % min_n)

Y       = [range_tot[x] for x in size_range]
rects2  = ax.bar(ind+(k+2)*width, Y, width, color='black', label='All contigs')

ax.set_xlabel('Contig size range (Kbp)')
ax.set_ylabel('# contigs')
ax.set_xticks(ind - float(width)/2)
ax.set_xticklabels(map(lambda x: x/1000, size_range), rotation=45, ha="right")
ax.set_xlim([0,N])
ax.set_ylim([0,max(range_tot.values())+2])
plt.grid()
pub_figs.remove_top_right_axes(ax)
ax.legend(prop={'size':20})
font_size = 20
adjust    = 0.15
pub_figs.change_font_size(ax, font_size=font_size, bottom_adjust=adjust, left_adjust=adjust*1.3)
# plt.gcf().subplots_adjust(top=top_adjust, bottom=bottom_adjust, left=left_adjust, right=right_adjust, hspace=hspace, wspace=wspace)
plt.savefig("cbolteae_ipd_count_vs_contig_size.png", dpi=300)

