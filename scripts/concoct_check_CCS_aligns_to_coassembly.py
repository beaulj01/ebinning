import os,sys
from Bio import SeqIO
from collections import defaultdict,Counter
import pysam
import operator

sam_fn       = sys.argv[1]

contig_aligns = Counter()
samfile = pysam.AlignmentFile(sam_fn, "rb")
sizes   = {}
for k,aln in enumerate(samfile.fetch()):
	if aln.mapq == 254:
		contig_aligns[aln.reference_name] += 1
		sizes[aln.reference_name] = aln.seq

contig_aligns_s = sorted(contig_aligns.items(), key=operator.itemgetter(1))
for (contig,n) in contig_aligns_s:
	print n,contig