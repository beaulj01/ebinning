import os,sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import subprocess
from collections import defaultdict
import pub_figs
import operator

pub_figs.setup_math_fonts(24)

labels = {"A_odontolyticus_ATCC_17982_Scfld021" 	:r"\textit{A. odontolyticus} ATCC 17982", \
		  "D_radiodurans_R1" 				    	:r"\textit{D. radiodurans} R1", \
		  "P_aeruginosa_PAO1"  						:r"\textit{P. aeruginosa} PAO1", \
		  "R_sphaeroides_2.4.1" 					:r"\textit{R. sphaeroides} 2.4.1", \
		  "A_baumannii_ATCC_17978": 				r"\textit{A. baumannii} ATCC 17978", \
		  "B_cereus_ATCC_10987": 					r"\textit{B. cereus} ATCC 10987", \
		  "B_vulgatus_ATCC_8482": 					r"\textit{B. vulgatus} ATCC 8482", \
		  "C_beijerinckii_NCIMB_8052": 				r"\textit{C. beijerinckii} NCIMB 8052", \
		  "E_coli_str_K-12_substr_MG1655": 			r"\textit{E. coli} str. K-12 substr. MG1655", \
		  "E_faecalis_OG1RF": 						r"\textit{E. faecalis} OG1RF", \
		  "H_pylori_26695": 						r"\textit{H. pylori} 26695", \
		  "L_gasseri_ATCC_33323": 					r"\textit{L. gasseri} ATCC 33323", \
		  "L_monocytogenes_EGD-e": 					r"\textit{L. monocytogenes} EGD-e", \
		  "N_meningitidis_MC58": 					r"\textit{N. meningitidis} MC58", \
		  "P_acnes_KPA171202": 						r"\textit{P. acnes KPA171202", \
		  "S_agalactiae_2603V_R": 					r"\textit{S. agalactiae} 2603V R", \
		  "S_aureus_subsp_aureus_USA300_TCH1516": 	r"\textit{S. aureus} subsp. aureus USA300 TCH1516", \
		  "S_epidermidis_ATCC_12228": 				r"\textit{S. epidermidis} ATCC 12228", \
		  "S_mutans_UA159": 						r"\textit{S. mutans} UA159", \
		  "S_pneumoniae_TIGR4": 					r"\textit{S. pneumoniae} TIGR4"}

SLR_pct  = {"A_odontolyticus_ATCC_17982_Scfld021" :	5.2, \
		    "D_radiodurans_R1"  :					4.7, \
		    "P_aeruginosa_PAO1"   :					75.2, \
		    "R_sphaeroides_2.4.1" :					60.0, \
		    "A_baumannii_ATCC_17978": 				53.1, \
		    "B_cereus_ATCC_10987": 					89.5, \
		    "B_vulgatus_ATCC_8482": 				18.7, \
		    "C_beijerinckii_NCIMB_8052": 			57.7, \
		    "E_coli_str_K-12_substr_MG1655": 		99.9, \
		    "E_faecalis_OG1RF": 					7.6, \
		    "H_pylori_26695": 						99.8, \
		    "L_gasseri_ATCC_33323": 				72.6, \
		    "L_monocytogenes_EGD-e": 				98.8, \
		    "N_meningitidis_MC58": 					93.0, \
		    "P_acnes_KPA171202": 					99.8, \
		    "S_agalactiae_2603V_R": 				99.4, \
		    "S_aureus_subsp_aureus_USA300_TCH1516": 97.9, \
		    "S_epidermidis_ATCC_12228": 			100.0, \
		    "S_mutans_UA159": 						100.0, \
		    "S_pneumoniae_TIGR4": 					8.5}

SMRT_pct = {"A_odontolyticus_ATCC_17982_Scfld021" :	74.4, \
		    "D_radiodurans_R1"  :					71.8, \
		    "P_aeruginosa_PAO1"   :					100.0, \
		    "R_sphaeroides_2.4.1" :					99.8, \
		    "A_baumannii_ATCC_17978": 				89.7, \
		    "B_cereus_ATCC_10987": 					98.8, \
		    "B_vulgatus_ATCC_8482": 				44.8, \
		    "C_beijerinckii_NCIMB_8052": 			80.6, \
		    "E_coli_str_K-12_substr_MG1655": 		100.0, \
		    "E_faecalis_OG1RF": 					97.2, \
		    "H_pylori_26695": 						99.8, \
		    "L_gasseri_ATCC_33323": 				99.1, \
		    "L_monocytogenes_EGD-e": 				99.9, \
		    "N_meningitidis_MC58": 					99.1, \
		    "P_acnes_KPA171202": 					100.0, \
		    "S_agalactiae_2603V_R": 				100.0, \
		    "S_aureus_subsp_aureus_USA300_TCH1516": 100.0, \
		    "S_epidermidis_ATCC_12228": 			100.0, \
		    "S_mutans_UA159": 						100.0, \
		    "S_pneumoniae_TIGR4": 					99.9}

GC_pct   = {"A_odontolyticus_ATCC_17982_Scfld021" :	0.65, \
		    "D_radiodurans_R1"  :					0.67, \
		    "P_aeruginosa_PAO1"   :					0.67, \
		    "R_sphaeroides_2.4.1" :					0.69, \
		    "A_baumannii_ATCC_17978": 				0.39, \
		    "B_cereus_ATCC_10987": 					0.35, \
		    "B_vulgatus_ATCC_8482": 				0.42, \
		    "C_beijerinckii_NCIMB_8052": 			0.30, \
		    "E_coli_str_K-12_substr_MG1655": 		0.51, \
		    "E_faecalis_OG1RF": 					0.38, \
		    "H_pylori_26695": 						0.39, \
		    "L_gasseri_ATCC_33323": 				0.35, \
		    "L_monocytogenes_EGD-e": 				0.38, \
		    "N_meningitidis_MC58": 					0.52, \
		    "P_acnes_KPA171202": 					0.60, \
		    "S_agalactiae_2603V_R": 				0.36, \
		    "S_aureus_subsp_aureus_USA300_TCH1516": 0.33, \
		    "S_epidermidis_ATCC_12228": 			0.32, \
		    "S_mutans_UA159": 						0.37, \
		    "S_pneumoniae_TIGR4": 					0.40}

names     = labels.keys()
sorted_GC = sorted(GC_pct.items(), key=operator.itemgetter(1), reverse=True)

mat = np.zeros([len(names),3])
for i,(name,GC) in enumerate(sorted_GC):
	mat[i,0] = SLR_pct[name]
	mat[i,1] = SMRT_pct[name]
	mat[i,2] = GC*100

fig   = pub_figs.init_fig(15,12)
N     = len(names)
ind   = np.arange(N) # the x locations for the groups
width = 0.35         # the width of the bars

#####################
# Plot the pct genome coverage by SLR and SMRT
#####################
ax_x = 0.20
ax_y = 0.45
ax_w = 0.7
ax_h = 0.23
ax   = fig.add_axes([ax_x, ax_y, ax_w, ax_h], frameon=True)

rects0 = ax.bar(ind - 2*width, mat[:,0], width, color='lightgrey')
rects1 = ax.bar(ind - width,   mat[:,1], width, color='k')

ax.set_ylabel(pub_figs.tex_escape("Positions covered\nin genome (%)"))
ax.set_xticks(ind - float(width)/2)
ax.set_xticklabels([labels[name] for (name,GC) in sorted_GC], rotation=45, ha="right")
ax.set_xlim([-1,19.5])
ax.set_ylim([0,100])
ax.grid(True)
# ax.legend([rects0,rects1],["SLR", "SMRT"], loc='center left', bbox_to_anchor=(1, 0.5), frameon=False)
ax.legend([rects0,rects1],["SLR", "SMRT"], ncol=2, loc='lower center', bbox_to_anchor=(0.5, 0.965), frameon=False)
pub_figs.remove_top_right_axes(ax)

#####################
# Plot the GC content at the top
#####################
axgc_x = ax_x
axgc_y = ax_y + ax_h + 0.08
axgc_w = ax_w
axgc_h = 0.2
axgc   = fig.add_axes([axgc_x, axgc_y, axgc_w, axgc_h], frameon=True)

rects0 = axgc.bar(ind - width*2, mat[:,2], width*2, color='g')

axgc.set_ylabel(pub_figs.tex_escape("Genome GC\ncontent (%)"))
axgc.set_xticks(ind - float(width)/2)
axgc.set_xticklabels(["" for lab in names])
axgc.set_xlim([-1,19.5])
axgc.set_ylim([0,100])
axgc.grid(True)
pub_figs.remove_top_right_axes(axgc)

fn = "SLR_vs_SMRT_genome_cov_bar.png"
plt.savefig(fn, dpi=300)