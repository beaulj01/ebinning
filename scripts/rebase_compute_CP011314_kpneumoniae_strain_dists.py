import os,sys
import numpy as np
import urllib2
from bs4 import BeautifulSoup
from collections import Counter
from Bio import Entrez
sys.path.append( "/hpc/users/beaulj01/gitRepo/eBinning/src" )
import read_scanner
from itertools import groupby
import math

all_comps_fn  = "comp_vectors.out"
pls_info_fn   = "dists.out"
pb_acc_org_fn = "pb_acc_org_mapping.txt"

class Opts:
	def __init__(self):
		self.comp_kmer = 5

def fasta_iter( fasta_name ):
	fh = open(fasta_name)
	# ditch the boolean (x[0]) and just keep the header or sequence since
	# we know they alternate.
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		# drop the ">"
		header = header.next()[1:].strip()
		# join all sequence lines to one.
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

def pull_fasta_get_comp( accs ):
	# Download all the fastas from this NCBI page
	db           = "nuccore"
	Entrez.email = "john.beaulaurier@mssm.edu"
	batchSize    = 100
	retmax       = 10**9

	# all_accs = list(np.loadtxt("accessions.txt", dtype="str"))
	all_accs   = accs
	fasta_name = "tmp.fasta"
	f_comps    = open("comp_vectors.kp_strains.out", "wb")
	f_out      = open("dists.kp_strains.out", "wb")
	try:
		for k,start in enumerate(range( 0,len(all_accs),batchSize )):
			#first get GI for query accesions
			batch_accs = all_accs[start:(start+batchSize)]
			# sys.stderr.write( "Fetching %s entries from GenBank: %s\n" % (len(batch_accs), ", ".join(batch_accs[:10])))
			query  = " ".join(batch_accs)
			handle = Entrez.esearch( db=db,term=query,retmax=retmax )
			giList = Entrez.read(handle)['IdList']
			# sys.stderr.write( "Found %s GI: %s\n" % (len(giList), ", ".join(giList[:10])))
			#post NCBI query
			search_handle     = Entrez.epost(db=db, id=",".join(giList))
			search_results    = Entrez.read(search_handle)
			webenv,query_key  = search_results["WebEnv"], search_results["QueryKey"]
			#fetch entries in batch
			# handle = Entrez.efetch(db=db, rettype="fasta", retstart=start, retmax=batchSize, webenv=webenv, query_key=query_key)
			handle = Entrez.efetch(db=db, rettype="fasta", webenv=webenv, query_key=query_key)
			# sys.stdout.write(handle.read())
			f = open(fasta_name, "wb")
			f.write(handle.read())
			f.close()
	except urllib2.HTTPError as err:
		if err.code==404:
			print "**** HTTPError 404! ****"
			error_urls.append(err.geturl())
			errors_n += 1
		else:
			raise

	contigs_comp_vectors = {}
	opts_obj      = Opts()
	len_map       = {}
	tp_map        = {}
	full_name_map = {}
	org_list      = []
	for i,(header,contig_seq) in enumerate(fasta_iter(fasta_name)):
		contig_comp  = []
		contig_kmers = read_scanner.kmer_freq( "cmp", contig_seq, 0, opts_obj )
		length       = len(contig_seq)
		for kmer,count in contig_kmers.iteritems():
			kmer_normed_comp = math.log(float(count) / sum(contig_kmers.values()))
			contig_comp.append(kmer_normed_comp)
		comp_vector = np.array(contig_comp)
		acc       = header.split("|")[3].split(".")[0]
		full_name = header.split("|")[-1]
		strain    = full_name.split("lasmid")[0]
		if strain[-2:]==" p":
			strain = strain[:-2]
			tp     = "pla"
		else:
			tp     = "chr"
		tp_map[acc]               = tp
		len_map[acc]              = length
		name                      = header.split("|")[-1].split(",")[0]
		full_name_map[acc]        = full_name
		contigs_comp_vectors[acc] = comp_vector
		org_list.append( (strain, acc, tp) )
		f_comps.write("%s\t%s\n" % (acc, "\t".join(map(lambda x: str(round(x,4)), comp_vector))) )

	return contigs_comp_vectors

mix_orgs    = set(["Klebsiella pneumoniae 303K chr.", \
				   "Klebsiella pneumoniae 32192 chr.", \
				   "Klebsiella pneumoniae 34618 chr.", \
				   "Klebsiella pneumoniae 38544 chr.", \
				   "Klebsiella pneumoniae 38547 chr.", \
				   "Klebsiella pneumoniae 500_1420 chr.", \
				   "Klebsiella pneumoniae AATZP chr.", \
				   "Klebsiella pneumoniae ATCC 43816 chr.", \
				   "Klebsiella pneumoniae DMC1097 chr.", \
				   "Klebsiella pneumoniae Kb677 chr.", \
				   "Klebsiella pneumoniae MS6671 chr.", \
				   "Klebsiella pneumoniae UHKPC07 chr.", \
				   "Klebsiella pneumoniae UHKPC33 chr.", \
				   "Klebsiella pneumoniae subsp. pneumoniae 234_12 chr.", \
				   "Klebsiella pneumoniae subsp. pneumoniae 234_12 plasmid pKpn23412-362", \
				   "Klebsiella pneumoniae subsp. pneumoniae KPNIH1 chr.", \
				   "Klebsiella pneumoniae subsp. pneumoniae KPNIH10 chr.", \
				   "Klebsiella pneumoniae subsp. pneumoniae KPNIH24 chr.", \
				   "Klebsiella pneumoniae subsp. pneumoniae KPNIH27 chr.", \
				   "Klebsiella pneumoniae subsp. pneumoniae KPNIH29 chr.", \
				   "Klebsiella pneumoniae subsp. pneumoniae KPNIH30 chr.", \
				   "Klebsiella pneumoniae subsp. pneumoniae KPNIH31 chr.", \
				   "Klebsiella pneumoniae subsp. pneumoniae KPNIH32 chr.", \
				   "Klebsiella pneumoniae subsp. pneumoniae KPNIH33 chr.", \
				   "Klebsiella pneumoniae subsp. pneumoniae KPNIH5 chr.", \
				   "Klebsiella pneumoniae subsp. pneumoniae KPR0928 chr."])

org_motifs  = {}
pb_acc_orgs = {}
for line in open(pb_acc_org_fn).xreadlines():
	line   = line.strip()
	acc    = line.split("\t")[0]
	org    = line.split("\t")[1]
	if len(line.split("\t"))==3:
		motifs = line.split("\t")[2].split(" ")
	if acc=="CP011314":
		pass
	else:
		org += " chr."
	org_motifs[org]  = motifs
	pb_acc_orgs[acc] = org

plasmids = set()
for line in open(pls_info_fn).xreadlines():
	line = line.strip()
	acc  = line.split("\t")[2].split(".")[0]
	plasmids.add(acc)

for line in open(all_comps_fn).xreadlines():
	line = line.strip()
	acc  = line.split("\t")[1].split(".")[0]
	if acc=="CP011314":
		CP011314_comp = np.array(map(lambda x: float(x), line.split("\t")[2:]))

kp_motifs = ["CCAYNNNNNTCC", "GATC"]
kp_accs   = set(["CP011313","CP011314"])

names     = {"CP011313"     : "Klebsiella pneumoniae subsp. pneumoniae 234-12 chr.", \
			 "CP011314"     : "Klebsiella pneumoniae subsp. pneumoniae 234-12 plasmid pKpn23412-362", \
			 "CP009114"     : "Klebsiella pneumoniae 303K chr.", \
			 "CP010361"     : "Klebsiella pneumoniae 32192 chr.", \
			 "CP010392"     : "Klebsiella pneumoniae 34618 chr.", \
			 "JWRM01000001" : "Klebsiella pneumoniae 38544 chr.", \
			 "JWRL01000009" : "Klebsiella pneumoniae 38547 chr.", \
			 "CP011980"     : "Klebsiella pneumoniae 500_1420 chr.", \
			 "CP014755"     : "Klebsiella pneumoniae AATZP chr.", \
			 "CP009208"     : "Klebsiella pneumoniae ATCC 43816 chr.", \
			 "CP011976"     : "Klebsiella pneumoniae DMC1097 chr.", \
			 "AQPG01000012" : "Klebsiella pneumoniae Kb677 chr.", \
			 "LN824133"     : "Klebsiella pneumoniae MS6671 chr.", \
			 "CP011985"     : "Klebsiella pneumoniae UHKPC07 chr.", \
			 "CP011989"     : "Klebsiella pneumoniae UHKPC33 chr.", \
			 "CP008827"     : "Klebsiella pneumoniae subsp. pneumoniae KPNIH1 chr.", \
			 "CP007727"     : "Klebsiella pneumoniae subsp. pneumoniae KPNIH10 chr.", \
			 "CP008797"     : "Klebsiella pneumoniae subsp. pneumoniae KPNIH24 chr.", \
			 "CP007731"     : "Klebsiella pneumoniae subsp. pneumoniae KPNIH27 chr.", \
			 "CP009863"     : "Klebsiella pneumoniae subsp. pneumoniae KPNIH29 chr.", \
			 "CP009872"     : "Klebsiella pneumoniae subsp. pneumoniae KPNIH30 chr.", \
			 "CP009876"     : "Klebsiella pneumoniae subsp. pneumoniae KPNIH31 chr.", \
			 "CP009775"     : "Klebsiella pneumoniae subsp. pneumoniae KPNIH32 chr.", \
			 "CP009771"     : "Klebsiella pneumoniae subsp. pneumoniae KPNIH33 chr.", \
			 "AJZX01000004" : "Klebsiella pneumoniae subsp. pneumoniae KPNIH5 chr.", \
			 "CP008831"     : "Klebsiella pneumoniae subsp. pneumoniae KPR0928 chr."}

contigs_comp_vectors = pull_fasta_get_comp(list(names.keys()))

kp_strains_accs = set(names.keys())
for acc,comp in contigs_comp_vectors.iteritems():
	dist = np.linalg.norm(comp - contigs_comp_vectors["CP011314"])
	org    = names[acc]
	if acc in kp_accs:
		motifs = kp_motifs
	else:
		motifs = org_motifs[org]
	motifs.sort()
	print "%.4f\t%s\t%s\t%s\t%s" % (dist, acc, names[acc], len(motifs), " ".join(motifs))
