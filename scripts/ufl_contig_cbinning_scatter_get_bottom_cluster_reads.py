import os,sys
from pbcore.io.align.CmpH5IO import CmpH5Reader
from itertools import groupby
from collections import Counter
import re
import numpy as np

cluster_fasta = "box_bottom_contigs.fasta"
h5_fn         = "22316_aligned_reads.cmp.h5"
movies_fofn   = "/hpc/users/beaulj01/projects/ebinning/ufl/105_439/mix.pid20464.input.fofn"

def fasta_iter( fasta_name ):
	"""
	Given a fasta file, yield tuples of (header, sequence).
	"""
	fh = open(fasta_name)
	# ditch the boolean (x[0]) and just keep the header or sequence since
	# we know they alternate.
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		# drop the ">"
		header = header.next()[1:].strip()
		# join all sequence lines to one.
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

strain_dict = {}
for line in open(movies_fofn, "r").xreadlines():
	line   = line.strip()
	strain = line.split("/")[9]
	movie  = line.split("/")[-1].split(".")[0]
	strain_dict[movie] = strain

cluster_contigs = set()
for name,seq in fasta_iter(cluster_fasta):
	cluster_contigs.add(name.split("|")[0])

reader        = CmpH5Reader(h5_fn)
strain_counts = Counter()
contigs       = set()
for r in reader:
	if r.referenceName in cluster_contigs:
		strain = strain_dict[r.movieName]
		print r.readName, r.referenceName, strain
		contigs.add(r.referenceName)
		strain_counts[strain] += 1

frac_105 = strain_counts["105"] / float(np.sum(strain_counts.values()))
frac_439 = strain_counts["439"] / float(np.sum(strain_counts.values()))
print ""
print "Analyzed subreads from %s contigs..." % len(contigs)
print "Frac 105 subreads = %.4f (N=%s)" % (frac_105, strain_counts["105"])
print "Frac 439 subreads = %.4f (N=%s)" % (frac_439, strain_counts["439"])