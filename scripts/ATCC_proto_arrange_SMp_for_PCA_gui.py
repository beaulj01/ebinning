import os,sys
import numpy as np

smp_fn   = "reads.SMp.20kb.no0s"
names_fn = "reads.names.20kb.no0s"
labs_fn  = "reads.labels.20kb.no0s"
motif_fn = "ordered_motifs.txt"

smp    = np.loadtxt(smp_fn, dtype="float")
names  = np.loadtxt(names_fn, dtype="str")
labs   = np.loadtxt(labs_fn, dtype="str")
motifs = np.loadtxt(motif_fn, dtype="str")

names_str = "\t".join(names)
print "\t%s" % names_str

labs_str = "\t".join(labs)
print "Strain\t%s" % labs_str
for j,motif in enumerate(motifs):
	vals    = smp[:,j]
	scp_str = "\t".join(map(str,vals))
	print "%s\t%s" % (motif, scp_str)