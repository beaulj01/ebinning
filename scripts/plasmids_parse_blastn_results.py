import os,sys
import numpy as np

blastn = np.loadtxt(sys.argv[1], dtype="str", delimiter="\t")

# qseqid sseqid pident length qlen qstart qend slen sstart send mismatch gapopen evalue bitscore

qseqid   = 0
sseqid   = 1
pident   = 2
length   = 3
qlen     = 4
qstart   = 5
qend     = 6
slen     = 7
sstart   = 8
send     = 9
mismatch = 10
gapopen  = 11
evalue   = 12
bitscore = 13

for i in range(blastn.shape[0]):
	frac_q = float(blastn[i,length]) / float(blastn[i,qlen])
	frac_r = float(blastn[i,length]) / float(blastn[i,slen])
	if int(blastn[i,qlen]) < 100000 and int(blastn[i,length]) > 100:
	# if frac_q > 0.25 and frac_r > 0.5:
		print "%.2f\t%.2f\t%s" % (frac_q, frac_r, "\t".join(blastn[i,:]))