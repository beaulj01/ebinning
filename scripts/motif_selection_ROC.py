import os,sys
import numpy as np
from collections import Counter
import re

# Biggest contigs for each species
# R_gnavus        unitig_61       744973
# B_caccae        unitig_401      1186674
# B_theta 		  unitig_33       1400986
# B_ovatus        unitig_411      1661069
# C_aerofaciens   unitig_414      1954868
# B_ovatus        unitig_417      2122760
# B_vulgatus      unitig_407      3123081
# E_coli  		  unitig_17       4668944

big_contigs = {"rgnavus":        "unitig_61",  \
			   "bcaccae":        "unitig_401", \
			   "btheta": 		 "unitig_33",  \
			   "bovatus":        "unitig_411", \
			   "caero":          "unitig_414", \
			   "bovatus":        "unitig_417", \
			   "bvulgatus":      "unitig_407", \
			   "ecoli":  		 "unitig_17"}

big_contigs_rev = dict (zip(big_contigs.values(),big_contigs.keys()))

true_motifs = {"AATCC-1":          ["btheta"],    \
			   "CCANNNNNNCAT-2":   ["btheta"],    \
			   "ATGNNNNNNTGG-0":   ["btheta"],    \
			   # "GGCANNNNNNNRTTT-3":["btheta"],    \
			   "GGCANNNNNNNATTT-3":["btheta"],    \
			   "GGCANNNNNNNGTTT-3":["btheta"],    \
			   # "AAAYNNNNNNNTGCC-2":["btheta"],    \
			   "AAACNNNNNNNTGCC-2":["btheta"],    \
			   "AAATNNNNNNNTGCC-2":["btheta"],    \
			   # "RGATCY-2":         ["btheta"],    \
			   "AGATCC-2":         ["btheta"],    \
			   "GGATCC-2":         ["btheta"],    \
			   "AGATCT-2":         ["btheta"],    \
			   "GGATCT-2":         ["btheta"],    \
			   # "CAYNNNNNRTG-1":    ["bvulgatus"], \
			   "CACNNNNNATG-1":    ["bvulgatus"], \
			   "CACNNNNNGTG-1":    ["bvulgatus"], \
			   "CATNNNNNATG-1":    ["bvulgatus"], \
			   "CATNNNNNGTG-1":    ["bvulgatus"], \
			   "GAAGNNNNNNNTCC-2": ["bvulgatus"], \
			   "GGANNNNNNNCTTC-2": ["bvulgatus"], \
			   # "GACNNNNNRGAC-1":   ["bcaccae"],   \
			   "GACNNNNNAGAC-1":   ["bcaccae"],   \
			   "GACNNNNNGGAC-1":   ["bcaccae"],   \
			   "CAGNNNNNGGA-1":    ["bcaccae", "bovatus"],   \
			   "GATGG-1":          ["bcaccae"],   \
			   "CCATC-2":          ["bcaccae"],   \
			   # "GACNNNNNNRTTG-1":  ["bcaccae"],   \
			   "GACNNNNNNATTG-1":  ["bcaccae"],   \
			   "GACNNNNNNGTTG-1":  ["bcaccae"],   \
			   # "CAAYNNNNNNGTC-2":  ["bcaccae"],   \
			   "CAACNNNNNNGTC-2":  ["bcaccae"],   \
			   "CAATNNNNNNGTC-2":  ["bcaccae"],   \
			   # "CGMAGG-3":         ["bcaccae"],   \
			   "CGAAGG-3":         ["bcaccae"],   \
			   "CGCAGG-3":         ["bcaccae"],   \
			   # "GATGNAG-5":        ["bovatus"],   \
			   "GATGAAG-5":        ["bovatus"],   \
			   "GATGCAG-5":        ["bovatus"],   \
			   "GATGGAG-5":        ["bovatus"],   \
			   "GATGTAG-5":        ["bovatus"],   \
			   "CAGNNNNNGGA-1":    ["bovatus", "bcaccae"],   \
			   "GATC-1":           ["bovatus", "ecoli"],   \
			   "TAANNNNNNCTTG-2":  ["bovatus"],   \
			   "CAAGNNNNNNTTA-2":  ["bovatus"],   \
			   # "WGATC-2":          ["cbolteae"],  \
			   "AGATC-2":          ["cbolteae"],  \
			   "TGATC-2":          ["cbolteae"],  \
			   "CTAAG-3":          ["cbolteae"],  \
			   # "GAYNNNNNNNTCGC-1": ["cbolteae"],  \
			   "GACNNNNNNNTCGC-1": ["cbolteae"],  \
			   "GATNNNNNNNTCGC-1": ["cbolteae"],  \
			   # "GCGANNNNNNNRTC-3": ["cbolteae"],  \
			   "GCGANNNNNNNATC-3": ["cbolteae"],  \
			   "GCGANNNNNNNGTC-3": ["cbolteae"],  \
			   # "SGAKC-2":          ["cbolteae"],  \
			   "CGAGC-2":          ["cbolteae"],  \
			   "CGATC-2":          ["cbolteae"],  \
			   "GGAGC-2":          ["cbolteae"],  \
			   "GGATC-2":          ["cbolteae"],  \
			   "CAGNNNNNCTG-1":    ["cbolteae"],  \
			   "GATC-1":           ["bovatus", "ecoli"],     \
			   "GCACNNNNNNGTT-2":  ["ecoli"],     \
			   "AACNNNNNNGTGC-1":  ["ecoli"],     \
			   "CAGGAG-4":         ["caero"]}

configs = Counter()
for motif in true_motifs.keys():
	if motif.find("N") > -1:
		# Bipartite motif
		m      = re.search(r"([ACGT]+)(N+)([ACGT]+)", motif)
		first  = m.groups()[0]
		Ns     = m.groups()[1]
		second = m.groups()[2]
		config = ( len(first), len(Ns), len(second) )
	else:
		# Contiguous motif
		m      = re.search(r"[ACGT]+", motif)
		bases  = m.group()
		config = ( len(bases) )
	configs[config] += 1

for config in configs.keys():
	print configs[config], config
print ""

contig_labels = dict( zip(np.loadtxt("contigs.names", dtype="str"), np.loadtxt("contigs.labels", dtype="str")) )
contig_stats  = np.loadtxt(sys.argv[2], dtype="str")
j = 1
contig_rank = 0
contigs     = set()
for i in range(contig_stats.shape[0]):
	contig   =       contig_stats[i,0]
	motif    =       contig_stats[i,1]
	score    = float(contig_stats[i,2])
	case     = float(contig_stats[i,3])
	control  = float(contig_stats[i,4])
	ncase    =   int(contig_stats[i,5])
	ncontrol =   int(contig_stats[i,6])
	spec     = contig_labels[contig]
	if contig not in contigs:
		contigs.add(contig)
		contig_rank = 1
	else:
		contig_rank += 1
	if contig in big_contigs_rev.keys():
		if motif in true_motifs.keys() and big_contigs_rev[contig] in true_motifs[motif]:
		# if motif in true_motifs.keys():
			print "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s" % (j, big_contigs_rev[contig], contig_rank, contig, motif, score, case, control, ncase, ncontrol)
			j += 1
print ""
called_motifs = set(np.loadtxt(sys.argv[1], dtype="str"))
overlap = set(true_motifs.keys()) & called_motifs 

print "Called %s motifs" % len(called_motifs)
print "Mix contains %s true motifs" % len(true_motifs.keys())
print "============================="
print "Called motifs contain %s true motifs" % len( overlap )

for motif in overlap:
	print motif, true_motifs[motif]

subs = {"W":"[AT]",  \
		"S":"[CG]",  \
		"M":"[AC]",  \
		"K":"[GT]",  \
		"R":"[AG]",  \
		"Y":"[CT]",  \
		"B":"[CGT]", \
		"D":"[AGT]", \
		"H":"[ACT]", \
		"V":"[ACG]", \
		"N":"[ACGT]"}







# # btheta
# "AATCC-1"	        "btheta"	"0.97368723"	"16874"	"17330"
# "CCANNNNNNCAT-2"	"btheta"	"0.9730379"	    "3645"	"3746"
# "ATGNNNNNNTGG-0"	"btheta"	"0.96956754"	"3632"	"3746"
# "GGCANNNNNNNRTTT-3"	"btheta"	"0.97025496"	"685"	"706"
# "AAAYNNNNNNNTGCC-2"	"btheta"	"0.94617563"	"668"	"706"
# "RGATCY-2"	        "btheta"	"0.96710527"	"4263"	"4408"
# # "GCGATCC-3"	      "0.16053511"	"48"	"299"
# # "GGATCGNG-2"	      "0.15100671"	"45"	"298"

# # bvulgatus (ALL LOW FRACTIONS)
# "CAYNNNNNRTG-1"	    "bvulgatus"	"0.5229797"	    "5462"	"10444"
# "GAAGNNNNNNNTCC-2"	"bvulgatus"	"0.50759876"	"501"	"987"
# "GGANNNNNNNCTTC-2"	"bvulgatus"	"0.485309"	    "479"	"987"
# # "CAAGNNNNNNNTRVG-2"	"bvulgatus"	"0.19104992"	"111"	"581"
# # "CYHANNNNNNNCTTG-3"	"bvulgatus"	"0.17996605"	"106"	"589"

# # bcaccae
# "GACNNNNNRGAC-1"	"bcaccae"	"1.0"	        "591"	"591"
# "CAGNNNNNGGA-1"	    "bcaccae"	"0.99887514"	"2664"	"2667"
# "GATGG-1"	        "bcaccae"	"0.99776506"	"4018"	"4027"
# "CCATC-2"	        "bcaccae"	"0.99726844"	"4016"	"4027"
# "GACNNNNNNRTTG-1"	"bcaccae"	"0.9971884"	    "1064"	"1067"
# "CAAYNNNNNNGTC-2"	"bcaccae"	"0.99625117"	"1063"	"1067"
# "CGMAGG-3"	        "bcaccae"	"0.82220894"	"2747"	"3341"

# # bovatus
# "GATGNAG-5"	        "bovatus"	"0.9555833"	    "5357"	"5606"
# "CAGNNNNNGGA-1"	    "bovatus"	"0.95495725"	"3795"	"3974"
# "GATC-1"	        "bovatus"	"0.9538313"	    "27684"	"29024"
# "TAANNNNNNCTTG-2"	"bovatus"	"0.92975205"	"675"	"726"
# "CAAGNNNNNNTTA-2"	"bovatus"	"0.92699724"	"673"	"726"

# # cbolteae
# "WGATC-2"	        "cbolteae"	"0.9100649"	    "9259"	"10174"
# "CTAAG-3"	        "cbolteae"	"0.88920355"	"2512"	"2825"
# "GAYNNNNNNNTCGC-1"	"cbolteae"	"0.8649469"	    "570"	"659"
# "GCGANNNNNNNRTC-3"	"cbolteae"	"0.83915025"	"553"	"659"
# "SGAKC-2"	        "cbolteae"	"0.8378556"	    "25537"	"30479"
# "CAGNNNNNCTG-1"	    "cbolteae"	"0.67683107"	"4639"	"6854"

# # ecoli
# "GATC-1"	        "ecoli"	"0.9978608"	    "38250"	"38332"
# "GCACNNNNNNGTT-2"	"ecoli"	"0.9966499"	    "595"	"597"
# "AACNNNNNNGTGC-1"	"ecoli"	"0.9966499"	    "595"	"597"

# # caero
# "CAGGAG-4"	        "caero"	"0.85913044"	"1482"	"1725"