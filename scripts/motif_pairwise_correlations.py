import os,sys
import pandas as pd
import numpy as np
import scipy.stats as stats
import matplotlib as mpl; mpl.use('Agg')
import matplotlib.pyplot as plt

data   = pd.read_table(sys.argv[1])
motifs = np.loadtxt(sys.argv[2], dtype="str")

corr = data.corr()

corr_array = corr.values
n          = len(motifs)

# zeroed     = 0
# for i in range(n):
# 	motif1 = motifs[i]
# 	for j in range(n):
# 		motif2 = motifs[j]
# 		if motif1.split("-")[0].find(motif2.split("-")[0]) > -1 or motif2.split("-")[0].find(motif1.split("-")[0]) > -1:
# 			print motif1, motif2
# 			zeroed += 1
# 			corr_array[i,j] = 0

f_corr = open("binning.pairwise_corr", "w")
for i in range(n):
	f_corr.write("%s\n" % "\t".join(map(lambda x: str(round(x,3)), list(corr_array[i,:]))) )

f_corr.close()
# print "Replaced %.2f%% of entries with zeroes" % (float(zeroed) / (n*n)*100)

plt.pcolor(corr_array)
plt.savefig("corr_heatmap.png")
# m      = np.loadtxt(sys.argv[1], dtype="float")

# n = len(motifs)

# corrs = np.zeros((n,n))
# pvals = np.zeros((n,n)) + 1

# # i = list(motifs).index("GATGG-1")
# # j = list(motifs).index("CCATC-2")
# # c,p = stats.pearsonr(m[:,i], m[:,j])
# # print motifs[i],motifs[j],c,p
# # sys.exit()

# z = 0
# for i in range(n):
# 	motif1 = motifs[i].split("-")[0]
	
# 	for j in range(n):
# 		motif2 = motifs[j].split("-")[0]
# 		bad    = False

# 		# Loop over the various suffixes and prefixes of motif1; query in motif2
# 		motif1_in_2 = False
# 		for k in range(3,len(motif1)+1):
# 			# Prefixes
# 			prefix1_str = motif1[:k]
# 			# Suffixes
# 			suffix1_str = motif1[(len(motif1)-k):]

# 			if motif2.find(prefix1_str) > -1 or motif2.find(suffix1_str) > -1:
# 				motif1_in_2 = True
# 		if motif1_in_2:
# 			bad = True			

# 		# Loop over the various suffixes and prefixes of motif2; query in motif1
# 		motif2_in_1 = False
# 		for k in range(3,len(motif2)+1):
# 			# Prefixes
# 			prefix2_str = motif2[:k]
# 			# Suffixes
# 			suffix2_str = motif2[(len(motif2)-k):]

# 			if motif1.find(prefix2_str) > -1 or motif1.find(suffix2_str) > -1:
# 				motif2_in_1 = True
# 		if motif2_in_1:
# 			bad = True

# 		# Check if at least one motif contains a homopolymer of length >= 3
# 		max1 = max(motif1.count("A"), motif1.count("C"), motif1.count("G"), motif1.count("T"))
# 		if max1 >=3:
# 			bad = True

# 		max2 = max(motif2.count("A"), motif2.count("C"), motif2.count("G"), motif2.count("T"))
# 		if max2 >=3:
# 			bad = True

# 		c,p = stats.pearsonr(m[:,i], m[:,j])
# 		if not bad and p < 1e-40 and c > 0.1:
# 			print motifs[i],motifs[j],c,p
# 			corrs[i,j] = c
# 			pvals[i,j] = p
# 			z += 1

# print "Started with %s comparisons" % (n*n)
# print "Keeping %s comparisons" % z