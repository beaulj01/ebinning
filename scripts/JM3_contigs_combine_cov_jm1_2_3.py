import os,sys
import numpy as np

jm1_cov = np.loadtxt("JM1_to_contigs-s.coverage.percontig", dtype="str", delimiter=" ")
jm2_cov = np.loadtxt("JM2_to_contigs-s.coverage.percontig", dtype="str", delimiter=" ")
jm3_cov = np.loadtxt("JM3_to_contigs-s.coverage.percontig", dtype="str", delimiter=" ")

jm1_cov_d = dict( (jm1_cov) )
jm2_cov_d = dict( (jm2_cov) )
jm3_cov_d = dict( (jm3_cov) )

f = open("jm3_contigs_jm1_2_3_cov.out", "wb")
f.write("contig\tjm1\tjm2\tjm3\n")
for contig in jm1_cov_d.keys():
	if contig!="genome":
		f.write("%s\t%s\t%s\t%s\n" % (contig, jm1_cov_d[contig], jm2_cov_d[contig], jm3_cov_d[contig]))
f.close()