import os,sys
import math
from itertools import groupby,product
from collections import Counter
import numpy as np
import operator
import subprocess
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt

contigs_seqs_fn = sys.argv[1]
contig_names    = np.loadtxt(sys.argv[2], dtype="str")
contig_labs     = np.loadtxt(sys.argv[3], dtype="str")

def scatterplot(results, labels, plot_fn, sizes):
	label_set = list(set(labels))
	label_set.sort()
	colors    = plt.get_cmap('rainbow')(np.linspace(0, 1.0, len(label_set)))
	fig       = plt.figure(figsize=[12,12])
	ax        = fig.add_subplot(111)
	if len(sizes)>0:
		sizes[sizes<100000] = 100000
		scaled_sizes = sizes**1.5 / max(sizes**1.5) * 2000
	for k,target_lab in enumerate(label_set):
		idxs  = [j for j,label in enumerate(labels) if label==target_lab]
		X     = results[idxs,0]
		Y     = results[idxs,1]
		if len(sizes)>0:
			scaled_sizes_idx = np.array(scaled_sizes)[idxs]
			ax.scatter(X, Y, edgecolors=colors[k], label=target_lab, facecolors='none', alpha=1.0, s=scaled_sizes_idx)
		else:
			ax.scatter(X, Y, edgecolors=colors[k], label=target_lab, facecolors='none', alpha=0.7)
	box = ax.get_position()
	ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
	ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':16})
	plt.savefig(plot_fn)

labs = {}
for i in range(contig_names.shape[0]):
	labs[contig_names[i]] = contig_labs[i]

def fasta_iter(fasta_name):
	"""
	Given a fasta file, yield tuples of (header, sequence).
	"""
	fh = open(fasta_name)
	# ditch the boolean (x[0]) and just keep the header or sequence since
	# we know they alternate.
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		# drop the ">"
		header = header.next()[1:].strip()
		# join all sequence lines to one.
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

def rev_comp_motif( motif ):
	COMP = {"A":"T", \
			"T":"A", \
			"C":"G", \
			"G":"C", \
			"W":"S", \
			"S":"W", \
			"M":"K", \
			"K":"M", \
			"R":"Y", \
			"Y":"R", \
			"B":"V", \
			"D":"H", \
			"H":"D", \
			"V":"B", \
			"N":"N", \
			"X":"X", \
			"*":"*"}
	rc_motif = []
	for char in motif[::-1]:
		rc_motif.append( COMP[char] )
	return "".join(rc_motif)

def kmer_freq ( contig_seq ):
	k = 5
	kmers = []
	for seq in product("ATGC",repeat=k):
		kmers.append( "".join(seq) )

	kmer_counts = Counter()
	for j in range( len(contig_seq)-(k-1) ):
		motif               = contig_seq[j:j+k]
		kmer_counts[motif] += 1
	# Combine forward and reverse complement motifs into one count
	combined_kmer = Counter()
	for kmer in kmers:
		kmer_rc = rev_comp_motif(kmer)
		if not combined_kmer.get(kmer_rc):
			combined_kmer[kmer] = kmer_counts[kmer] + kmer_counts[kmer_rc] + 1
	return combined_kmer

def chunks(seq, n):
	"""
	Yield successive n-sized sequences from seq.
	"""
	for i in xrange(0, len(seq), n):
		yield seq[i:i+n]

f     = open("contigs.comp", "w")
f_len = open("contigs.lengths", "w")
f_lab = open("contigs.labels", "w")
f_whole = open("contigs.whole", "w")
whole_contig = []
for i,(name,contig_seq) in enumerate(fasta_iter(contigs_seqs_fn)):
	name = name.split("|")[0]
	if len(contig_seq)>50000:
		try:
			lab = labs[name]
		except KeyError:
			lab = "unknown"
		
		unsplit = True
		print i, name, lab, len(contig_seq)
		if len(contig_seq)>500000:
			seq_chunks = chunks(contig_seq, 500000)
			seq_chunks = list(seq_chunks)
			seq_chunks.insert(0, contig_seq)
			unsplit = False
		else:
			seq_chunks = [contig_seq]

		for i,seq_chunk in enumerate(seq_chunks):
			if i==0:
				whole_contig.append(True)
			else:
				whole_contig.append(unsplit)

			contig_kmers           = kmer_freq( seq_chunk )
			contig_comp_tups_alpha = sorted(contig_kmers.items(), key=operator.itemgetter(0))
			
			contig_comp_orig = []
			for (kmer,count) in contig_comp_tups_alpha:
				kmer_normed_comp = math.log(float(count) / sum(contig_kmers.values()))
				contig_comp_orig.append(kmer_normed_comp)

			contig_comp_str = "\t".join(map(lambda x: str(round(x,3)), contig_comp_orig))
			f.write("%s\n" % contig_comp_str)
			f_len.write("%s\n" % len(seq_chunk))
			f_lab.write("%s\n" % lab)
			f_whole.write("%s\n" % unsplit)
whole_contig = np.array(whole_contig)
f.close()
f_len.close()
f_lab.close()
f_whole.close()

sne_CMD   = "python ~/gitRepo/eBinning/src/bhtsne.py -v -i contigs.comp -l contigs.labels -o contigs.comp.2D -p 5 -s contigs.comp.scatter.png -z contigs.lengths"
p         = subprocess.Popen(sne_CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
stdOutErr = p.communicate()
sts       = p.returncode
if sts != 0:
	print "Failed bhtsne command: %s" % sne_CMD

results = np.loadtxt("contigs.comp.2D",  dtype="float")
labels  = np.loadtxt("contigs.labels",   dtype="str")
sizes   = np.loadtxt("contigs.lengths",  dtype="float")
scatterplot(results[whole_contig], labels[whole_contig], "contigs.comp.WHOLE.scatter.png", sizes[whole_contig])