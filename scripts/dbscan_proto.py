import os,sys
import shutil
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs
from sklearn.preprocessing import StandardScaler

##############################################################################
# Generate sample data
centers = [[1, 1], [-1, -1], [1, -1]]
# X, labels_true = make_blobs(n_samples=750, centers=centers, cluster_std=0.4,
#                             random_state=0)

barcodes_fn = sys.argv[1]
labels_fn   = sys.argv[2]
reads_fn    = sys.argv[3]
eps         = float(sys.argv[4])
min_samples = int(sys.argv[5])

labels         = np.genfromtxt(labels_fn, dtype='str')
labels_set     = set(labels)
labels_map     = {}
labels_map_rev = {-1: "unknown"}
for i,label in enumerate(labels_set):
	labels_map[label] = i
	labels_map_rev[i] = label

labels_true = np.array(map(lambda x: labels_map[x], labels))
X           = StandardScaler().fit_transform(np.loadtxt(barcodes_fn))
##############################################################################
# Compute DBSCAN
# eps         = 0.17
# min_samples = 300
db = DBSCAN(eps=eps, min_samples=min_samples).fit(X)
core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
core_samples_mask[db.core_sample_indices_] = True
labels = db.labels_

# Number of clusters in labels, ignoring noise if present.
n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

print('---- Parameters ----')
print('Eps: %0.3f' % eps)
print('Min_samples: %s' % min_samples)

print('---- Results ----')
print('Estimated number of clusters: %d' % n_clusters_)
print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels_true, labels))
print("Completeness: %0.3f" % metrics.completeness_score(labels_true, labels))
print("V-measure: %0.3f" % metrics.v_measure_score(labels_true, labels))
print("Adjusted Rand Index: %0.3f" % metrics.adjusted_rand_score(labels_true, labels))
print("Adjusted Mutual Information: %0.3f" % metrics.adjusted_mutual_info_score(labels_true, labels))
print("Silhouette Coefficient: %0.3f" % metrics.silhouette_score(X, labels))
##############################################################################
# Plot result

# Black removed and is used for noise instead.
unique_labels = set(labels)
colors = plt.cm.Spectral(np.linspace(0, 1, len(unique_labels)))

reads = np.genfromtxt(reads_fn,  dtype='str')
labs  = np.genfromtxt(labels_fn, dtype='str')
read_labs = np.array(zip(reads,labs))

base_dir = os.getcwd()
clusters_dir = "clusters"
if os.path.exists(clusters_dir):
	shutil.rmtree(clusters_dir)
os.mkdir(clusters_dir)
os.chdir(clusters_dir)
for k, col in zip(unique_labels, colors):
	clust_dir = "k%s" % int(k)
	if k == -1:
		# Black used for noise.
		col       = 'k'
		clust_dir = "k_unknown"

	current   = os.getcwd()
	if os.path.exists(clust_dir):
		shutil.rmtree(clust_dir)
	os.mkdir(clust_dir)
	os.chdir(clust_dir)

	class_member_mask = (labels == k)

	# f_reads = open("core_reads.txt", "w")
	# f_labs  = open("core_labels.txt", "w")
	f_reads = open("reads.txt", "w")
	f_labs  = open("labels.txt", "w")
	core_mask = class_member_mask & core_samples_mask
	xy  = X[core_mask]
	plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=col, markeredgecolor='k', markersize=14)
	for (read,label) in read_labs[core_mask]:
		f_reads.write("%s\n" % read)
		f_labs.write("%s\n" % label)
	# f_reads.close()
	# f_labs.close()
			
	# f_reads = open("noncore_reads.txt", "w")
	# f_labs  = open("noncore_labels.txt", "w")
	noncore_mask = class_member_mask & ~core_samples_mask
	xy           = X[noncore_mask]
	tot_points   = len(X[core_mask]) + len(X[noncore_mask])
	if k==-1:
		lab = "unknown (%s)" % tot_points
	else:
		lab = "%s (%s)" % (int(k), tot_points)
	plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=col, markeredgecolor='k', markersize=6, label=lab)
	for (read,label) in read_labs[noncore_mask]:
		f_reads.write("%s\n" % read)
		f_labs.write("%s\n" % label)
	f_reads.close()
	f_labs.close()

	os.chdir(current)

os.chdir(base_dir)

plt.legend(loc=2,prop={'size':6})
plt.title('Estimated number of clusters: %d' % n_clusters_)
plt.savefig("dbscan_%s_%s.png" % (eps, min_samples))