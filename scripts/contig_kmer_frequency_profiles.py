import os,sys
import numpy as np
import math
sys.path.append( "/hpc/users/beaulj01/gitRepo/eBinning/src" )
import read_scanner
from itertools import groupby
from collections import defaultdict
import re
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import scipy.stats as stats

contigs_fasta = sys.argv[1]
info_map      = sys.argv[2]

class Opts:
	def __init__(self):
		self.comp_kmer = 5

def fasta_iter( fasta_name ):
	fh = open(fasta_name)
	# ditch the boolean (x[0]) and just keep the header or sequence since
	# we know they alternate.
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		# drop the ">"
		header = header.next()[1:].strip()
		# join all sequence lines to one.
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

spec_map    = {}
spec_map_rv = defaultdict(list)
tp_map      = {}
length_map  = {}
for line in open(info_map, "r").xreadlines():
	line   = line.strip()
	vals   = line.split("\t")
	acc    = vals[0]
	spec   = vals[1]
	tp     = vals[2]
	length = int(vals[3])
	spec_map[acc]     = spec
	spec_map_rv[spec].append(acc)
	tp_map[acc]       = tp
	length_map[acc]   = length

contigs_comp_vectors = {}
opts_obj    = Opts()
name_map    = {}
strain_map  = {}
new_strain  = True
prev_strain_pref = "garbage"
org_list    = []
dist_list   = []
for i,(header,contig_seq) in enumerate(fasta_iter(contigs_fasta)):
	# if i<240:
	# 	continue

	contig_comp  = []
	contig_kmers = read_scanner.kmer_freq( "cmp", contig_seq, 0, opts_obj )
	for kmer,count in contig_kmers.iteritems():
		kmer_normed_comp = math.log(float(count) / sum(contig_kmers.values()))
		contig_comp.append(kmer_normed_comp)
	comp_vector = np.array(contig_comp)
	acc       = header.split("|")[3]
	full_name = header.split("|")[-1]
	strain    = full_name.split("lasmid")[0]
	if strain[-2:]==" p":
		strain = strain[:-2]
		tp     = "pla"
	else:
		tp     = "chr"
	tp_map[acc]               = tp
	strain_map[acc]           = strain
	name                      = header.split("|")[-1].split(",")[0]
	name_map[acc]             = name
	contigs_comp_vectors[acc] = comp_vector

	strain_pref = " ".join(strain.split(" ")[:3])
	print strain_pref, org_list
	if strain_pref == prev_strain_pref:
		new_strain = False
		org_list.append( (strain, acc, tp) )
	else:
		new_strain = True
		if i>0:
			# Do calculations for previous strain
			chr_accs = [x[1] for x in org_list if x[2]=="chr"]
			pls_accs = [x[1] for x in org_list if x[2]=="pla"]
			
			if len(chr_accs)>1:
				# Take mean chr composition vector for pls <--> chr distance
				all_chrs = []
				for chr_acc in chr_accs:
					all_chrs.append( contigs_comp_vectors[chr_acc] )
				all_chrs = np.array(all_chrs)
				chr_vector = np.mean(all_chrs, axis=0)
			elif len(chr_accs)==1:
				chr_vector = contigs_comp_vectors[chr_accs[0]]
			elif len(chr_accs)==0:
				continue
			
			# Calc euclidian distance
			for pls_acc in pls_accs:
				pls_vector = contigs_comp_vectors[pls_acc]
				dist = np.linalg.norm(chr_vector - pls_vector)
				print i, spec_map[pls_acc], tp_map[pls_acc], pls_acc, dist
				out_str = "%s\t%s\t%s\t%.4f\t%s\n" % (spec_map[pls_acc], tp_map[pls_acc], pls_acc, dist, strain_map[pls_acc])
				dist_list.append( (spec_map[pls_acc], tp_map[pls_acc], pls_acc, dist, strain_map[pls_acc]) )

		# Start new org_list
		org_list   = [ (strain, acc, tp) ]

	prev_strain_pref = strain_pref

dists = map(lambda x: x[3], dist_list)

plt.figure()
# kde        = stats.kde.gaussian_kde( dists )
# dist_space = np.linspace( min(dists), max(dists), 50 )
# plt.plot( dist_space, kde(dist_space), label="Distances")
plt.hist( dists, 50)
plt.grid()

plt.legend(loc=2)
plt.xlabel("Distance")
plt.ylabel("Count")
plt.title("Distribution of plasmid/chromosome composition distance")
plt.savefig("dist_hist.png")

