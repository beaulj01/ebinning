import os,sys
import numpy as np
from Bio import SeqIO
import glob
from collections import Counter
import subprocess


scores_mats   = []
scores_N_mats = []
coords_mats   = []
names_mats    = []
lengths_mats  = []
box_id_mats   = []
for box_id in range(1,10):
	scores   = np.atleast_2d(np.loadtxt("box%s/contig_binning/contigs.SCp" % box_id,     dtype="float"))
	scores_N = np.atleast_2d(np.loadtxt("box%s/contig_binning/contigs.SCp_N" % box_id,   dtype="int"))
	coords   = np.atleast_2d(np.loadtxt("box%s/contig_binning/contigs.SCp.2D" % box_id,  dtype="float"))
	names    = np.atleast_1d(np.loadtxt("box%s/contig_binning/contigs.names" % box_id,   dtype="str"))
	lengths  = np.atleast_1d(np.loadtxt("box%s/contig_binning/contigs.lengths" % box_id, dtype="int"))
	box_maps = np.zeros(len(names))+box_id

	motifs   = np.loadtxt("box%s/contig_binning/ordered_motifs.txt" % box_id, dtype="str")
	order    = motifs.argsort()
	motifs   = motifs[order]

	scores_mats.append(scores[:,order])
	scores_N_mats.append(scores_N[:,order])
	coords_mats.append(coords)
	names_mats.append(names)
	lengths_mats.append(lengths)
	box_id_mats.append(box_maps)

scores_all   = np.concatenate(scores_mats)
scores_N_all = np.concatenate(scores_N_mats)
coords_all   = np.concatenate(coords_mats)
names_all    = np.concatenate(names_mats)
lengths_all  = np.concatenate(lengths_mats)
box_id_all   = np.concatenate(box_id_mats)

new_names = []
for i,name in enumerate(names_all):
	new_names.append("box%s_%s" % (int(box_id_all[i]),name))

bin_ids_map = dict([(n,int(b)) for n,b in zip(new_names, box_id_all)])

min_scp   = 1.6
min_scp_N = 10
min_plasmid_length = 5000
min_mapped_pct = 75

plasmids = ["box1_unitig_8", \
			"box2_unitig_10", \
			"box7_unitig_1", \
			"box7_unitig_14", \
			"box7_unitig_15", \
			"box7_unitig_16", \
			"box8_unitig_5", \
			"box8_unitig_6", \
			# CT elements
			"box1_unitig_9", \
			"box1_unitig_6", \
			"box1_unitig_1", \
			"box5_unitig_5", \
			"box6_unitig_11", \
			"box7_unitig_9", \
			"box7_unitig_11", \
			"box7_unitig_5", \
			"box8_unitig_1", \
			]

plasmid_maps_to_contig = {}
contig_lens            = {}
for plasmid in plasmids:
	
	plasmid_maps_to_contig[plasmid] = []

	for i in range(len(new_names)):
		if new_names[i]==plasmid:
			pl_scp   = scores_all[i,:]
			pl_scp_N = scores_N_all[i,:]
			print "Checking suspected plasmid %s (%sbp)" % (plasmid, lengths_all[i])
			pl_idxs  = (pl_scp>=min_scp) & (pl_scp_N>=min_scp_N) 
			print "   Has motifs %s, N=%s" % (",".join(motifs[pl_idxs]), ",".join(map(str,pl_scp_N[pl_idxs])))
			# Find contigs with these motifs
			for row in range(scores_all.shape[0]):
				contig      = new_names[row]
				keep_contig = False

				if contig!=plasmid:

					con_scp   = scores_all[row,pl_idxs]
					con_scp_N = scores_N_all[row,pl_idxs]
					match_idx = (con_scp[:]>=min_scp) & (con_scp_N[:]>=min_scp_N)

					if match_idx.all() and len(con_scp)>0:
						# print "   possible matching contig: %s" % contig, con_scp, con_scp_N
						keep_contig = True

						# Check for additional motifs on this contig
						con_all_scp   = scores_all[row,:]
						con_all_scp_N = scores_N_all[row,:]
						contig_idxs   = (con_all_scp[:]>=min_scp) & (con_all_scp_N>=min_scp_N)
						for k,idx in enumerate(contig_idxs):

							if motifs[k]=="AGATGT-2":
								continue

							if (idx!=pl_idxs[k]) & (pl_scp_N[k]>=min_scp_N):
								# Found a sig motif on contig thats not sig on plasmid
								print "     -- but %s on plasmid %s = %.3f (N=%s)" % (motifs[k], plasmid, pl_scp[k], pl_scp_N[k])
								keep_contig = False


				if keep_contig:
					plasmid_maps_to_contig[plasmid].append(contig)

				contig_lens[contig] = lengths_all[row]

bin_ids_list = list(set(bin_ids_map.values()))
bin_ids_list.sort()

print ""

for plasmid,contigs in plasmid_maps_to_contig.iteritems():
	
	if contig_lens[plasmid]>=min_plasmid_length:
		print "Plasmid %s (%s bp): " % (plasmid, contig_lens[plasmid])

		if len(contigs)==0:
			print "   *** no call ***"
		else:
			contig_counts  = Counter()
			all_bin_counts = map(lambda x: bin_ids_map[x], contigs)

			bin_bps = Counter()
			for bin_id in bin_ids_list:
				bin_contigs = [contig for contig in contigs if bin_ids_map[contig]==bin_id]
				for c in bin_contigs:
					bin_bps[bin_id] += contig_lens[c]
				contig_counts[bin_id] += all_bin_counts.count(bin_id)

			for bin_id in bin_ids_list:
				bin_tot_bps = np.sum(lengths_all[box_id_all==float(bin_id)])
				mapped_pct  = 100*float(bin_bps[bin_id])/bin_tot_bps
				# if mapped_pct >= min_mapped_pct:
				print "   bin %s: %s\t(%s mapped bp; %.1f%% of bin)" % (bin_id, contig_counts[bin_id], bin_bps[bin_id], mapped_pct)
		print ""