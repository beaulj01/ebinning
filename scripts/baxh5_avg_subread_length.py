import os,sys
import numpy as np

fn = sys.argv[1]
sublengths = []
for line in open(fn).xreadlines():
	line       = line.strip()
	read       = line.split(" ")[0]
	start_stop = read.split("/")[-1]
	start      = int(start_stop.split("_")[0])
	stop       = int(start_stop.split("_")[1])
	sublengths.append( stop - start )

print "Avg. sublength = %s" % np.mean(sublengths)