import os,sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import random
import pub_figs

pcafile   = sys.argv[1]
labfile   = sys.argv[2]

def scatterplot(results, labels, plot_fn, title):
	label_set = list(set(labels))
	label_set.sort()
	colors    = plt.get_cmap('spectral')(np.linspace(0, 1.0, len(label_set)+1))
	shapes    = ["o","s","^"]
	fig       = plt.figure(figsize=[15,12])
	ax        = fig.add_axes([0.05, 0.05, 0.75, 0.75])
	# ax.axis("off")
	ax.grid()
	res       = []
	for k,target_lab in enumerate(label_set):
		idxs  = [j for j,label in enumerate(labels) if label==target_lab]
		X     = results[idxs,0]
		Y     = results[idxs,1]
		color = colors[k]
		for i,x in enumerate(X):
			res.append( (x, Y[i], target_lab, color, shapes[k%len(shapes)]) ) 
	
	np.random.shuffle(res) 
	plotted_labs = set()
	legend_plots = []
	legend_labs  = []
	for m,(x,y, target_lab, color, shape) in enumerate(res):
		if target_lab=="unknown" or target_lab=="Unlabeled":
			plot = ax.scatter(x,y, marker="+",   s=15 , edgecolors="grey", label=target_lab, facecolors="grey")
		else:
			plot = ax.scatter(x,y, marker=shape, s=15 , edgecolors="None", label=target_lab, facecolors=color, alpha=0.8)
		if target_lab not in plotted_labs:
			plotted_labs.add(target_lab)
			legend_plots.append(plot)
			legend_labs.append(target_lab)
	box = ax.get_position()
	ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
	leg_tup = zip(legend_labs,legend_plots)
	leg_tup.sort(key=lambda x: x[0])
	legend_labs, legend_plots = zip(*leg_tup)
	leg = ax.legend(legend_plots, legend_labs, loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':18}, frameon=False)
	for i in range(len(legend_labs)):
		leg.legendHandles[i]._sizes = [50]
	ax.set_xlabel("PCA1")
	ax.set_ylabel("PCA2")
	plt.savefig(plot_fn)

pcas   = np.loadtxt(pcafile,   dtype="str", delimiter=",", skiprows=1)
labs   = np.loadtxt(labfile,   dtype="str", delimiter=",")

scatterplot(pcas[:,1:3].astype(float), labs[:,1], labfile.replace(".csv", ".labeled.png"), "")