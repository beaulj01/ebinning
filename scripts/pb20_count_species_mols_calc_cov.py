import os,sys
import glob
import numpy as np
from pbcore.io.BasH5IO import BasH5Reader
from collections import Counter

read_mapping = {}
specs        = set()
for line in open("/hpc/users/beaulj01/projects/ebinning/pacbio20/all7_sets/aligns_to_refs/read_species_mapping.txt", "r").xreadlines():
	line = line.strip()
	read = line.split("\t")[0]
	spec = line.split("\t")[1]
	read_mapping[read] = spec
	specs.add(spec)

# HMP all sets (49 SMRT cells)
baxh5_files   = np.loadtxt("/hpc/users/beaulj01/projects/ebinning/pacbio20/all7_sets/aligns_to_refs/147_bax.fofn", dtype="str")

# HMP sets 5 & 6 (15 SMRT cells)
# baxh5_files   = np.loadtxt("/hpc/users/beaulj01/projects/smrtportal_jobs_022/022165/input.fofn", dtype="str")
bases_counter = Counter()
for i,baxh5_file in enumerate(baxh5_files):
	# if i==1:
	# 	break
	reader = BasH5Reader(baxh5_file)
	for r in reader:
		if r.productivity==1:
			readname = r.zmwName
			try:
				spec = read_mapping[readname]
			except:
				continue
			if spec=="H_pylori_26695" and bases_counter[spec] > 333573400:
				# We only want enough H. pylori reads for 200x coverage
				pass
			else:
			# if spec=="H_pylori_26695" and bases_counter[spec] > 116750690:
			# 	# We only want enough H. pylori reads for 70x coverage
			# 	pass
			# else:
				read_bases = 0
				for sub in r.subreads:
					bases                = sub.readEnd - sub.readStart
					read_bases          += bases
					bases_counter[spec] += bases
				# Print readname for whitelist
				print "%s\t%s\t%s" % (readname, spec, read_bases)

f = open("baseline_base_counts.out", "w")
for spec in list(specs):
	f.write("%s\t%s\n" % (spec, bases_counter[spec]))
f.close()
