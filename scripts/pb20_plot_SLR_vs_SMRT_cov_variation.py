import os,sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import subprocess
from collections import defaultdict
import pub_figs
import operator
import pysam 

pub_figs.setup_math_fonts(24)
windowsize = 10000

# function for setting the colors of the box plots pairs
def setBoxColors(bp):
	try:
		plt.setp(bp['boxes'][0], color='blue')
		plt.setp(bp['caps'][0], color='blue')
		plt.setp(bp['caps'][1], color='blue')
		plt.setp(bp['whiskers'][0], color='blue')
		plt.setp(bp['whiskers'][1], color='blue')
		plt.setp(bp['fliers'][0], color='blue')
		plt.setp(bp['fliers'][1], color='blue')
		plt.setp(bp['medians'][0], color='blue')

		plt.setp(bp['boxes'][1], color='red')
		plt.setp(bp['caps'][2], color='red')
		plt.setp(bp['caps'][3], color='red')
		plt.setp(bp['whiskers'][2], color='red')
		plt.setp(bp['whiskers'][3], color='red')
		plt.setp(bp['fliers'][2], color='red')
		plt.setp(bp['fliers'][3], color='red')
		plt.setp(bp['medians'][1], color='red')
	except IndexError:
		pass

def make_boxplot( ax, vals, org_n ):
	bp = ax.boxplot(vals, positions = [-0.5,0.5], widths = 0.5)
	setBoxColors( bp )

labels = {"A_odontolyticus":r"\textit{A. odontolyticus} ATCC 17982", \
		  "D_radiodurans":	r"\textit{D. radiodurans} R1", \
		  "P_aeruginosa":	r"\textit{P. aeruginosa} PAO1", \
		  "R_sphaeroides":	r"\textit{R. sphaeroides} 2.4.1", \
		  "A_baumannii": 	r"\textit{A. baumannii} ATCC 17978", \
		  "B_cereus": 		r"\textit{B. cereus} ATCC 10987", \
		  "B_vulgatus": 	r"\textit{B. vulgatus} ATCC 8482", \
		  "C_beijerinckii": r"\textit{C. beijerinckii} NCIMB 8052", \
		  "E_coli": 		r"\textit{E. coli} str. K-12 substr. MG1655", \
		  "E_faecalis": 	r"\textit{E. faecalis} OG1RF", \
		  "H_pylori": 		r"\textit{H. pylori} 26695", \
		  "L_gasseri": 		r"\textit{L. gasseri} ATCC 33323", \
		  "L_monocytogenes":r"\textit{L. monocytogenes} EGD-e", \
		  "N_meningitidis": r"\textit{N. meningitidis} MC58", \
		  "P_acnes": 		r"\textit{P. acnes KPA171202", \
		  "S_agalactiae": 	r"\textit{S. agalactiae} 2603V R", \
		  "S_aureus": 		r"\textit{S. aureus} subsp. aureus USA300 TCH1516", \
		  "S_epidermidis": 	r"\textit{S. epidermidis} ATCC 12228", \
		  "S_mutans": 		r"\textit{S. mutans} UA159", \
		  "S_pneumoniae": 	r"\textit{S. pneumoniae} TIGR4"}

org_fns_SMRT = {"A_baumannii":     "A_baumannii_ATCC_17978", \
				"A_odontolyticus": "A_odontolyticus_ATCC_17982_Scfld021", \
				"B_cereus":        "B_cereus_ATCC_10987", \
				"B_vulgatus":      "B_vulgatus_ATCC_8482", \
				"C_beijerinckii":  "C_beijerinckii_NCIMB_8052", \
				"D_radiodurans":   "D_radiodurans_R1", \
				"E_faecalis":      "E_faecalis_OG1RF", \
				"E_coli":          "E_coli_str_K_12_substr_MG1655", \
				"H_pylori":        "H_pylori_26695", \
				"L_gasseri":       "L_gasseri_ATCC_33323", \
				"L_monocytogenes": "L_monocytogenes_EGD_e", \
				"N_meningitidis":  "N_meningitidis_MC58", \
				"P_acnes":         "P_acnes_KPA171202", \
				"P_aeruginosa":    "P_aeruginosa_PAO1", \
				"R_sphaeroides":   "R_sphaeroides_2_4_1", \
				"S_aureus":        "S_aureus_subsp_aureus_USA300_TCH1516", \
				"S_epidermidis":   "S_epidermidis_ATCC_12228", \
				"S_agalactiae":    "S_agalactiae_2603V_R", \
				"S_mutans":        "S_mutans_UA159", \
				"S_pneumoniae":    "S_pneumoniae_TIGR4"} 

org_fns_SLR  = {"A_baumannii":     "A_baumannii_ATCC_17978", \
				"A_odontolyticus": "A_odontolyticus_ATCC_17982_Scfld021", \
				"B_cereus":        "B_cereus_ATCC_10987", \
				"B_vulgatus":      "B_vulgatus_ATCC_8482", \
				"C_beijerinckii":  "C_beijerinckii_NCIMB_8052", \
				"D_radiodurans":   "D_radiodurans_R1", \
				"E_faecalis":      "E_faecalis_OG1RF", \
				"E_coli":          "E_coli_str_K-12_substr_MG1655", \
				"H_pylori":        "H_pylori_26695", \
				"L_gasseri":       "L_gasseri_ATCC_33323", \
				"L_monocytogenes": "L_monocytogenes_EGD-e", \
				"N_meningitidis":  "N_meningitidis_MC58", \
				"P_acnes":         "P_acnes_KPA171202", \
				"P_aeruginosa":    "P_aeruginosa_PAO1", \
				"R_sphaeroides":   "R_sphaeroides_2.4.1", \
				"S_aureus":        "S_aureus_subsp_aureus_USA300_TCH1516", \
				"S_epidermidis":   "S_epidermidis_ATCC_12228", \
				"S_agalactiae":    "S_agalactiae_2603V_R", \
				"S_mutans":        "S_mutans_UA159", \
				"S_pneumoniae":    "S_pneumoniae_TIGR4"} 

names = labels.keys()
names.sort()

SLR_cov = {"A_baumannii" : 	10894845, \
	    "A_odontolyticus" : 3730113, \
	    "B_cereus" : 		26483461, \
	    "B_vulgatus" : 		3579005, \
	    "C_beijerinckii" : 	11135316, \
	    "D_radiodurans" : 	3983663, \
	    "E_faecalis" : 		11270120, \
	    "E_coli" : 			2133476693, \
	    "H_pylori" : 		26015813, \
	    "L_gasseri" : 		10760149, \
	    "L_monocytogenes" : 24364014, \
	    "N_meningitidis" : 	15910092, \
	    "P_acnes" : 		26717866, \
	    "P_aeruginosa" : 	170029436, \
	    "R_sphaeroides" : 	29901273, \
	    "S_aureus" : 		61148568, \
	    "S_epidermidis" : 	173408151, \
	    "S_agalactiae" : 	49104157, \
	    "S_mutans" : 		252711874, \
	    "S_pneumoniae" : 	23107608}

SMRT_cov = {"A_baumannii" : 458337257, \
	    "A_odontolyticus" : 390178052, \
	    "B_cereus" : 		101134305, \
	    "B_vulgatus" : 		744355850, \
	    "C_beijerinckii" : 	518725397, \
	    "D_radiodurans" : 	112809810, \
	    "E_faecalis" : 		388729626, \
	    "E_coli" : 			607875898, \
	    "H_pylori" : 		1637499690, \
	    "L_gasseri" : 		436941561, \
	    "L_monocytogenes" : 770815426, \
	    "N_meningitidis" : 	448123214, \
	    "P_acnes" : 		533794211, \
	    "P_aeruginosa" : 	1072961775, \
	    "R_sphaeroides" : 	363229532, \
	    "S_aureus" : 		616183706, \
	    "S_epidermidis" : 	444655487, \
	    "S_agalactiae" : 	277523113, \
	    "S_mutans" : 		506964576, \
	    "S_pneumoniae" : 	76800567}

font_size      = 14
adjust         = 0.15
fontProperties = pub_figs.setup_math_fonts(font_size)
fig            = pub_figs.init_fig(x=28,y=20)
ax_x           = 0.07
ax_y           = 0.17
ax_w           = 0.025
ax_h           = 0.60

SMRT_base = "/hpc/users/beaulj01/projects/ebinning/pacbio20/all7_sets/aligns_to_refs"
SLR_base  = "/hpc/users/beaulj01/projects/ebinning/pacbio20/SLR/align_to_refs"

SMRT_even_yield_bams = {}
SLR_even_yield_bams  = {}
for n,org in enumerate(names):
	
	# if n==6:
	# 	break

	ax   = fig.add_axes([ax_x, ax_y, ax_w, ax_h])
	ax_x = ax_x + ax_w + 0.0187

	SLR_bam  = os.path.join(SLR_base, "%s.sorted.bam" % org_fns_SLR[org])
	SMRT_bam = os.path.join(SMRT_base, org_fns_SMRT[org], "%s.sorted.bam" % org_fns_SMRT[org])

	min_idx      = np.argmin([SMRT_cov[org], SLR_cov[org]])
	target_yield = [SMRT_cov[org], SLR_cov[org]][min_idx]

	if min_idx==0:
		# Downsample SLR
		tech                      = "SLR"
		target_frac               = round(float(target_yield) / SLR_cov[org], 4)
		down_bam_fn               = "%s.%s.down.%s.bam" % (SLR_bam.split(".sorted")[0], tech, target_frac)
		SLR_even_yield_bams[org]  = down_bam_fn
		SMRT_even_yield_bams[org] = os.path.join(SMRT_base, org_fns_SMRT[org], "%s.sorted.bam" % org_fns_SMRT[org])
	else:
		# Downsample SMRT
		tech                      = "SMRT"
		target_frac               = round(float(target_yield) / SMRT_cov[org], 4)
		down_bam_fn               = "%s.%s.down.%s.bam" % (SMRT_bam.split(".sorted")[0], tech, target_frac)
		SLR_even_yield_bams[org]  = os.path.join(SLR_base, "%s.sorted.bam" % org_fns_SLR[org])
		SMRT_even_yield_bams[org] = down_bam_fn

	vals = [[],[]]
	for j,tech in enumerate(["SLR", "SMRT"]):
		if tech=="SLR":
			bam       = SLR_even_yield_bams[org]
			bamfile   = pysam.AlignmentFile(os.path.join(SLR_base, bam), "rb")
		else:
			bam       = SMRT_even_yield_bams[org]
			bamfile   = pysam.AlignmentFile(os.path.join(SMRT_base, org_fns_SMRT[org], bam), "rb")

		pileup = bamfile.pileup()

		# Build bit array to track non-zero cov
		cov_means   = []	
		window_vals = defaultdict(list)
		for p in pileup:
			pos  = p.pos
			
			# if pos==100000:
			# 	break

			cov  = p.n
			rpos = p.reference_pos
			rid  = p.reference_id
			if p.reference_id==1:
				pos += bamfile.lengths[0]
			window_id = pos/windowsize
			window_vals[window_id].append(cov)

		for i in range(np.sum(bamfile.lengths)/windowsize+1):
			if len(window_vals[i])==0:
				mean = 0
			elif len(window_vals[i])==1:
				mean = window_vals[i][0]
			else:
				mean = np.mean(window_vals[i])
			pos  = i * windowsize
			cov_means.append(mean)
			# print tech, org, i, pos, mean
		vals[j] = cov_means
	make_boxplot( ax, vals, n)

	ax.set_xlim(-1,1)
	# ax.set_ylim(0,20)
	ax.set_xticklabels([labels[org]], rotation=45, ha="right")
	ax.set_xticks(np.arange(1))
	if n==0:
		ax.set_ylabel("Coverage")
	elif n==(len(names)-1):
		# draw temporary red and blue lines and use them to create a legend
		hB, = ax.plot([1,1],'b-')
		hR, = ax.plot([1,1],'r-')
		ax.legend((hB, hR),('SLR', 'SMRT'), loc='center left', bbox_to_anchor=(1, 0.5), frameon=False)
		hB.set_visible(False)
		hR.set_visible(False)

plt.savefig('SLR_vs_SMRT_cov_variance_box.png', dpi=300)