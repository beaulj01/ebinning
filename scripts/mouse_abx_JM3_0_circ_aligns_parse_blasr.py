import os,sys
import glob

out_fns = glob.glob("*.out")

# qname 0
# tname 1
# score 2
# pctsimilarity 3
# qstrand 4
# qstart 5
# qend 6
# qseqlength 7
# tstrand 8
# tstart 9
# tend 10
# tseqlength 11
# mapqv 12
# ncells 13
# clusterScore 14
# probscore 15
# numSigClusters 16

for fn in out_fns:
	for line in open(fn, "rb").xreadlines():
		qstart  = int(line.split()[5])
		aln_len = int(line.split()[6])
		seq_len = int(line.split()[7])
		# if qstart>0:
		print line.strip()