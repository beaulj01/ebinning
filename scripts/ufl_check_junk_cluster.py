import os,sys
from itertools import groupby
import numpy as np
from collections import Counter
import re

def fasta_iter( fasta_name ):
	fh = open(fasta_name)
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		header = header.next()[1:].strip()
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

def check_homopolymer( seq, homo_counter, total_bases ):
	match = re.findall(r'AAAAAA+|TTTTTT+|CCCCCC+|GGGGGG+', seq)
	for m in match:
		if m.find("A")>-1:
			homo_counter["A"] += 1
		elif m.find("T")>-1:
			homo_counter["T"] += 1
		elif m.find("C")>-1:
			homo_counter["C"] += 1
		elif m.find("G")>-1:
			homo_counter["G"] += 1
	total_bases += len(seq)
	return homo_counter, total_bases

rnames = set(np.loadtxt("left_cluster_rnames.txt", dtype="str"))
fasta  = "22314_filtered_subreads.fasta"
junk_homo_counter    = Counter()
junk_tot_bases       = 0
nonjunk_homo_counter = Counter()
nonjunk_tot_bases    = 0

for i,(name,seq) in enumerate(fasta_iter(fasta)):
	read = "/".join(name.split("/")[:2])
	if read in rnames:
		junk_homo_counter, junk_tot_bases = check_homopolymer(seq, junk_homo_counter, junk_tot_bases)
	else:
		nonjunk_homo_counter, nonjunk_tot_bases = check_homopolymer(seq, nonjunk_homo_counter, nonjunk_tot_bases)

print "========Left cluster homopolymers (6+)========"
print "A: %.5f per 100bp" % (float(junk_homo_counter["A"])/junk_tot_bases*100)
print "T: %.5f per 100bp" % (float(junk_homo_counter["T"])/junk_tot_bases*100)
print "C: %.5f per 100bp" % (float(junk_homo_counter["C"])/junk_tot_bases*100)
print "G: %.5f per 100bp" % (float(junk_homo_counter["G"])/junk_tot_bases*100)
print ""

print "======Everything else homopolymers (6+)======="
print "A: %.5f per 100bp" % (float(nonjunk_homo_counter["A"])/nonjunk_tot_bases*100)
print "T: %.5f per 100bp" % (float(nonjunk_homo_counter["T"])/nonjunk_tot_bases*100)
print "C: %.5f per 100bp" % (float(nonjunk_homo_counter["C"])/nonjunk_tot_bases*100)
print "G: %.5f per 100bp" % (float(nonjunk_homo_counter["G"])/nonjunk_tot_bases*100)