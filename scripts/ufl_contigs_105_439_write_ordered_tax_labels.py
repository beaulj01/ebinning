import os,sys
import numpy as np

contigs_tax_fn = "contigs.tax.unordered"
labels_digest  = np.loadtxt("contigs.labels.digest", dtype="str")
labels         = np.loadtxt("contigs.labels",        dtype="str")
names_digest   = np.loadtxt("contigs.names.digest",  dtype="str")
names          = np.loadtxt("contigs.names",         dtype="str")

contig_specs = {}
for line in open(contigs_tax_fn, "r").xreadlines():
	line = line.strip()
	name = line.split("\t")[0].split("|")[0]
	tax  = line.split("\t")[1]
	spec = " ".join(tax.split(";")[-1].split(" ")[:2])
	contig_specs[name] = "_".join(spec.split(" "))

f = open("contigs.kraken.digest", "w")
for i,name in enumerate(names_digest):
	if contig_specs.get(name) and contig_specs[name].find("dorei")<0:
		f.write("%s\n" % contig_specs[name])
	elif contig_specs.get(name) and contig_specs[name].find("dorei")>-1:
		f.write("Bacteroides_dorei_%s\n" % labels_digest[i])
	else:
		f.write("Unlabeled\n")
f.close()

f = open("contigs.kraken", "w")
for i,name in enumerate(names):
	if contig_specs.get(name) and contig_specs[name].find("dorei")<0:
		f.write("%s\n" % contig_specs[name])
	elif contig_specs.get(name) and contig_specs[name].find("dorei")>-1:
		f.write("Bacteroides_dorei_%s\n" % labels[i])
	else:
		f.write("Unlabeled\n")
f.close()