import os,sys
import numpy as np
import operator
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import scipy.stats as stats

control_kmer_header = np.loadtxt(sys.argv[1], dtype="str")
control_barcodes    = np.loadtxt(sys.argv[2], dtype="float")
case_kmer_header    = np.loadtxt(sys.argv[3], dtype="str")
case_barcodes       = np.loadtxt(sys.argv[4], dtype="float")

motifs = set(["AATCC-1", \
			 "AGATCC-2", \
			 "AGATCT-2", \
			 "GGATCC-2", \
			 "GGATCT-2", \
			 "GATGG-1",  \
			 "CCATC-2",  \
			 "GATC-1",   \
			 "AGATC-2",  \
			 "TGATC-2",  \
			 "CGAGC-2",  \
			 "GGAGC-2",  \
			 "CGATC-2",  \
			 "GGATC-2",  \
			 "CAGGAG-4"])

colors = plt.get_cmap('rainbow')(np.linspace(0, 1.0, len(motifs)))


control_means = {}
for j in range(control_barcodes.shape[1]):
	kmer                = control_kmer_header[j]
	control_means[kmer] = control_barcodes[:,j].mean()
	if kmer == plot_kmer:
		vals = control_barcodes[:,j]
		if len(vals)>10:
			kde        = stats.kde.gaussian_kde( vals )
			dist_space = np.linspace( min(vals), max(vals), 100 )
			plt.plot( dist_space, kde(dist_space), label="R. gnavus", linestyle="--")

case_vals_noSubtract = {}
case_vals            = {}
for j in range(case_barcodes.shape[1]):
	kmer                        = case_kmer_header[j]
	case_vals_noSubtract[kmer]  = case_barcodes[:,j].mean()
	case_barcodes[:,j]         -= control_means[kmer]
	case_vals[kmer]             = case_barcodes[:,j].mean()
	if kmer == plot_kmer:
		vals = case_barcodes[:,j]
		if len(vals)>10:
			kde        = stats.kde.gaussian_kde( vals )
			dist_space = np.linspace( min(vals), max(vals), 100 )
			plt.plot( dist_space, kde(dist_space), label="Gnoto mix", linestyle="-")

sorted_case_vals_noSubtract = sorted(case_vals_noSubtract.items(), key=operator.itemgetter(1), reverse=True)
for i,(kmer,mean) in enumerate(sorted_case_vals_noSubtract):
	if kmer in motifs:
		print "%s / %s %s %s" % (i, len(sorted_case_vals_noSubtract), kmer, mean)
		
print ""

sorted_case_vals = sorted(case_vals.items(), key=operator.itemgetter(1), reverse=True)
for i,(kmer,mean) in enumerate(sorted_case_vals):
	if kmer in motifs:
		print "%s / %s %s %s" % (i, len(sorted_case_vals), kmer, mean)

fn = "binning.barcode.minusControl"
np.savetxt(fn, case_barcodes, fmt="%.3f", delimiter="\t")

plt.legend(loc=2)
plt.xlabel("SMp")
plt.ylabel("Density")
plt.xlim([-3,4])
plt.savefig("SMp_%s_PDF.png" % plot_kmer)