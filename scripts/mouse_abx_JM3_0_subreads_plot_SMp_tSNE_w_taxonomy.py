import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import scipy
from scipy.stats import gaussian_kde
import sklearn.cluster as cluster
import pub_figs
import random
from pbcore.io.BasH5IO import BasH5Reader
from itertools import groupby
from collections import Counter

pub_figs.setup_math_fonts(font_size=24, font="Computer Modern Sans serif")

plot_fn  = "reads.SMp.gt20kb.tax.png"
data     = np.loadtxt("reads.SMp.gt20kb.2D",   dtype="float")
labels   = np.loadtxt("subreads.kraken.gt20kb", dtype="S40")
names    = np.loadtxt("reads.names.gt20kb",     dtype="str")
sizes    = np.zeros(len(names))+10

title    = "subread-level e/cBinning"

def fasta_iter( fasta_name ):
	fh = open(fasta_name)
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		header = header.next()[1:].strip()
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

for i,lab in enumerate(labels):
	labels[i] = lab.replace("_", " ")

lab_counter = Counter()
for i,label in enumerate(labels):
	lab_counter[label] += 1

for i,label in enumerate(labels):
	if lab_counter[label]<=2 or len(labels[i].split(" "))==1:
		labels[i] = "Unlabeled subread"

print "All points\t\t",     len(labels)
print "Unlabeled subread\t",len(labels[labels=="Unlabeled subread"])

fig                     = plt.figure(figsize=[15,12])
ax                      = fig.add_axes([0.1, 0.1, 0.6, 0.6])
# ax.axis("off")
pub_figs.remove_top_right_axes( ax )
lab_set   = set(labels)
label_list = list(lab_set)
label_list.sort()
colors    = plt.get_cmap('spectral')(np.linspace(0.0, 0.95, len(label_list)))
sizes[sizes<50000] = 100000
scaled_sizes = sizes**1.5 / max(sizes**1.5) * 1000
shapes    = ["o", "v", "^", "s", "D"]
res       = []
lab_hits  = Counter()
for k,target_lab in enumerate(label_list):
	idxs                  = [j for j,label in enumerate(labels) if label==target_lab]
	scaled_sizes_idx      = np.array(scaled_sizes)[idxs]
	X                     = data[idxs,0]
	Y                     = data[idxs,1]
	color                 = colors[k]
	lab_hits[target_lab] += len(idxs)
	for i,x in enumerate(X):
		res.append( (x, Y[i], target_lab, color, scaled_sizes_idx[i], shapes[k%len(shapes)]) ) 
np.random.shuffle(res) 
plotted_labs = set()
legend_plots = []
legend_labs  = []
for i,(x,y, target_lab, color, size, shape) in enumerate(res):
	#########
	if lab_hits[target_lab] > 50:
	#########
		if target_lab=="Unlabeled subread":
			shape = "*"
			color = "r"
		plot = ax.scatter(x,y, marker=shape, edgecolors=color, label=target_lab, facecolors="None", linewidth=1.2, alpha=0.8)
		if target_lab not in plotted_labs:
			plotted_labs.add(target_lab)
			legend_plots.append(plot)
			legend_labs.append(target_lab)
box     = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
leg_tup = zip(legend_labs,legend_plots)
leg_tup.sort(key=lambda x: x[0])
legend_labs, legend_plots = zip(*leg_tup)
leg = ax.legend(legend_plots, legend_labs, loc='center left', prop={'size':24}, bbox_to_anchor=(1, 0.5), frameon=False, scatterpoints=1)
for i in range(len(legend_labs)):
	leg.legendHandles[i]._sizes = [100]
xmin = min(data[:,0])
xmax = max(data[:,0])
ymin = min(data[:,1])
ymax = max(data[:,1])
ax.set_xlim([xmin-3, xmax+3])
ax.set_ylim([ymin-3, ymax+3])
plt.savefig(plot_fn)
