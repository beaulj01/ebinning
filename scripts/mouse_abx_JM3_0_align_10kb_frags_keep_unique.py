import os,sys
import subprocess
import glob
import multiprocessing

procs = int(sys.argv[1])

frag_fastas = glob.glob("./explode/*fa")
assembly    = "polished_assembly.renamed.c10K.fasta"
def run_OS_command( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts != 0:
		print "Failed command: %s" % CMD
	return sts, stdOutErr

def launch_pool( procs, funct, args ):
	p    = multiprocessing.Pool(processes=procs)
	try:
		results = p.map(funct, args)
		p.close()
		p.join()
	except KeyboardInterrupt:
		p.terminate()
	return results

def run_nucmer( args ):
	i   = args[0]
	tot = args[1]
	CMD = args[2]
	print "...nucmer %s / %s" % ((i+1), tot)
	sts,stdOutErr = run_OS_command(CMD)

def run_show_coords( args ):
	i   = args[0]
	tot = args[1]
	CMD = args[2]
	print "...show-coords %s / %s" % ((i+1), tot)
	sts,stdOutErr = run_OS_command(CMD)

args = []
for i,frag in enumerate(frag_fastas):
	prefix  = os.path.basename(frag).replace(".fa","")
	nuc_CMD = "nucmer -p %s %s %s" % (prefix, assembly, frag)
	args.append( (i,len(frag_fastas),nuc_CMD) )

results = launch_pool(procs, run_nucmer, args)

args = []
for i,frag in enumerate(frag_fastas):
	prefix  = os.path.basename(frag).replace(".fa","")
	sc_CMD = "show-coords -T -c %s.delta > %s.coords" % (prefix, prefix)
	args.append( (i,len(frag_fastas),sc_CMD) )

results = launch_pool(procs, run_show_coords, args)

