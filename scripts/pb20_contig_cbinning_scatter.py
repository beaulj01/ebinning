import sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import random

label_dict      =  {"S_mutans_UA159" :                       r"${S. mutans}$ UA159", \
		   			"L_monocytogenes_EGD_e" :                r"${L. monocytogenes}$ EGD-e", \
		   			"S_epidermidis_ATCC_12228" :             r"${S. epidermidis}$ ATCC 12228", \
		   			"L_gasseri_ATCC_33323" :                 r"${L. gasseri}$ ATCC 33323", \
		   			"P_acnes_KPA171202" :                    r"${P. acnes}$ KPA171202", \
		   			"S_aureus_subsp_aureus_USA300_TCH1516" : r"${S. aureus}$ subsp aureus USA300 TCH1516", \
		   			"N_meningitidis_MC58" :                  r"${N. meningitidis}$ MC58", \
		   			"P_aeruginosa_PAO1" :                    r"${P. aeruginosa}$ PAO1", \
					"H_pylori_26695" :                       r"${H. pylori}$ 26695", \
					"D_radiodurans_R1" :                     r"${D. radiodurans}$ R1", \
		   			"A_odontolyticus_ATCC_17982_Scfld021" :  r"${A. odontolyticus}$ ATCC 17982 Scfld021", \
		   			"B_vulgatus_ATCC_8482" :                 r"${B. vulgatus}$ ATCC 8482", \
		   			"E_faecalis_OG1RF" :                     r"${E. faecalis}$ OG1RF", \
		   			"E_coli_str_K_12_substr_MG1655" :        r"${E. coli}$ str K-12 substr MG1655", \
		   			"S_agalactiae_2603V_R" :                 r"${S. agalactiae}$ 2603V R", \
					"A_baumannii_ATCC_17978" :               r"${A. baumannii}$ ATCC 17978", \
					"R_sphaeroides_2_4_1" :                  r"${R. sphaeroides}$ 2.4.1", \
		   			"S_pneumoniae_TIGR4":                    r"${S. pneumoniae}$ TIGR4", \
		   			"C_beijerinckii_NCIMB_8052" :            r"${C. beijerinckii}$ NCIMB 8052", \
		   			"B_cereus_ATCC_10987" :                  r"${B. cereus}$ ATCC 10987"}

def scatterplot(results, labels, sizes, plot_fn, title):
	label_set = list(set(labels))
	label_set.sort()
	colors    = plt.get_cmap('spectral')(np.linspace(0, 1.0, len(label_set)+1))
	shapes    = ["o", "v", "^", "s", "D", "p", "d"]
	fig       = plt.figure(figsize=[15,12])
	ax        = fig.add_axes([0.1, 0.1, 0.6, 0.6])
	ax.axis("off")
	if len(sizes)>0:
		sizes[sizes<100000] = 100000
		scaled_sizes = sizes**1.5 / max(sizes**1.5) * 2000
	res       = []
	for k,target_lab in enumerate(label_set):
		if target_lab=="unknown":
			continue
		idxs             = [j for j,label in enumerate(labels) if label==target_lab]
		scaled_sizes_idx = np.array(scaled_sizes)[idxs]
		X                = results[idxs,0]
		Y                = results[idxs,1]
		color            = colors[k]
		for i,x in enumerate(X):
			res.append( (x, Y[i], target_lab, color, scaled_sizes_idx[i], shapes[k%len(shapes)]) ) 
	
	np.random.shuffle(res)
	plotted_labs = set()
	legend_plots = []
	legend_labs  = []
	for (x,y, target_lab, color, size, shape) in res:
		plot = ax.scatter(x,y, edgecolors=color, \
							   facecolors="None", \
							   marker=shape, \
							   label=label_dict[target_lab], \
							   alpha=0.8, \
							   s=size, \
							   lw=3)
		if label_dict[target_lab] not in plotted_labs:
			plotted_labs.add(label_dict[target_lab])
			legend_plots.append(plot)
			legend_labs.append(label_dict[target_lab])
	box = ax.get_position()
	ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
	leg_tup = zip(legend_labs,legend_plots)
	leg_tup.sort(key=lambda x: x[0])
	legend_labs, legend_plots = zip(*leg_tup)
	leg = ax.legend(legend_plots, legend_labs, loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':18}, scatterpoints=1,frameon=False)
	for i in range(len(legend_labs)):
		leg.legendHandles[i]._sizes = [200]
	# ax.set_title(title)
	xmin = min(results[:,0])
	xmax = max(results[:,0])
	ymin = min(results[:,1])
	ymax = max(results[:,1])
	ax.set_xlim([xmin-5, xmax+5])
	ax.set_ylim([ymin-5, ymax+5])
	plt.savefig(plot_fn)

results = np.loadtxt(sys.argv[1], dtype="float")
labels  = np.loadtxt(sys.argv[2], dtype="str")
sizes   = np.loadtxt(sys.argv[3], dtype="int")

plot_fn = "pub.contigs.comp.png"
title   = "contig-level cBinning"

scatterplot(results, labels, sizes, plot_fn, title)