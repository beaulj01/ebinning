import os,sys
import numpy as np
import glob
from Bio import SeqIO

beds_fn = glob.glob("/hpc/users/beaulj01/projects/ebinning/mouse_abx/JM3/0/10kb/contig_binning/rerun_motif_discov/debug/box_assemblies/box*/data/coverage.bed")

for bed_fn in beds_fn:
	box = bed_fn.split(os.sep)[-3]

	vals    = []
	table   = np.loadtxt(bed_fn, dtype="str", delimiter="\t", skiprows=1)
	contigs = list(set(table[:,0]))
	
	for contig in contigs:
		contig_table = table[table[:,0]==contig]
		covs         = map(float, contig_table[:,4])
		length       = max(map(int, contig_table[:,2]))
		vals.append( (box, contig, np.mean(covs), length) )

	vals.sort(key=lambda x: x[3], reverse=True)
	for val in vals:
		box    = val[0]
		contig = val[1]
		cov    = val[2]
		length = val[3]
		print "%s\t%s\t%.2f\t%s" % (box,contig,cov,length)

	print""