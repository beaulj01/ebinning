import os,sys
import glob
import subprocess

def run_OS_command( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts != 0:
		print "Failed command: %s" % CMD
	return sts, stdOutErr

uchimuras = glob.glob(os.path.join("mouse_smrt_12_refs", "*.fasta"))

for uchimura in uchimuras:
	uch = uchimura.split("/")[1].split(".")[0]
	for box_id in range(1,10):
		compare   = "%s_vs_box%s" % (uch, box_id)
		box_fasta = "box%s_gt100kb.fasta" % box_id
		CMD1 = "nucmer %s %s" % (uchimura, box_fasta)
		CMD2 = "delta-filter -m out.delta > out.delta.m"
		sts,stdOutErr = run_OS_command(CMD1)
		sts,stdOutErr = run_OS_command(CMD2)
		
		print compare, os.path.getsize("out.delta.m")


		if os.path.getsize("out.delta.m")>500:
			CMD3 = "./my_mummerplot -color -layout out.delta.m --png -t %s -p %s" % (compare, compare)
			sts,stdOutErr = run_OS_command(CMD3)

		to_rm = glob.glob("*.filter")
		for f in to_rm:
			os.remove(f)
		to_rm = glob.glob("*.*plot")
		for f in to_rm:
			os.remove(f)
		to_rm = glob.glob("*.gp")
		for f in to_rm:
			os.remove(f)