import os,sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
from matplotlib.colors import LogNorm

fn = sys.argv[1]

x = []
y = []
for line in open(fn,'r').xreadlines():
	line = line.strip()
	x.append(float(line.split()[0]))
	y.append(float(line.split()[1]))

heatmap, xedges, yedges = np.histogram2d(x, y, bins=100)
extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]

H,xedges,yedges = np.histogram2d(x,y,100, normed=False)
X,Y = np.meshgrid(xedges,yedges)
plt.imshow(H, extent=extent, origin="lower")#, norm=LogNorm())

# plt.clf()
# plt.imshow(heatmap, norm=LogNorm(), extent=extent, origin="lower")
# plt.colorbar()
plt.savefig("density_heatmap.png")