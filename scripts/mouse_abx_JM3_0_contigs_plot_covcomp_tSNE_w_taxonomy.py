import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import scipy
from scipy.stats import gaussian_kde
import sklearn.cluster as cluster
import pub_figs
import random
from pbcore.io.BasH5IO import BasH5Reader
from itertools import groupby
from collections import Counter

pub_figs.setup_math_fonts(font_size=24, font="Computer Modern Sans serif")

def scatterplot(results, labels, plot_fn, title, contig_names, extra, sizes):
	label_set = list(set(labels))
	label_set.sort()
	colors    = plt.get_cmap('spectral')(np.linspace(0, 0.9, len(label_set)))
	sizes[sizes<50000] = 100000
	scaled_sizes = sizes**1.5 / max(sizes**1.5) * 1000
	shapes    = ["o", "v", "^", "s", "D"]
	fig       = plt.figure(figsize=[15,12])
	ax        = fig.add_axes([0.1, 0.1, 0.6, 0.6])
	res       = []
	for k,target_lab in enumerate(label_set):
		# if target_lab == "unknown":
		# 	continue
		idxs  = [j for j,label in enumerate(labels) if label==target_lab]
		scaled_sizes_idx = np.array(scaled_sizes)[idxs]
		X     = results[idxs,0]
		Y     = results[idxs,1]
		color = colors[k]
		for i,x in enumerate(X):
			res.append( (x, Y[i], target_lab, scaled_sizes_idx[i], color, shapes[k%len(shapes)]) ) 
	
	np.random.shuffle(res) 
	plotted_labs = set()
	legend_plots = []
	legend_labs  = []
	for (x,y, target_lab, size, color, shape) in res:
		if target_lab=="Unlabeled":
			shape = "*"
			color = "gray"
		else:
			target_lab = r"$%s$" % target_lab
		plot = ax.scatter(x,y, marker=shape, s=size , edgecolors=color, label=target_lab, facecolors="None", linewidth=1.2, alpha=0.8)
		if target_lab not in plotted_labs:
			plotted_labs.add(target_lab)
			legend_plots.append(plot)
			legend_labs.append(target_lab)
	box = ax.get_position()
	ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
	leg_tup = zip(legend_labs,legend_plots)
	leg_tup.sort(key=lambda x: x[0])
	
	# Put "Unlabeled" at end of legend
	unlabeled = [tup for tup in leg_tup if tup[0]=="Unlabeled"][0]
	leg_tup.remove(unlabeled)
	leg_tup.append(unlabeled)

	legend_labs, legend_plots = zip(*leg_tup)
	leg = ax.legend(legend_plots, legend_labs, loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':18}, frameon=False, scatterpoints=1)
	for i in range(len(legend_labs)):
		leg.legendHandles[i]._sizes = [200]
	minx = min(map(lambda x: x[0], res))
	maxx = max(map(lambda x: x[0], res))
	miny = min(map(lambda x: x[1], res))
	maxy = max(map(lambda x: x[1], res))
	ax.set_ylim([-100,105])
	pub_figs.remove_top_right_axes(ax)

	plt.savefig(plot_fn, dpi=300)

results    = np.loadtxt("contigs.cov_comp.2D", dtype="float")
sizes      = np.loadtxt("contigs.lengths", dtype="int")
labels     = np.loadtxt("contigs.kraken",  dtype="str")
names      = np.loadtxt("contigs.names",   dtype="str")
extra      = np.array([]) 
motifs     = np.loadtxt("ordered_motifs.txt", dtype="str")

minlen = 5000

results    = results[sizes>minlen,:]
labels     = labels[sizes>minlen]
names      = names[sizes>minlen]
sizes      = sizes[sizes>minlen]

# Remove the digested subcontigs
results    = results[sizes!=50000,:]
labels     = labels[sizes!=50000]
names      = names[sizes!=50000]
sizes      = sizes[sizes!=50000]

plot_fn    = "contigs.cov_comp.tax.png"
title      = ""

for i,lab in enumerate(labels):
	labels[i] = lab.replace("_", " ")

lab_counter = Counter()
for i,label in enumerate(labels):
	label     = label.strip("[")
	label     = label.strip("]")
	labels[i] = label
	lab_counter[label] += 1

for i,label in enumerate(labels):
	# if lab_counter[label]<=2 or len(labels[i].split(" "))==1:
	# if lab_counter[label]<5 or label=="Unlabeled":
	label_sum = np.sum(sizes[labels==label])
	if lab_counter[label]<5 and label_sum<100000:
		labels[i] = "Unlabeled"

scatterplot(results, labels, plot_fn, title, names, extra, sizes)