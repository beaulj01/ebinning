import os,sys
import numpy as np

blastn = np.loadtxt(sys.argv[1], dtype="str", delimiter="\t")

min_sfrac = 0.75
min_ident = 60.0

poss_CTs = ["box1_unitig_9", \
			"box1_unitig_6", \
			"box1_unitig_4", \
			"box3_unitig_2", \
			"box5_unitig_5", \
			"box6_unitig_11", \
			"box8_unitig1"]

qseqid   = 0
sseqid   = 1
pident   = 2
length   = 3
qlen     = 4
qstart   = 5
qend     = 6
slen     = 7
sstart   = 8
send     = 9
mismatch = 10
gapopen  = 11
evalue   = 12
bitscore = 13

for i in range(blastn.shape[0]):
	frac_q = float(blastn[i,length]) / float(blastn[i,qlen])
	frac_s = float(blastn[i,length]) / float(blastn[i,slen])
	if frac_s > min_sfrac:
		if float(blastn[i,pident]) > min_ident:
			print "%.2f\t%.2f\t%s" % (frac_q, frac_s, "\t".join(blastn[i,:]))