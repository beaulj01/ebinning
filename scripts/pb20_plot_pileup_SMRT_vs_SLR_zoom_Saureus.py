import pysam
import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from collections import defaultdict
import numpy as np
import pub_figs
import itertools
import operator
import subprocess
import matplotlib.gridspec as gridspec

########################
bounds = [1400000,1440000]
########################

def run_OS( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts != 0:
		print "Failed command: %s" % CMD
	return sts, stdOutErr

def get_tot_bases( bam ):
	f      = pysam.AlignmentFile(bam, "rb")
	pileup = f.pileup()
	return np.sum([p.n for p in pileup])

def downsample_bam( orig_fn, target_frac, tech, seed=1 ):
	down_fn         = "%s.%s.down.%s.bam" % (orig_fn.split(".sorted")[0], tech, target_frac)
	target_frac_str = str(target_frac).split(".")[1]
	CMD             = "samtools view -s %s.%s -h -b %s > %s" % (seed, target_frac_str, orig_fn, down_fn)
	print CMD
	if not os.path.exists(down_fn):
		sts, stdOutErr  = run_OS( CMD )
	if not os.path.exists(down_fn+".bai"):
		pysam.index(down_fn, "%s.bai" % down_fn)
	
	orig_y = get_tot_bases(orig_fn)
	down_y = get_tot_bases(down_fn)
	
	print "downsampling:",tech, "frac:",target_frac, "orig_yield:",orig_y, "new_yield:",down_y, orig_fn.split(os.sep)[-1]
	return down_fn, down_y

def format_axes( ax, bounds=[0,1000000000] ):
	ax.set_xlim([(bounds[0]-2000),(bounds[1]+2000)])
	tick_labs  = []
	for n in range(bounds[0],bounds[1]+5000, 5000):
		tick_labs.append( "%.3f" % (float(n)/1000000) )
	ax.set_xticks(range(bounds[0],bounds[1]+5000, 5000))
	ax.set_xticklabels(tick_labs, rotation=45, ha="right")

org_fns_SLR  = {"S_aureus":    "S_aureus_subsp_aureus_USA300_TCH1516"} 

org_fns_SMRT = {"S_aureus":    "S_aureus_subsp_aureus_USA300_TCH1516"}

orgs = org_fns_SMRT.keys()
orgs.sort()

SLR_cov  = {"S_aureus" : 	61148568}

SMRT_cov = {"S_aureus" : 	616183706}

SMRT_base = "/hpc/users/beaulj01/projects/ebinning/pacbio20/all7_sets/aligns_to_refs"
SLR_base  = "/hpc/users/beaulj01/projects/ebinning/pacbio20/SLR/align_to_refs"

SMRT_even_yield_bams = {}
SLR_even_yield_bams  = {}
print "Downsampling to match yields..."
for i,org in enumerate(orgs):
	i +=1
	
	SLR_bam  = os.path.join(SLR_base, "%s.sorted.bam" % org_fns_SLR[org])
	SMRT_bam = os.path.join(SMRT_base, org_fns_SMRT[org], "%s.sorted.bam" % org_fns_SMRT[org])
	
	min_idx      = np.argmin([SMRT_cov[org], SLR_cov[org]])
	target_yield = [SMRT_cov[org], SLR_cov[org]][min_idx]

	if min_idx==0:
		# Downsample SLR
		tech                      = "SLR"
		target_frac               = round(float(target_yield) / SLR_cov[org], 4)
		down_bam_fn,down_y        = downsample_bam( SLR_bam, target_frac, tech )
		SLR_cov[org]              = down_y
		SLR_even_yield_bams[org]  = down_bam_fn
		SMRT_even_yield_bams[org] = os.path.join(SMRT_base, org_fns_SMRT[org], "%s.sorted.bam" % org_fns_SMRT[org])
	else:
		# Downsample SMRT
		tech                      = "SMRT"
		target_frac               = round(float(target_yield) / SMRT_cov[org], 4)
		down_bam_fn,down_y        = downsample_bam( SMRT_bam, target_frac, tech )
		# Update yield with downsampled value
		SMRT_cov[org]             = down_y
		SLR_even_yield_bams[org]  = os.path.join(SLR_base, "%s.sorted.bam" % org_fns_SLR[org])
		SMRT_even_yield_bams[org] = down_bam_fn

def get_window_covs(bamfile):
	pileup = bamfile.pileup()
	# Build bit array to track non-zero cov
	pos_array        = np.zeros(np.sum(bamfile.lengths))
	x                = []
	y                = []	
	windowsize       = 1
	window_vals      = defaultdict(list)
	for p in pileup:
		pos  = p.pos
		cov  = p.n
		rpos = p.reference_pos
		rid  = p.reference_id
		if p.reference_id==1:
			pos += bamfile.lengths[0]
		window_id = pos/windowsize
		window_vals[window_id].append(cov)
		# print rid, pos, window_id, cov
		if cov>0:
			pos_array[pos] = 1

	cov_pct   = 100*float(pos_array.sum()) / np.sum(bamfile.lengths)


	# Compile list of no-coverage intervals
	no_cov_intervals = []
	no_cov_blocks    = [[i for i,value in it] for key,it in \
						itertools.groupby(enumerate(pos_array), key=operator.itemgetter(1)) if key==0]
	for block in no_cov_blocks:
		no_cov_intervals.append( (block[0], block[-1]) )

	for i in range(np.sum(bamfile.lengths)/windowsize+1):
		if len(window_vals[i])==0:
			mean = 0
		elif len(window_vals[i])==1:
			mean = window_vals[i][0]
		else:
			mean = np.mean(window_vals[i])
		pos  = i * windowsize
		# print i, pos, mean
		x.append(pos)
		y.append(mean)

	return x,y,no_cov_intervals

print "Computing percent covered fractions..."
for i,org in enumerate(orgs):
	i +=1 
	
	font_size      = 25
	adjust         = 0.15
	# fontProperties = pub_figs.setup_math_fonts(font_size)
	fig            = pub_figs.init_fig(x=15,y=10)
	
	ax_x           = 0.1
	ax_y           = 0.2
	ax_w           = 0.6
	ax_h           = 0.18
	ax   = fig.add_axes([ax_x, ax_y, ax_w, ax_h])
	ax.hold(True)

	# SLR
	bam       = SLR_even_yield_bams[org]
	bamfile   = pysam.AlignmentFile(os.path.join(SLR_base, bam), "rb")
	tot_bases = SLR_cov[org]
	x_SLR,y_SLR,no_cov_intervals = get_window_covs(bamfile)
	ax.plot(x_SLR, y_SLR, linestyle="-", color="r", linewidth=3)
	for x in no_cov_intervals:
		ax.axvspan(x[0], x[1], color='pink')
	format_axes( ax, bounds )
	
	# SMRT
	bam       = SMRT_even_yield_bams[org]
	bamfile   = pysam.AlignmentFile(os.path.join(SMRT_base, org_fns_SMRT[org], bam), "rb")
	tot_bases = SMRT_cov[org]
	x_SMRT,y_SMRT,no_cov_intervals = get_window_covs(bamfile)
	ax.plot(x_SMRT, y_SMRT, linestyle="-", color="b", linewidth=3)
	format_axes( ax, bounds )

	maxy = max(y_SLR[bounds[0]:bounds[1]])
	ax.set_ylim([0,(maxy+0.1*maxy)])
	ax.set_ylabel("Coverage")
	ax.set_xlabel("Genome position (Mb)")
	pub_figs.remove_top_right_axes(ax)
	ax.tick_params(axis='both', direction='out')
	ax.set_yticks(range(0,80,20))
	ax.set_yticklabels(map(lambda x: str(x), range(0,80,20)))
	pub_figs.setup_math_fonts(font_size)

	# draw temporary red and blue lines and use them to create a legend
	hR,  = ax.plot([1,1],'r-', linewidth=3)
	hB,  = ax.plot([1,1],'b-', linewidth=3)
	hR1, = ax.plot([1,1], color="pink", linewidth=20)
	ax.legend((hR, hR1, hB ),('SLR', 'No SLR coverage', 'SMRT'), loc='center left', bbox_to_anchor=(1, 0.5), frameon=False, fontsize=font_size)
	hB.set_visible(False)
	hR.set_visible(False)
	hR1.set_visible(False)

	plot_fn = "%s.cov.zoom.%s_%s.png" % (org, bounds[0], bounds[1])
	plt.savefig(plot_fn, dpi=300)