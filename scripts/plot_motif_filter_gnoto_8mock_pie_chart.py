import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})
import numpy as np

true_motifs = {"AATCC-1":          ["btheta"],    \
			   "CCANNNNNNCAT-2":   ["btheta"],    \
			   "ATGNNNNNNTGG-0":   ["btheta"],    \
			   # "GGCANNNNNNNRTTT-3":["btheta"],    \
			   "GGCANNNNNNNATTT-3":["btheta"],    \
			   "GGCANNNNNNNGTTT-3":["btheta"],    \
			   # "AAAYNNNNNNNTGCC-2":["btheta"],    \
			   "AAACNNNNNNNTGCC-2":["btheta"],    \
			   "AAATNNNNNNNTGCC-2":["btheta"],    \
			   # "RGATCY-2":         ["btheta"],    \
			   "AGATCC-2":         ["btheta"],    \
			   "GGATCC-2":         ["btheta"],    \
			   "AGATCT-2":         ["btheta"],    \
			   "GGATCT-2":         ["btheta"],    \
			   # "CAYNNNNNRTG-1":    ["bvulgatus"], \
			   "CACNNNNNATG-1":    ["bvulgatus"], \
			   "CACNNNNNGTG-1":    ["bvulgatus"], \
			   "CATNNNNNATG-1":    ["bvulgatus"], \
			   "CATNNNNNGTG-1":    ["bvulgatus"], \
			   "GAAGNNNNNNNTCC-2": ["bvulgatus"], \
			   "GGANNNNNNNCTTC-2": ["bvulgatus"], \
			   # "GACNNNNNRGAC-1":   ["bcaccae"],   \
			   "GACNNNNNAGAC-1":   ["bcaccae"],   \
			   "GACNNNNNGGAC-1":   ["bcaccae"],   \
			   "CAGNNNNNGGA-1":    ["bcaccae", "bovatus"],   \
			   "GATGG-1":          ["bcaccae"],   \
			   "CCATC-2":          ["bcaccae"],   \
			   # "GACNNNNNNRTTG-1":  ["bcaccae"],   \
			   "GACNNNNNNATTG-1":  ["bcaccae"],   \
			   "GACNNNNNNGTTG-1":  ["bcaccae"],   \
			   # "CAAYNNNNNNGTC-2":  ["bcaccae"],   \
			   "CAACNNNNNNGTC-2":  ["bcaccae"],   \
			   "CAATNNNNNNGTC-2":  ["bcaccae"],   \
			   # "CGMAGG-3":         ["bcaccae"],   \
			   "CGAAGG-3":         ["bcaccae"],   \
			   "CGCAGG-3":         ["bcaccae"],   \
			   # "GATGNAG-5":        ["bovatus"],   \
			   "GATGAAG-5":        ["bovatus"],   \
			   "GATGCAG-5":        ["bovatus"],   \
			   "GATGGAG-5":        ["bovatus"],   \
			   "GATGTAG-5":        ["bovatus"],   \
			   "GATC-1":           ["bovatus", "ecoli"],   \
			   "TAANNNNNNCTTG-2":  ["bovatus"],   \
			   "CAAGNNNNNNTTA-2":  ["bovatus"],   \
			   # "WGATC-2":          ["cbolteae"],  \
			   "AGATC-2":          ["cbolteae"],  \
			   "TGATC-2":          ["cbolteae"],  \
			   "CTAAG-3":          ["cbolteae"],  \
			   # "GAYNNNNNNNTCGC-1": ["cbolteae"],  \
			   "GACNNNNNNNTCGC-1": ["cbolteae"],  \
			   "GATNNNNNNNTCGC-1": ["cbolteae"],  \
			   # "GCGANNNNNNNRTC-3": ["cbolteae"],  \
			   "GCGANNNNNNNATC-3": ["cbolteae"],  \
			   "GCGANNNNNNNGTC-3": ["cbolteae"],  \
			   # "SGAKC-2":          ["cbolteae"],  \
			   "CGAGC-2":          ["cbolteae"],  \
			   "CGATC-2":          ["cbolteae"],  \
			   "GGAGC-2":          ["cbolteae"],  \
			   "GGATC-2":          ["cbolteae"],  \
			   "CAGNNNNNCTG-1":    ["cbolteae"],  \
			   "GATC-1":           ["bovatus", "ecoli"],     \
			   "GCACNNNNNNGTT-2":  ["ecoli"],     \
			   "AACNNNNNNGTGC-1":  ["ecoli"],     \
			   "CAGGAG-4":         ["caero"]}

# called_motifs =     {"CCANNNNNGCAT-2":   "TV", \
# 					 "CAGGAG-4":         "TP", \
# 					 "GAGC-1":           "TV", \
# 					 "ATGNNNNNTTGG-0":   "TV", \
# 					 "GAGCA-1":          "TV", \
# 					 "CAGNNNNNGGAA-1":   "TV", \
# 					 "CCANNNNNTCAT-2":   "TV", \
# 					 "AGATCT-2":         "TP", \
# 					 "GGAG-2":           "TV", \
# 					 "ATGNNNNNNTGG-0":   "TP", \
# 					 "CACNNNNNATG-1":    "TP", \
# 					 "ACAGNNNNNGGA-2":   "TV", \
# 					 "CAATNNNNNNGTC-2":  "TP", \
# 					 "CCATC-2":          "TP", \
# 					 "ATGANNNNNTGG-0":   "TV", \
# 					 "AACNNNNNNGTGC-1":  "TP", \
# 					 "CAGNNNNNGGA-1":    "TP", \
# 					 "CAGNNNNNGGAT-1":   "TV", \
# 					 "GATC-1":           "TP", \
# 					 "AGATCC-2":         "TP", \
# 					 "AGCGNNNNNNCGGG-0": "FP", \
# 					 "ATGNNNNNATGG-0":   "TV", \
# 					 "GCACNNNNNNGTT-2":  "TP", \
# 					 "TCAGNNNNNGGA-2":   "TV", \
# 					 "CACNNNNNGTG-1":    "TP", \
# 					 "AATCC-1":          "TP", \
# 					 "CCATNNNNNGTG-2":   "TV", \
# 					 "CCANNNNNNCAT-2":   "TP", \
# 					 "GGAGC-2":          "TP", \
# 					 "ATGNNNNNNTGGA-0":  "TV", \
# 					 "GAGCT-1":          "TV", \
# 					 "GCAGNNNNNGGA-2":   "TV", \
# 					 "GGATCT-2":         "TP", \
# 					 "CCGANNNNNNGGCG-3": "FP", \
# 					 "GATGG-1":          "TP"}

called_motifs = {"CAGNNNNNGGA-1": "TP", \
				 "CAGGAG-4":      "TP", \
				 "GATC-1":        "TP", \
				 "AGATCC-2":      "TP", \
				 "GGAGC-2":       "TP", \
				 "GAGC-1":        "TV", \
				 "CACNNNNNATG-1": "TP", \
				 "CCANNNNNNCAT-2":"TP", \
				 "AGATCT-2":      "TP", \
				 "GGATCT-2":      "TP", \
				 "ATGNNNNNNTGG-0":"TP", \
				 "CCATC-2":       "TP", \
				 "AATCC-1":       "TP", \
				 "GATGG-1":       "TP"}

print len(called_motifs.keys()), len(set(true_motifs.keys())), len(set(called_motifs.keys()) & set(true_motifs.keys()))
print len([ (motif,m) for motif,m in called_motifs.items() if m=="TP"])
print len([ (motif,m) for motif,m in called_motifs.items() if m=="TV"])
print len([ (motif,m) for motif,m in called_motifs.items() if m=="FP"])

n_filtered = len(called_motifs)
n_TP       = len([ (motif,m) for motif,m in called_motifs.items() if m=="TP"])
n_TV       = len([ (motif,m) for motif,m in called_motifs.items() if m=="TV"])
n_FP       = len([ (motif,m) for motif,m in called_motifs.items() if m=="FP"])
# make a square figure and axes
plt.figure(1, figsize=(8,8))
ax = plt.axes([0.1, 0.05, 0.8, 0.75])

# The slices will be ordered and plotted counter-clockwise.
# labels = 'True motifs', 'False motifs', 'True motif variants'
labels = []
labels.append('True motifs:             N =%s (%.1f%%)' % (n_TP, (100*float(n_TP)/np.sum([n_TP, n_TV, n_FP]))))
labels.append('True motif variants: N = %s (%.1f%%)' % (n_TV, (100*float(n_TV)/np.sum([n_TP, n_TV, n_FP]))))
labels.append('False motifs:            N = %s (%.1f%%)' % (n_FP, (100*float(n_FP)/np.sum([n_TP, n_TV, n_FP]))))
# fracs = [n_TP, n_FP, n_TV]
fracs = [n_TP, n_TV, n_FP]
explode=(0, 0, 0)
patches, texts = ax.pie(fracs, explode=explode, shadow=True, colors=["deepskyblue","lightslategray","orangered"])
# for t in texts:
	# t.set_horizontalalignment('center')
# ax.legend(patches, labels, loc="lower left", prop={'size':24})
ax.legend(patches, labels, loc='lower center', bbox_to_anchor=(0.5, 0.9), prop={'size':24}, frameon=False)
# plt.title('Detected %s motifs' % np.sum([n_TP, n_TV, n_FP]), y=0.92)
# bottom_adjust=0.2
# left_adjust=0.5
# hspace=0.5
# wspace=0.4 
# plt.gcf().subplots_adjust(bottom=bottom_adjust, left=left_adjust, hspace=hspace, wspace=wspace)
# plt.gca().tight_layout()
pie_fn = "filtered_motifs_pie.png"
plt.savefig(pie_fn)