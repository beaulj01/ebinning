import os,sys
import numpy as np
from scipy import stats

matrix_fn = sys.argv[1]
out_fn    = sys.argv[2]

m = np.loadtxt(matrix_fn, dtype="float")

m_std = stats.mstats.zscore(m, axis=0)

np.savetxt(out_fn, m_std, fmt='%.3f', delimiter="\t")