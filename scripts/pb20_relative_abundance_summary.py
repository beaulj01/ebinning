import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os,sys
import math
from collections import Counter
import operator
import pub_figs

baseline_readlist_fn             = sys.argv[1]
log_abundance_readlist_49cell_fn = sys.argv[2]

ax,fig = pub_figs.format_pb20_assembly_axes()

genome_sizes     = {"A_baumannii_ATCC_17978" :               3976747, \
		   			"A_odontolyticus_ATCC_17982_Scfld021" :  2391230, \
		   			"B_cereus_ATCC_10987" :                  5224283, \
		   			"B_vulgatus_ATCC_8482" :                 5163189, \
		   			"C_beijerinckii_NCIMB_8052" :            6000632, \
		   			"D_radiodurans_R1" :                     3060986, \
		   			"E_coli_str_K_12_substr_MG1655" :        4641652, \
		   			"E_faecalis_OG1RF" :                     2739625, \
		   			"H_pylori_26695" :                       1667867, \
		   			"L_gasseri_ATCC_33323" :                 1894360, \
		   			"L_monocytogenes_EGD_e" :                2944528, \
		   			"N_meningitidis_MC58" :                  2272360, \
		   			"P_acnes_KPA171202" :                    2560265, \
		   			"P_aeruginosa_PAO1" :                    6264404, \
		   			"R_sphaeroides_2_4_1" :                  4131542, \
		   			"S_agalactiae_2603V_R" :                 2160267, \
		   			"S_aureus_subsp_aureus_USA300_TCH1516" : 2872915, \
		   			"S_epidermidis_ATCC_12228" :             2499279, \
		   			"S_mutans_UA159" :                       2032925, \
		   			"S_pneumoniae_TIGR4" :                   2160842}

genomes_str      = [r"${S. mutans}$ UA159", \
		   			r"${L. monocytogenes}$ EGD-e", \
		   			r"${S. epidermidis}$ ATCC 12228", \
		   			r"${L. gasseri}$ ATCC 33323", \
		   			r"${P. acnes}$ KPA171202", \
		   			r"${S. aureus}$ subsp aureus USA300 TCH1516", \
		   			r"${N. meningitidis}$ MC58", \
		   			r"${P. aeruginosa}$ PAO1", \
					r"${H. pylori}$ 26695", \
					r"${D. radiodurans}$ R1", \
		   			r"${A. odontolyticus}$ ATCC 17982 Scfld021", \
		   			r"${B. vulgatus}$ ATCC 8482", \
		   			r"${E. faecalis}$ OG1RF", \
		   			r"${E. coli}$ str K-12 substr MG1655", \
		   			r"${S. agalactiae}$ 2603V R", \
					r"${A. baumannii}$ ATCC 17978", \
					r"${R. sphaeroides}$ 2.4.1", \
		   			r"${S. pneumoniae}$ TIGR4", \
		   			r"${C. beijerinckii}$ NCIMB 8052", \
		   			r"${B. cereus}$ ATCC 10987"]	

set5_movies = set(["m131212_215237_42141_c100615350310000001823112507181490_s1_p0", \
			       "m131213_004143_42141_c100615350310000001823112507181491_s1_p0", \
			       "m131213_033402_42141_c100615350310000001823112507181492_s1_p0", \
			       "m131213_062626_42141_c100615350310000001823112507181493_s1_p0", \
			       "m131213_091821_42141_c100615350310000001823112507181494_s1_p0", \
			       "m131213_121001_42141_c100615620070000001823112507181480_s1_p0", \
			       "m131213_150511_42141_c100615620070000001823112507181481_s1_p0", \
			       "m131213_175715_42141_c100615620070000001823112507181482_s1_p0"])

allsets_baseline_tot_bases = Counter()
set5_tot_bases    = Counter()
for line in open(baseline_readlist_fn, "r").xreadlines():
	line     =     line.strip()
	readname =     line.split()[0]
	spec     =     line.split()[1]
	readlen  = int(line.split()[2])
	movie    = readname.split("/")[0]
	allsets_baseline_tot_bases[spec] += readlen
	if movie in set5_movies:
		# H. pylori -- target 35x (58375345bp of sequence)
		if spec == "H_pylori_26695" and set5_tot_bases[spec] > 58375345:
			continue
		set5_tot_bases[spec] += readlen

allsets_log_tot_bases = Counter()
for line in open(log_abundance_readlist_49cell_fn, "r").xreadlines():
	line     =     line.strip()
	readname =     line.split()[0]
	spec     =     line.split()[1]
	readlen  = int(line.split()[2])
	allsets_log_tot_bases[spec] += readlen

# Calculate the baseline relative abundances
allsets_baseline_cov = {}
allsets_log_cov      = {}
set5_baseline_cov    = {}
for s in genome_sizes.keys():
	allsets_baseline_cov[s] = float(allsets_baseline_tot_bases[s]) / genome_sizes[s]
	allsets_log_cov[s]      = float(allsets_log_tot_bases[s])      / genome_sizes[s]
	set5_baseline_cov[s]    = float(set5_tot_bases[s])             / genome_sizes[s]

sorted_allsets_baseline_cov = sorted(allsets_baseline_cov.items(), key=operator.itemgetter(1), reverse=True)
rank          = []
xlabs         = []
Y_49_baseline = []
Y_49_log      = []
Y_8_baseline  = []
for i,(s,c) in enumerate(sorted_allsets_baseline_cov):
	rank.append(i+1)
	xlabs.append(genomes_str[i])
	Y_49_baseline.append(allsets_baseline_cov[s])
	Y_49_log.append(allsets_log_cov[s])
	Y_8_baseline.append(set5_baseline_cov[s])
	print i, s, allsets_baseline_cov[s], allsets_log_cov[s], set5_baseline_cov[s]

ax.plot(rank, Y_49_baseline, marker="o", markersize=15, linestyle="-", color="r",      label="49 SMRT cells")
ax.plot(rank,Y_49_log,       marker="o", markersize=15, linestyle="-", color="orange", label="49 SMRT cells (log-curve abundances)")
ax.plot(rank,Y_8_baseline,   marker="o", markersize=15, linestyle="-", color="y",      label="8  SMRT cells")

ax.set_ylabel("Genomic coverage")
ax.set_xticks(range(1,22))
ax.set_xticklabels(xlabs, rotation=45, ha="right")
leg = ax.legend(loc='lower center', bbox_to_anchor=(0.5, 1.0), numpoints=1, frameon=False)
pub_figs.remove_top_right_axes(ax)
plt.grid()
plt.savefig("abundance_summary_49_baseline_log_and_8_baseline.png")