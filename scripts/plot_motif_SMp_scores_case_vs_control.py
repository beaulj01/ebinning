import os,sys
import numpy as np
import operator
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import scipy.stats as stats
import math
import operator
import heapq

control_kmer_header = np.loadtxt(sys.argv[1], dtype="str")
control_barcodes    = np.loadtxt(sys.argv[2], dtype="float")
case_kmer_header    = np.loadtxt(sys.argv[3], dtype="str")
case_barcodes       = np.loadtxt(sys.argv[4], dtype="float")

subsample = 1000

target_motifs = set(["AATCC-1", \
			 "AGATCC-2", \
			 "AGATCT-2", \
			 "GGATCC-2", \
			 "GGATCT-2", \
			 "GATGG-1",  \
			 "CCATC-2",  \
			 "GATC-1",   \
			 "AGATC-2",  \
			 "TGATC-2",  \
			 "CGAGC-2",  \
			 "GGAGC-2",  \
			 "CGATC-2",  \
			 "GGATC-2",  \
			 "CAGGAG-4"])

FIG_SIZE = [15,15]
plt.figure(figsize=FIG_SIZE)
# dims = int(math.ceil(math.sqrt(len(target_motifs))))
dims = 5

def get_rank_sum(case, control):
	z,p = stats.ranksums(case, control)
	return z,p

ranksums     = {}
control_vals = {}
case_vals    = {}
for motif in control_kmer_header:
	w                   = list(control_kmer_header).index(motif)
	control_vals[motif] = [val for val in control_barcodes[:,w] if val!=0.0]
	control_vals[motif] = np.random.choice(control_vals[motif], subsample)

	z                = list(case_kmer_header).index(motif)
	case_vals[motif] = [val for val in case_barcodes[:,z] if val!=0.0]
	case_vals[motif] = np.random.choice(case_vals[motif], subsample)

	z,p             = get_rank_sum(case_vals[motif], control_vals[motif])
	ranksums[motif] = (z,p)
	
pos_z        = dict([(motif, p) for motif,(z,p) in ranksums.items() if z>0])
sorted_pos_z = sorted(pos_z.items(), key=operator.itemgetter(1))

fn = "ranked_motifs.out"
f  = open(fn, "w")
for i,(motif,p) in enumerate(sorted_pos_z):
	f.write("%s %s\n" % (motif,p))

	if motif in target_motifs:
		print "%s / %s %s %s" % (i, len(ranksums), motif, p)

	if i<(dims*dims):
		ax = plt.subplot(dims,dims,i+1)
		if motif in target_motifs:
			c="r"
		else:
			c="k"

		if len(control_vals[motif])>10:
			kde        = stats.kde.gaussian_kde( control_vals[motif] )
			dist_space = np.linspace( min(control_vals[motif]), max(control_vals[motif]), 100 )
			lab        = "R. gnavus"
			ax.plot( dist_space, kde(dist_space), label=lab, color=c, linestyle="--")
		
		if len(case_vals[motif])>10:
			kde        = stats.kde.gaussian_kde( case_vals[motif] )
			dist_space = np.linspace( min(case_vals[motif]), max(case_vals[motif]), 100 )
			lab        = "Synthetic mix"
			ax.plot( dist_space, kde(dist_space), label=lab, color=c, linestyle="-")
		ax.set_title(motif)
		ax.legend(loc=2, prop={'size':6})
		ax.set_xlim([-2,3])

f.close()
plt.savefig("SMp_all_motifs_PDF.png")