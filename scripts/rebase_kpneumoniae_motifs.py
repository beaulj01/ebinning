import os,sys
import numpy as np

kpneumoniae = np.loadtxt("pb_acc_org_mapping.k_pneumoniae.txt", dtype="str", delimiter="\t")

all_motifs = set()
org_motifs = {}
for i in range(kpneumoniae.shape[0]):
	strain = kpneumoniae[i,0]
	motifs = set(kpneumoniae[i,1].split(" "))
	map(lambda motif: all_motifs.add(motif), motifs)
	n_motifs = len(all_motifs)
	org_motifs[strain] = motifs

all_motifs = list(all_motifs)
all_motifs.sort()

for i in range(kpneumoniae.shape[0]):
	strain     = kpneumoniae[i,0]
	motifs     = set(kpneumoniae[i,1].split(" "))
	org_array  = np.zeros(n_motifs)
	for j,motif in enumerate(all_motifs):
		if motif in motifs:
			org_array[j] = 1

	print strain
	print org_array
	print ""