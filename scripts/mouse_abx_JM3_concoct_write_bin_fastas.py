import os,sys
from Bio import SeqIO
import glob
import numpy as np
from collections import defaultdict
import shutil

contigs_bins  = dict(np.loadtxt(sys.argv[1], delimiter=",", dtype="str"))
contigs_fasta = "Contigs.fasta"

bins      = list(set(contigs_bins.values()))
if os.path.exists("bins"):
	shutil.rmtree("bins")
os.mkdir("bins")

f_handles   = {}
for bin_id in bins:
	bin_fa_fn         = "bins/bin_%s.fa" % bin_id
	f_handles[bin_id] = open(bin_fa_fn, "wb")

bin_records = defaultdict(list)
for record in SeqIO.parse(contigs_fasta, "fasta"):
	if contigs_bins.get(record.id):
		bin_id = contigs_bins[record.id]
		bin_records[bin_id].append(record)

for bin_id in bins:
	f = f_handles[bin_id]
	SeqIO.write(bin_records[bin_id], f, "fasta")
	f.close()