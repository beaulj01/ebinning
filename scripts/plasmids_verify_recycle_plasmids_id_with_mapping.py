import os,sys
import numpy as np
from collections import defaultdict,Counter
import pysam
import re

recyc_fn = sys.argv[1]
bam_fn   = sys.argv[2]

plasmid_edges = {}
edges_set     = set()
plasmid_names = []
id = 0
for line in open(recyc_fn, "rb").xreadlines():
	line = line.strip()
	if line[0]=="(":
		REGEX = r"EDGE_\d+_length_\d+_cov_\d+\.\d+"
		match = re.findall(REGEX, line)
		for m in match:
			#print id,m
			plasmid_edges[m] = id
			edges_set.add(m)
		id += 1
	elif line[:5]=="RNODE":
		plasmid_names.append(line)

plasmid_org_n = {}
for i in range(id):
	plasmid_org_n[i] = Counter()

bamfile = pysam.AlignmentFile(bam_fn, "rb")
for k,aln in enumerate(bamfile.fetch()):
	#if k==1000000:
	#	break
	if aln.mapq == 60:
		edge_name = aln.reference_name
		if edge_name in edges_set:
			read_org = aln.query_name.split("_from_")[1]
			plasmid_id = plasmid_edges[edge_name]
			plasmid_org_n[plasmid_id][read_org] += 1
for plasmid_id in plasmid_org_n.keys():
	print plasmid_id, plasmid_names[plasmid_id], plasmid_org_n[plasmid_id]		
		
