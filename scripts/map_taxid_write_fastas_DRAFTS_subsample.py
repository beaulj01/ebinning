import os,sys
import subprocess
import glob
from collections import Counter

def run_OS( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	return sts, stdOutErr

spec_counter = Counter()
fastas       = glob.glob("../*.fna")
for fasta in fastas:
	# uln_CMD  = "unlink %s" % fasta.split("/")[1]
	# sts,stdOutErr = run_OS(uln_CMD)
	# continue
	species = "_".join(fasta.split("_")[:2])
	spec_counter[species] += 1
	if spec_counter[species] <= 1:
		ln_CMD  = "ln -s %s" % fasta
		sts,stdOutErr = run_OS(ln_CMD)
		# print ln_CMD