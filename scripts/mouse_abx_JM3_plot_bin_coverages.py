import os,sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import random
import pub_figs
from collections import Counter,defaultdict
from Bio import SeqIO
import math

font_size = 18
pub_figs.setup_math_fonts(font_size=font_size, font="Computer Modern Sans serif")

cov_bed   = np.loadtxt("coverage.bed",    dtype="str", skiprows=1)
labels    = np.loadtxt("contigs.kraken",  dtype="str")
names     = np.loadtxt("contigs.names",   dtype="str")
lengths   = np.loadtxt("contigs.lengths", dtype="int")
min_len   = 100000

bin7_cov_divider = 250

size_dict   = dict(zip(names, lengths))
kraken_dict = dict(zip(names, labels))

for i,lab in enumerate(labels):
	labels[i] = lab.replace("_", " ")

lab_counter = Counter()
for i,label in enumerate(labels):
	labels[i] = label
	lab_counter[label] += 1

for i,label in enumerate(labels):
	label_sum = np.sum(lengths[labels==label])
	if lab_counter[label]<5 and label_sum<100000:
		labels[i] = "Unlabeled"

label_set = list(set(labels))
label_set.sort()
colors    = plt.get_cmap('spectral')(np.linspace(0, 0.9, len(label_set)))

col_map = dict(zip(label_set,colors))

def get_contig_mean_cov( name, cov_bed ):
	name      = name.split("|")[0]
	cov_names = cov_bed[:,0]
	mask      = cov_names==name
	covs      = cov_bed[mask,4].astype(float)
	# Ignore edges of contigs
	# covs      = covs[2:-2]
	return covs.mean()

box_covs  = defaultdict(list)
sizes     = []
bin7a     = []
bin7b     = []
for box_id in range(1,10):
	box_fasta = "contigs.box.%s.fasta" % box_id

	for Seq in SeqIO.parse(box_fasta, "fasta"):
		name = Seq.id.split("|")[0]
		size = size_dict[name]
		cov = get_contig_mean_cov( Seq.id, cov_bed)
		if size>=min_len:
			sizes.append(size)
			box_covs[box_id].append( (cov, size, name) )

		if box_id==7 and cov>bin7_cov_divider:
			bin7a.append(Seq)
		elif box_id==7 and cov<=bin7_cov_divider:
			bin7b.append(Seq)

SeqIO.write(bin7a, "contigs.box.7a.fasta", "fasta")
SeqIO.write(bin7b, "contigs.box.7b.fasta", "fasta")

for box_id in range(1,10):
	box_covs[box_id].sort(key=lambda x: x[1], reverse=True)


fig = plt.figure(figsize=[12,6])
ax  = fig.add_axes([0.11, 0.15, 0.6, 0.7])

plotted_labs = set()
legend_plots = []
legend_labs  = []
for box_id in range(1,10):
	for (cov,size,name) in box_covs[box_id]:
		scaled_size = size**1.5 / max(np.array(sizes)**1.5) * 2000
		print box_id, size, name, cov
		
		plot   = ax.scatter(box_id, cov, c=col_map[kraken_dict[name]], label=kraken_dict[name], s=scaled_size, linewidths=2, edgecolor='k', alpha=0.4)
		border = ax.scatter(box_id, cov, label=kraken_dict[name], s=scaled_size, linewidths=2, edgecolor='k', facecolors='none')
		
		if kraken_dict[name] not in plotted_labs:
			plotted_labs.add(kraken_dict[name])
			legend_plots.append( (plot) )
			legend_labs.append(kraken_dict[name])

	box_cov_vals = map(lambda x: x[0], box_covs[box_id])
	print box_id, np.mean(box_cov_vals), np.std(box_cov_vals)
	print ""

leg_tup = zip(legend_labs,legend_plots)
leg_tup.sort(key=lambda x: x[0])
legend_labs, legend_plots = zip(*leg_tup)
leg = ax.legend(legend_plots, legend_labs, loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':18}, frameon=False, scatterpoints=1)
for i in range(len(legend_labs)):
	leg.legendHandles[i]._sizes = [300]

ax.set_xlim([0,10])
ax.set_ylim([0,400])
ax.set_xticks(range(1,10,1))
ax.set_yticks(range(0,401,50))
pub_figs.remove_top_right_axes(ax)
# ax.minorticks_on()
ax.tick_params('both', length=10, width=2, which='major')
ax.tick_params('both', length=5, width=1, which='minor')
ax.tick_params(pad=10, labelsize=font_size)
ax.set_xlabel(r"Methylation bin ID", fontsize=font_size)
ax.set_ylabel('Contig coverage', fontsize=font_size)
plt.grid()

plt.savefig("scp_bin_contig_covs.png", dpi=300)