import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import scipy
from scipy.stats import gaussian_kde
import sklearn.cluster as cluster
import pub_figs
import random
from pbcore.io.BasH5IO import BasH5Reader
from itertools import groupby
from collections import Counter

pub_figs.setup_math_fonts(font_size=24, font="Computer Modern Sans serif")

data     = np.loadtxt("contigs.noBD.comp.2D", dtype="float")
labels   = np.loadtxt("contigs.noBD.labels",  dtype="S40")
names    = np.loadtxt("contigs.noBD.names",   dtype="str")
sizes    = np.loadtxt("contigs.noBD.lengths", dtype="int")
plot_fn  = "contigs.noBD.comp.tax.png"
title    = "contig-level cBinning (no B. dorei)"
contigs_tax_fn = "no_bdorei.contigs.unordered.labels"

def move_unknowns_to_bottom( leg_tup ):
	for tup in leg_tup:
		if tup[0]=="Unlabeled contig":
			unknown_tup = tup
	leg_tup.remove(unknown_tup)
	leg_tup.append(unknown_tup)
	return leg_tup

contig_specs = {}
for line in open(contigs_tax_fn, "r").xreadlines():
	line = line.strip()
	name = line.split("\t")[0].split("|")[0]
	tax  = line.split("\t")[1]
	spec = " ".join(tax.split(";")[-1].split(" ")[:2])
	contig_specs[name] = spec

def fasta_iter( fasta_name ):
	fh = open(fasta_name)
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		header = header.next()[1:].strip()
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

for i,name in enumerate(names):
	if contig_specs.get(name):
		labels[i] = contig_specs[name]
	else:
		labels[i] = "Unlabeled contig"

lab_counter = Counter()
for i,label in enumerate(labels):
	lab_counter[label] += 1

for i,label in enumerate(labels):
	if lab_counter[label]<=2 or len(labels[i].split(" "))==1:
		labels[i] = "Unlabeled contig"

labels = ["Eubacterium eligens" if x=="[Eubacterium] eligens" else x for x in labels]

print "All points\t\t",     len(labels)
print "Unlabeled contigs\t",len(labels[labels=="Unlabeled contig"])

fig                     = plt.figure(figsize=[15,12])
ax                      = fig.add_axes([0.1, 0.1, 0.6, 0.6])
# ax.axis("off")
pub_figs.remove_top_right_axes( ax )
lab_set   = set(labels)
label_list = list(lab_set)
label_list.sort()
colors    = plt.get_cmap('spectral')(np.linspace(0.0, 1.0, len(label_list)))
sizes[sizes<25000] = 25000
scaled_sizes = sizes**1.5 / max(sizes**1.5) * 1000
shapes    = ["o", "v", "^", "s", "D"]
res       = []
for k,target_lab in enumerate(label_list):
	idxs             = [j for j,label in enumerate(labels) if label==target_lab]
	scaled_sizes_idx = np.array(scaled_sizes)[idxs]
	X                = data[idxs,0]
	Y                = data[idxs,1]
	color            = colors[k]
	for i,x in enumerate(X):
		res.append( (x, Y[i], target_lab, color, scaled_sizes_idx[i], shapes[k%len(shapes)]) ) 
np.random.shuffle(res) 
plotted_labs = set()
legend_plots = []
legend_labs  = []
for i,(x,y, target_lab, color, size, shape) in enumerate(res):
	if target_lab=="Unlabeled contig":
		shape = "*"
		color = "r"
	plot = ax.scatter(x,y, marker=shape, s=size , edgecolors=color, label=target_lab, facecolors="None", linewidth=1.2, alpha=0.8)
	if target_lab not in plotted_labs:
		plotted_labs.add(target_lab)
		legend_plots.append(plot)
		legend_labs.append(target_lab)
box     = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
ax.set_ylim([-6,6])
leg_tup = zip(legend_labs,legend_plots)
leg_tup.sort(key=lambda x: x[0])
leg_tup = move_unknowns_to_bottom( leg_tup )
legend_labs, legend_plots = zip(*leg_tup)
leg = ax.legend(legend_plots, legend_labs, loc='center left', prop={'size':24}, bbox_to_anchor=(1, 0.5), frameon=False, scatterpoints=1)
for i in range(len(legend_labs)):
	leg.legendHandles[i]._sizes = [100]
plt.savefig(plot_fn)