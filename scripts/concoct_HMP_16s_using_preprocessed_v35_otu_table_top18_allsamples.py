import os,sys
import numpy as np
import shutil

N_SAMPLES = 64
table_fn  = "/hpc/users/beaulj01/projects/ebinning/concoct_integration/illumina_sim/20_mock/16s_abund/HMP_processed/otu_table_psn_v35.txt"
map_fn    = "/hpc/users/beaulj01/projects/ebinning/concoct_integration/illumina_sim/20_mock/16s_abund/HMP_processed/v35_map_uniquebyPSN.txt"
mapping   = np.loadtxt(map_fn,   dtype="str", delimiter="\t", skiprows=1)
table     = np.loadtxt(table_fn, dtype="str", delimiter="\t")

refs = ["EC_BAA_2196.labeled.fasta", \
		"EC_BAA_2215.labeled.fasta", \
		"EC_BAA_2440.labeled.fasta", \
		"NC_004663.1.fa", \
		"NC_008497.1.fa", \
		"NC_009614.1.fa", \
		"NC_010655.1.fa", \
		"NC_012781.1.fa", \
		"NC_014656.1.fa", \
		"NC_014833.1.fa", \
		"NC_014933.1.fa", \
		"NC_015164.1.fa", \
		"NC_015977.1.fa", \
		"NC_016776.1.fa", \
		"NC_017179.1.fa", \
		"NC_018221.1.fa", \
		"NC_018937.1.fa", \
		"NC_021042.1.fa"]

# QIIME v1.3.0-dev OTU table
#OTU ID 700114607       700114380       700114716       700114798       700114710       700114614       700114755       700114715       700114706       700114613       700114803       700114714       7001
# OTU_97.1        0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
# OTU_97.10       0       0       0       0       0       0       0       0       1       0       0       0       0       1       0       0       0       0       0       0       0       0       0       0
# OTU_97.100      0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
# OTU_97.1000     0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
# OTU_97.10000    0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
# OTU_97.10001    0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0


#SampleID       RSID    visitno sex     RUNCENTER       HMPbodysubsite  Mislabeled      Contaminated    Description
# 700013549       158013734       1       female  BCM     Stool   NA      NA      HMP_Human_metagenome_sample_700013549_from_subject_158013734__sex_female_
# 700014386       158398106       1       male    "BCM,BI"        Stool   NA      NA      HMP_Human_metagenome_sample_700014386_from_subject_158398106__sex_male_
# 700014403       158398106       1       male    "BCM,BI"        Saliva  NA      NA      HMP_Human_metagenome_sample_700014403_from_subject_158398106__sex_male_
# 700014409       158398106       1       male    "BCM,BI"        Tongue_dorsum   NA      NA      HMP_Human_metagenome_sample_700014409_from_subject_158398106__sex_male_
# 700014412       158398106       1       male    "BCM,BI"        Hard_palate     NA      NA      HMP_Human_metagenome_sample_700014412_from_subject_158398106__sex_male_
# 700014415       158398106       1       male    "BCM,BI"        Buccal_mucosa   NA      NA      HMP_Human_metagenome_sample_700014415_from_subject_158398106__sex_male_

# Get only the sample IDs corresponding to stool samples
stool_entries = mapping[:,5]=="Stool"
stool_ids     = np.array(mapping[stool_entries,0])

for i,line in enumerate(open(table_fn).xreadlines()):
	if i==1:
		break
line    = line.strip()
samples = np.array(line.split("\t")[1:])

stool_mask  = np.in1d(samples, stool_ids)
stool_table = table[:,1:][:,stool_mask]
stool_table = stool_table[:,:N_SAMPLES]

otu_all_samps = []
for j in range(stool_table.shape[0]):
	# Iterate over OTUs
	otu_tot = stool_table[j,:].astype(int).sum()
	otu_all_samps.append( otu_tot )

otu_all_samps = np.array(otu_all_samps)
sort_idx      = np.argsort(otu_all_samps)
top18_idx     = sort_idx[-18:][::-1]

outdir = "abundance_inputs_top18allsamples"
if os.path.exists(outdir):
	shutil.rmtree(outdir)
	os.mkdir(outdir)
else:
	os.mkdir(outdir)

n = len(refs)
for j in range(stool_table.shape[1]):
	# Iterate over samples
	fn = "mock_abund.%s.txt" % (j+1)
	f  = open(os.path.join(outdir, fn), "wb")
	samp_n       = stool_table[:,j].astype(int)
	abunds       = np.divide(samp_n[top18_idx], float(np.sum(samp_n[top18_idx])) )
	tot          = 0
	for i,ref in enumerate(refs):
		f.write("%s\t%.4f\n" % (ref, abunds[i]))
		tot += abunds[i]
	f.close()

