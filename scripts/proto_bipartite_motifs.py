import os,sys
import re
import random
from itertools import product
from collections import defaultdict

def sub_bases( motif ):
	subs = {"W":"[AT]",  \
			"S":"[CG]",  \
			"M":"[AC]",  \
			"K":"[GT]",  \
			"R":"[AG]",  \
			"Y":"[CT]",  \
			"B":"[CGT]", \
			"D":"[AGT]", \
			"H":"[ACT]", \
			"V":"[ACG]", \
			"N":"[ACGT]"}
	for symbol,sub in subs.iteritems():
		if motif.find(symbol) > -1:
			motif = motif.replace(symbol, sub)
	return motif

def rev_sub_bases( motif ):
	subs = {"[AT]":"W",  \
			"[CG]":"S",  \
			"[AC]":"M",  \
			"[GT]":"K",  \
			"[AG]":"R",  \
			"[CT]":"Y",  \
			"[CGT]":"B", \
			"[AGT]":"D", \
			"[ACT]":"H", \
			"[ACG]":"V", \
			"[ACGT]":"N"}
	for symbol,sub in subs.iteritems():
		if motif.find(symbol) > -1:
			motif = motif.replace(symbol, sub)
	return motif

def comp_motif( motif ):
	"""
	
	"""
	COMP = {"A":"T", \
			"T":"A", \
			"C":"G", \
			"G":"C", \
			"W":"S", \
			"S":"W", \
			"M":"K", \
			"K":"M", \
			"R":"Y", \
			"Y":"R", \
			"B":"V", \
			"D":"H", \
			"H":"D", \
			"V":"B", \
			"N":"N", \
			"X":"X", \
			"*":"*"}
	r_motif = []
	for char in motif:
		r_motif.append( COMP[char] )
	return "".join(r_motif)

def rev_comp_motif( motif ):
	"""
	
	"""
	COMP = {"A":"T", \
			"T":"A", \
			"C":"G", \
			"G":"C", \
			"W":"S", \
			"S":"W", \
			"M":"K", \
			"K":"M", \
			"R":"Y", \
			"Y":"R", \
			"B":"V", \
			"D":"H", \
			"H":"D", \
			"V":"B", \
			"N":"N", \
			"X":"X", \
			"*":"*"}
	rc_motif = []
	for char in motif[::-1]:
		rc_motif.append( COMP[char] )
	return "".join(rc_motif)

def find_motif_matches( mode, motif, ref_str, read_str, strand ):
	if mode == "cmp":
		if strand == 0:
			q_motif = sub_bases( rev_comp_motif(motif) )
		elif strand == 1:
			q_motif = sub_bases( comp_motif(motif) )
		matches_iter = re.finditer(q_motif, ref_str)
	elif mode == "bas":
		q_motif = sub_bases( rev_comp_motif(motif) )
		matches_iter = re.finditer(q_motif, read_str)

	matches_list = []
	for match in matches_iter:
		matches_list.append(match)
	return matches_list

def kmer_freq ( mode, read_str, opts ):
	"""
	"""
	k = opts.comp_kmer
	kmers = []
	for seq in product("ATGC",repeat=k):
		kmers.append( "".join(seq) )

	kmer_counts = Counter()
	for j in range( len(read_str)-(k-1) ):
		motif    = read_str[j:j+k]
		kmer_counts[motif] += 1

	# Combine forward and reverse complement motifs into one count
	combined_kmer = Counter()
	for kmer in kmers:
		kmer_rc = rev_comp_motif(kmer)
		if not combined_kmer.get(kmer_rc):
			combined_kmer[kmer] = kmer_counts[kmer] + kmer_counts[kmer_rc] + 1

	return combined_kmer

def walk_over_read_bipartite( bipartite_motifs, read_str, read_ipds ):
	"""
	Loop over each position in the read string, adding motifs as they
	are encountered in the walk.
	"""
	subread_ipds = defaultdict(list)
	mod_bases = ["A"]
	# Config: part1_len, Ns, part2_len, max_mod_pos
	bipart_config = [(3,5,3,2),(3,6,3,2)]
	for config in bipart_config:
		length = sum(config[:3])
		for j in range( len(read_str)-3 ):
			read_motif = read_str[j:j+length]
			ref_motif  = rev_comp_motif( read_motif )
			ipds       = read_ipds[j:j+length]

			if ref_motif.find("*") == -1 and ref_motif.find("X") == -1:
				for base in mod_bases:
					ref_indexes = [m.start() for m in re.finditer(base, ref_motif) if m.start() <= config[3]]
					for ref_index in ref_indexes:
						rc_index = len(ref_motif) - 1 - ref_index
						
						IPD = ipds[rc_index]

						bi_motif      = "".join( [ref_motif[:config[0]],"N"*config[1],ref_motif[-config[2]:]] )
						ref_motif_str = "%s-%s" % (bi_motif, ref_index)
						subread_ipds[ref_motif_str].append( IPD )
	return subread_ipds

bipartite_motifs     = set()
NUCS       = "ACGT"
mod_bases  = ["A"]
part1_lens = [3]
part2_lens = [3]
N_lens     = [5,6]
for part1_len in part1_lens:
	for part2_len in part2_lens:
		for seq1 in product("ATGC",repeat=part1_len):
			for seq2 in product("ATGC",repeat=part2_len):
				for N_len in N_lens:
					string = "".join(seq1) + ("N"*N_len) + "".join(seq2)
					for base in mod_bases:
						indexes = [m.start() for m in re.finditer(base, string)]
						valid_idx = [ind for ind in indexes if ind < part1_len]
						for index in valid_idx:
							motif = "%s-%s" % (string, index)
							bipartite_motifs.add(motif)
print "Done: %s possible bipartite motifs\n" % len(bipartite_motifs)

seq = "TGATAGTGATGTGATATACAAGTATATATAAGATGATAAGTGAGCAGATCTGCTTAGCGTGTGTGTATGAAGAAGATAGTGCTGAGGTCGCGGTACGTAGTTTGGAGGAGCGCAGTGGAGCATTGAGGAGAGAAGATGATGAAGGGAAAATAAAAAAATTAACCAAAAAATAAAACCTAAAAATCCTTTCATGTTTATTTTATCAAAAATATATTT"
read_ipds = random.sample(xrange(1000), len(seq))
ref_str   = None
strand    = None
subread_ipds = walk_over_read_bipartite( bipartite_motifs, seq, read_ipds )
