import os,sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import scipy.stats as stats
import math

fn = sys.argv[1]

plt.figure()
# for cov_thresh in range(20,121,20):
vals    = []
for line in open(fn).xreadlines():
	line      =       line.strip()
	ipd       = float(line.split("\t")[0])
	vals.append(ipd)

if len(vals)>10:
	kde        = stats.kde.gaussian_kde( vals )
	dist_space = np.linspace( min(vals), max(vals), 50 )
	# plt.plot( dist_space, kde(dist_space), label="sites >= %s" % cov_thresh)
	plt.plot( dist_space, kde(dist_space))

plt.legend(loc=2)
plt.xlabel("Control IPD")
plt.ylabel("Density")
# plt.title("50/50 strain 105/439 mixture")
plt.xlim([-3,4])
plt.savefig("control_ipds_dist.png")
