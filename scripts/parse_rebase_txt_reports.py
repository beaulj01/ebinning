import os,sys
import glob
from collections import Counter

txt_fns = glob.glob("*.txt")

total_motifs = Counter()
motifs_dict = {}
for txt_fn in txt_fns:
	species = txt_fn.strip(".txt")
	print species
	txt = open(txt_fn).read()
	motifs_dict[species] = {}
	for RM_type in ["I", "II", "III"]:
		splits = txt.split("\n%s\n" % RM_type)
		M = [x for x in splits if len(x.split("\n"))<7 and x.split("\n")[0] in ["M", "RM"]]
		motifs = map(lambda x: x.split("\n")[2], M)
		motifs = list(set([motif for motif in motifs if len(motif)>0]))
		motifs_dict[species][RM_type] = motifs
		print RM_type, motifs
		for motif in motifs:
			total_motifs[motif] += 1
print total_motifs, len(total_motifs)