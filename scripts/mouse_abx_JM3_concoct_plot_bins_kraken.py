import os,sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import random
import pub_figs
from collections import Counter,defaultdict
from Bio import SeqIO
import math

font_s = 18
pub_figs.setup_math_fonts(font_size=font_s, font="Computer Modern Sans serif")

labels  = np.loadtxt("contigs.kraken",  dtype="str")
names   = np.loadtxt("contigs.names",   dtype="str")
lengths = np.loadtxt("contigs.lengths", dtype="int")
clusts  = np.loadtxt("clustering_gt1000.csv", delimiter=",", dtype="str")

minlen = 5000

labels  = labels[lengths>minlen]
names   = names[lengths>minlen]
lengths = lengths[lengths>minlen]

lab_counter = Counter()
for i,label in enumerate(labels):
	label     = label.strip("[")
	label     = label.strip("]")
	labels[i] = label
	lab_counter[label] += 1


for i,label in enumerate(labels):
	label_sum = np.sum(lengths[labels==label])
	if lab_counter[label]<5 and label_sum<100000:
		labels[i] = "Unlabeled"

bins_map  = dict(clusts)
bp_map    = dict(zip(names,lengths))
krak_map  = dict(zip(names,labels))   

label_set = list(set(labels))
label_set.sort()
colors    = plt.get_cmap('spectral')(np.linspace(0, 0.9, len(label_set)))

# Need to count bp for each kraken label in each bin
bin_ids = list(set(bins_map.values()))
bin_ids.sort()
bin_ids = np.array(bin_ids)

bin_krak_bp = {}
for bin_id in bin_ids:
	bin_krak_bp[bin_id] = Counter()

	b_contigs = np.array([name for name in names if bins_map.get(name)==bin_id])
	for name in b_contigs:
		lab                       = krak_map[name]
		bin_krak_bp[bin_id][lab] += bp_map[name]

fig     = plt.figure(figsize=[12,7])
ax      = fig.add_axes([0.07, 0.12, 0.9, 0.8])
width   = 0.85
bin_ids = np.array(map(int,bin_ids))
adj_ids = bin_ids - (width/2)

prev_top  = np.zeros(len(bin_ids))
leg_labs  = []
leg_plots = []
for i,lab in enumerate(label_set):

	bin_lab_Mbps = []
	for bin_id in bin_ids:
		bin_lab_Mbps.append( float(bin_krak_bp[str(bin_id)][lab])/1000000 )

	print i,lab
	if lab=="Unlabeled":
		c = "gray"
	else:
		c = colors[i]
	p = ax.bar(adj_ids, bin_lab_Mbps, width, bottom=prev_top, color=c, label=lab, linewidth=0)
	leg_labs.append(lab)
	leg_plots.append(p)
	prev_top += bin_lab_Mbps

ax.set_xlim([-1, len(bin_ids)])
ax.set_xticks(range(0,len(bin_ids)))
ax.set_xticklabels(ax.get_xticks(), rotation=45)
ax.set_xlabel("CONCOCT Bin")
ax.set_ylabel("Bin size (Mbp)")
pub_figs.remove_top_right_axes(ax)

# Put "Unlabeled" at end of legend
leg_tup = zip(leg_labs,leg_plots)
leg_tup.sort(key=lambda x: x[0])

unlabeled = [tup for tup in leg_tup if tup[0]=="Unlabeled"][0]
leg_tup.remove(unlabeled)
leg_tup.append(unlabeled)
legend_labs, legend_plots = zip(*leg_tup)
leg = ax.legend(legend_plots, legend_labs, loc='upper right', prop={'size':font_s}, frameon=False)
plt.savefig("concoct_bins_kraken_bps.png", dpi=300)








