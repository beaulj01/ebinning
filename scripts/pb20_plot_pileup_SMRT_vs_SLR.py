import pysam
import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from collections import defaultdict
import numpy as np
import pub_figs
import itertools
import operator
import subprocess
import matplotlib.gridspec as gridspec

def run_OS( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts != 0:
		print "Failed command: %s" % CMD
	return sts, stdOutErr

def get_tot_bases( bam ):
	f      = pysam.AlignmentFile(bam, "rb")
	pileup = f.pileup()
	return np.sum([p.n for p in pileup])

def downsample_bam( orig_fn, target_frac, tech, seed=1 ):
	down_fn         = "%s.%s.down.%s.bam" % (orig_fn.split(".sorted")[0], tech, target_frac)
	target_frac_str = str(target_frac).split(".")[1]
	CMD             = "samtools view -s %s.%s -h -b %s > %s" % (seed, target_frac_str, orig_fn, down_fn)
	print CMD
	if not os.path.exists(down_fn):
		sts, stdOutErr  = run_OS( CMD )
	if not os.path.exists(down_fn+".bai"):
		pysam.index(down_fn, "%s.bai" % down_fn)
	
	orig_y = get_tot_bases(orig_fn)
	down_y = get_tot_bases(down_fn)
	
	print "downsampling:",tech, "frac:",target_frac, "orig_yield:",orig_y, "new_yield:",down_y, orig_fn.split(os.sep)[-1]
	return down_fn, down_y

def format_axes( ax, bamfile, pos_array, tech, cov_pct, bounds=[0,1000000000] ):
	# tick_labs  = []
	# for n in range(500000,np.sum(bamfile.lengths), 500000):
	# 	tick_labs.append( float(n)/1000000 )
	# ax.set_xticks(range(500000,np.sum(bamfile.lengths), 500000))
	# ax.set_xticklabels(tick_labs, rotation=45, ha="right")
	# ax.set_ylabel("Coverage")
	# ax.set_xlim([0,len(pos_array)])
	#####################
	ax.set_xlim(bounds)
	ax.set_ylim([0,30])
	tick_labs  = []
	for n in range(bounds[0],bounds[1], 10000):
		tick_labs.append( n )
	ax.set_xticks(range(bounds[0],bounds[1], 10000))
	ax.set_xticklabels(tick_labs, rotation=45, ha="right")
	#####################
	ax.set_title( "%s: %.1f%s covered" % (tech, cov_pct, pub_figs.tex_escape("%")) )

org_fns_SMRT = {"A_baumannii":     "A_baumannii_ATCC_17978", \
				"A_odontolyticus": "A_odontolyticus_ATCC_17982_Scfld021", \
				"B_cereus":        "B_cereus_ATCC_10987", \
				"B_vulgatus":      "B_vulgatus_ATCC_8482", \
				"C_beijerinckii":  "C_beijerinckii_NCIMB_8052", \
				"D_radiodurans":   "D_radiodurans_R1", \
				"E_faecalis":      "E_faecalis_OG1RF", \
				"E_coli":          "E_coli_str_K_12_substr_MG1655", \
				"H_pylori":        "H_pylori_26695", \
				"L_gasseri":       "L_gasseri_ATCC_33323", \
				"L_monocytogenes": "L_monocytogenes_EGD_e", \
				"N_meningitidis":  "N_meningitidis_MC58", \
				"P_acnes":         "P_acnes_KPA171202", \
				"P_aeruginosa":    "P_aeruginosa_PAO1", \
				"R_sphaeroides":   "R_sphaeroides_2_4_1", \
				"S_aureus":        "S_aureus_subsp_aureus_USA300_TCH1516", \
				"S_epidermidis":   "S_epidermidis_ATCC_12228", \
				"S_agalactiae":    "S_agalactiae_2603V_R", \
				"S_mutans":        "S_mutans_UA159", \
				"S_pneumoniae":    "S_pneumoniae_TIGR4"} 

org_fns_SLR  = {"A_baumannii":     "A_baumannii_ATCC_17978", \
				"A_odontolyticus": "A_odontolyticus_ATCC_17982_Scfld021", \
				"B_cereus":        "B_cereus_ATCC_10987", \
				"B_vulgatus":      "B_vulgatus_ATCC_8482", \
				"C_beijerinckii":  "C_beijerinckii_NCIMB_8052", \
				"D_radiodurans":   "D_radiodurans_R1", \
				"E_faecalis":      "E_faecalis_OG1RF", \
				"E_coli":          "E_coli_str_K-12_substr_MG1655", \
				"H_pylori":        "H_pylori_26695", \
				"L_gasseri":       "L_gasseri_ATCC_33323", \
				"L_monocytogenes": "L_monocytogenes_EGD-e", \
				"N_meningitidis":  "N_meningitidis_MC58", \
				"P_acnes":         "P_acnes_KPA171202", \
				"P_aeruginosa":    "P_aeruginosa_PAO1", \
				"R_sphaeroides":   "R_sphaeroides_2.4.1", \
				"S_aureus":        "S_aureus_subsp_aureus_USA300_TCH1516", \
				"S_epidermidis":   "S_epidermidis_ATCC_12228", \
				"S_agalactiae":    "S_agalactiae_2603V_R", \
				"S_mutans":        "S_mutans_UA159", \
				"S_pneumoniae":    "S_pneumoniae_TIGR4"} 

org_fns_SMRT = {"P_acnes":         "P_acnes_KPA171202"}

orgs = org_fns_SMRT.keys()
orgs.sort()

SLR_cov = {"A_baumannii" : 	10894845, \
	    "A_odontolyticus" : 3730113, \
	    "B_cereus" : 		26483461, \
	    "B_vulgatus" : 		3579005, \
	    "C_beijerinckii" : 	11135316, \
	    "D_radiodurans" : 	3983663, \
	    "E_faecalis" : 		11270120, \
	    "E_coli" : 			2133476693, \
	    "H_pylori" : 		26015813, \
	    "L_gasseri" : 		10760149, \
	    "L_monocytogenes" : 24364014, \
	    "N_meningitidis" : 	15910092, \
	    "P_acnes" : 		26717866, \
	    "P_aeruginosa" : 	170029436, \
	    "R_sphaeroides" : 	29901273, \
	    "S_aureus" : 		61148568, \
	    "S_epidermidis" : 	173408151, \
	    "S_agalactiae" : 	49104157, \
	    "S_mutans" : 		252711874, \
	    "S_pneumoniae" : 	23107608}

SMRT_cov = {"A_baumannii" : 458337257, \
	    "A_odontolyticus" : 390178052, \
	    "B_cereus" : 		101134305, \
	    "B_vulgatus" : 		744355850, \
	    "C_beijerinckii" : 	518725397, \
	    "D_radiodurans" : 	112809810, \
	    "E_faecalis" : 		388729626, \
	    "E_coli" : 			607875898, \
	    "H_pylori" : 		1637499690, \
	    "L_gasseri" : 		436941561, \
	    "L_monocytogenes" : 770815426, \
	    "N_meningitidis" : 	448123214, \
	    "P_acnes" : 		533794211, \
	    "P_aeruginosa" : 	1072961775, \
	    "R_sphaeroides" : 	363229532, \
	    "S_aureus" : 		616183706, \
	    "S_epidermidis" : 	444655487, \
	    "S_agalactiae" : 	277523113, \
	    "S_mutans" : 		506964576, \
	    "S_pneumoniae" : 	76800567}

SMRT_base = "/hpc/users/beaulj01/projects/ebinning/pacbio20/all7_sets/aligns_to_refs"
SLR_base  = "/hpc/users/beaulj01/projects/ebinning/pacbio20/SLR/align_to_refs"

SMRT_even_yield_bams = {}
SLR_even_yield_bams  = {}
print "Downsampling to match yields..."
for i,org in enumerate(orgs):
	i +=1
	
	SLR_bam  = os.path.join(SLR_base, "%s.sorted.bam" % org_fns_SLR[org])
	SMRT_bam = os.path.join(SMRT_base, org_fns_SMRT[org], "%s.sorted.bam" % org_fns_SMRT[org])
	
	min_idx      = np.argmin([SMRT_cov[org], SLR_cov[org]])
	target_yield = [SMRT_cov[org], SLR_cov[org]][min_idx]

	if min_idx==0:
		# Downsample SLR
		tech                      = "SLR"
		target_frac               = round(float(target_yield) / SLR_cov[org], 4)
		down_bam_fn,down_y        = downsample_bam( SLR_bam, target_frac, tech )
		SLR_cov[org]              = down_y
		SLR_even_yield_bams[org]  = down_bam_fn
		SMRT_even_yield_bams[org] = os.path.join(SMRT_base, org_fns_SMRT[org], "%s.sorted.bam" % org_fns_SMRT[org])
	else:
		# Downsample SMRT
		tech                      = "SMRT"
		target_frac               = round(float(target_yield) / SMRT_cov[org], 4)
		down_bam_fn,down_y        = downsample_bam( SMRT_bam, target_frac, tech )
		# Update yield with downsampled value
		SMRT_cov[org]             = down_y
		SLR_even_yield_bams[org]  = os.path.join(SLR_base, "%s.sorted.bam" % org_fns_SLR[org])
		SMRT_even_yield_bams[org] = down_bam_fn

print "Computing percent covered fractions..."
for i,org in enumerate(orgs):
	i +=1 
	
	font_size      = 25
	adjust         = 0.15
	fontProperties = pub_figs.setup_math_fonts(font_size)
	fig1           = pub_figs.init_fig(x=20,y=10)
	gs             = gridspec.GridSpec(2, 3)
	gs.update(hspace=0.4)
	ax1            = plt.subplot(gs[:-1, :])
	ax2            = plt.subplot(gs[-1, :])

	for j,tech in enumerate(["SMRT", "SLR"]):
		if tech=="SLR":
			bam       = SLR_even_yield_bams[org]
			bamfile   = pysam.AlignmentFile(os.path.join(SLR_base, bam), "rb")
			tot_bases = SLR_cov[org]
		else:
			bam     = SMRT_even_yield_bams[org]
			bamfile = pysam.AlignmentFile(os.path.join(SMRT_base, org_fns_SMRT[org], bam), "rb")
			tot_bases = SMRT_cov[org]

		pileup = bamfile.pileup()

		# Build bit array to track non-zero cov
		pos_array        = np.zeros(np.sum(bamfile.lengths))
		x                = []
		y                = []	
		windowsize       = 1
		window_vals      = defaultdict(list)
		for p in pileup:
			pos  = p.pos
			cov  = p.n
			rpos = p.reference_pos
			rid  = p.reference_id
			if p.reference_id==1:
				pos += bamfile.lengths[0]
			window_id = pos/windowsize
			window_vals[window_id].append(cov)
			# print rid, pos, window_id, cov
			if cov>0:
				pos_array[pos] = 1

		cov_pct   = 100*float(pos_array.sum()) / np.sum(bamfile.lengths)

		print "%s\t%s\t%s\t%.2f" % (tech, org, tot_bases, cov_pct)

		# Compile list of no-coverage intervals
		no_cov_intervals = []
		no_cov_blocks    = [[i for i,value in it] for key,it in \
							itertools.groupby(enumerate(pos_array), key=operator.itemgetter(1)) if key==0]
		for block in no_cov_blocks:
			no_cov_intervals.append( (block[0], block[-1]) )

		for i in range(np.sum(bamfile.lengths)/windowsize+1):
			if len(window_vals[i])==0:
				mean = 0
			elif len(window_vals[i])==1:
				mean = window_vals[i][0]
			else:
				mean = np.mean(window_vals[i])
			pos  = i * windowsize
			# print i, pos, mean
			x.append(pos)
			y.append(mean)

		########################
		bounds = [2025000,2045000]
		########################

		if j==1:
			# SLR
			ax1.plot(x, y, linestyle="-", color="k")
			for x in no_cov_intervals:
				ax1.axvspan(x[0], x[1], color='tomato')
			# format_axes( ax1, bamfile, pos_array, tech, cov_pct )
			format_axes( ax1, bamfile, pos_array, tech, cov_pct, bounds )
		elif j==0:
			# SMRT
			ax2.plot(x, y, linestyle="-", color="k")
			for x in no_cov_intervals:
				ax2.axvspan(x[0], x[1], color='tomato')
			# format_axes( ax2, bamfile, pos_array, tech, cov_pct )
			format_axes( ax2, bamfile, pos_array, tech, cov_pct, bounds )
			ax2.set_xlabel("Genome position (Mb)")

	# fig = plt.figure(figsize=[10,10])
	# ax  = fig.add_axes([0.1, 0.3, 0.8, 0.4])

	# ax.plot(x, y, linestyle="-", color="k")
	# for x in no_cov_intervals:
	# 	ax.axvspan(x[0], x[1], color='tomato')

	# pub_figs.remove_top_right_axes(ax)
	# pub_figs.setup_math_fonts(24)
	fig1.subplots_adjust(bottom=0.12, top=0.95)
	plot_fn = "%s.cov.zoom.%s_%s.png" % (org, bounds[0], bounds[1])
	plt.savefig(plot_fn)