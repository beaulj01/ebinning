import os,sys
from pbcore.io.CmpH5IO import CmpH5Reader

cmph5_fn           = sys.argv[1]
ebarcodes_fn       = sys.argv[2]
node_strain_map_fn = sys.argv[3]

reader = CmpH5Reader(cmph5_fn)
table  = reader.referenceInfoTable

# table:
# (2214, 2214, 'ref002214', 'NODE_3592_length_17309_cov_65.830261', 17339, '62ff268ac659c9e40db3e5a18ad00273'),

ref_map = dict( [(ref_id, contig_name) for (a,b,ref_id,contig_name,c,d) in table] )

strain_map = {}
for line in open(node_strain_map_fn).xreadlines():
	line                  = line.strip()
	node                  = line.split("\t")[0]
	strain                = line.split("\t")[1]
	confidence            = line.split("\t")[4]
	strain_map[node] = (strain, confidence)

fn = "%s.mapped" % ebarcodes_fn
f  = open(fn, "w")
for line in open(ebarcodes_fn).xreadlines():
	line   =       line.strip()
	refID  =       line.split("\t")[0]
	motif  =       line.split("\t")[1]
	count  =   int(line.split("\t")[2])
	score  = float(line.split("\t")[3])
	
	node               = ref_map[refID]
	strain, confidence = strain_map[node]

	string = "%s\t%s\t%s\t%s\t%s\t%s\n" % (refID, strain, confidence, motif, count, score)
	f.write(string)
f.close()