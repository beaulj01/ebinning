import os,sys
import numpy as np
import re

called_motifs = np.loadtxt(sys.argv[1], dtype="str")
min_SCp       = 1.7

called_motifs = {}
for line in open(sys.argv[1],"r").xreadlines():
	line  = line.strip()
	motif =       line.split("\t")[1]
	SCp   = float(line.split("\t")[2])
	N     =   int(line.split("\t")[5])
	if SCp > min_SCp and N >= 20: 
		# print motif
		called_motifs[motif] = SCp

shortest_contiguous = min([len(m.split("-")[0]) for m in called_motifs.keys()])
shortest_motifs     = [m for m in called_motifs.keys() if len(m.split("-")[0])==shortest_contiguous]

print "Before:", len(called_motifs.keys())
print ""
to_del = []
for shorty in shortest_motifs:
	shorty_str =     shorty.split("-")[0]
	shorty_idx = int(shorty.split("-")[1])
	for motif in called_motifs.keys():
		if motif!=shorty:
			motif_str =     motif.split("-")[0]
			motif_idx = int(motif.split("-")[1])
			match = re.search(shorty_str, motif_str)
			if match != None:
				if (shorty_idx + match.start()) == motif_idx:
					to_del.append( motif )
		
for m in to_del:
	# print m
	del called_motifs[m]

print "After:", len(called_motifs.keys())
for m in called_motifs.keys():
	print m
