import os,sys
import numpy as np
from itertools import groupby
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import re
import urllib2
from bs4 import BeautifulSoup
from collections import defaultdict
import pub_figs

FONTSIZE=30
dists_fn        = "dists.out"
comp_vectors_fn = "comp_vectors.out"
comp_matrix     = np.loadtxt(comp_vectors_fn, dtype="str")
pb_org_accs     = set(np.loadtxt("pb_all_accs.txt", dtype="str"))

# Read all Rebase PacBio organisms
# pb_org_accs = set()
# response    = urllib2.urlopen("http://rebase.neb.com/cgi-bin/pacbiolist?0")
# html        = response.read()
# soup        = BeautifulSoup(html, "html.parser")

# f = open("pb_all_accs.txt", "wb")
# tables = soup.find_all("table")
# org_n  = 0
# for i,table in enumerate(tables):
# 	if table.attrs.get("bgcolor")=="beige":
# 		all_as = table.find_all("a")
# 		for j,a in enumerate(all_as):
# 			if a.attrs.get("href").find("pacbioget")>-1:
# 				org = a.text
# 				org_url = "http://rebase.neb.com"+a.attrs["href"]

# 				response = urllib2.urlopen(org_url)
# 				html     = response.read()
# 				soup     = BeautifulSoup(html, "html.parser")

# 				fonts = soup.find_all("font")
# 				for i,font in enumerate(fonts):
# 					if font!=None:
# 						if font.b!=None:
# 							if font.b.string==r"Chromosome:":
# 								org_n +=1
# 								all_as = font.find_all("a")
# 								for a in all_as:
# 									if a!=None:
# 										if a.attrs.get("href").find("seqsget")>-1:
# 											print org_n, org, a.string
# 											pb_org_accs.add(a.string)
# 											f.write("%s\n" % a.string)
# f.close()

# Make acc <--> org_id map
acc_orgID_map = dict([ (acc.split(".")[0], int(org_id)) for (org_id, acc) in comp_matrix[:,:2]])
orgID_acc_map = defaultdict(list)
for acc,orgID in acc_orgID_map.iteritems():
	orgID_acc_map[orgID].append(acc)

d_dict   = {}
org_dict = {}
for line in open(dists_fn, "rb").xreadlines():
	line   = line.strip()
	org    = line.split("\t")[0]
	acc    = line.split("\t")[2].split(".")[0]
	true_d = float(line.split("\t")[3])
	d_dict[acc]   = true_d
	org_dict[acc] = org

true_dists  = []
pls_accs    = set()
pb_pls_accs = set()
for line in open(dists_fn, "rb").xreadlines():
	line   = line.strip()
	strain = org.split(" plasmid")[0]
	acc    = line.split("\t")[2].split(".")[0]
	true_d = float(line.split("\t")[3])
	true_dists.append(true_d)
	pls_accs.add(acc)
	if acc in pb_org_accs:
		org_id       = acc_orgID_map[acc]
		all_org_accs = orgID_acc_map[org_id]
		for acc in all_org_accs:
			pb_pls_accs.add(acc)
			try:
				print "%s\t%s\t%s" % (org_dict[acc], acc, d_dict[acc])
			except KeyError:
				pass

shuff_dists = []
to_sample   = len(true_dists)
i           = 0
while i<to_sample:
	two_idx = np.random.choice(comp_matrix.shape[0],2)
	acc_one = comp_matrix[two_idx[0]][1].split(".")[0]
	acc_two = comp_matrix[two_idx[1]][1].split(".")[0]
	if acc_one in pls_accs and acc_two not in pls_accs:

		comp_pls = np.array(map(lambda x: float(x), comp_matrix[two_idx[0]][2:]))
		comp_chr = np.array(map(lambda x: float(x), comp_matrix[two_idx[1]][2:]))
		d        = np.linalg.norm(comp_chr - comp_pls)
		i       += 1
		shuff_dists.append(d)

fig  = pub_figs.init_fig(x=10,y=10)
ax_x = 0.15
ax_y = 0.10
ax_w = 0.8
ax_h = 0.4
ax   = fig.add_axes([ax_x, ax_y, ax_w, ax_h])

bins=np.histogram(np.hstack((true_dists,shuff_dists)), bins=50)[1] #get the bin edges

ax.hist( true_dists,  bins, color="b",            label=r"plasmid $\Leftrightarrow$ host chr.",   linewidth=0)
ax.hist( shuff_dists, bins, color="r", alpha=0.5, label=r"plasmid $\Leftrightarrow$ random chr.", linewidth=0)

ax.set_xlabel("Composition-based Euclidian distance", fontsize=FONTSIZE)
ax.set_ylabel("Count", fontsize=FONTSIZE)
# ax.set_title("Plasmid $\Leftrightarrow$ chr. sequence similarity", fontsize=FONTSIZE)
ax.legend(frameon=False, loc=1, fontsize=FONTSIZE)
pub_figs.setup_math_fonts(30)
pub_figs.remove_top_right_axes(ax)
ax.tick_params(axis='both', direction='out')
plt.savefig("dist_shuff_hist.png", dpi=300)