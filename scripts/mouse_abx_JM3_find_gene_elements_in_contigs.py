import os,sys
from Bio import SeqIO
import glob
from collections import Counter

keyword = "onjugative transpos"
min_n   = 5

box_contig_counter = {}
lengths            = {}

for box_id in range(1,10):
	box_fn = "box%s.gbk" % box_id
	box_contig_counter[box_id] = Counter()
	lengths[box_id]            = {}
	for record in SeqIO.parse(open(box_fn,"r"), "genbank"):
		record_contig = record.annotations["accessions"][0].split("|")[0]

		for feature in record.features:
			if feature.type=="CDS":
				product = feature.qualifiers["product"][0]

				# if product.find("hypothetical protein")>-1:
				#       continue

				start   = feature.location.nofuzzy_start
				end     = feature.location.nofuzzy_end

				if product.find(keyword)>-1 and len(record.seq)<200000:
					box_contig_counter[box_id][record_contig] += 1
					n = box_contig_counter[box_id][record_contig]
					lengths[box_id][record_contig] = len(record.seq)
					print "box%s\t%s\t%s bp\t%s\t%s\t%s\t%s" % (box_id, record_contig, len(record.seq), n, start, end, product[:50])

print ""
for box_id in box_contig_counter.keys():
	for contig in box_contig_counter[box_id]:
		if box_contig_counter[box_id][contig]>=min_n:
			print "%s\t%s\t%s\t%s" % (box_id, contig, lengths[box_id][contig], box_contig_counter[box_id][contig])
