import os,sys
from Bio import SeqIO
import numpy as np

names  = np.loadtxt("contigs.names", dtype="str")
labels = np.loadtxt("contigs.labels", dtype="str")
fasta  = "polished_assembly.fasta"

specs  = dict(zip(names,labels))

spec_names = {
	"B_caccae" : "Bacteroides caccae", \
	"B_ovatus" : "Bacteroides ovatus", \
	"B_theta" : "Bacteroides thetaiotaomicron", \
	"B_vulgatus" : "Bacteroides vulgatus", \
	"C_aerofaciens" : "collinsella aerofaciens", \
	"C_bolteae" : "Clostridium bolteae", \
	"E_coli" : "Escherichia coli", \
	"R_gnavus" : "Ruminococcus gnavus", \
}

with open("polished_assembly_labeled.fasta", "w") as output_handle:
	for entry in SeqIO.parse(fasta, "fasta"):
		contig = entry.id.split("|")[0]
		if specs.get(contig):
			entry.id = ""
			entry.id          = "%s [note=%s]" % (contig, spec_names[specs[contig]])
			entry.description = ""
			# entry.description = "%s [note=%s]" % (contig, spec_names[specs[contig]])
			# entry.name        = "%s [note=%s]" % (contig, spec_names[specs[contig]])
		else:
			entry.id          = "%s [note=Unknown]" % contig
			entry.description = ""
			# entry.description = "%s [note=unknown]" % contig
			# entry.name        = "%s [note=unknown]" % contig
		
		SeqIO.write(entry, output_handle, "fasta")