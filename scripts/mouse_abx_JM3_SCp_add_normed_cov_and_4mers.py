import os,sys
import numpy as np

kmer_cov  = np.loadtxt("original_data_gt1000.csv", dtype="str", skiprows=1, delimiter=",")
motifs    = np.loadtxt("ordered_motifs.txt",       dtype="str")
names     = np.loadtxt("contigs.names.orig",       dtype="str")
scores    = np.loadtxt("contigs.SCp.orig",         dtype="float")
lengths   = np.loadtxt("contigs.lengths.orig",     dtype="int")
labels    = np.loadtxt("contigs.labels.orig",      dtype="str")
kraken    = np.loadtxt("contigs.kraken.orig",      dtype="str")

fk  = open("contigs.4mers", "wb")
fc  = open("contigs.normcovs", "wb")
fn  = open("contigs.names", "wb")
fs  = open("contigs.SCp", "wb")
fl  = open("contigs.lengths", "wb")
fla = open("contigs.labels", "wb")
fkr = open("contigs.kraken", "wb")
for i,contig in enumerate(names):
	find_in_table = kmer_cov[:,0]==contig
	if find_in_table.any():
		
		kmer_str      = "\t".join(kmer_cov[find_in_table,1:137][0])
		cov_str       = "\t".join(kmer_cov[find_in_table,137:][0])
		SCp_str       = "\t".join(map(str,map(lambda x: round(x,3), scores[i,:])))
		fk.write("%s\n" % kmer_str)
		fc.write("%s\n" % cov_str)
		fn.write("%s\n" % names[i])
		fs.write("%s\n" % SCp_str)
		fl.write("%s\n" % lengths[i])
		fla.write("%s\n" % labels[i])
		fkr.write("%s\n" % kraken[i])
	else:
		# print contig, lengths[i], "NOT FOUND"
		pass
fk.close()
fc.close()
fn.close()
fs.close()
fl.close()
fla.close()
fkr.close()
