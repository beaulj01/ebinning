import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import subprocess
import numpy as np
from collections import defaultdict
import pub_figs

def run_OS( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts != 0:
		raise Exception("Failed command: %s" % CMD)
	return sts, stdOutErr

contigs_fns      = ["/hpc/users/beaulj01/projects/ebinning/pacbio20/all7_sets/HGAP_naive/whitelist_baseline/data/polished_assembly.fasta", \
					"/hpc/users/beaulj01/projects/ebinning/pacbio20/all7_sets/HGAP_naive/whitelist_log_abund/data/polished_assembly.fasta", \
					"/hpc/users/beaulj01/projects/ebinning/pacbio20/hmp_set5/HGAP/data/polished_assembly.fasta"]

genome_sizes     = {"A_baumannii_ATCC_17978" :               3976747, \
		   			"A_odontolyticus_ATCC_17982_Scfld021" :  2391230, \
		   			"B_cereus_ATCC_10987" :                  5224283, \
		   			"B_vulgatus_ATCC_8482" :                 5163189, \
		   			"C_beijerinckii_NCIMB_8052" :            6000632, \
		   			"D_radiodurans_R1" :                     3060986, \
		   			"E_coli_str_K-12_substr_MG1655" :        4641652, \
		   			"E_faecalis_OG1RF" :                     2739625, \
		   			"H_pylori_26695" :                       1667867, \
		   			"L_gasseri_ATCC_33323" :                 1894360, \
		   			"L_monocytogenes_EGD-e" :                2944528, \
		   			"N_meningitidis_MC58" :                  2272360, \
		   			"P_acnes_KPA171202" :                    2560265, \
		   			"P_aeruginosa_PAO1" :                    6264404, \
		   			"R_sphaeroides_2.4.1" :                  4131542, \
		   			"S_agalactiae_2603V_R" :                 2160267, \
		   			"S_aureus_subsp_aureus_USA300_TCH1516" : 2872915, \
		   			"S_epidermidis_ATCC_12228" :             2499279, \
		   			"S_mutans_UA159" :                       2032925, \
		   			"S_pneumoniae_TIGR4" :                   2160842}

genomes          = ["S_mutans_UA159", \
		   			"L_monocytogenes_EGD-e", \
		   			"S_epidermidis_ATCC_12228", \
		   			"L_gasseri_ATCC_33323", \
		   			"P_acnes_KPA171202", \
		   			"S_aureus_subsp_aureus_USA300_TCH1516", \
		   			"N_meningitidis_MC58", \
		   			"P_aeruginosa_PAO1", \
					"H_pylori_26695", \
					"D_radiodurans_R1", \
		   			"A_odontolyticus_ATCC_17982_Scfld021", \
		   			"B_vulgatus_ATCC_8482", \
		   			"E_faecalis_OG1RF", \
		   			"E_coli_str_K-12_substr_MG1655", \
		   			"S_agalactiae_2603V_R", \
					"A_baumannii_ATCC_17978", \
					"R_sphaeroides_2.4.1", \
		   			"S_pneumoniae_TIGR4", \
		   			"C_beijerinckii_NCIMB_8052", \
		   			"B_cereus_ATCC_10987"]
		   		

genomes_str      = [r"${S. mutans}$ UA159", \
		   			r"${L. monocytogenes}$ EGD-e", \
		   			r"${S. epidermidis}$ ATCC 12228", \
		   			r"${L. gasseri}$ ATCC 33323", \
		   			r"${P. acnes}$ KPA171202", \
		   			r"${S. aureus}$ subsp aureus USA300 TCH1516", \
		   			r"${N. meningitidis}$ MC58", \
		   			r"${P. aeruginosa}$ PAO1", \
					r"${H. pylori}$ 26695", \
					r"${D. radiodurans}$ R1", \
		   			r"${A. odontolyticus}$ ATCC 17982 Scfld021", \
		   			r"${B. vulgatus}$ ATCC 8482", \
		   			r"${E. faecalis}$ OG1RF", \
		   			r"${E. coli}$ str K-12 substr MG1655", \
		   			r"${S. agalactiae}$ 2603V R", \
					r"${A. baumannii}$ ATCC 17978", \
					r"${R. sphaeroides}$ 2.4.1", \
		   			r"${S. pneumoniae}$ TIGR4", \
		   			r"${C. beijerinckii}$ NCIMB 8052", \
		   			r"${B. cereus}$ ATCC 10987"]		   			

covs        = defaultdict(list)
max_contigs = 50
mat         = {}
mean_QVs    = {}
for k,contigs_fn in enumerate(contigs_fns):
	qual_fn = contigs_fn.replace("data/polished_assembly.fasta", "results/polished_coverage_vs_quality.csv")
	qual    = np.loadtxt(qual_fn, delimiter=",", dtype="str")

	labels = []
	mat[k] = np.zeros([max_contigs,len(genomes)])
	mean_QVs[contigs_fn] = {}
	for j,spec in enumerate(genomes):
		nucmer_CMD     = "nucmer -p %s.%s ~/projects/data_repo/refs/%s.fasta %s" % (spec, k, spec,contigs_fn)
		showcoords_CMD = "show-coords -H -r -l -d -L 10000 %s.%s.delta" % (spec,k)

		print nucmer_CMD
		# sts, stdOutErr = run_OS(nucmer_CMD)
		sts, stdOutErr = run_OS(showcoords_CMD)
		print showcoords_CMD

		coords_out  = stdOutErr[0].split("\n")
		
		out         = []
		chroms_set  = set()
		chroms      = {}
		for i,line in enumerate(coords_out):
			
			if len(line)>0:
				ref_start   =   int(line.split()[0])
				ref_stop    =   int(line.split()[1])
				q_align_len =   int(line.split()[7])
				ident       = float(line.split()[9])
				r_len       =   int(line.split()[11])
				q_len       =   int(line.split()[12])
				q_dir       =   int(line.split()[15])
				r_name      =     line.split()[-2]
				q_name      =     line.split()[-1]
				if ident>99 and (100*float(q_align_len)/q_len>95 or (ref_start<10 or ref_stop>(r_len-10))):
					print line.strip()
					name    = q_name.split("|")[0]
					quality = qual[qual[:,0]==name][0][2].astype(np.float)
					out.append( (q_len, r_name, q_name, q_align_len, quality) )
		
		out.sort(key=lambda x: x[0], reverse=True)
		keeping_contigs   = set()
		contigs_lengths   = []
		n_aligned_contigs = len(set(map(lambda x: x[2], out)))
		i                 = 0
		QVs               = []
		for entry in out:
			contig_size    = entry[0]
			ref_name       = entry[1]
			q_name         = entry[2]
			q_align_len    = entry[3]
			contig_QV      = entry[4]
			align_cov_pct  = 100*float(q_align_len)/genome_sizes[spec]
			if q_name not in keeping_contigs:
				keeping_contigs.add(q_name)
				contig_cov_pct = 100*float(contig_size)/genome_sizes[spec]
				contigs_lengths.append(contig_cov_pct)
				print spec, q_name, contig_size, contig_cov_pct, contig_QV
				QVs.append(contig_QV)
				mat[k][i,j] = contig_cov_pct
				i += 1
				if i==max_contigs:
					break
		labels.append(genomes_str[j])
		mean_QVs[contigs_fn][spec] = np.mean(QVs)

N     = len(labels)
covs1 = covs[0]

ind   = np.arange(N) # the x locations for the groups
width = 0.2          # the width of the bars

def transform_matrix( mat ):
	"Change each entry to the sum of that column below the entry"
	for i in range(mat.shape[0]):
		mat[i,:] = np.sum(mat[i:,:], axis=0)
	return mat

ax,fig = pub_figs.format_pb20_assembly_axes()
mat[0] = transform_matrix(mat[0])
for i in range(max_contigs):
	rects0  = ax.bar(ind - 2*width, mat[0][i,:], width, color='r')

mat[1] = transform_matrix(mat[1])
for i in range(max_contigs):
	rects1 = ax.bar(ind - width, mat[1][i,:], width, color='orange')

mat[2] = transform_matrix(mat[2])
for i in range(max_contigs):
	rects2 = ax.bar(ind, mat[2][i,:], width, color='y')

ax.set_ylabel('Percent genome covered')
ax.set_xticks(ind - float(width)/2)
ax.set_xticklabels(labels, rotation=45, ha="right")
ax.set_xlim([-1,20])
ax.set_ylim([0,100])
# box = ax.get_position()
# ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
# ax.legend((rects0[0], rects1[0], rects2[0]), ('49 SMRT cells', '49 SMRT cells (log-curve abundances)', '8 SMRT cells'), loc='lower center', bbox_to_anchor=(0.5, 1), prop={'size':14}, frameon=False)
# top_adjust=0.80
# bottom_adjust=0.40
# left_adjust=0.05
# right_adjust=0.90
# hspace=0.5
# wspace=0.2 
plt.grid()
pub_figs.remove_top_right_axes(ax)
# plt.gcf().subplots_adjust(top=top_adjust, bottom=bottom_adjust, left=left_adjust, right=right_adjust, hspace=hspace, wspace=wspace)
plt.savefig("assembly_summaries.png")

ax,fig = pub_figs.format_pb20_assembly_axes()
Y0     = []
Y1     = []
Y2     = []
for i,contig_fn in enumerate(contigs_fns):
	for spec in genomes:
		val = mean_QVs[contig_fn][spec]
		print contig_fn, spec, val
		if i==0:
			Y0.append(val)
		if i==1:
			Y1.append(val)
		if i==2:
			Y2.append(val)
rects0  = ax.bar(ind - 2*width, Y0, width, color='r')
rects1 = ax.bar(ind - width,    Y1, width, color='orange')
rects2 = ax.bar(ind,            Y2, width, color='y')
ax.set_ylabel("Mean QV")
ax.set_xticks(ind - float(width)/2)
ax.set_xticklabels(labels, rotation=45, ha="right")
ax.xaxis.labelpad = 30
ax.set_xlim([-1,20])
ax.set_ylim([30,50])
pub_figs.remove_top_right_axes(ax)
# top_adjust=0.80
# bottom_adjust=0.40
# left_adjust=0.05
# right_adjust=0.90
# hspace=0.5
# wspace=0.2
# ax.legend(loc='lower left', prop={'size':14})
plt.grid()
# plt.gcf().subplots_adjust(top=top_adjust, bottom=bottom_adjust, left=left_adjust, right=right_adjust, hspace=hspace, wspace=wspace)
fig.savefig("mean_contig_qvs.png")