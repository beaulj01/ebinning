import os,sys
from pbcore.io.align.CmpH5IO import CmpH5Reader
from Bio import SeqIO

box_contigs_fa = sys.argv[1]
cmph5          = "aligned_reads.cmp.h5"

box_contigs = set()
for contig in SeqIO.parse(box_contigs_fa, "fasta"):
	box_contigs.add(contig.name.split("|")[0])

reader = CmpH5Reader(cmph5)
box_reads = set()
for r in reader:
	if r.referenceName in box_contigs:
		box_reads.add(r.zmwName)

for read in list(box_reads):
	print read