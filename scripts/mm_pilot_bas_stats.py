import os,sys
import math
import numpy as np
from pbcore.io.BasH5IO import BasH5Reader
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pub_figs
from collections import Counter
import locale
locale.setlocale(locale.LC_ALL, 'en_US')

fofn = np.loadtxt(sys.argv[1], dtype="str")
pref = sys.argv[2]
fig  = pub_figs.init_fig(x=10,y=15)
ax1    = fig.add_subplot(4,1,1)
ax2    = fig.add_subplot(4,1,2)
ax3    = fig.add_subplot(4,1,3)
ax4    = fig.add_subplot(4,1,4)

colors = plt.get_cmap('spectral')(np.linspace(0, 0.95, len(fofn)))

n_bp    = 0
n_subs  = 0
n_reads = 0
for i,fn in enumerate(fofn):
	productivity = Counter()
	readscore    = []
	readlens     = []
	sublens      = []
	reader       = BasH5Reader(fn)
	movie        = "/".join(fn.split("/")[-3:])
	for j,r in enumerate(reader):
		# if j%1000==0:
			n_reads += 1
			productivity[r.productivity] += 1
			readscore.append(r.readScore)
			rlen = 0
			for s in r.subreads:
				n_subs += 1
				slen    = len(s.basecalls())
				rlen   += slen
				n_bp += slen
				sublens.append( slen )
			readlens.append(rlen)

	y,binEdges = np.histogram(sublens, bins=300)
	bincenters = 0.5*(binEdges[1:]+binEdges[:-1])
	ax1.plot(bincenters, y, linestyle='-', color=colors[i], linewidth=2, label=movie)
	ax1.set_xlabel("Subread lengths")
	ax1.set_xlim([0,20000])
	ax1.set_ylabel("N")
	
	y,binEdges = np.histogram(readlens,  bins=300)
	bincenters = 0.5*(binEdges[1:]+binEdges[:-1])
	ax2.plot(bincenters, y, linestyle='-', color=colors[i], linewidth=2)
	ax2.set_xlabel("Read lengths")
	ax2.set_ylabel("N")
	
	y,binEdges = np.histogram(readscore, bins=300)
	bincenters = 0.5*(binEdges[1:]+binEdges[:-1])
	ax3.plot(bincenters, y, linestyle='-', color=colors[i], linewidth=2)
	ax3.set_xlabel("Readscore")
	ax3.set_xlim([0.5,1.0])
	ax3.set_ylabel("N")

	ind     = np.arange(0,4)
	w       = 1.0/len(fofn)*0.8
	heights = map(lambda x: productivity[x], np.arange(0,4))
	offset  = w * i
	ax4.bar(ind+offset-(w/2), heights, width=w, facecolor=colors[i], linewidth=0)
	ax4.set_xlabel("ZMW loading")
	ax4.set_ylabel("N")
	ax4.set_xticks(range(0,4))
	ax4.set_xticklabels(range(0,4))
	ax4.set_xlim([-0.5,3.5])

ax1.legend(ncol=1, loc='lower left', bbox_to_anchor=(0.0, 1.0), frameon=False, fontsize=10)
avg_slen     = float(n_bp)/n_subs
avg_rlen     = float(n_bp)/n_reads
n_bp         = locale.format("%d", n_bp, grouping=True)
slens_gt3kb  = [slen for slen in sublens if slen > 3000]
slens_gt5kb  = [slen for slen in sublens if slen > 5000]
slens_gt10kb = [slen for slen in sublens if slen > 10000]
sum_3kb      = locale.format("%d", np.sum(slens_gt3kb),  grouping=True)
sum_5kb      = locale.format("%d", np.sum(slens_gt5kb),  grouping=True)
sum_10kb     = locale.format("%d", np.sum(slens_gt10kb), grouping=True)
string       = "Yield: %s bp\nReads: %s\nSubreads: %s\nAvg. readlength: %.1f\nAvg. sublength: %.1f\nSubreads>3kb: %s (%s bp)\nSubreads>5kb: %s (%s bp)\nSubreads>10kb: %s (%s bp)" % (n_bp, n_reads, n_subs, avg_rlen, avg_slen, len(slens_gt3kb), sum_3kb, len(slens_gt5kb), sum_5kb, len(slens_gt10kb), sum_10kb)
ax1.text(0.95, 0.95, string, fontsize=14, transform=ax1.transAxes, horizontalalignment='right', verticalalignment='top')

fig.savefig("%s.diagnostics.png" % pref)