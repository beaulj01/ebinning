import os,sys
import numpy as np
import glob
from Bio import SeqIO

bed_fn  = "coverage.bed"
contigs = "polished_assembly.fasta"


table   = np.loadtxt(bed_fn, dtype="str", delimiter="\t", skiprows=1)

new = []
for Seq in SeqIO.parse(contigs, "fasta"):
	contig = Seq.id.split("|")[0]

	contig_table = table[table[:,0]==contig]
	covs         = map(float, contig_table[:,4])
	cov_mean     = np.mean(covs)

	Seq.description = "%s_[cov=%.2f]" % (contig, cov_mean)
	Seq.id          = "%s_[cov=%.2f]" % (contig, cov_mean)
	new.append(Seq)

out_fn = contigs.replace(".fasta", ".mgrast.fasta")
SeqIO.write(new, out_fn, "fasta")