#!/bin/bash
module use /hpc/packages/minerva-mothra/modulefiles
module unload smrtanalysis
module load python samtools mpc


# Need to start in a directory with a directory called 'refs' that contains the reference
# fasta files that you'd like to use to simulate the short reads. The filenames should
# descriptive, since these will be propogated to all the simulated reads.

########################################################################
# Create versions of each reference that only contain the main chromosome
########################################################################
basedir=`pwd`
# echo "Preprocessing reference fastas; rename and remove plasmid entries"
ref_dir="refs"
ref1="C227.fasta"
ref1_abund=0.25
ref2="J99.fasta"
ref2_abund=0.75
# cd $ref_dir
# module load python
# python ~/gitRepo/eBinning/scripts/preprocess_assemblies_for_GemSim.py
ref_combo="combined.fasta"
# cat $ref1 $ref2 > $ref_combo
# cd ../
# echo "Done."



########################################################################
# Simulate short reads from these new 1-contig references
########################################################################
short_dir="short_reads"
# mkdir $short_dir
cd $short_dir
# abund_fn="abundance.txt"
# echo "Writing ${abund_fn}..."
# echo -e "$ref1\t$ref1_abund" >  $abund_fn
# echo -e "$ref2\t$ref2_abund" >> $abund_fn
# echo "Done."
# Nreads=10000000
# r_len=100
ins_size=250
# ln -s ~/projects/software/GemSIM_v1.6
ln -s ../refs
# GemSIM_v1.6/GemReads.py -R $ref_dir -a $abund_fn -n $Nreads -l $r_len -m GemSIM_v1.6/models/ill100v5_p.gzip -c -q 64 -o metashort -p -s 30 -u $ins_size
# # echo "GemSIM_v1.6/GemReads.py -R $ref_dir -a $abund_fn -n $Nreads -l $r_len -m GemSIM_v1.6/models/ill100v5_p.gzip -c -q 64 -o metashort -p -s 30 -u $ins_size" > run.bsub
# # bsub -m mothra -P acc_fangg03a -J sim -oo myjob.log -eo myjob${i}.err -q alloc -W 12:00 -u john.beaulaurier@gmail.com -M 1000000 < run.bsub



########################################################################
# Do assembly of metagenomic short reads
########################################################################
R1="metashort_fir.fastq"
R2="metashort_sec.fastq"
# python ~/gitRepo/eBinning/scripts/rename_GemSim_reads.py $R1 R1 > ${R1}.tmp
# python ~/gitRepo/eBinning/scripts/rename_GemSim_reads.py $R2 R2 > ${R2}.tmp
# mv ${R1}.tmp $R1
# mv ${R2}.tmp $R2

mkdir velvet
cd velvet
ln -s ../$R1
ln -s ../$R2
~/projects/software/velvet_1.2.10/velveth velveth_k31 31 -fastq -shortPaired -separate $R1 $R2
~/projects/software/velvet_1.2.10/velvetg velveth_k31 -ins_length $ins_size -exp_cov auto -cov_cutoff auto -min_contig_lgth 100
~/projects/software/MetaVelvet-1.2.02/meta-velvetg velveth_k31/ -ins_length $ins_size | tee metavelvet.out
mkdir contigs
cp velveth_k31/meta-velvetg.contigs.fa contigs/velvet_31.fa
# cp velveth_k31/contigs.fa contigs/velvet_31.fa

contigs="contigs/velvet_31.fa"

CONCOCT_SCRIPTS="/hpc/users/beaulj01/projects/ebinning/CONCOCT_compare/CONCOCT-0.4.0/scripts"
ln -s $contigs
# Chunk the assembled metagenomic contigs into 10Kb fragments (OPTIONAL)
# chunked="$(basename $contigs).c10K"
# python $CONCOCT_SCRIPTS/cut_up_fasta.py -c 10000 -o 0 -m $contigs > $chunked
# contigs=$chunked
# echo -e "\nAssembled contigs here: $contigs\n"



########################################################################
# Align the reads to the metagenomic contigs
# (1) First create the index on the assembly for bowtie2
# (2) For each sample create a folder and run map-bowtie2-markduplicates.sh
########################################################################
export MRKDUP=/hpc/packages/minerva-common/picard/1.112/bin/MarkDuplicates.jar
module load bowtie2 samtools bedtools py_packages
bowtie2-build $contigs $contigs

echo "Converting fastq to fasta for Bowtie alignment..."
R1_fa=$(echo $R1 | sed s/.fastq/.fasta/)
R2_fa=$(echo $R2 | sed s/.fastq/.fasta/)
awk '{if(NR%4==1) {printf(">%s\n",substr($0,2));} else if(NR%4==2) print;}' $R1 > $R1_fa
awk '{if(NR%4==1) {printf(">%s\n",substr($0,2));} else if(NR%4==2) print;}' $R2 > $R2_fa
echo "Done."

echo "Marking duplicates..."
mkdir -p map/$(basename $R1_fa)
cd map/$(basename $R1_fa)
bash $CONCOCT_SCRIPTS/map-bowtie2-markduplicates.sh -ct 1 -p '-f' ../../$R1_fa ../../$R2_fa pair ../../$contigs asm bowtie2
cd ../..
echo "Done."
# NOTE: this leaves us in the 'velvet' directory



########################################################################
# Label the contigs using the BAM files and contig_read_count_per_genome.py
########################################################################
counts="map/${R1_fa}/bowtie2/contig_read_counts.txt"
python ~/projects/ebinning/CONCOCT_compare/CONCOCT-0.4.0/scripts/contig_read_count_per_genome.py -m 4 $contigs ../$ref_dir/$ref_combo map/$R1_fa/bowtie2/asm_pair-smds.bam > $counts
mapping="map/${R1_fa}/bowtie2/contig_mapping.txt"
python ~/gitRepo/eBinning/scripts/assign_ref_to_contig.py $counts > $mapping
cd ../..
# NOTE: this returns us to the starting directory



########################################################################
# Align the strain-specific bax.h5 files to the metagenomic contig fragments
########################################################################
module unload gcc/4.8.2 py_packages
module load smrtpipe/2.2.0
long_dir="long_reads"
cd $long_dir
samples="C227 J99"
for sample in $samples
do
	cd $sample
	bax_fofn="${sample}.bax.h5.fofn"
	echo $bax_fofn
	metrics="DeletionQV,DeletionTag,InsertionQV,MergeQV,SubstitutionQV,IPD"
	pbalign.py -v --nproc 8 --forQuiver --minAccuracy 80 --metrics=$metrics $bax_fofn ../../short_reads/velvet/$contigs ${sample}.cmp.h5
	# echo "pbalign.py -v --nproc 8 --forQuiver --minAccuracy 80 --metrics=$metrics $bax_fofn ../../short_reads/velvet/$contigs ${sample}.cmp.h5" > run_pbalign.sh
	# bsub -m mothra -n 8 -R span[hosts=1] -P acc_fangg03a -J ${sample}pblalign -oo myjob.log -eo myjob.err -q alloc -W 8:00 -M 6000000 < run_pbalign.sh
	cd ..
done



########################################################################
# Downsample the individual cmp.h5 files to get desired number of reads
########################################################################
n_total_LR=80000

sample1="C227"
cd $sample1
stats="${sample1}.cmp.h5.stats"
cmph5tools.py summarize ${sample1}.cmp.h5 > $stats
n_reads=`grep "n reads" ${sample1}.cmp.h5.stats | cut -d" " -f4`
sample1_reads=`python -c "print ${ref1_abund} * ${n_total_LR}" | cut -d"." -f1`
frac=`python -c "print float(${sample1_reads})/${n_reads}"`
down_file1=${sample1}_${sample1_reads}.cmp.h5
cmph5tools.py select --where "SubSample(rate=$frac)" --outFile $down_file1 ${sample1}.cmp.h5
cd ..

sample2="J99"
cd $sample2
stats="${sample2}.cmp.h5.stats"
cmph5tools.py summarize ${sample2}.cmp.h5 > $stats
n_reads=`grep "n reads" ${sample2}.cmp.h5.stats | cut -d" " -f4`
sample2_reads=`python -c "print ${ref2_abund} * ${n_total_LR}" | cut -d"." -f1`
frac=`python -c "print float(${sample2_reads})/${n_reads}"`
down_file2=${sample2}_${sample2_reads}.cmp.h5
cmph5tools.py select --where "SubSample(rate=$frac)" --outFile $down_file2 ${sample2}.cmp.h5
cd ..



########################################################################
# Merge the individual cmp.h5 files to get a metagenomic cmp.h5 file
########################################################################
combo_sample="C227_J99_${n_total_LR}.cmp.h5"
cmph5tools.py merge --outFile ${combo_sample} ${sample1}/${down_file1} ${sample2}/${down_file2}



########################################################################
# Run the eBarcoding code in the metagenomic cmp.h5 file
########################################################################
module unload smrtpipe/2.2.0
module load py_packages
# n_reads=5000
motifs="motifs.txt"
echo "GATC-1"   > $motifs
echo "ACCACC-3" >> $motifs
echo "CTGCAG-4" >> $motifs
echo "GANTC-1" >> $motifs
echo "CATG-1" >> $motifs
echo "AAGNNNNNNCTC-1" >> $motifs
echo "GAGG-1" >> $motifs
echo "GAGNNNNNNCTT-1" >> $motifs
echo "AAGNNNNNCTT-1" >> $motifs
echo "CYANNNNNNTGA-2" >> $motifs
echo "TCANNNNNNTRG-2" >> $motifs
echo "AAGNNNNNNCTC-1" >> $motifs
echo "GCCTA-4" >> $motifs
echo "TCNNGA-5" >> $motifs
echo "CCGG-0" >> $motifs
echo "CCNNGG-0" >> $motifs
echo "CGBBV-0" >> $motifs
echo "GTAC-2" >> $motifs
echo "ATTAAT-4" >> $motifs
echo "RTAYNNNNNRTAY-2" >> $motifs
echo "CTTTANNNNNNCTT-4" >> $motifs
echo "AAGNNNNNNTAAAG-1" >> $motifs
echo "$combo_sample X" > cmp.h5.fofn
# python ~/gitRepo/eBinning/src/run_eBinning_cmph5.py -i --N_reads=$n_reads --procs=8 --cmph5_files=cmp.h5.fofn --motifs_file=$motifs
python ~/gitRepo/eBinning/src/run_eBinning_cmph5.py -i --procs=8 --cmph5_files=cmp.h5.fofn --motifs_file=$motifs
# Writes contig eBarcodes to the file contig_eBarcodes.txt



########################################################################
# Use the ref-contig mappings to add putative strain/species info to the contig eBarcodes
########################################################################
mapping="${basedir}/${short_dir}/velvet/${mapping}"
eBarcodes="${basedir}/${long_dir}/contig_eBarcodes.txt"
python ~/gitRepo/eBinning/scripts/map_cmph5ref0000_to_contig_name.py $combo_sample $eBarcodes $mapping