import shutil
import os,sys
import subprocess
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt

def run_OS_command( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	return sts, stdOutErr

def drop_appended_lines( fn, n_to_keep ):
	head_CMD       = "head -%s %s > tmp.head" % (n_to_keep, fn)
	sts, stdOutErr = run_OS_command( head_CMD )
	os.rename("tmp.head", fn)

def scatterplot(results, labels, plot_fn, sizes, title):
	if len(labels)>0:
		label_set = list(set(labels))
		label_set.sort()
		colors    = plt.get_cmap('rainbow')(np.linspace(0, 1.0, len(label_set)))
		shapes    = ["o", "v", "^", "s"]
		fig       = plt.figure(figsize=[15,12])
		ax        = fig.add_subplot(111)
		if len(sizes)>0:
			sizes[sizes<100000] = 100000
			scaled_sizes = sizes**1.5 / max(sizes**1.5) * 2000
		for k,target_lab in enumerate(label_set):
			idxs  = [j for j,label in enumerate(labels) if label==target_lab]
			X     = results[idxs,0]
			Y     = results[idxs,1]
			if len(sizes)>0:
				if target_lab=="unknown":
					ax.scatter(X, Y, edgecolors="r", label=target_lab, marker="+", facecolors="r", lw=3, alpha=0.3, s=15)
				else:
					scaled_sizes_idx = np.array(scaled_sizes)[idxs]
					ax.scatter(X, Y, edgecolors=colors[k], label=target_lab, marker=shapes[k%4], facecolors="None", lw=3, alpha=0.7, s=scaled_sizes_idx)
			else:
				if target_lab=="unknown":
					ax.scatter(X, Y, marker="+", s=15 , edgecolors="r", label=target_lab, facecolors="r")
				else:
					# ax.scatter(X, Y, edgecolors=colors[k], label=target_lab, facecolors="None", alpha=0.7)
					ax.scatter(X, Y, marker="o", s=15 , edgecolors="None", label=target_lab, facecolors=colors[k])
		box = ax.get_position()
		ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
		ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':14})
		ax.set_title(title)
		plt.savefig(plot_fn)
	else:
		fig       = plt.figure(figsize=[12,12])
		ax        = fig.add_subplot(111)
		if len(sizes)>0:
			sizes[sizes<100000] = 100000
			scaled_sizes = sizes**1.5 / max(sizes**1.5) * 2000
		X     = results[:,0]
		Y     = results[:,1]
		if len(sizes)>0:
			scaled_sizes_idx = np.array(scaled_sizes)[idxs]
			ax.scatter(X, Y, marker="o", lw=3, alpha=0.5, s=scaled_sizes_idx)
		else:
			ax.scatter(X, Y, marker="o", s=15 ,edgecolors="None")
		ax.set_title(title)
		plt.savefig(plot_fn)

contig_names_fn         = "contigs.names"
contig_labels_fn        = "contigs.labels"
contig_lengths_fn       = "contigs.lengths"
contig_comp_fn          = "contigs.comp"
contig_comp_2D_fn       = "contigs.comp.2D"
# contig_comp_2D_z_fn     = "contigs.comp.2D.zscores"
contig_cov_comp_3D_fn   = "contigs.cov_comp.3D"
contig_cov_comp_2D_fn   = "contigs.cov_comp.2D"
contig_cov_comp_2D_z_fn = "contigs.cov_comp.2D.zscores"
contig_SCp_fn           = "contigs.SCp"
contig_SCp_2D_fn        = "contigs.SCp.2D"
contig_SCp_2D_z_fn      = "contigs.SCp.2D.zscores"
contig_cov_fn           = "contigs.cov"
contig_4D_z_fn          = "contigs.combined.4D.zscores"
contig_combo_2D_fn      = "contigs.combined.2D"

shutil.copy( contig_comp_fn,          contig_comp_fn+".digest" )
shutil.copy( contig_comp_2D_fn,       contig_comp_2D_fn+".digest" )
# shutil.copy( contig_comp_2D_z_fn,     contig_comp_2D_z_fn+".digest" )
shutil.copy( contig_cov_comp_3D_fn,   contig_cov_comp_3D_fn+".digest" )
shutil.copy( contig_cov_comp_2D_fn,   contig_cov_comp_2D_fn+".digest" )
shutil.copy( contig_cov_comp_2D_z_fn, contig_cov_comp_2D_z_fn+".digest" )
shutil.copy( contig_SCp_fn,           contig_SCp_fn+".digest" )
shutil.copy( contig_SCp_2D_fn,        contig_SCp_2D_fn+".digest" )
shutil.copy( contig_SCp_2D_z_fn,      contig_SCp_2D_z_fn+".digest" )
shutil.copy( contig_4D_z_fn,          contig_4D_z_fn+".digest" )
shutil.copy( contig_combo_2D_fn,      contig_combo_2D_fn+".digest" )
shutil.copy( contig_names_fn,         contig_names_fn+".digest" )
shutil.copy( contig_labels_fn,        contig_labels_fn+".digest" )
shutil.copy( contig_cov_fn,           contig_cov_fn+".digest" )
shutil.copy( contig_lengths_fn,       contig_lengths_fn+".digest" )

n_contigs = int(sys.argv[1])

drop_appended_lines( contig_comp_fn,          n_contigs )
drop_appended_lines( contig_comp_2D_fn,       n_contigs )
# drop_appended_lines( contig_comp_2D_z_fn,     n_contigs )
drop_appended_lines( contig_cov_comp_3D_fn,   n_contigs )
drop_appended_lines( contig_cov_comp_2D_fn,   n_contigs )
drop_appended_lines( contig_cov_comp_2D_z_fn, n_contigs )
drop_appended_lines( contig_SCp_fn,           n_contigs )
drop_appended_lines( contig_SCp_2D_fn,        n_contigs )
drop_appended_lines( contig_SCp_2D_z_fn,      n_contigs )
drop_appended_lines( contig_4D_z_fn,          n_contigs )
drop_appended_lines( contig_combo_2D_fn,      n_contigs )
drop_appended_lines( contig_names_fn,         n_contigs )
drop_appended_lines( contig_labels_fn,        n_contigs )
drop_appended_lines( contig_cov_fn,           n_contigs )
drop_appended_lines( contig_lengths_fn,       n_contigs )
scatterplot(np.loadtxt(contig_combo_2D_fn,    dtype="float"),  np.loadtxt(contig_labels_fn, dtype="str"), "contigs.combined.noDigest.scatter.png", np.loadtxt(contig_lengths_fn, dtype="int"), "contigs_combined_noDigest")
scatterplot(np.loadtxt(contig_comp_2D_fn,     dtype="float"),  np.loadtxt(contig_labels_fn, dtype="str"), "contigs.comp.noDigest.scatter.png",     np.loadtxt(contig_lengths_fn, dtype="int"), "contigs_comp_noDigest")
scatterplot(np.loadtxt(contig_cov_comp_2D_fn, dtype="float"),  np.loadtxt(contig_labels_fn, dtype="str"), "contigs.cov_comp.noDigest.scatter.png", np.loadtxt(contig_lengths_fn, dtype="int"), "contigs_cov_comp_noDigest")