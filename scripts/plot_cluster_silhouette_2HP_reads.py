"""
===============================================================================
Selecting the number of clusters with silhouette analysis on KMeans clustering
===============================================================================

Silhouette analysis can be used to study the separation distance between the
resulting clusters. The silhouette plot displays a measure of how close each
point in one cluster is to points in the neighboring clusters and thus provides
a way to assess parameters like number of clusters visually. This measure has a
range of [-1, 1].

Silhoette coefficients (as these values are referred to as) near +1 indicate
that the sample is far away from the neighboring clusters. A value of 0
indicates that the sample is on or very close to the decision boundary between
two neighboring clusters and negative values indicate that those samples might
have been assigned to the wrong cluster.

In this example the silhouette analysis is used to choose an optimal value for
``n_clusters``. The silhouette plot shows that the ``n_clusters`` value of 3, 5
and 6 are a bad pick for the given data due to the presence of clusters with
below average silhouette scores and also due to wide fluctuations in the size
of the silhouette plots. Silhouette analysis is more ambivalent in deciding
between 2 and 4.

Also from the thickness of the silhouette plot the cluster size can be
visualized. The silhouette plot for cluster 0 when ``n_clusters`` is equal to
2, is bigger in size owing to the grouping of the 3 sub clusters into one big
cluster. However when the ``n_clusters`` is equal to 4, all the plots are more
or less of similar thickness and hence are of similar sizes as can be also
verified from the labelled scatter plot on the right.
"""
from sklearn.metrics import silhouette_samples, silhouette_score
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import sys

def h(seq):
	#http://stackoverflow.com/questions/3382352/equivalent-of-numpy-argsort-in-basic-python/3382369#3382369
	#by unutbu
	return sorted(range(len(seq)), key=seq.__getitem__)

X              = np.loadtxt(sys.argv[1], dtype="float")
cluster_labels = np.loadtxt(sys.argv[2], dtype="str")

label_dict = {"26695": r"${H. pylori}$ 26695", \
			  "J99": r"${H. pylori}$ J99", \
			  "C227": r"${E. coli}$ C227", \
			  "Steno": r"${S. maltophilia}$"}

# Sort the data points alphabetically by cluster label
idx            = h(cluster_labels)
cluster_labels = cluster_labels[idx]
X              = X[idx]
n_clusters     = len(set(cluster_labels))
labels_list    = list(set(cluster_labels))
labels_list.sort()

label_ids      = []
for lab in cluster_labels:
	label_ids.append(labels_list.index(lab))
label_ids = np.array(label_ids)

# Create a subplot with 1 row and 2 columns
fig, ax1 = plt.subplots(1, 1)
fig.set_size_inches(7, 7)

# The 1st subplot is the silhouette plot
# The silhouette coefficient can range from -1, 1 but in this example all
# lie within [-0.1, 1]
ax1.set_xlim([-0.1, 1])
# The (n_clusters+1)*10 is for inserting blank space between silhouette
# plots of individual clusters, to demarcate them clearly.
ax1.set_ylim([0, len(X) + (n_clusters + 1) * 10])

# The silhouette_score gives the average value for all the samples.
# This gives a perspective into the density and separation of the formed
# clusters
silhouette_avg = silhouette_score(X, cluster_labels)
print "For n_clusters =", n_clusters, "The average silhouette_score is :", silhouette_avg

# Compute the silhouette scores for each sample
sample_silhouette_values = silhouette_samples(X, cluster_labels)

y_lower = 10
# for i in range(n_clusters):
c1 = cm.spectral(float(0)  / 20)
c2 = cm.spectral(float(6)  / 20)
c3 = cm.spectral(float(15) / 20)
c4 = cm.spectral(float(17) / 20)
for i in range(n_clusters):
	# Aggregate the silhouette scores for samples belonging to
	# cluster i, and sort them
	ith_cluster_silhouette_values = sample_silhouette_values[cluster_labels == labels_list[i]]

	ith_cluster_silhouette_values.sort()

	size_cluster_i = ith_cluster_silhouette_values.shape[0]
	y_upper = y_lower + size_cluster_i

	color = cm.spectral(float(i) / n_clusters)
	ax1.fill_betweenx(np.arange(y_lower, y_upper),
					  0, ith_cluster_silhouette_values,
					  facecolor=color, edgecolor=color, alpha=0.7)

	# Label the silhouette plots with their cluster numbers at the middle
	ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

	# Compute the new y_lower for next plot
	y_lower = y_upper + 10  # 10 for the 0 samples

ax1.set_title("Silhouette plot for the clusters")
ax1.set_xlabel("Silhouette coefficient values")
ax1.set_ylabel("Cluster label")

# The vertical line for average silhoutte score of all the values
ax1.axvline(x=silhouette_avg, color="red", linestyle="--")

ax1.set_yticks([])  # Clear the yaxis labels / ticks
ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])

# 2nd Plot showing the actual clusters formed
# colors = cm.spectral(label_ids.astype(float) / n_clusters)
# ax2.scatter(X[:, 0], X[:, 1], marker='.', s=30, lw=0, alpha=0.7, c=colors)

# ax2.set_title("The visualization of the clustered data")
# ax2.set_xlabel("Feature space for the 1st feature")
# ax2.set_ylabel("Feature space for the 2nd feature")

plt.suptitle("Silhouette analysis (avg = %.3f)" % silhouette_avg, fontsize=14, fontweight='bold')

plot_fn = sys.argv[1]+".sil.png"
plt.savefig(plot_fn)
