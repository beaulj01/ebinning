import os,sys
import urllib2
from Bio import SeqIO
import glob
from itertools import groupby
import re
import subprocess

def run_OS( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	return sts, stdOutErr

org_dirs = glob.glob("*")
orig     = os.getcwd()
for i,org_dir in enumerate(org_dirs):
	if org_dir=="0_final_fasta":
		continue
	print i, org_dir
	os.chdir(org_dir)
	to_rm  = glob.glob("*.taxid.fna")
	for bad in to_rm:
		os.remove(bad)
	fn     = glob.glob("*.gbk")[0]
	f      = open(fn, "r").read()
	m      = re.search(r"taxon:\w+", f)
	taxid  = m.group().split(":")[1]
	fastas = glob.glob("*.fna")
	for fasta in fastas:
		if fasta.find(".taxid.")>-1:
			continue
		f = open(fasta.replace(".fna", ".taxid.fna"),"w")
		for j,seq_record in enumerate(SeqIO.parse(fasta, "fasta")):
			tax_str       = "kraken:taxid|%s|" % taxid
			seq_record.id = tax_str + seq_record.id
			f.write(seq_record.format("fasta"))
		f.close()

	to_cat  = " ".join(glob.glob("*.taxid.fna"))
	out     = os.path.join("..", "0_final_fasta", org_dir+".taxid.fna")
	cat_CMD = "cat %s > %s" % (to_cat, out)
	sts, stdOutErr = run_OS(cat_CMD)
	os.chdir(orig)
