import os,sys

"""
Read names are expected to look like this:

>gi|29611500|ref|NC_004703.1|_Bacteroides_thetaiotaomicron_VPI-5482_nr0_+_R1
"""

fn = sys.argv[1]
R  = sys.argv[2]

for i,line in enumerate(open(fn).xreadlines()):
	if i%4==0: # name
		line = line.strip()
		ref_name = "_".join(line.split("_")[2:5])
		read_num = line.split("_")[0].strip("@r")
		strand   = 0
		print "@%s_nr%s_+_%s" % (ref_name, read_num, R)
	else:
		line = line.strip()
		print line