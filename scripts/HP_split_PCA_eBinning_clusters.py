import os,sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import random

#####################################
PCA      = "reads.SMp.gt5000.PCA"
labels   = "reads.raw.labels.gt5000"
names    = "reads.raw.names.gt5000"
#####################################

combined_dirs    = ["/hpc/users/beaulj01/projects/ebinning/2HP/combined_HGAP/J99_150x_and_26695_50x/binning_bas", \
					"/hpc/users/beaulj01/projects/ebinning/2HP/combined_HGAP/J99_150x_and_26695_100x/binning_bas", \
					"/hpc/users/beaulj01/projects/ebinning/2HP/combined_HGAP/J99_150x_and_26695_150x/binning_bas"]

assembly_names  = ["J99_150x_and_26695_50x", \
				   "J99_150x_and_26695_100x", \
				   "J99_150x_and_26695_150x"]

assembly_labels = {"J99_150x_and_26695_30x" : " 30x 26695, 150x J99", \
				   "J99_150x_and_26695_50x" : " 50x 26695, 150x J99", \
				   "J99_150x_and_26695_100x": "100x 26695, 150x J99", \
				   "J99_150x_and_26695_150x": "150x 26695, 150x J99"}

label_dict = {"26695": r"${H. pylori}$ 26695", \
			  "J99": r"${H. pylori}$ J99"}

fig = plt.figure(figsize=[12,12])
ax  = fig.add_subplot(111) 
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.tick_params(labelcolor='w', top='off', bottom='off', left='off', right='off')

for d,dir_name in enumerate(combined_dirs):
	ax          = fig.add_subplot(len(assembly_names),2,2*d+1)

	pca_fn      = os.path.join(dir_name, PCA)
	read_labs   = os.path.join(dir_name, labels)
	read_names  = os.path.join(dir_name, names)
	SMp_coords  = np.loadtxt(pca_fn,     dtype="float")
	read_labels = np.loadtxt(read_labs,  dtype="str")
	read_names  = np.loadtxt(read_names, dtype="str")

	label_set = list(set(read_labels))
	label_set.sort()

	colors    = plt.get_cmap('rainbow')(np.linspace(0, 1.0, len(label_set)))
	to_plot   = []
	for k,target_lab in enumerate(label_set):
		idxs             = [j for j,label in enumerate(read_labels) if label==target_lab]
		X                = SMp_coords[idxs,0]
		Y                = SMp_coords[idxs,1]
		scaled_sizes_idx = (np.zeros(len(read_labels))+1)*10
		for i,(x,y) in enumerate(SMp_coords[idxs,:]):
			to_plot.append( (x,y,target_lab, colors[k], scaled_sizes_idx[i]) )
	np.random.shuffle(to_plot)
	plotted_labs = set()
	legend_plots = []
	legend_labs  = []
	for i,(x,y,lab,color,scaled_size) in enumerate(to_plot):
		plot = ax.scatter(x, y, edgecolors=color, label=label_dict[lab], facecolors="None", lw=3, alpha=0.15, s=scaled_size)
		if lab not in plotted_labs:
			plotted_labs.add(lab)
			legend_plots.append(plot)
			legend_labs.append(label_dict[lab])
	box = ax.get_position()
	ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
	leg_tup = zip(legend_labs,legend_plots)
	leg_tup.sort(key=lambda x: x[0])
	legend_labs, legend_plots = zip(*leg_tup)
	# ax.legend(legend_plots, legend_labs, bbox_to_anchor=(1.05, 1.05), prop={'size':24}, scatterpoints=1, frameon=False)
	# ax.legend(legend_plots, legend_labs, loc="upper right", prop={'size':24}, scatterpoints=1)
	
	if d==0:
		if PCA.find("gt10000")>-1:
			m    = -0.8
			b    = -1.1
			xmin = -4
			xmax = 1
			ymin = -3
			ymax = 3
		elif PCA.find("gt8000")>-1:
			m    = -.8
			b    = 1.1
			xmin = 0
			xmax = 4
			ymin = -3
			ymax = 3
		elif PCA.find("gt7000")>-1:
			m    = 0.8
			b    = 1.0
			xmin = -4
			xmax = 1
			ymin = -3
			ymax = 3
		elif PCA.find("gt5000")>-1:
			m    = 0.8
			b    = -1.1
			xmin = 0
			xmax = 4
			ymin = -3
			ymax = 3
	elif d==1:
		if PCA.find("gt10000")>-1:
			m    = 0.9
			b    = -1.5
			xmin = -1
			xmax = 4
			ymin = -3
			ymax = 3
		elif PCA.find("gt8000")>-1:
			m    = -0.85
			b    = 1.4
			xmin = 0
			xmax = 4
			ymin = -3
			ymax = 3
		elif PCA.find("gt7000")>-1:
			m    = 0.9
			b    = -1.5
			xmin = -1
			xmax = 4
			ymin = -3
			ymax = 3
		elif PCA.find("gt5000")>-1:
			m    = -0.8
			b    = -1.3
			xmin = -4
			xmax = 0
			ymin = -3
			ymax = 3
	elif d==2:
		if PCA.find("gt10000")>-1:
			m    = -0.8
			b    = -1.5
			xmin = -4
			xmax = 1
			ymin = -3
			ymax = 3
		elif PCA.find("gt8000")>-1:
			m    = -0.8
			b    = 1.4
			xmin = 0
			xmax = 4
			ymin = -3
			ymax = 3
		elif PCA.find("gt7000")>-1:
			m    = -0.8
			b    = 1.4
			xmin = 0
			xmax = 4
			ymin = -3
			ymax = 3
		elif PCA.find("gt5000")>-1:
			m    = 0.8
			b    = -1.5
			xmin = 0
			xmax = 4
			ymin = -3
			ymax = 3

	ax.set_xlim([xmin, xmax])
	ax.set_ylim([ymin, ymax])
	x1 = np.array(range(xmin,xmax+1)) 
	y1 = eval("m*x1 + b")
	ax.plot(x1, y1, linestyle="--", color="k")
	ax.set_xticks(range(xmin,xmax+1,1))
	ax.set_yticks(range(ymin,ymax+1,1))
	
	ax                = fig.add_subplot(len(assembly_names),2,2*d+2)
	extent            = [xmin, xmax, ymin, ymax]
	H, xedges, yedges = np.histogram2d(SMp_coords[:,0], SMp_coords[:,1], bins=25, range=[extent[:2], extent[2:]])
	H = np.rot90(H)
	H = np.flipud(H)
	Hmasked = np.ma.masked_where(H==0,H) # Mask pixels with a value of zero
	plt.hot()                                    # set 'hot' as default colour map
	im = plt.imshow(H, interpolation='bilinear', # creates background image
	                origin='lower', cmap=cm.Blues, 
	                extent=extent)
	levels            = [200,100,50,25]
	cols              = ["k","0.4","0.6","0.8"]
	cset              = plt.contour(H, levels, origin="lower",colors=cols,linewidths=1.4,extent=extent)
	plt.clabel(cset, inline=1, fontsize=10, fmt="%1.0i")
	for c in cset.collections:
		c.set_linestyle("solid")
	y1 = eval("m*x1 + b")
	plt.plot(x1, y1, linestyle="--", color="k")
	ax.set_xticks(range(xmin,xmax+1,1))
	ax.set_yticks(range(ymin,ymax+1,1))
	
	if PCA.find("gt10000")>-1:
		fn1 = "%s_whitelist_J99.gt10000.txt" % assembly_names[d]
		fn2 = "%s_whitelist_26695.gt10000.txt" % assembly_names[d]
		plot_fn = "reads.raw.SMp.gt10000.PCA.png"
	elif PCA.find("gt8000")>-1:
		fn1 = "%s_whitelist_J99.gt8000.txt" % assembly_names[d]
		fn2 = "%s_whitelist_26695.gt8000.txt" % assembly_names[d]
		plot_fn = "reads.raw.SMp.gt8000.PCA.png"
	elif PCA.find("gt7000")>-1:
		fn1 = "%s_whitelist_J99.gt7000.txt" % assembly_names[d]
		fn2 = "%s_whitelist_26695.gt7000.txt" % assembly_names[d]
		plot_fn = "reads.raw.SMp.gt7000.PCA.png"
	elif PCA.find("gt5000")>-1:
		fn1 = "%s_whitelist_J99.gt5000.txt" % assembly_names[d]
		fn2 = "%s_whitelist_26695.gt5000.txt" % assembly_names[d]
		plot_fn = "reads.raw.SMp.gt5000.PCA.png"
	f1 = open(fn1, "w")
	f2 = open(fn2, "w")
	for i in range(SMp_coords.shape[0]):
		x = SMp_coords[i,0]
		y = SMp_coords[i,1]
		if PCA.find("gt10000")>-1:
			if y<(m*x + b):
				f1.write("%s\n" % read_names[i])
			elif y>(m*x + b):
				f2.write("%s\n" % read_names[i])
		elif PCA.find("gt8000")>-1:
			if y>(m*x + b):
				f1.write("%s\n" % read_names[i])
			elif y<(m*x + b):
				f2.write("%s\n" % read_names[i])
		elif PCA.find("gt7000")>-1:
			if d==1:
				if y<(m*x + b):
					f1.write("%s\n" % read_names[i])
				elif y>(m*x + b):
					f2.write("%s\n" % read_names[i])
			else:
				if y>(m*x + b):
					f1.write("%s\n" % read_names[i])
				elif y<(m*x + b):
					f2.write("%s\n" % read_names[i])
		elif PCA.find("gt5000")>-1:
			if y<(m*x + b):
				f1.write("%s\n" % read_names[i])
			elif y>(m*x + b):
				f2.write("%s\n" % read_names[i])
	f1.close()
	f2.close()

plot_fn = "reads.raw.SMp.gt5000.PCA.png"
plt.savefig(plot_fn)