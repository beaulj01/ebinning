import os,sys
import numpy as np
import csv
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import scipy.spatial.distance as dist
from sklearn import manifold,metrics

comp_cov = np.loadtxt(sys.argv[1], dtype="float")
SCp      = np.loadtxt(sys.argv[2], dtype="float")
labels   = np.loadtxt(sys.argv[3], dtype="str")
sizes    = np.loadtxt(sys.argv[4], dtype="int")

comp_cov /= comp_cov.max()
SCp      /= SCp.max()

def rank_array_vals( array ):
	order = array.argsort()
	ranks = order.argsort()
	return ranks

def scatterplot(results, labels, plot_fn, sizes, title):
	if len(labels)>0:
		label_set = list(set(labels))
		label_set.sort()
		colors    = plt.get_cmap('rainbow')(np.linspace(0, 1.0, len(label_set)))
		shapes    = ["o", "v", "^", "s"]
		fig       = plt.figure(figsize=[15,12])
		ax        = fig.add_subplot(111)
		if len(sizes)>0:
			sizes[sizes<100000] = 100000
			scaled_sizes = sizes**1.5 / max(sizes**1.5) * 2000
		for k,target_lab in enumerate(label_set):
			idxs  = [j for j,label in enumerate(labels) if label==target_lab]
			X     = results[idxs,0]
			Y     = results[idxs,1]
			if len(sizes)>0:
				if target_lab=="unknown":
					ax.scatter(X, Y, edgecolors="r", label=target_lab, marker="+", facecolors="r", lw=3, alpha=0.3, s=15)
				else:
					scaled_sizes_idx = np.array(scaled_sizes)[idxs]
					ax.scatter(X, Y, edgecolors=colors[k], label=target_lab, marker=shapes[k%4], facecolors="None", lw=3, alpha=0.7, s=scaled_sizes_idx)
			else:
				if target_lab=="unknown":
					ax.scatter(X, Y, marker="+", s=15 , edgecolors="r", label=target_lab, facecolors="r")
				else:
					# ax.scatter(X, Y, edgecolors=colors[k], label=target_lab, facecolors="None", alpha=0.7)
					ax.scatter(X, Y, marker="o", s=15 , edgecolors="None", label=target_lab, facecolors=colors[k])
		box = ax.get_position()
		ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
		ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':14})
		ax.set_title(title)
		plt.savefig(plot_fn)

comp_cov_d_array = dist.pdist(comp_cov, 'euclidean')
SCp_d_array      = dist.pdist(SCp,      'euclidean')

com_cov_ranks = rank_array_vals( comp_cov_d_array )
SCp_ranks     = rank_array_vals( SCp_d_array )

comp_cov_d_mat     = dist.squareform( comp_cov_d_array )
comp_cov_ranks_mat = dist.squareform( com_cov_ranks )
SCp_d_mat          = dist.squareform( SCp_d_array )
SCp_ranks_mat      = dist.squareform( SCp_ranks )

dists = np.zeros(comp_cov_ranks_mat.shape)
for i in range(comp_cov_ranks_mat.shape[0]):
	for j in range(comp_cov_ranks_mat.shape[1]):
		# if comp_cov_ranks_mat[i,j] < SCp_ranks_mat[i,j]:
		if comp_cov_ranks_mat[i,j] > SCp_ranks_mat[i,j]:
			val = comp_cov_d_mat[i,j]
		# elif comp_cov_ranks_mat[i,j] >= SCp_ranks_mat[i,j]:
		elif comp_cov_ranks_mat[i,j] <= SCp_ranks_mat[i,j]:
			val = SCp_d_mat[i,j]
		# val = (comp_cov_d_mat[i,j] + SCp_d_mat[i,j]) / 2
		
		dists[i,j] = val

adist   = np.array(dists)
amax    = np.amax(adist)
adist  /= amax

mds     = manifold.MDS(n_components=2, dissimilarity="precomputed", random_state=6)
results = mds.fit(adist)

coords = results.embedding_
fn     = "distance_matrix_maxRank_covComp_SCp.noDigest.2D"
f = open(fn, "w")
for coord in coords:
	f.write("%s\t%s\n" % (coord[0], coord[1]))
f.close

print "Silhouette Coefficient: %0.3f" % metrics.silhouette_score(coords, labels)

scatterplot(coords, labels, "%s.png" % fn, sizes, "title")
