import os,sys
import glob

coords_fns = glob.glob("*.coords")

for coords_fn in coords_fns:
	prefix = coords_fn.replace(".coords", "")
	lines  = open(coords_fn, "rb").readlines()
	if len(lines)==5 and lines[-1].split("\t")[9]==prefix:
		print prefix