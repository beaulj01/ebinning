import pysam
import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from collections import defaultdict
import numpy as np
import pub_figs
import itertools
import operator

org     = sys.argv[1]
bam     = "%s.sorted.bam" % org
bamfile = pysam.AlignmentFile(bam, "rb")
pileup  = bamfile.pileup()

# Build bit array to track non-zero cov
pos_array = np.zeros(np.sum(bamfile.lengths))

pileup  = bamfile.pileup()
x = []
y = []	
windowsize       = 1
window_vals      = defaultdict(list)
nonzero_cov_pos  = 0
total_pos        = 0
for p in pileup:
	pos       = p.pos
	cov       = p.n
	rpos      = p.reference_pos
	rid       = p.reference_id
	if p.reference_id==1:
		pos += bamfile.lengths[0]
	window_id = pos/windowsize
	window_vals[window_id].append(cov)
	# print rid, pos, window_id, cov
	if cov>0:
		pos_array[pos] = 1

cov_pct = 100*float(pos_array.sum()) / np.sum(bamfile.lengths)

print "%s\t%.2f" % (org, cov_pct)

# Compile list of no-coverage intervals
no_cov_intervals = []
no_cov_blocks    = [[i for i,value in it] for key,it in itertools.groupby(enumerate(pos_array), key=operator.itemgetter(1)) if key==0]
for block in no_cov_blocks:
	no_cov_intervals.append( (block[0], block[-1]) )

for i in range(np.sum(bamfile.lengths)/windowsize+1):
	if len(window_vals[i])==0:
		mean = 0
	elif len(window_vals[i])==1:
		mean = window_vals[i][0]
	else:
		mean = np.mean(window_vals[i])
	pos  = i * windowsize
	# print i, pos, mean
	x.append(pos)
	y.append(mean)

fig = plt.figure(figsize=[18,10])
ax  = fig.add_axes([0.1, 0.3, 0.8, 0.4])
ax.plot(x, y, linestyle="-", color="k")
for x in no_cov_intervals:
	ax.axvspan(x[0], x[1], color='tomato')

tick_labs  = []
for n in range(500000,np.sum(bamfile.lengths), 500000):
	tick_labs.append( float(n)/1000000 )
ax.set_xticks(range(500000,np.sum(bamfile.lengths), 500000))
ax.set_xticklabels(tick_labs, rotation=45, ha="right")
ax.set_xlabel("Genome position (Mb)")
ax.set_ylabel("Coverage")
ax.set_xlim([0,len(pos_array)])
ax.set_title( pub_figs.tex_escape(org) )
pub_figs.remove_top_right_axes(ax)
pub_figs.setup_math_fonts(24)
plot_fn = "%s.cov.png" % org
plt.savefig(plot_fn)