import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import scipy
from scipy.stats import gaussian_kde
import sklearn.cluster as cluster
import pub_figs
import random
from pbcore.io.BasH5IO import BasH5Reader
from itertools import groupby

def fasta_iter( fasta_name ):
	fh = open(fasta_name)
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		header = header.next()[1:].strip()
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

data          = np.loadtxt("both.comp.2D", dtype="float")
labels        = np.loadtxt("both.labels",  dtype="str")
names         = np.loadtxt("both.names",   dtype="str")
contigs_fasta = "polished_assembly.fasta"
plot_fn       = "pub.reads.contigs.contour.png"
title         = "contig- and read-level cBinning"
# data     = np.loadtxt("reads.raw.comp.2D", dtype="float")
# labels   = np.loadtxt("reads.raw.labels",  dtype="str")
# names    = np.loadtxt("reads.raw.names",   dtype="str")
# plot_fn  = "pub.reads.comp.contour.png"
# title    = "read-level cBinning"
box_dims = [int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4]),]
pre      = sys.argv[5]
bins     = 70

fig               = plt.figure()
ax                = fig.add_subplot(111)
extent            = [-60,60,-60,60]
H, xedges, yedges = np.histogram2d(data[:,0], data[:,1], bins=bins, range=[extent[:2], extent[2:]])
H = np.rot90(H)
H = np.flipud(H)
Hmasked = np.ma.masked_where(H==0,H)         # Mask pixels with a value of zero
plt.hot()                                    # set 'hot' as default colour map
im = plt.imshow(H, interpolation='bilinear', # creates background image
                origin='lower', cmap=cm.Blues, 
                extent=extent)
plt.subplots_adjust(bottom=0.15, left=0.15)
levels            = [500,400,300,200]
cols              = ["k","0.4","0.6","0.8"]
cset              = plt.contour(H, levels, origin="lower",colors=cols,linewidths=1.4,extent=extent)
plt.clabel(cset, inline=1, fontsize=10, fmt="%1.0i")
for c in cset.collections:
	c.set_linestyle("solid")
minx = min(data[:,0])
maxx = max(data[:,0])
miny = min(data[:,1])
maxy = max(data[:,1])
box_xmin = box_dims[0]
box_xmax = box_dims[1]
box_ymin = box_dims[2]
box_ymax = box_dims[3]
ax.set_xlim([min(minx-1, box_xmin-1), max(maxx+1, box_xmax+1)])
ax.set_ylim([min(miny-1, box_ymin-1), max(maxy+1, box_ymax+1)])
ax.plot([box_xmin,box_xmax,box_xmax,box_xmin,box_xmin], [box_ymax,box_ymax,box_ymin,box_ymin,box_ymax], linestyle="--", color="k", linewidth=3)
plt.savefig(plot_fn)

box_contignames = set()
for i in range(data.shape[0]):
	x = data[i,0]
	y = data[i,1]
	if y>box_ymin and y<box_ymax and x>box_xmin and x<box_xmax and names[i].find("unitig")>-1:
		box_contignames.add(names[i])

print "Found %s contigs in box..." % len(box_contignames)
f = open("box_reads_contigs_%s.fasta" % pre, "w")
for name,seq in fasta_iter(contigs_fasta):
	if name.split("|")[0] in box_contignames:
		f.write(">%s\n" % name.split("|")[0])
		f.write("%s\n" % seq)
f.close()

plt.gca().set_aspect('equal', adjustable='box')
plt.savefig("2d_density_plot_%s.png" % pre)

