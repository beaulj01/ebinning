import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import scipy
from scipy.stats import gaussian_kde
import sklearn.cluster as cluster
import pub_figs
import random
from pbcore.io.BasH5IO import BasH5Reader
from itertools import groupby

data     = np.loadtxt("both.comp.2D", dtype="float")
labels   = np.loadtxt("both.labels",  dtype="S40")
names    = np.loadtxt("both.names",   dtype="str")
plot_fn  = "reads.comp.tax.png"
title    = "contig-level cBinning"
reads_tax_fn = "reads.tax.unordered"
corrected_fn = "corrected.fasta"
box_dims = [int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4]),]

def fasta_iter( fasta_name ):
	fh = open(fasta_name)
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		header = header.next()[1:].strip()
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

corrected = set()
for name,seq in fasta_iter(corrected_fn):
	read = "/".join(name.split("/")[:2])
	corrected.add(read)

read_specs = {}
for line in open(reads_tax_fn, "r").xreadlines():
	line = line.strip()
	name = "/".join(line.split("\t")[0].split("/")[:2])
	tax  = line.split("\t")[1]
	spec = " ".join(tax.split(";")[-1].split(" ")[:2])
	read_specs[name] = spec

reads_labeled = set(read_specs.keys())
both_names    = set(names)

for i,name in enumerate(names):
	if name.find("unitig")>-1:
		labels[i] = "Contig"
	elif name in corrected:
		if read_specs.get(name):
			labels[i] = read_specs[name]
		else:
			labels[i] = "Unknown read"
	else:
		labels[i] = "Uncorrected read"

print "All sequences",     len(labels)
print "Unlabeled reads",   len(labels[labels=="Unknown read"])
print "Uncorrected reads", len(labels[labels=="Uncorrected read"])
print "Contigs",           len(labels[labels=="Contig"])

fig        = plt.figure(figsize=[15,12])
ax         = fig.add_axes([0.1, 0.1, 0.8, 0.8])
lab_set    = set(labels)
label_list = list(lab_set)
label_list.sort()
colors    = plt.get_cmap('spectral')(np.linspace(0.0, 1.0, len(label_list)))
shapes    = ["o", "v", "^", "s", "D"]
res       = []
for k,target_lab in enumerate(label_list):
	if target_lab=="Uncorrected read" or target_lab=="Contig":
		continue
	idxs  = [j for j,label in enumerate(labels) if label==target_lab]
	X     = data[idxs,0]
	Y     = data[idxs,1]
	color = colors[k]
	for i,x in enumerate(X):
		res.append( (x, Y[i], target_lab, color, shapes[k%len(shapes)]) ) 
np.random.shuffle(res) 
plotted_labs = set()
legend_plots = []
legend_labs  = []
for i,(x,y, target_lab, color, shape) in enumerate(res):
	plot = ax.scatter(x,y, marker=shape, s=15 , edgecolors=color, label=target_lab, facecolors="None", linewidth=1.2, alpha=0.8)
	if target_lab not in plotted_labs:
		plotted_labs.add(target_lab)
		legend_plots.append(plot)
		legend_labs.append(target_lab)
box     = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
leg_tup = zip(legend_labs,legend_plots)
leg_tup.sort(key=lambda x: x[0])
legend_labs, legend_plots = zip(*leg_tup)
leg = ax.legend(legend_plots, legend_labs, loc='center left', prop={'size':10}, bbox_to_anchor=(1, 0.5), frameon=False, scatterpoints=1)
for i in range(len(legend_labs)):
	leg.legendHandles[i]._sizes = [100]

minx = min(map(lambda x: x[0], res))
maxx = max(map(lambda x: x[0], res))
miny = min(map(lambda x: x[1], res))
maxy = max(map(lambda x: x[1], res))
box_xmin = box_dims[0]
box_xmax = box_dims[1]
box_ymin = box_dims[2]
box_ymax = box_dims[3]
ax.set_xlim([min(minx-1, box_xmin-1), max(maxx+1, box_xmax+1)])
ax.set_ylim([min(miny-1, box_ymin-1), max(maxy+1, box_ymax+1)])
ax.plot([box_xmin,box_xmax,box_xmax,box_xmin,box_xmin], [box_ymax,box_ymax,box_ymin,box_ymin,box_ymax], linestyle="--", color="k", linewidth=3)
plt.savefig(plot_fn)

fig        = plt.figure(figsize=[15,12])
ax         = fig.add_axes([0.1, 0.1, 0.8, 0.8])
# ax.axis("off")
lab_set    = set(labels)
label_list = list(lab_set)
label_list.sort()
colors    = plt.get_cmap('spectral')(np.linspace(0.0, 1.0, len(label_list)))
shapes    = ["o", "v", "^", "s", "D"]
res       = []
for k,target_lab in enumerate(label_list):
	if target_lab=="Uncorrected read" or target_lab=="Contig":
		continue
	idxs  = [j for j,label in enumerate(labels) if label==target_lab]
	X     = data[idxs,0]
	Y     = data[idxs,1]
	color = colors[k]
	for i,x in enumerate(X):
		res.append( (x, Y[i], target_lab, color, shapes[k%len(shapes)]) ) 
np.random.shuffle(res) 
plotted_labs = set()
legend_plots = []
legend_labs  = []
for i,(x,y, target_lab, color, shape) in enumerate(res):
	plot = ax.scatter(x,y, marker=shape, s=40 , edgecolors=color, label=target_lab, facecolors="None", linewidth=1.2, alpha=0.8)
	if target_lab not in plotted_labs:
		plotted_labs.add(target_lab)
		legend_plots.append(plot)
		legend_labs.append(target_lab)
box     = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
leg_tup = zip(legend_labs,legend_plots)
leg_tup.sort(key=lambda x: x[0])
legend_labs, legend_plots = zip(*leg_tup)
leg = ax.legend(legend_plots, legend_labs, loc='center left', prop={'size':10}, bbox_to_anchor=(1, 0.5), frameon=False, scatterpoints=1)
for i in range(len(legend_labs)):
	leg.legendHandles[i]._sizes = [100]
ax.set_xlim([box_xmin, box_xmax])
ax.set_ylim([box_ymin, box_ymax])
plt.savefig(plot_fn.replace(".png", ".zoom.png"))

box_corrected_names = set()
for i in range(data.shape[0]):
	if names[i] in corrected:
		x = data[i,0]
		y = data[i,1]
		if y>box_ymin and y<box_ymax and x>box_xmin and x<box_xmax:
			box_corrected_names.add(names[i])
			print names[i]

print "Found %s corrected reads in box..." % len(box_corrected_names)
f = open("box_corrected_reads.fasta", "w")
for name,seq in fasta_iter(corrected_fn):
	if "/".join(name.split("/")[:2]) in box_corrected_names:
		f.write(">%s\n" % name)
		f.write("%s\n" % seq)
f.close()

