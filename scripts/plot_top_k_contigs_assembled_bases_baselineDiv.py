import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pub_figs
import numpy as np
import matplotlib.gridspec as gridspec
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

font_size      = 25
adjust         = 0.15
fontProperties = pub_figs.setup_math_fonts(font_size)
fig1           = pub_figs.init_fig(x=15,y=15)
gs             = gridspec.GridSpec(3, 3)
gs.update(hspace=0.2)
ax1            = plt.subplot(gs[:-1, :])
ax2            = plt.subplot(gs[-1, :])

baseline_fn       = "/hpc/users/beaulj01/projects/ebinning/pacbio20/all7_sets/HGAP_naive/whitelist_baseline/data/polished_assembly.fasta.lengths"
baseline_div2_fn  = "/hpc/users/beaulj01/projects/ebinning/pacbio20/all7_sets/HGAP_naive/whitelist_baseline_div2/data/polished_assembly.fasta.lengths"
baseline_div5_fn  = "/hpc/users/beaulj01/projects/ebinning/pacbio20/all7_sets/HGAP_naive/whitelist_baseline_div5/data/polished_assembly.fasta.lengths"
baseline_div10_fn = "/hpc/users/beaulj01/projects/ebinning/pacbio20/all7_sets/HGAP_naive/whitelist_baseline_div10/data/polished_assembly.fasta.lengths"
baseline_div20_fn = "/hpc/users/beaulj01/projects/ebinning/pacbio20/all7_sets/HGAP_naive/whitelist_baseline_div20/data/polished_assembly.fasta.lengths"

k           = 50
for j,fn in enumerate([baseline_fn, baseline_div2_fn, baseline_div5_fn, baseline_div10_fn, baseline_div20_fn]):
	qual_fn = fn.replace("data/polished_assembly.fasta.lengths", "results/polished_coverage_vs_quality.csv")
	qual    = np.loadtxt(qual_fn, delimiter=",", dtype="str")
	bases   = 0
	Y1      = [bases]
	Y2      = []
	labs    = []
	ind     = np.arange(k+1)
	for i,line in enumerate(open(fn, "r").xreadlines()):
		if i==k: break
		# labs.append(i)
		name    = line.split()[1].split("|")[0]
		length  = line.split()[0]
		quality = qual[qual[:,0]==name][0][2].astype(np.float)
		bases  += int(length)
		Y1.append(float(bases)/1000000)
		Y2.append(quality)
	if j==0:
		bases1 = ax1.plot(ind, Y1, "bo-", linewidth=7, markersize=10)
		quals1 = ax2.scatter(ind[1:], Y2, color="b", marker="o", s=30)
	elif j==1:
		bases2 = ax1.plot(ind, Y1, "ro-", linewidth=7, markersize=10)
		quals2 = ax2.scatter(ind[1:], Y2, color="r", marker="o", s=30)
	elif j==2:
		bases3 = ax1.plot(ind, Y1, "ko-", linewidth=7, markersize=10)
		quals3 = ax2.scatter(ind[1:], Y2, color="k", marker="o", s=30)
	elif j==3:
		bases4 = ax1.plot(ind, Y1, "mo-", linewidth=7, markersize=10)
		quals4 = ax2.scatter(ind[1:], Y2, color="m", marker="o", s=30)
	# elif j==4:
	# 	bases5 = ax1.plot(ind, Y1, "go-", linewidth=7, markersize=10)
	# 	quals5 = ax2.scatter(ind[1:], Y2, color="g", marker="o", s=30)

majorLocator   = MultipleLocator(5)
majorFormatter = FormatStrFormatter('%d')
minorLocator   = MultipleLocator(1)

ax1.set_ylabel("Total assembly (Mbp)")
ax1.set_xticks(ind)
ax1.set_xticklabels( labs, rotation=90)
ax1.xaxis.labelpad = 30
ax1.yaxis.labelpad = 30
ax1.set_xlim([0,k+1])
ax1.xaxis.set_major_locator(majorLocator)
ax1.xaxis.set_major_formatter(majorFormatter)
ax1.xaxis.set_minor_locator(minorLocator)
legend = ax1.legend( (bases1[0], bases2[0], bases3[0], bases4[0]), ('Baseline', 'Baseline / 2', 'Baseline / 5', 'Baseline / 10'), loc=2 )
legend.draw_frame(False)
pub_figs.change_font_size(ax1, font_size=font_size, bottom_adjust=adjust, left_adjust=adjust)
pub_figs.remove_top_right_axes(ax1)

ax2.set_xlabel("Contigs (%s largest)" % k)
ax2.set_ylabel("Mean QV")
ax2.set_xticks(ind)
ax2.set_xticklabels( labs, rotation=90)
ax2.xaxis.labelpad = 30
ax2.yaxis.labelpad = 30
ax2.set_xlim([0,k+1])
ax2.set_ylim([0,60])
ax2.xaxis.set_major_locator(majorLocator)
ax2.xaxis.set_major_formatter(majorFormatter)
ax2.xaxis.set_minor_locator(minorLocator)
pub_figs.change_font_size(ax2, font_size=font_size, bottom_adjust=adjust, left_adjust=adjust)
pub_figs.remove_top_right_axes(ax2)
fn = "top_k_contig_mean_qv"
fig1.savefig(fn+".png")

