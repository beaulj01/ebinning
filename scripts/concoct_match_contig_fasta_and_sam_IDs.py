import os,sys
from Bio import SeqIO

contig_fn = sys.argv[1]
lab       = sys.argv[2]

f = open(contig_fn.replace(".fasta", ".renamed.fasta"),"w")
for j,seq_record in enumerate(SeqIO.parse(contig_fn, "fasta")):
        # seq_record.id = seq_record.id.split(" ")[0]
        seq_record.id = seq_record.id.split("|")[0]
        f.write(">%s_%s\n" % (lab, seq_record.id))
        f.write("%s\n" % seq_record.seq)
f.close()