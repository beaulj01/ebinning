import os,sys
from itertools import groupby
from collections import Counter
from operator import itemgetter
from itertools import product
import math

reads_fasta = sys.argv[1]

def fasta_iter(fasta_name):
	"""
	Given a fasta file, yield tuples of (header, sequence).
	"""
	fh = open(fasta_name)
	# ditch the boolean (x[0]) and just keep the header or sequence since
	# we know they alternate.
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		# drop the ">"
		header = header.next()[1:].strip()
		# join all sequence lines to one.
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

def rev_comp_motif( motif ):
	COMP = {"A":"T", \
			"T":"A", \
			"C":"G", \
			"G":"C", \
			"W":"S", \
			"S":"W", \
			"M":"K", \
			"K":"M", \
			"R":"Y", \
			"Y":"R", \
			"B":"V", \
			"D":"H", \
			"H":"D", \
			"V":"B", \
			"N":"N", \
			"X":"X", \
			"*":"*"}
	rc_motif = []
	for char in motif[::-1]:
		rc_motif.append( COMP[char] )
	return "".join(rc_motif)

def kmer_freq ( read_str ):
	k = 4
	kmers = []
	for seq in product("ATGC",repeat=k):
		kmers.append( "".join(seq) )

	kmer_counts = Counter()
	for j in range( len(read_str)-(k-1) ):
		motif    = read_str[j:j+k]
		kmer_counts[motif] += 1

	# Combine forward and reverse complement motifs into one count
	combined_kmer = Counter()
	for kmer in kmers:
		kmer_rc = rev_comp_motif(kmer)
		if not combined_kmer.get(kmer_rc):
			combined_kmer[kmer] = kmer_counts[kmer] + kmer_counts[kmer_rc] + 1

	# Normalize the read-level kmer counts
	total_counts = sum(combined_kmer.values())
	for kmer,N in combined_kmer.items():
		combined_kmer[kmer] = math.log( N / float(total_counts) )

	return combined_kmer

for i,entry in enumerate(fasta_iter(reads_fasta)):
	name         = entry[0]
	seq          = entry[1]
	read_kmers   = kmer_freq( seq )
	barcode_str  = "%s\t" % "B_vulgatus"
	sorted_freq  = sorted(read_kmers.items(), key=itemgetter(0))
	barcode_str += "\t".join(map(lambda x: str(round(x[1],3)), sorted_freq))
	print barcode_str