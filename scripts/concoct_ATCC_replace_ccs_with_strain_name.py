import os,sys
from Bio import SeqIO

ccs_fn = sys.argv[1]
strain = os.path.basename(ccs_fn.split(".")[0])

f = open(ccs_fn.replace(".fasta", ".renamed.fasta"),"w")
for j,seq_record in enumerate(SeqIO.parse(ccs_fn, "fasta")):
        read          = seq_record.id
        seq_record.id = ""
        seq_record.id = "%s_%s" % (strain, read)
        f.write(">%s\n" % seq_record.id)
        f.write("%s\n" % seq_record.seq)
        # f.write(seq_record.format("fasta"))
f.close()