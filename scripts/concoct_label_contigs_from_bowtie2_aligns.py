import os,sys
import pysam
from collections import Counter,defaultdict
import glob
import numpy as np

sample_bams = glob.glob("map/sample_*/bowtie2/*.bam")
aln_counter = defaultdict(Counter)

ref_map = {"EC_BAA_2215" : "E. coli BAA-2215", \
		   "EC_BAA_2440" : "E. coli BAA-2440", \
		   "EC_BAA_2196" : "E. coli BAA-2196", \
		   "EC_BAA_2219" : "E. coli BAA-2219", \
		   "NC_004663.1" : "B. thetaiotaomicron VPI-5482", \
		   "NC_008497.1" : "L. brevis ATCC 367", \
		   "NC_009614.1" : "B. vulgatus ATCC 8482", \
		   "NC_010655.1" : "A. muciniphila ATCC BAA-835", \
		   "NC_012781.1" : "E. rectale ATCC 33656", \
		   "NC_014656.1" : "B. longum subsp. longum BBMN68", \
		   "NC_014833.1" : "R. albus 7", \
		   "NC_014933.1" : "B. helcogenes P 36-108", \
		   "NC_015164.1" : "B. salanitronis DSM 18170", \
		   "NC_015977.1" : "R. hominis A2-183", \
		   "NC_016776.1" : "B. fragilis 638", \
		   "NC_017179.1" : "C. difficile BI1", \
		   "NC_018221.1" : "E. faecalis D32", \
		   "NC_018937.1" : "H. pylori Rif1", \
		   "NC_021042.1" : "F. prausnitzii L2-6", \
}

for bam in sample_bams:
	samfile = pysam.AlignmentFile(bam, "rb")
	for aln in samfile.fetch():
		ref = "_".join(aln.query_name.split("_")[2:])
		ref = ref.split("_ln")[0]
		if ref.find("EC_BAA_")>-1:
			ref = ref.split("_contig")[0]
		if aln.mapq==42:
			aln_counter[aln.reference_name][ref_map[ref]] += 1

contig_list = sys.argv[1]
for line in open(contig_list, "r").xreadlines():
	line   = line.strip()
	contig = line.split(",")[0]
	total  = np.sum(aln_counter[contig].values())
	orgs   = aln_counter[contig].keys()
	orgs.sort()
	fracs  = map(lambda x: float(aln_counter[contig][x]) / total, orgs)
	if len(fracs)>0:
		if max(fracs)>=0.50:
			max_id = np.argmax(fracs)
			org    = orgs[max_id]
			if aln_counter[contig][org]>=10:
				label = org
				frac  = max(fracs)
			else:
				label = "Unlabeled"
				frac  = "N/A"
		else:
			label = "Unlabeled"
			frac  = "N/A"
	else:
		label = "Unlabeled"
		frac  = "N/A"
	print "%s,%s,%s" % (contig,label,frac)
