import os,sys
import glob
import numpy as np
import logging
from collections import defaultdict
from itertools import product
import re
from pbcore.io.align.CmpH5IO import CmpH5Reader
from pbcore.io.BasH5IO import BasH5Reader
import cmph5_read
import shutil
import pickle
import multiprocessing
import math
import subprocess

def build_motif_dict( ):
	"""
	Generate a set of all possible motifs within provided parameters.
	"""
	motifs      = set()
	bi_motifs   = set()
	min_kmer    = 4
	max_kmer    = 6
	mod_bases   = "A"
	bipartite   = True
	bipart_config = [(3,4), (5,6), (3,4)]
	NUCS        = "ACGT"
	total_kmers = 0
	logging.info("Initiating dictionary of all possible motifs...")
	#########################################
	# Generate all possible contiguous motifs
	#########################################
	for kmer in range(min_kmer,max_kmer+1):
		total_kmers += len(NUCS)**kmer
		logging.info("  - Adding %s %s-mer motifs..." % (len(NUCS)**kmer, kmer))
		for seq in product("ATGC",repeat=kmer):
			string = "".join(seq)
			for base in mod_bases:
				indexes = [m.start() for m in re.finditer(base, string)]
				for index in indexes:
					motif = "%s-%s" % (string, index)							
					motifs.add(motif)
		logging.info("Done: %s possible contiguous motifs\n" % len(motifs))

	if bipartite:
		####################################################################################
		# Generate all possible bipartite motifs. The parameters specifying 
		# the acceptable forms of bipartite motifs are currently hard-coded as:

		# bipart_config = [(3,4), (5,6), (3,4)]
		
		# where the first item in tuple is the possible lengths of the first part 
		# of the motif, the second item contains the possible number of Ns, and 
		# the third item contains the possible lengths of the second part of the motif.
		# Adding to this motif space greatly increases compute time and memory requirements.
		####################################################################################
		logging.info("  - Adding bipartite motifs to search space...")
		firsts  = bipart_config[0]
		Ns      = bipart_config[1]
		seconds = bipart_config[2]
		for bases1 in firsts:
			for num_Ns in Ns:
				for bases2 in seconds:
					last_mod_pos = bases1-1
					for seq1 in product("ATGC",repeat=bases1):
						for seq2 in product("ATGC",repeat=bases2):
							string = "".join(seq1) + ("N"*num_Ns) + "".join(seq2)
							for base in mod_bases:
								indexes = [m.start() for m in re.finditer(base, string) if m.start()<=last_mod_pos]
								for index in indexes:
									motif = "%s-%s" % (string, index)							
									bi_motifs.add(motif)
		logging.info("Done: %s possible bipartite motifs\n" % len(bi_motifs))
	return motifs, bi_motifs

def run_OS_command( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts != 0:
		print "Failed command: %s" % CMD
	return sts, stdOutErr

def launch_pool( procs, funct, args ):
	p    = multiprocessing.Pool(processes=procs)
	try:
		results = p.map(funct, args)
		p.close()
		p.join()
	except KeyboardInterrupt:
		p.terminate()
	return results

def chunks( l, n ):
	"""
	Yield successive n-sized chunks from l.
	"""
	for i in xrange(0, len(l), n):
		yield l[i:i+n]

def process_contig_chunk( args ):
	chunk_id      = args[0]
	cut_CMDs      = args[1]
	kmers         = args[2]
	cols_chunk    = args[3]
	n_chunks      = args[4]
	min_motif_N   = args[5]
	print "  - Control data: chunk %s/%s" % ((chunk_id+1), (n_chunks+1))
	control_means = {}
	
	for cut_CMD in cut_CMDs:
		sts,stdOutErr = run_OS_command( cut_CMD )
	
	fns                = map(lambda x: x.split("> ")[-1], cut_CMDs)
	control_ipds_sub   = np.loadtxt(fns[0], dtype="float")
	control_ipds_N_sub = np.loadtxt(fns[1], dtype="int")
	# If there is only one row (read) for this contig, still treat as
	# a 2d matrix of many reads
	control_ipds_sub   = np.atleast_2d(control_ipds_sub)
	control_ipds_N_sub = np.atleast_2d(control_ipds_N_sub)
	
	not_found     = 0
	for j in range(len(cols_chunk)):
		motif = kmers[cols_chunk[j]]
		if np.sum(control_ipds_N_sub[:,j])>=min_motif_N:
			if np.sum(control_ipds_N_sub[:,j])>0:
				control_mean = np.dot(control_ipds_sub[:,j], control_ipds_N_sub[:,j]) / np.sum(control_ipds_N_sub[:,j])
			else:
				control_mean = 0
			control_means[motif] = control_mean
		else:
			not_found += 1

	return control_means,not_found

def chunk_control_matrices( control_ipds_fn, control_ipds_N_fn, control_kmers_fn ):
	"""

	"""
	min_motif_N = 20
	procs       = 24

	kmers       = np.loadtxt(control_kmers_fn, dtype="str")
	fns         = [control_ipds_fn, control_ipds_N_fn]
	n_chunks    = 99
	chunksize   = int(math.ceil(float( len(kmers)/n_chunks )))
	cols_chunks = list(chunks( range(len(kmers)), chunksize ))
	args        = []
	for i,cols_chunk in enumerate(cols_chunks):
		cut_CMDs = []
		for fn in fns:
			cut_cols = "%s-%s" % ((cols_chunk[0]+1), (cols_chunk[-1]+1))
			in_fn    = fn
			out_fn   = fn+".sub.%s" % i
			cut_CMD  = "cut -d$\'\\t\' -f%s %s > %s" % (cut_cols, in_fn, out_fn)
			cut_CMDs.append(cut_CMD)
		args.append( (i, cut_CMDs, kmers, cols_chunk, n_chunks, min_motif_N) )
	
	results = launch_pool(procs, process_contig_chunk, args)
	
	print "Combining motifs from all chunks of control data..."
	not_found     = 0
	control_means = {}
	for i,result in enumerate(results):
		not_found += result[1]
		for motif in result[0].keys():
			control_means[motif] = result[0][motif]
	print "Done."

	return control_means,not_found

def build_control_IPD_dict( motifs, bi_motifs ):
	"""

	"""
	control_dir = "control_456_bipart_20K"
	tmp         = "tmp"
	control_pkl = os.path.join(control_dir, tmp, "control_means.pkl")

	control_ipds_fn   = os.path.join(control_dir, tmp, "control_ipds.tmp" )
	control_ipds_N_fn = os.path.join(control_dir, tmp, "control_ipdsN.tmp")
	control_kmers_fn  = os.path.join(control_dir, tmp, "control_ipdskmers.tmp")

	control_means,not_found = chunk_control_matrices(control_ipds_fn, control_ipds_N_fn, control_kmers_fn)

	if not_found > 0:
		print "WARNING: %s/%s motifs did not have sufficient coverage in control data!" % (not_found, (len(motifs)+len(bi_motifs)))
		print "   * Try increasing --N_motif_reads to get higher coverage and make sure N_reads > N_motif_reads"
	
	print "Writing control data to a pickled file for future re-use: %s" % control_pkl
	pickle.dump( control_means, open( control_pkl, "wb" ) )
	print "Done."

	return control_means

motifs, bi_motifs = build_motif_dict()
control_means     = build_control_IPD_dict(motifs,bi_motifs)
