import pysam
import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from collections import defaultdict
import numpy as np
import pub_figs

bamfile = pysam.AlignmentFile(sys.argv[1], "rb")
pileup  = bamfile.pileup()

x = []
y = []	
windowsize  = 10000
window_vals = defaultdict(list)
for p in pileup:
	pos       = p.pos
	cov       = p.n
	rpos      = p.reference_pos
	rid       = p.reference_id
	if p.reference_id==1:
		pos += bamfile.lengths[0]
	window_id = pos/windowsize
	window_vals[window_id].append(cov)
	print rid, pos, window_id, cov

for i in range(np.sum(bamfile.lengths)/windowsize+1):
	if len(window_vals[i])==0:
		mean = 0
	elif len(window_vals[i])==1:
		mean = window_vals[i][[0]]
	else:
		mean = np.mean(window_vals[i])
	pos  = i * windowsize
	# print i, pos, mean
	x.append(pos)
	y.append(mean)

fig = plt.figure(figsize=[15,12])
ax  = fig.add_axes([0.1, 0.3, 0.8, 0.4])
ax.plot(x, y, linestyle="-", color="k")
tick_labs  = []
for n in range(500000,np.sum(bamfile.lengths), 500000):
	tick_labs.append( float(n)/1000000 )
ax.set_xticks(range(500000,np.sum(bamfile.lengths), 500000))
ax.set_xticklabels(tick_labs, rotation=45, ha="right")
ax.set_xlabel("Genome position (Mb)")
ax.set_ylabel("Coverage")
pub_figs.remove_top_right_axes(ax)
pub_figs.setup_math_fonts(24)
plot_fn = "r_sphaer_aligned_cov.png"
plt.savefig(plot_fn)