import os,sys
import re
from collections import Counter

contigs_fn       = sys.argv[1]
contig_labels_fn = sys.argv[2]

species_names = {"gi|187734516|ref|NC_010655.1|_Akkermansia_muciniphila_ATCC_BAA-835_chromosome,_complete_genome"            : "Akkermansia muciniphila ATCC BAA-835", \
				 "gi|375356399|ref|NC_016776.1|_Bacteroides_fragilis_638R,_complete_genome"                                  : "Bacteroides fragilis 638R", \
				 "gi|319899888|ref|NC_014933.1|_Bacteroides_helcogenes_P_36-108_chromosome,_complete_genome"                 : "Bacteroides helcogenes P 36-108", \
				 "gi|324959701|ref|NC_015166.1|_Bacteroides_salanitronis_DSM_18170_plasmid_pBACSA03,_complete_sequence"      : "Bacteroides salanitronis BL78, DSM 18170", \
				 "gi|29611500|ref|NC_004703.1|_Bacteroides_thetaiotaomicron_VPI-5482_plasmid_p5482,_complete_sequence"       : "Bacteroides thetaiotaomicron VPI-5482", \
				 "gi|150002608|ref|NC_009614.1|_Bacteroides_vulgatus_ATCC_8482_chromosome,_complete_genome"                  : "Bacteroides vulgatus ATCC 8482", \
				 "gi|312132275|ref|NC_014656.1|_Bifidobacterium_longum_subsp._longum_BBMN68_chromosome,_complete_genome"     : "Bifidobacterium longum subsp. longum BBMN68", \
				 "gi|383843669|ref|NC_017179.1|_Clostridium_difficile_BI1,_complete_genome"                                  : "Clostridium difficile BI1", \
				 "gi|397698507|ref|NC_018223.1|_Enterococcus_faecalis_D32_plasmid_EFD32pB,_complete_sequence"                : "Enterococcus faecalis D32", \
				 "gi|556503834|ref|NC_000913.3|Escherichia_coli_str._K-12_substr._MG1655,complete_genome"                    : "Escherichia coli K-12, MG1655", \
				 "gi|407479587|ref|NC_018658.1|Escherichia_coli_O104:H4_str._2011C-3493_chromosome+plasmids,complete_genome" : "Escherichia coli O104-H4 2011C-3493", \
				 "gi|215485161|ref|NC_011601.1|Escherichia_coli_O127:H6_str.E2348/69_chromosome+plasmids,complete_genome"    : "Escherichia coli O127-H6 E2348-69", \
				 "gi|15829254|ref|NC_002695.1|Escherichia_coli_O157:H7_str.Sakai_chromosome+plasmids,complete_genome"        : "Escherichia coli O157-H7 Sakai", \
				 "gi|91209055|ref|NC_007946.1|Escherichia_coli_UTI89_chromosome+plasmid,complete_genome"                     : "Escherichia coli UTI89", \
				 "gi|238922432|ref|NC_012781.1|_Eubacterium_rectale_ATCC_33656,_complete_genome"                             : "Eubacterium rectale ATCC 33656", \
				 "gi|479208076|ref|NC_021042.1|_Faecalibacterium_prausnitzii_L2-6,_complete_genome"                          : "Faecalibacterium prausnitzii L2-6", \
				 "gi|410023236|ref|NC_018937.1|_Helicobacter_pylori_Rif1_chromosome,_complete_genome"                        : "Helicobacter pylori Rif1", \
				 "gi|116334879|ref|NC_008499.1|_Lactobacillus_brevis_ATCC_367_plasmid_2,_complete_sequence"                  : "Lactobacillus brevis ATCC 367", \
				 "gi|347530298|ref|NC_015977.1|_Roseburia_hominis_A2-183_chromosome,_complete_genome"                        : "Roseburia hominis A2-183", \
				 "gi|317054731|ref|NC_014833.1|_Ruminococcus_albus_7_chromosome,_complete_genome"                            : "Ruminococcus albus 7I"}

species_motifs = {"Akkermansia muciniphila ATCC BAA-835" : ["RAATTY","ACGGC"], \
				  "Bacteroides fragilis 638R" : ["CCWGG"], \
				  "Bacteroides helcogenes P 36-108" : ["AGTACT","GGYRCC","CTRYAG"], \
				  "Bacteroides salanitronis BL78, DSM 18170" : ["GNNGAYNNNNNNGAT","GGCC","GCSGC","GATC","CTSAG"], \
				  "Bacteroides thetaiotaomicron VPI-5482" : ["CTCGAG"], \
				  "Bacteroides vulgatus ATCC 8482" : ["GATC"], \
				  "Bifidobacterium longum subsp. longum BBMN68" : ["GATC"], \
				  "Clostridium difficile BI1" : [], \
				  "Enterococcus faecalis D32" : [], \
				  "Escherichia coli K-12, MG1655" : ["AACNNNNNNGTGC","CCWGG"], \
				  "Escherichia coli O104-H4 2011C-3493" : ["A","CTGCAG","GATC","ATGCAT","CCWGG"], \
				  "Escherichia coli O127-H6 E2348-69" : ["GATC","ATGCAT","CCWGG"], \
				  "Escherichia coli O157-H7 Sakai" : ["GATC","ATGCAT","CCWGG"], \
				  "Escherichia coli UTI89" : ["GTCGAC","GATC","ATGCAT","CCWGG"], \
				  "Eubacterium rectale ATCC 33656" : ["GGCC","GATC","ACCGGT","CCWGG"], \
				  "Faecalibacterium prausnitzii L2-6" : ["GACGC","GATC","CTSAG"], \
				  "Helicobacter pylori Rif1" : ["ACANNNNNNNNTAG","GTNNAC","CCGG","TGCA","ACGT","GCGC","TCGA","GANTC","GTAC","CATG","TCNNGA","CCTTC","CCTC","GATC","GAAGA","GCGTA","ATTAAT","ACNGT","CTGCAG"], \
				  "Lactobacillus brevis ATCC 367" : ["GATC"], \
				  "Roseburia hominis A2-183" : ["GATC","CTSAG"], \
				  "Ruminococcus albus 7I" : ["GGNCC","GATC","GASTC","GCWGC"] }

def read_contig_labels( fn ):
	"""
	Each contig is assigned a species based on which simulated reads aligned to it.
	"""
	labels = {}
	for line in open(fn).xreadlines():
		line.strip("\n")
		contig  = line.split(",")[0]
		species = line.split(",")[1]
		labels[contig] = species
	return labels

def fasta_iter( fasta_name ):
	"""
	Given a fasta file, yield tuples of (header, sequence).
	"""
	fh = open(fasta_name)
	# ditch the boolean (x[0]) and just keep the header or sequence since
	# we know they alternate.
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		# drop the ">"
		header = header.next()[1:].strip()
		# join all sequence lines to one.
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

def scan_motifs( motifs, min_IPD=1, bases=["A"] ):
	"""
	For each motif, find all occurrences in read.
	"""
	contig_motifs = {}
	motif_counter = Counter()
	for motif in motifs:
		matches_iter = re.finditer(motif, self.read_str)
		matches_list = []
		for match in matches_iter:
			matches_list.append(match)

		motif_ipds = []
		for match in matches_list:
			# For each motif occurrence, add the IPDs (list of size kmer) to 'motif_ipds'
			motif_counter[motif] += 1
			motif_start           = match.span()[0]
			motif_end             = match.span()[1]
			# motif_ipds.append( self.ipds[motif_start:motif_end] )
			motif_ipds.append( ***SIMULATE_N_IPD_VALS*** )
		means = []
		for col in range(len(motif)):
			if len(motif_ipds) > 0:
				means.append( np.mean(map(lambda x: x[col], motif_ipds)) )
			else:
				means.append(0)

		# Only include motifs that contain at least one of the target bases
		for base in bases:
			indexes = [m.start() for m in re.finditer(base, motif)]
			for index in indexes:
				# If more than one target base, specify which one maps to the IPD score we're storing
				motif_string = "%s-%s" % (motif, index)
				IPD          = means[index]
				N_sites      = motif_counter[motif]
				if IPD > min_IPD:
					contig_motifs[motif_string] = {"IPD": IPD, "N": N_sites}
				else:
					contig_motifs[motif_string] = {"IPD": 0,   "N": N_sites}
	return contig_motifs

labels  = read_contig_labels( contig_labels_fn )
contigs = fasta_iter( contigs_fn )
for contig_name,seq in contigs.iteritems():
	species_name = species_names[labels[contig_name]]
	motifs       = species_motifs[species_name]
	scan_motifs( motifs )
