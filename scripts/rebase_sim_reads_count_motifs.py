import sys
from Bio import Entrez
from itertools import groupby
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import urllib2
# sys.path.append("/Users/beaulj01/mothra_home/gitRepo/eBinning/src")
sys.path.append("/hpc/users/beaulj01/gitRepo/eBinning/src")
import read_scanner
from collections import Counter,defaultdict
from Bio import SeqIO
import random
import numpy as np
import pub_figs
import math

fasta = "all_plasmids.fasta"

acc_motifs = {}
for line in open("pb_rebase_seqs_and_motifs.6mA_only.out", "rb").xreadlines():
	line = line.strip()
	vals   = line.split("\t")
	org_id = int(vals[0])
	acc    =     vals[1]
	if len(vals)>4:
		motifs          = vals[4]
		acc_motifs[acc] = motifs

n_iters    = 500
n_iters    = 50
motifs_100 = []
motifs_75  = []
motifs_50  = []
# rls        = [5,10,15,20,25,30,35,40,45,50]
rls        = range(5,101,5)

# f       = open("sim_read_motif_fracs.txt", "wb")
# results = []
# for rl in rls:
# 	rl_bp = rl*1000
# 	for z,record in enumerate(SeqIO.parse(fasta, "fasta")):

# 		# if z==100:break

# 		for i in range(n_iters):
# 			if len(record.seq)>rl_bp:
				
# 				start    = random.randint(1, (len(record.seq)-rl_bp))
# 				sim_read = record.seq[start:(start+rl_bp)]
# 				acc      = record.id.split(".")[0]
				
# 				try:
# 					motifs   = acc_motifs[acc].split(";")
# 					motif_there = np.zeros(len(motifs))
# 					for j,motif in enumerate(motifs):
# 						matches_list = read_scanner.find_motif_matches("bas", motif, str(sim_read), 0)
# 						if len(matches_list)>0:
# 							motif_there[j] = 1

# 					pct = 100*float(motif_there.sum()/len(motifs))
# 					results.append( (rl,pct) )
# 					f.write("%s\t%s\n" % (rl,pct))
# 				except KeyError:
# 					pass
# f.close()
# mat    = np.array(results)

mat = np.loadtxt("sim_read_motif_fracs.txt", dtype="float")

font_size      = 25
adjust         = 0.15
fontProperties = pub_figs.setup_math_fonts(font_size)
plot_fn        = "sim_reads_motif_counts.png"
plt.figure(1, figsize=(12,10))
ax     = plt.axes([0.17, 0.3, 0.5, 0.6])

colors = ["k", "b", "r"]
for k,pct in enumerate([100,75,50]):
	X       = []
	Y       = []
	stderrs = []
	for rl in rls:
		print rl
		rl_mat  = mat[mat[:,0]==rl]
		pct_mat = rl_mat[rl_mat[:,1]>=pct]
		rl_pct  = float(len(pct_mat[:,1])) / len(rl_mat[:,1])
		X.append(rl)
		Y.append(rl_pct)
		stderr = np.std(pct_mat[:,1])/math.sqrt(len(pct_mat[:,1]))
		stderrs.append(stderr)
	lab = "%s" % pct
	ax.errorbar(X, Y, yerr=stderr, linestyle="-", color=colors[k], linewidth=3, label=lab+"\%")
	ax.hold(True)

ax.set_ylim([0,1.0])
ax.set_xlim([rls[0],(rls[-1]+1)])	
ax.set_xlabel("Sequence length (Kb)")
ax.set_ylabel("Fraction sequences with motif sites")
pub_figs.remove_top_right_axes(ax)
ax.tick_params(axis='both', direction='out')
ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
ax.set_xticks(range(10,101,10))
ax.set_xticklabels(ax.get_xticks(), rotation=45)
ax.grid()
# ax.set_xticks(n_orgs)
# ax.set_xticklabels(n_orgs, rotation=45)
# ax.set_yticks(np.arange(92,101,1)/float(100))
# ax.set_yticklabels(map(lambda x: str(x), range(0,80,20)))
tit = "\% of genome\'s 6mA motifs\npresent in sequence"
leg = ax.legend(title=tit, prop={'size':font_size}, frameon=False)
# get handles, remove the errorbars, use them in the legend
handles, labels = ax.get_legend_handles_labels()
handles = [h[0] for h in handles]
leg = ax.legend(handles, labels, loc=4, numpoints=1,title=tit, prop={'size':font_size}, frameon=False)
plt.setp(leg.get_title(), multialignment='center')
pub_figs.setup_math_fonts(font_size)
plt.savefig(plot_fn, dpi=300)
