import os,sys
import urllib2
from BeautifulSoup import BeautifulSoup
from Bio import SeqIO
import subprocess

all_downloads    = "/hpc/users/beaulj01/projects/data_repo/ncbi/bacteria/all_downloaded_files.txt"
assembly_summary = "/hpc/users/beaulj01/projects/software/ncbi-blast-2.3.0+/bin/assembly_summary.txt"

def run_OS( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts != 0:
		print "Failed command: %s" % CMD
	return sts, stdOutErr

f = open("phymmbl_config.txt", "w")
for i,line in enumerate(open(assembly_summary, "r").xreadlines()):
	if i==0: continue # skip header
	if i%50==0:
		print "%s..." % i
	line     = line.strip()
	cols     = line.split("\t")
	taxid    = cols[5]
	name     = cols[7]
	strain   = cols[8]
	ftp_path = cols[19]
	suffix   = "_genomic.gbff.gz"
	fn       = ftp_path.split("/")[-1]+suffix
	path     = os.path.join("/hpc/users/beaulj01/projects/data_repo/ncbi/bacteria", fn)

	gunzip_CMD    = "gunzip %s" % path
	sts,stdOutErr = run_OS(gunzip_CMD)
	
	unzipped = path.replace(".gbff.gz", ".gbff")
	fasta_fn = unzipped.replace(".gbff", ".fasta")
	fasta    = open(fasta_fn, "w")
	for j,seq_record in enumerate(SeqIO.parse(unzipped, "genbank")):
		fasta.write(seq_record.format("fasta"))
	fasta.close()

	gzip_CMD      = "gzip %s" % unzipped
	sts,stdOutErr = run_OS(gzip_CMD)

	response    = urllib2.urlopen("http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=%s" % taxid)
	html        = response.read()
	parsed_html = BeautifulSoup(html)
	
	phylum    = parsed_html.body.find('a', attrs={'alt':'phylum'})
	if phylum==None:
		phylum="NO_VALUE"
	else:
		phylum = phylum.text
	classname = parsed_html.body.find('a', attrs={'alt':'class'})
	if classname==None:
		classname="NO_VALUE"
	else:
		classname = classname.text
	order     = parsed_html.body.find('a', attrs={'alt':'order'})
	if order==None:
		order="NO_VALUE"
	else:
		order = order.text
	family    = parsed_html.body.find('a', attrs={'alt':'family'})
	if family==None:
		family="NO_VALUE"
	else:
		family = family.text
	genus     = parsed_html.body.find('a', attrs={'alt':'genus'})
	if genus==None:
		genus="NO_VALUE"
	else:
		genus = genus.text
	species   = parsed_html.body.find('a', attrs={'alt':'species'})
	if species==None:
		species="NO_VALUE"
	else:
		species = species.text.split()[-1]
	# strain    = parsed_html.body.find('a', attrs={'alt':'strain'})
	# if strain==None:
	# 	strain="NO_VALUE"
	# else:
	# 	strain = strain.text
	f.write("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" % (fasta_fn, 1, species, classname, order, family, genus, species, "NO_VALUE"))
	if i==20: break
f.close()
