import os,sys
import subprocess

fofn = sys.argv[1]

def run_OS( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts!=0:
		print stdOutErr[1]

for i,line in enumerate(open(fofn, "r").xreadlines()):
	line  = line.strip()
	print i, line, "...."
	fn    = line
	movie = ".".join(os.path.basename(fn).split(".")[:2])

	CMD = "bash5tools.py -i --outFilePref %s %s" % (movie, fn)
	run_OS(CMD)