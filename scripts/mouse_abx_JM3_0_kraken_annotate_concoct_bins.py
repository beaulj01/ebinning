import os,sys
import numpy as np
from Bio import SeqIO
from collections import defaultdict,Counter

names       = np.loadtxt("contigs.names", dtype="str")
kraken      = np.loadtxt("contigs.kraken", dtype="str")

kraken_d    = dict( zip(names,kraken) )

box_contigs = defaultdict(list)
contig_lens = {}
for box in range(0,29):
	fn = "bins/bin%s.fasta" % box

	for Seq in SeqIO.parse(fn, "fasta"):
		name = Seq.id

		box_contigs[box].append(name)
		contig_lens[name] = len(Seq.seq)

for box in range(0,29):

	specs_n = Counter()

	for contig in box_contigs[box]:
		for i in range(contig_lens[contig]):
			if kraken_d.get(contig):
				spec = kraken_d[contig]
				specs_n[spec] += 1
			else:
				specs_n["unknown"] += 1

	top_spec  = specs_n.most_common(1)[0][0]
	top_bases = specs_n.most_common(1)[0][1]
	tot_bases = np.sum(specs_n.values())
	top_frac  = float(top_bases)/tot_bases
	print box, top_spec, top_frac
