import sys
from Bio import Entrez
from itertools import groupby
import urllib2
sys.path.append("/Users/beaulj01/mothra_home/gitRepo/eBinning/src")
# sys.path.append("/hpc/users/beaulj01/gitRepo/eBinning/src")
import read_scanner
from collections import Counter

accs      = ["CP015229", \
			 "CP013662", \
			 "CP015228", \
			 "CP015240", \
			 "CP015241", \
			 "CP010371", \
			 "CP009106", \
			 "CP014268", \
			 "CP014269", \
			 "CP011416", \
			 "NC_004431", \
			 "CP016404", \
			 "CP016182", \
			 "CP015159", \
			 "CP009859", \
			 "CP014667", \
			 "CP009578", \
			 "CP013663", \
			 "CP014272", \
			 "CP014270", \
			 "CP014197", \
			 "CP011495", \
			 "CP011331",
			 "CP007133", \
			 "CP015832", \
			 "CP015831", \
			 "HG941718", \
			 "CP009104", \
			 "CP011061", \
			 "JMIZ01000004, JMIZ01000006, JMIZ01000008, JMIZ01000009", \
			 "JMJA01000001, JMJA01000002", \
			 "JMJB01000001", \
			 "JMJC01000003, JMJC01000005, JMJC01000010, JMJC01000011, JMJC01000012, JMJC01000014", \
			 "JMJD01000001, JMJD01000003", \
			 "JMJE01000004, JMJE01000006, JMJE01000011, JMJE01000013, JMJE01000015, JMJE01000025", \
			 "CP011134"]

def pull_fasta_get_comp( accs ):
	# Download all the fastas from this NCBI page
	db           = "nuccore"
	Entrez.email = "john.beaulaurier@mssm.edu"
	batchSize    = 100
	retmax       = 10**9

	# all_accs = list(np.loadtxt("accessions.txt", dtype="str"))
	all_accs   = accs
	fasta_name = "tmp.fasta"
	try:
		for k,start in enumerate(range( 0,len(all_accs),batchSize )):
			#first get GI for query accesions
			batch_accs = all_accs[start:(start+batchSize)]
			sys.stderr.write( "Fetching %s entries from GenBank: %s\n" % (len(batch_accs), ", ".join(batch_accs[:10])))
			query  = " ".join(batch_accs)
			handle = Entrez.esearch( db=db,term=query,retmax=retmax )
			giList = Entrez.read(handle)['IdList']
			sys.stderr.write( "Found %s GI: %s\n" % (len(giList), ", ".join(giList[:10])))
			#post NCBI query
			search_handle     = Entrez.epost(db=db, id=",".join(giList))
			search_results    = Entrez.read(search_handle)
			webenv,query_key  = search_results["WebEnv"], search_results["QueryKey"]
			#fetch entries in batch
			# handle = Entrez.efetch(db=db, rettype="fasta", retstart=start, retmax=batchSize, webenv=webenv, query_key=query_key)
			handle = Entrez.efetch(db=db, rettype="fasta", webenv=webenv, query_key=query_key)
			# sys.stdout.write(handle.read())
			f = open(fasta_name, "wb")
			f.write(handle.read())
			f.close()
	except urllib2.HTTPError as err:
		if err.code==404:
			print "**** HTTPError 404! ****"
			error_urls.append(err.geturl())
			errors_n += 1
		else:
			raise

pull_fasta_get_comp( accs )