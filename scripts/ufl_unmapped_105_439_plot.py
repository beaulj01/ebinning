import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import scipy
from scipy.stats import gaussian_kde
import sklearn.cluster as cluster
import pub_figs
import random
from pbcore.io.BasH5IO import BasH5Reader
from itertools import groupby

data     = np.loadtxt("both.comp.unmapped.2D", dtype="float")
labels   = np.loadtxt("both.labels.unmapped",  dtype="S15")
names    = np.loadtxt("both.names.unmapped",   dtype="str")
sizes    = np.loadtxt("both.lengths.unmapped", dtype="int")
plot_fn  = "reads.unmapped.png"
title    = "unmapped read-level cBinning: 105 and 439"
box_dims = [int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4]),]
pre      = sys.argv[5]
bins     = 100

def fasta_iter( fasta_name ):
	fh = open(fasta_name)
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		header = header.next()[1:].strip()
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

# First plot the background mapped/unmapped reads density
fig                     = plt.figure(figsize=[15,12])
ax                      = fig.add_axes([0.1, 0.1, 0.8, 0.8])
# ax.axis("off")
extent                  = [-40,40,-40,40]
H_105, xedges, yedges = np.histogram2d(data[labels=="105",0], data[labels=="105",1], bins=bins, range=[extent[:2], extent[2:]])
H_439, xedges, yedges = np.histogram2d(data[labels=="439",0], data[labels=="439",1], bins=bins, range=[extent[:2], extent[2:]])
H = H_439/float(np.amax(H_439)) - H_105/float(np.amax(H_105))
H = np.rot90(H)
H = np.flipud(H)
Hmasked = np.ma.masked_where(H==0,H)
im   = plt.imshow(H, interpolation='bilinear', origin='lower', cmap=cm.seismic, extent=extent, vmin=-1, vmax=1)
cbaxes = fig.add_axes([0.85, 0.5, 0.03, 0.4]) 
cbar = plt.colorbar(im, cax = cbaxes)  
cbar.set_ticks([-1,1])
cbar.ax.set_yticklabels(["105","439"])
cbar.ax.tick_params(labelsize=18)

minx = min(data[:,0])
maxx = max(data[:,0])
miny = min(data[:,1])
maxy = max(data[:,1])
box_xmin = box_dims[0]
box_xmax = box_dims[1]
box_ymin = box_dims[2]
box_ymax = box_dims[3]
ax.set_xlim([min(minx-1, box_xmin-1), max(maxx+1, box_xmax+1)])
ax.set_ylim([min(miny-1, box_ymin-1), max(maxy+1, box_ymax+1)])
ax.plot([box_xmin,box_xmax,box_xmax,box_xmin,box_xmin], [box_ymax,box_ymax,box_ymin,box_ymin,box_ymax], linestyle="--", color="k", linewidth=3)
plt.savefig(plot_fn)

box_readnames = set()
for i in range(data.shape[0]):
	x = data[i,0]
	y = data[i,1]
	if y>box_ymin and y<box_ymax and x>box_xmin and x<box_xmax:
		box_readnames.add(names[i])

print "Found %s reads in box..." % len(box_readnames)
f = open("box_reads_%s.fasta" % pre, "w")
for name,seq in fasta_iter("unmappedSubreads.combo.fasta"):
	if "/".join(name.split("/")[:-1]) in box_readnames:
		f.write(">%s\n" % name)
		f.write("%s\n" % seq)
f.close()