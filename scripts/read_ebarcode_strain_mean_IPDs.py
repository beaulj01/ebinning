import os,sys
import numpy as np
from itertools import groupby

read_barcodes_fn = sys.argv[1]

keyfunc  = lambda x: x.split("\t")[1]
data     = sorted(open(read_barcodes_fn).read().splitlines(), key=keyfunc)
for key,lines in groupby(data, keyfunc):
	lines  = list(lines)
	scores = map(lambda line: float(line.split("\t")[4]), lines)
	print key, len(lines), np.mean(scores)