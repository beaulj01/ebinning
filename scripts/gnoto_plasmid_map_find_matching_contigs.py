import os,sys
import numpy as np
from Bio import SeqIO
import glob
from collections import Counter

motifs   = np.loadtxt("ordered_motifs.txt", dtype="str")
scores   = np.loadtxt("contigs.SCp",        dtype="float")
scores_N = np.loadtxt("contigs.SCp_N",      dtype="int")
coords   = np.loadtxt("contigs.SCp.2D",     dtype="float")
names    = np.loadtxt("contigs.names",      dtype="str")
lengths  = np.loadtxt("contigs.lengths",    dtype="int")
labels   = np.loadtxt("contigs.labels",     dtype="str")

assembly = "polished_assembly.fasta"
contig_lens = {}
for Seq in SeqIO.parse(assembly, "fasta"):
	contig_lens[Seq.id.replace("|quiver", "")] = len(Seq.seq)

min_scp   = 1.6
min_scp_N = 10
min_plasmid_length = 5000
min_mapped_pct = 75

plasmids = ["unitig_82", \
			"unitig_96", \
			"unitig_73", \
			"unitig_75", \
			"unitig_240", \
			"unitig_87", \
			]

# Get the names of contigs in each box
bin_ids_map = {}
spec_fastas = glob.glob("*.contigs.fasta")
for fasta in spec_fastas:
	spec = fasta.split(".")[0]
	for Seq in SeqIO.parse(fasta, "fasta"):
		bin_ids_map[Seq.id.replace("|quiver","")] = spec

bin_tot_bps = Counter()
for (name,length) in zip(names,lengths):
	if bin_ids_map.get(name):
		bin_tot_bps[bin_ids_map[name]] += length

plasmid_maps_to_contig = {}
for plasmid in plasmids:
	
	plasmid_maps_to_contig[plasmid] = []

	for i in range(len(names)):
		if names[i]==plasmid:
			pl_scp   = scores[i,:]
			pl_scp_N = scores_N[i,:]
			pl_coord = coords[i,:]
			print "Checking suspected plasmid %s (%sbp): located at (%.1f, %.1f)" % (plasmid, lengths[i], pl_coord[0], pl_coord[1])
			pl_idxs  = (pl_scp>=min_scp) & (pl_scp_N>=min_scp_N)
			print "   Has motifs %s" % ",".join(motifs[pl_idxs])
			# Find contigs with these motifs
			for row in range(scores.shape[0]):
				contig      = names[row]
				keep_contig = False

				if contig!=plasmid:

					if not bin_ids_map.get(contig):
						bin_ids_map[contig] = 99

					con_scp   = scores[row,pl_idxs]
					con_scp_N = scores_N[row,pl_idxs]
					match_idx = (con_scp[:]>=min_scp) & (con_scp_N[:]>=min_scp_N)

					
					# if lengths[row]<100000:
					# 	continue


					if match_idx.all() and len(con_scp)>0:
						# print "   possible matching contig: %s" % contig, con_scp, con_scp_N
						keep_contig = True

						# Check for additional motifs on this contig
						con_all_scp   = scores[row,:]
						con_all_scp_N = scores_N[row,:]
						contig_idxs   = (con_all_scp[:]>=min_scp) & (con_all_scp_N>=min_scp_N)
						for k,idx in enumerate(contig_idxs):

							if motifs[k]=="AGATGT-2":
								continue

							if (idx!=pl_idxs[k]) & (pl_scp_N[k]>=min_scp_N):
								# Found a sig motif on contig thats not sig on plasmid
								# print "     -- but %s on plasmid %s = %.3f (N=%s)" % (motifs[k], plasmid, pl_scp[k], pl_scp_N[k])
								keep_contig = False

				if keep_contig:
					plasmid_maps_to_contig[plasmid].append(contig)

bin_ids_list = list(set(bin_ids_map.values()))
bin_ids_list.sort()

print ""

for plasmid,contigs in plasmid_maps_to_contig.iteritems():
	
	if contig_lens[plasmid]>=min_plasmid_length:
		print "Plasmid %s (%s bp): " % (plasmid, contig_lens[plasmid])
	
		if len(contigs)==0:
			print "   *** no call ***"
		else:
			contig_counts  = Counter()
			all_bin_counts = map(lambda x: bin_ids_map[x], contigs)

			bin_bps = Counter()
			for bin_id in bin_ids_list:
				bin_contigs = [contig for contig in contigs if bin_ids_map[contig]==bin_id]
				for c in bin_contigs:
					bin_bps[bin_id] += contig_lens[c]
				contig_counts[bin_id] += all_bin_counts.count(bin_id)
			
			for bin_id in bin_ids_list:
				mapped_pct  = 100*float(bin_bps[bin_id])/bin_tot_bps[bin_id]
				# if mapped_pct >= min_mapped_pct:
				print "   bin %s: %s\t(%s mapped bp; %.1f%% of bin)" % (bin_id, contig_counts[bin_id], bin_bps[bin_id], mapped_pct)
				# print "   bin %s: %s\t(%s total bp)" % (bin_id, contig_counts[bin_id], bin_bps[bin_id])
		print ""
