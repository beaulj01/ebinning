import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import scipy
from scipy.stats import gaussian_kde
import sklearn.cluster as cluster
import pub_figs
import random
from pbcore.io.BasH5IO import BasH5Reader
from itertools import groupby

data     = np.loadtxt("both.comp.2D", dtype="float")
labels   = np.loadtxt("both.labels",  dtype="S15")
names    = np.loadtxt("both.names",   dtype="str")
sizes    = np.loadtxt("both.lengths", dtype="int")
unmapped = "unmappedSubreads.fasta"
plot_fn  = "reads.contigs.mapped.unmapped.png"
title    = "contig- and read-level cBinning"

def fasta_iter( fasta_name ):
	fh = open(fasta_name)
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		header = header.next()[1:].strip()
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

unmapped_reads = set()
for name,seq in fasta_iter(unmapped):
	read = "/".join(name.split("/")[:-1])
	unmapped_reads.add(read)

for i,name in enumerate(names):
	if labels[i]=="mix":
		if name in unmapped_reads:
			labels[i] = "unmapped"
		else:
			labels[i] = "mapped"
	else:
		labels[i] = "Contig"

print "All points\t\t",   len(labels)
print "Unmapped reads\t", len(labels[labels=="unmapped"])
print "Mapped reads\t",   len(labels[labels=="mapped"])
print "Contigs\t\t\t",    len(labels[labels=="Contig"])

# First plot the background mapped/unmapped reads density
bins                    = int(sys.argv[1])
fig                     = plt.figure(figsize=[15,12])
ax                      = fig.add_axes([0.1, 0.1, 0.8, 0.8])
ax.axis("off")
extent                  = [-40,40,-40,40]
H_map, xedges, yedges   = np.histogram2d(data[labels=="mapped",0],   data[labels=="mapped",1],   bins=bins, range=[extent[:2], extent[2:]])
H_unmap, xedges, yedges = np.histogram2d(data[labels=="unmapped",0], data[labels=="unmapped",1], bins=bins, range=[extent[:2], extent[2:]])
H = H_unmap/float(np.amax(H_unmap)) - H_map/float(np.amax(H_map))
H = np.rot90(H)
H = np.flipud(H)
Hmasked = np.ma.masked_where(H==0,H)
im   = plt.imshow(H, interpolation='bilinear', origin='lower', cmap=cm.seismic, extent=extent, vmin=-1, vmax=1)
cbaxes = fig.add_axes([0.85, 0.5, 0.03, 0.4]) 
cbar = plt.colorbar(im, cax = cbaxes)  
cbar.set_ticks([-1,1])
cbar.ax.set_yticklabels(["Mapped","Unmapped"])
cbar.ax.tick_params(labelsize=18) 

# Now layer on the contigs
lab_set   = set(labels)
lab_set.remove("mapped")
lab_set.remove("unmapped")
label_list = list(lab_set)
label_list.sort()
colors    = plt.get_cmap('spectral')(np.linspace(0.0, 0.77, len(label_list)))
sizes[sizes<50000] = 50000
scaled_sizes = sizes**1.5 / max(sizes**1.5) * 1000
# shapes    = ["o", "v", "^", "s"]
shapes    = ["o", "o", "o", "o"]
res       = []
for k,target_lab in enumerate(label_list):
	idxs             = [j for j,label in enumerate(labels) if label==target_lab]
	scaled_sizes_idx = np.array(scaled_sizes)[idxs]
	X                = data[idxs,0]
	Y                = data[idxs,1]
	color            = colors[k]
	for i,x in enumerate(X):
		res.append( (x, Y[i], target_lab, color, scaled_sizes_idx[i], shapes[k%len(shapes)]) ) 
np.random.shuffle(res) 
plotted_labs = set()
legend_plots = []
legend_labs  = []
for i,(x,y, target_lab, color, size, shape) in enumerate(res):
	# plot = ax.scatter(x,y, marker=shape, s=size , edgecolors="k", label=target_lab, facecolors="None", linewidth=1.2, alpha=0.8)
	plot = ax.scatter(x,y, marker=shape, s=size , edgecolors=color, label=target_lab, facecolors="None", linewidth=1.2, alpha=0.8)
	if target_lab not in plotted_labs:
		plotted_labs.add(target_lab)
		legend_plots.append(plot)
		legend_labs.append(target_lab)
box     = ax.get_position()
leg_tup = zip(legend_labs,legend_plots)
leg_tup.sort(key=lambda x: x[0])
legend_labs, legend_plots = zip(*leg_tup)
leg = ax.legend(legend_plots, legend_labs, loc='upper center', prop={'size':18}, frameon=False, scatterpoints=1)
for i in range(len(legend_labs)):
	leg.legendHandles[i]._sizes = [100]
plt.savefig(plot_fn)
