import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os,sys
import math
from collections import Counter

baseline_readlist_fn  = sys.argv[1]
spec_tot_bases_fn     = sys.argv[2]

genome_sizes = {"J99" :   1698366, \
		   		"26695" : 1590068, \
		   		"C227" :  5292862, \
		   		"Steno" : 5233123}
rank         = np.array(range(1,len(genome_sizes)+1))

tot_bases = {}
cov       = {}
for line in open(spec_tot_bases_fn, "r").xreadlines():
	line         =     line.strip()
	s            =     line.split("\t")[0]
	s_bases      = int(line.split("\t")[1])
	tot_bases[s] = s_bases
	cov[s]       = float(s_bases) / genome_sizes[s]

species_baseline      = []
target_bases_div2     = {}
target_bases_div5     = {}

for s,size in genome_sizes.iteritems():
	abund = cov[s] / np.sum(cov.values())
	species_baseline.append( (s, size, tot_bases[s], round(cov[s],3), round(abund,3)) )
	if s=="26695":
		target_bases_div2[s]  = tot_bases[s] / 2
		target_bases_div5[s]  = tot_bases[s] / 5
	else:
		target_bases_div2[s]  = tot_bases[s]
		target_bases_div5[s]  = tot_bases[s]

species_baseline.sort(key=lambda x: x[3], reverse=True)
wl_name2  = "baseline_26695_div2_read_info.txt"
wl_name5  = "baseline_26695_div5_read_info.txt"
f2        = open(wl_name2,  "w")
f5        = open(wl_name5,  "w")
tot_bases_div2  = Counter()
tot_bases_div5  = Counter()
total_basesline_bases = np.sum(tot_bases.values())
for i,line in enumerate(open(baseline_readlist_fn, "r").xreadlines()):
	s     =     line.split("\t")[1]
	bases = int(line.split("\t")[2])
	if tot_bases_div2[s] < target_bases_div2[s]:
		tot_bases_div2[s] += bases
		f2.write(line)
	if tot_bases_div5[s] < target_bases_div5[s]:
		tot_bases_div5[s] += bases
		f5.write(line)
f2.close()
f5.close()

# Calculate the relative abundances
baseline_cov       = {}
baseline_div2_cov  = {}
baseline_div5_cov  = {}
for s in genome_sizes.keys():
	baseline_cov[s]       = float(tot_bases[s])       / genome_sizes[s]
	baseline_div2_cov[s]  = float(tot_bases_div2[s])  / genome_sizes[s]
	baseline_div5_cov[s]  = float(tot_bases_div5[s])  / genome_sizes[s]
baseline_abund       = []
baseline_div2_abund  = {}
baseline_div5_abund  = {}
for s in genome_sizes.keys():
	baseline_abund.append( (s, baseline_cov[s]        / np.sum(baseline_cov.values())) )
	baseline_div2_abund[s]  =   baseline_div2_cov[s]  / np.sum(baseline_div2_cov.values())
	baseline_div5_abund[s]  =   baseline_div5_cov[s]  / np.sum(baseline_div5_cov.values())
baseline_abund.sort(key=lambda x: x[1], reverse=True)

# Sort based on the baseline abundance ranking
base  = []
div2  = []
div5  = []
for s in map(lambda x: x[0], baseline_abund):
	base.append(baseline_cov[s])
	# div5.append(baseline_div2_cov[s])
	# div5.append(baseline_div5_cov[s])
	div2.append(baseline_div2_abund[s])
	div5.append(baseline_div5_abund[s])

fig = plt.figure(figsize=[6,6])
ax  = fig.add_subplot(111)
ax.plot(rank, map(lambda x: x[1], baseline_abund),       "k-o", label="Baseline")
# ax.plot(rank, base,  "k-o", label="Baseline")
ax.plot(rank, div2,  "r-o", label="26695 baseline/2")
ax.plot(rank, div5,  "b-o", label="26695 baseline/5")
ax.set_ylabel("Relative abundance")
# ax.set_ylabel("Coverage")
ax.set_xticks(range(1,len(rank)+1))
xlabs = []
for abund in baseline_abund:
	s = abund[0]
	xlabs.append(s)
ax.set_xticklabels(xlabs, rotation=90 )
ax.legend(loc='upper right', prop={'size':14})
plt.savefig("rank_abundance_baseline_and_lower_cov.png")
