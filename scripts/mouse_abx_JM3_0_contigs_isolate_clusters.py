import sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import random
import pub_figs
from collections import Counter
from Bio import SeqIO
import textwrap

pub_figs.setup_math_fonts(font_size=18, font="Computer Modern Sans serif")

def scatterplot(results, labels, plot_fn, title, contig_names, extra, boxes, sizes):
	label_set = list(set(labels))
	label_set.sort()
	colors    = plt.get_cmap('spectral')(np.linspace(0, 0.9, len(label_set)))
	sizes[sizes<50000] = 100000
	scaled_sizes = sizes**1.5 / max(sizes**1.5) * 1000
	shapes    = ["o", "v", "^", "s", "D"]
	fig       = plt.figure(figsize=[12,12])
	ax        = fig.add_axes([0.1, 0.1, 0.8, 0.8])
	leg_font  = 16
	res       = []
	for k,target_lab in enumerate(label_set):
		# if target_lab == "unknown":
		# 	continue
		idxs  = [j for j,label in enumerate(labels) if label==target_lab]
		scaled_sizes_idx = np.array(scaled_sizes)[idxs]
		X     = results[idxs,0]
		Y     = results[idxs,1]
		color = colors[k]
		for i,x in enumerate(X):
			res.append( (x, Y[i], target_lab, scaled_sizes_idx[i], color, shapes[k%len(shapes)]) ) 
	
	np.random.shuffle(res) 
	plotted_labs = set()
	legend_plots = []
	legend_labs  = []
	for (x,y, target_lab, size, color, shape) in res:
		if target_lab=="Unlabeled":
			shape = "*"
			color = "gray"
		else:
			target_lab = r"$%s$" % target_lab
		plot = ax.scatter(x,y, marker=shape, s=size , edgecolors=color, label=target_lab, facecolors="None", linewidth=1.2, alpha=0.8)
		if target_lab not in plotted_labs:
			plotted_labs.add(target_lab)
			legend_plots.append(plot)
			legend_labs.append(target_lab)
	box = ax.get_position()
	# ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
	leg_tup = zip(legend_labs,legend_plots)
	leg_tup.sort(key=lambda x: x[0])
	
	# Put "Unlabeled" at end of legend
	unlabeled = [tup for tup in leg_tup if tup[0]=="Unlabeled"][0]
	leg_tup.remove(unlabeled)
	leg_tup.append(unlabeled)

	legend_labs, legend_plots = zip(*leg_tup)
	# leg = ax.legend(legend_plots, legend_labs, loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':18}, frameon=False, scatterpoints=1)
	leg = ax.legend(legend_plots, legend_labs, loc='upper right', prop={'size':leg_font}, frameon=True, scatterpoints=1)
	for i in range(len(legend_labs)):
		# leg.legendHandles[i]._sizes = [200]
		leg.legendHandles[i]._sizes = [150]
	minx = min(map(lambda x: x[0], res))
	maxx = max(map(lambda x: x[0], res))
	miny = min(map(lambda x: x[1], res))
	maxy = max(map(lambda x: x[1], res))
	pub_figs.remove_top_right_axes(ax)

	for k,box_dims in enumerate(boxes):
		box_xmin = box_dims[0]
		box_xmax = box_dims[1]
		box_ymin = box_dims[2]
		box_ymax = box_dims[3]
		ax.plot([box_xmin,box_xmax,box_xmax,box_xmin,box_xmin], [box_ymax,box_ymax,box_ymin,box_ymin,box_ymax], linestyle="--", color="k", linewidth=2)
		labX = box_xmin + (box_xmax - box_xmin)/2
		labY = box_ymax + 1
		ax.text(labX,labY, "bin%s" % (k+1), fontsize=18, horizontalalignment="center", verticalalignment="bottom")
		# ax.set_xlim([min(minx-4, box_xmin-2), max(maxx+4, box_xmax+2)])
		# ax.set_ylim([min(miny-4, box_ymin-2), max(maxy+4, box_ymax+2)])
		ax.set_xlim([-65,85])
		ax.set_ylim([-65,75])
	plt.savefig(plot_fn, dpi=300)

results_nD = np.loadtxt("contigs.SCp", dtype="float")
results    = np.loadtxt("contigs.SCp.2D", dtype="float")
sizes      = np.loadtxt("contigs.lengths", dtype="int")
labels     = np.loadtxt("contigs.kraken",  dtype="str")
names      = np.loadtxt("contigs.names",   dtype="str")
cov_bed    = np.loadtxt("coverage.bed",    dtype="str", skiprows=1)
extra      = np.array([]) 
motifs     = np.loadtxt("ordered_motifs.txt", dtype="str")

minlen = 5000

results_nD = results_nD[sizes>minlen,:]
results    = results[sizes>minlen,:]
labels     = labels[sizes>minlen]
names      = names[sizes>minlen]
sizes      = sizes[sizes>minlen]

# Remove the digested subcontigs
results_nD = results_nD[sizes!=50000,:]
results    = results[sizes!=50000,:]
labels     = labels[sizes!=50000]
names      = names[sizes!=50000]
sizes      = sizes[sizes!=50000]

plot_fn    = "contigs.SCp.isoclusters.png"
title      = "contig-level eBinning"

boxes   = [(-50,-36,-37,-26), \
		   (-31,-17,-50,-41), \
		   (-2,12,-61,-46), \
		   (23,36,-46,-33), \
		   (38,49,-33,-23), \
		   (51,63,-7,4), \
		   # (2,22,48,62), \
		   (2,22,41,62), \
		   (-47,-34,24,36), \
		   (-63,-52,5,15)]

for i,lab in enumerate(labels):
	labels[i] = lab.replace("_", " ")

lab_counter = Counter()
for i,label in enumerate(labels):
	label     = label.strip("[")
	label     = label.strip("]")
	labels[i] = label
	lab_counter[label] += 1

for i,label in enumerate(labels):
	label_sum = np.sum(sizes[labels==label])
	if lab_counter[label]<5 and label_sum<100000:
		labels[i] = "Unlabeled"

def get_contig_mean_cov( name, cov_bed ):
	name      = name.split("|")[0]
	cov_names = cov_bed[:,0]
	mask      = cov_names==name
	covs      = cov_bed[mask,4].astype(float)
	# Ignore edges of contigs
	covs      = covs[2:-2]
	return covs.mean()

for i,box in enumerate(boxes):
	box_xmin = box[0]
	box_xmax = box[1]
	box_ymin = box[2]
	box_ymax = box[3]
	box_contigs = []
	box_SCp     = []
	for j in range(results.shape[0]):
		x = results[j,0]
		y = results[j,1]
		if y>box_ymin and y<box_ymax and x>box_xmin and x<box_xmax:
			box_contigs.append(names[j])
			box_SCp.append(results_nD[j])

	box_SCp       = np.matrix(box_SCp)
	box_SCp_means = box_SCp.mean(0).A1
	mask          = box_SCp_means > 1.0
	sig_motifs    = motifs[mask]
	mean_SCps     = box_SCp_means[mask]
	for j,motif in enumerate(sig_motifs):
		print "%s %s\t%.4f" % ((i+1), motif, mean_SCps[j])

	fasta_fn = "contigs.box.%s.fasta" % (i+1)
	f = open(fasta_fn, "wb")
	cluster_size = 0
	contigs_n    = 0
	for contig in SeqIO.parse("polished_assembly.fasta", "fasta"):
		if contig.name.split("|")[0] in box_contigs:
			print "%s %s\t%s\t%.2f" % ((i+1),contig.name, len(contig.seq), get_contig_mean_cov(contig.name, cov_bed))
			SeqIO.write(contig, f, "fasta")
			cluster_size += len(contig.seq)
			contigs_n    += 1
	f.close()
	print "Total contigs = %s" % contigs_n
	print "Total bases = %s bp" % cluster_size
	print ""

scatterplot(results, labels, plot_fn, title, names, extra, boxes, sizes)