import os,sys
from pbcore.io.FastaIO import FastaReader
import subprocess

assembly_fn = sys.argv[1]

awk_CMD   = "awk '$5>100 {print}' coverage.bed | cut -d$'\t' -f1 |sort |uniq"
p         = subprocess.Popen(awk_CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
stdOutErr = p.communicate()
sts       = p.returncode
if sts != 0:
	raise Exception("Failed cat command: %s" % awk_CMD)
contigs = stdOutErr[0].split("\n")[1:-1]

for entry in FastaReader(assembly_fn):
	if entry.name.split("|")[0] in contigs:
		print ">"+entry.name
		print entry.sequence