import os,sys
from Bio import SeqIO

ebi    = "ebi_plasmids.fasta"
rebase = "rebase_plasmids.fasta"

ebi_ids  = set()
combined = []
for record in SeqIO.parse(ebi, "fasta"):
	if record.id not in ebi_ids:
		ebi_ids.add(record.id)
		combined.append(record)

for record in SeqIO.parse(rebase, "fasta"):
	if record.id not in ebi_ids:
		combined.append(record)

out_fn = "combined_plasmids.fasta"
SeqIO.write(combined, out_fn, "fasta")