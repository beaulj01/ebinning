import os,sys
import json
import urllib

url_str = "http://api.metagenomics.anl.gov/m5nr/ontology?source=Subsystems&level=function"
f       = urllib.urlopen(url_str)

objects = json.load(f)
SS_map  = {}
for item in objects["data"]:
	acc         = item["accession"]
	SS_map[acc] = {}
	for key,val in item.iteritems():
		if key!="accession":
			SS_map[acc][key] = val

anno_fn = "ontology.subsystems.txt"
for i,line in enumerate(open(anno_fn, "rb").xreadlines()):
	if i>0 and line.find("Download complete")<0:
		line = line.strip()
		name        = line.split("\t")[0]
		md5         = line.split("\t")[1]
		sequence    = line.split("\t")[2]
		annotations = line.split("\t")[3]
		sub_acc     = annotations.split(",")[0].split("=")[1].strip("[]")
		contig      = name.split("|")[1]

		if SS_map.get(sub_acc):
			if SS_map[sub_acc].get("level1")=="Phages, Prophages, Transposable elements, Plasmids":
					print "%s\t%s\t%s" % (contig, sub_acc, SS_map[sub_acc]["level2"])
		else:
			pass
