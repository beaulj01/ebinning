import os,sys
import glob
import subprocess

def run_OS_command( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts != 0:
		print "Failed command: %s" % CMD
	return sts, stdOutErr

matches = {"box1" : ("Bacteroidales bacterium M1", "mouse_omerod_catalog/GCA_001689425.1_ASM168942v1_genomic.fna"), \
		   "box2" : ("MGS_0161", "MGS/MGS_0161.fasta"), \
		   "box3" : ("Bacteroidales bacterium M12", "mouse_omerod_catalog/GCA_001689575.1_ASM168957v1_genomic.fna"), \
		   "box4" : ("Akkermansia muciniphila strain YL44", "mouse_smrt_12_refs/Akkermansia_muciniphila_strain_YL44.fasta"), \
		   "box5" : ("Parabacteroides sp. YL27", "mouse_smrt_12_refs/Parabacteroides_sp_YL27.fasta"), \
		   "box6" : ("MGS_0004", "MGS/MGS_0004.fasta"), \
		   "box8" : ("Bacteroidales bacterium M2", "mouse_omerod_catalog/GCA_001689415.1_ASM168941v1_genomic.fna"), \
		   "box9" : ("MGS_0305", "MGS/MGS_0305.fasta"), \
			}

for i in range(1,10):
	if i==7:
		continue
	box        = "box%s" % i
	match_name = matches[box][0]
	match_fa   = matches[box][1]
	compare    = "box%s_vs_%s" % (i, match_name)
	compare    = compare.replace(" ","_")
	box_fasta  = "contigs.box.%s.fasta" % i
	
	CMD1 = "nucmer %s %s" % (match_fa, box_fasta)
	CMD2 = "delta-filter -m out.delta > out.delta.m"
	sts,stdOutErr = run_OS_command(CMD1)
	sts,stdOutErr = run_OS_command(CMD2)
	
	CMD3 = "./my_mummerplot -color -layout out.delta.m --png -t %s -p %s" % (compare, compare)
	sts,stdOutErr = run_OS_command(CMD3)

	# to_rm = glob.glob("*.filter")
	# for f in to_rm:
	# 	os.remove(f)
	# to_rm = glob.glob("*.*plot")
	# for f in to_rm:
	# 	os.remove(f)
	# to_rm = glob.glob("*.gp")
	# for f in to_rm:
	# 	os.remove(f)