import os,sys
from pbcore.io.BasH5IO import BasH5Reader
import numpy as np
from collections import Counter

movie_mapping_fn = "movie_mapping.txt"
input_fn         = np.loadtxt("input.fofn", dtype="str")

n_reads   = Counter()
n_sreads  = Counter()
n_bases   = Counter()
rlengths  = {}
slengths  = {}
movie_map = {}
for line in open(movie_mapping_fn, "r").xreadlines():
	line             = line.strip()
	spec             = line.split(":")[0]
	mov              = line.split(":")[1]
	movie_map[mov] = spec
	rlengths[spec] = []
	slengths[spec] = []

for i,f in enumerate(input_fn):
	print "bax %s" % i
	mov    = f.split("/")[-1]
	spec   = movie_map[mov]
	reader = BasH5Reader(f)
	for z in reader:
		zl = 0
		for s in z.subreads:
			sl = len(s.basecalls())
			slengths[spec].append(sl)
			zl += sl
			n_sreads[spec] += 1
		rlengths[spec].append(zl)
		n_bases[spec] += zl
		n_reads[spec] += 1

for spec in n_reads.keys():
	print spec, n_bases[spec], np.mean(slengths[spec]), np.mean(rlengths[spec]), n_sreads[spec], n_reads[spec]
