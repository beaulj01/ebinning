import os,sys
from Bio import SeqIO

ebi_fn    = "ebi_plasmids.fasta"
rat_fn    = "HG796247-HG796860.fasta"
rebase_fn = "rebase_plasmids.fasta"
refseq_fn = "plasmid.all.genomic.fna"

redund_check = set()
records      = []

for fn in [ebi_fn, rat_fn, rebase_fn, refseq_fn]:
	for Seq in SeqIO.parse(fn, "fasta"):
		name = " ".join(Seq.description.split(" ")[1:])
		if name not in redund_check:
			records.append(Seq)
			redund_check.add(name)
		else:
			print fn, name
			pass

out_fn = "combined.plasmids.fasta"
SeqIO.write(records, out_fn, "fasta")