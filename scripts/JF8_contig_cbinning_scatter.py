import sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import random
import pub_figs

label_dict      =  {"B_caccae":     r"\textit{B. caccae}", \
				    "B_ovatus":     r"\textit{B. ovatus}", \
				    "B_vulgatus":   r"\textit{B. vulgatus}", \
				    "C_aerofaciens":r"\textit{C. aerofaciens}", \
				    "R_gnavus":     r"\textit{R. gnavus} (no methylation)", \
				    "B_theta":      r"\textit{B. thetaiotaomicron}", \
				    "E_coli":       r"\textit{E. coli}", \
				    "C_bolteae":    r"\textit{C. bolteae}"}

pub_figs.setup_math_fonts(font_size=24, font="Computer Modern Sans serif")

def scatterplot(results, labels, sizes, plot_fn, title):
	label_set = list(set(labels))
	label_set.sort()
	colors    = plt.get_cmap('spectral')(np.linspace(0, 1.0, len(label_set)+1))
	# colors    = plt.get_cmap('spectral')(range(len(label_set)))
	shapes    = ["o", "v", "^", "s", "D", "p", "d"]
	fig       = plt.figure(figsize=[15,12])
	ax        = fig.add_axes([0.1, 0.1, 0.5, 0.6])
	# ax.axis("off")
	pub_figs.remove_top_right_axes( ax )
	if len(sizes)>0:
		sizes[sizes<100000] = 100000
		scaled_sizes = sizes**1.5 / max(sizes**1.5) * 2000
	res       = []
	for k,target_lab in enumerate(label_set):
		idxs             = [j for j,label in enumerate(labels) if label==target_lab]
		scaled_sizes_idx = np.array(scaled_sizes)[idxs]
		X                = results[idxs,0]
		Y                = results[idxs,1]
		color            = colors[k]
		for i,x in enumerate(X):
			res.append( (x, Y[i], target_lab, color, scaled_sizes_idx[i], shapes[k%len(shapes)]) ) 
	
	np.random.shuffle(res)
	plotted_labs = set()
	legend_plots = []
	legend_labs  = []
	for (x,y, target_lab, color, size, shape) in res:
		plot = ax.scatter(x,y, edgecolors=color, \
							   facecolors="None", \
							   marker=shape, \
							   label=label_dict[target_lab], \
							   alpha=0.8, \
							   s=size, \
							   lw=3)
		if label_dict[target_lab] not in plotted_labs:
			plotted_labs.add(label_dict[target_lab])
			legend_plots.append(plot)
			legend_labs.append(label_dict[target_lab])
	# box = ax.get_position()
	# ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
	leg_tup = zip(legend_labs,legend_plots)
	leg_tup.sort(key=lambda x: x[0])
	legend_labs, legend_plots = zip(*leg_tup)
	print legend_labs
	leg = ax.legend(legend_plots, legend_labs, loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':24}, scatterpoints=1, frameon=False)
	for i in range(len(legend_labs)):
		leg.legendHandles[i]._sizes = [200]
	# ax.set_title(title)
	xmin = min(results[:,0])
	xmax = max(results[:,0])
	ymin = min(results[:,1])
	ymax = max(results[:,1])
	ax.set_xlim([-35, xmax+2])
	ax.set_ylim([ymin-5, 35])
	plt.savefig(plot_fn)

results = np.loadtxt(sys.argv[1], dtype="float")
labels  = np.loadtxt(sys.argv[2], dtype="str")
sizes   = np.loadtxt(sys.argv[3], dtype="int")

# remove digested subcontigs
results = results[sizes!=50000]
labels  = labels[sizes!=50000]
sizes   = sizes[sizes!=50000]

plot_fn = "JF8.contigs.comp.png"
title   = "contig-level cBinning"

scatterplot(results, labels, sizes, plot_fn, title)