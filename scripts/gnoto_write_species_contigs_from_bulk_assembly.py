import os,sys
from Bio import SeqIO
import numpy as np
from collections import defaultdict

names    = np.loadtxt("contigs.names", dtype="str")
labels   = np.loadtxt("contigs.labels", dtype="str")
assembly = "polished_assembly.fasta"

lab_dict    = dict(zip(names, labels))
lab_records = defaultdict(list)

for Seq in SeqIO.parse(assembly, "fasta"):
	name = Seq.id.split("|")[0]
	try:
		lab  = lab_dict[name]
	except KeyError:
		print "huh: %s" % name
	lab_records[lab].append(Seq)

for lab in lab_records.keys():
	out_fn = "%s.contigs.fasta" % lab
	SeqIO.write(lab_records[lab], out_fn, "fasta")