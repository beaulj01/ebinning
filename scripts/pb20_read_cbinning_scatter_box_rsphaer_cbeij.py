import sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import random
import pub_figs
from collections import Counter
from pbcore.io.BasH5IO import BasH5Reader
import textwrap

label_dict      =  {"S_mutans_UA159" :                       r"${S. mutans}$ UA159", \
		   			"L_monocytogenes_EGD_e" :                r"${L. monocytogenes}$ EGD-e", \
		   			"S_epidermidis_ATCC_12228" :             r"${S. epidermidis}$ ATCC 12228", \
		   			"L_gasseri_ATCC_33323" :                 r"${L. gasseri}$ ATCC 33323", \
		   			"P_acnes_KPA171202" :                    r"${P. acnes}$ KPA171202", \
		   			"S_aureus_subsp_aureus_USA300_TCH1516" : r"${S. aureus}$ subsp aureus USA300 TCH1516", \
		   			"N_meningitidis_MC58" :                  r"${N. meningitidis}$ MC58", \
		   			"P_aeruginosa_PAO1" :                    r"${P. aeruginosa}$ PAO1", \
					"H_pylori_26695" :                       r"${H. pylori}$ 26695", \
					"D_radiodurans_R1" :                     r"${D. radiodurans}$ R1", \
		   			"A_odontolyticus_ATCC_17982_Scfld021" :  r"${A. odontolyticus}$ ATCC 17982 Scfld021", \
		   			"B_vulgatus_ATCC_8482" :                 r"${B. vulgatus}$ ATCC 8482", \
		   			"E_faecalis_OG1RF" :                     r"${E. faecalis}$ OG1RF", \
		   			"E_coli_str_K_12_substr_MG1655" :        r"${E. coli}$ str K-12 substr MG1655", \
		   			"S_agalactiae_2603V_R" :                 r"${S. agalactiae}$ 2603V R", \
					"A_baumannii_ATCC_17978" :               r"${A. baumannii}$ ATCC 17978", \
					"R_sphaeroides_2_4_1" :                  r"${R. sphaeroides}$ 2.4.1", \
		   			"S_pneumoniae_TIGR4":                    r"${S. pneumoniae}$ TIGR4", \
		   			"C_beijerinckii_NCIMB_8052" :            r"${C. beijerinckii}$ NCIMB 8052", \
		   			"B_cereus_ATCC_10987" :                  r"${B. cereus}$ ATCC 10987", \
		   			"unknown":    							 r"Unmapped"}

def scatterplot(results, labels, plot_fn, title, read_names):
	label_set = list(set(labels))
	label_set.sort()
	colors    = plt.get_cmap('spectral')(np.linspace(0, 1.0, len(label_set)+1))
	shapes    = ["o","s","^"]
	fig       = plt.figure(figsize=[15,12])
	ax        = fig.add_axes([0.1, 0.1, 0.6, 0.6])
	ax.axis("off")
	res       = []
	for k,target_lab in enumerate(label_set):
		# if target_lab == "unknown":
		# 	continue
		idxs  = [j for j,label in enumerate(labels) if label==target_lab]
		X     = results[idxs,0]
		Y     = results[idxs,1]
		color = colors[k]
		for i,x in enumerate(X):
			res.append( (x, Y[i], target_lab, color, shapes[k%len(shapes)]) ) 
	
	# res          = random.sample(res, 20000)
	np.random.shuffle(res) 
	plotted_labs = set()
	legend_plots = []
	legend_labs  = []
	for m,(x,y, target_lab, color, shape) in enumerate(res):
		plot = ax.scatter(x,y, marker=shape, s=15 , edgecolors="None", label=label_dict[target_lab], facecolors=color, alpha=0.8)
		if label_dict[target_lab] not in plotted_labs:
			plotted_labs.add(label_dict[target_lab])
			legend_plots.append(plot)
			legend_labs.append(label_dict[target_lab])
	box = ax.get_position()
	ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
	leg_tup = zip(legend_labs,legend_plots)
	leg_tup.sort(key=lambda x: x[0])
	legend_labs, legend_plots = zip(*leg_tup)
	leg = ax.legend(legend_plots, legend_labs, loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':18}, frameon=False)
	for i in range(len(legend_labs)):
		leg.legendHandles[i]._sizes = [50]
	minx = min(map(lambda x: x[0], res))
	maxx = max(map(lambda x: x[0], res))
	miny = min(map(lambda x: x[1], res))
	maxy = max(map(lambda x: x[1], res))

	# R. sphaeroides box
	box_xmin = 36.5
	box_xmax = 40
	box_ymin = 1.5
	box_ymax = 5
	ax.plot([box_xmin,box_xmax,box_xmax,box_xmin,box_xmin], [box_ymax,box_ymax,box_ymin,box_ymin,box_ymax], linestyle="--", color="k", linewidth=2, dashes=(3,2))
	box_readnames      = set()
	box_rspaer_counter = Counter()
	for i in range(results.shape[0]):
		x = results[i,0]
		y = results[i,1]
		if y>box_ymin and y<box_ymax and x>box_xmin and x<box_xmax:
			box_readnames.add(read_names[i])
			if labels[i] == "R_sphaeroides_2_4_1":
				box_rspaer_counter["R_sphaeroides_2_4_1"] += 1
			else:
				box_rspaer_counter["Other"] += 1
	target_reads = box_rspaer_counter["R_sphaeroides_2_4_1"]
	all_reads    = np.sum(box_rspaer_counter.values())
	print target_reads, all_reads, (target_reads / float(all_reads))
	f1 = open("box_rsphaer_reads.fasta", "w")
	for line in open("bas.h5.fofn", "r").xreadlines():
		line     = line.strip()
		baxh5    = line.split()[0]
		reader   = BasH5Reader(baxh5)
		zmws     = [z for z in reader if z.zmwName in box_readnames]
		for z in zmws:
			for sub in z.subreads:
				f1.write(">%s\n" % sub.readName)
				f1.write("%s" % sub.basecalls())
	f1.close()

	# B. cereus box
	box_xmin = -10
	box_xmax = -4
	box_ymin = 6
	box_ymax = 12
	ax.plot([box_xmin,box_xmax,box_xmax,box_xmin,box_xmin], [box_ymax,box_ymax,box_ymin,box_ymin,box_ymax], linestyle="--", color="k", linewidth=2, dashes=(3,2))
	box_readnames = set()
	box_bcereus_counter = Counter()
	for i in range(results.shape[0]):
		x = results[i,0]
		y = results[i,1]
		if y>box_ymin and y<box_ymax and x>box_xmin and x<box_xmax:
			box_readnames.add(read_names[i])
			if labels[i] == "B_cereus_ATCC_10987":
				box_bcereus_counter["B_cereus_ATCC_10987"] += 1
			else:
				box_bcereus_counter["Other"] += 1
	target_reads = box_bcereus_counter["B_cereus_ATCC_10987"]
	all_reads    = np.sum(box_bcereus_counter.values())
	print target_reads, all_reads, (target_reads / float(all_reads))
	f2 = open("box_bcereus_reads.fasta", "w")
	for line in open("bas.h5.fofn", "r").xreadlines():
		line     = line.strip()
		baxh5    = line.split()[0]
		reader   = BasH5Reader(baxh5)
		zmws     = [z for z in reader if z.zmwName in box_readnames]
		for z in zmws:
			for sub in z.subreads:
				f2.write(">%s\n" % sub.readName)
				f2.write("%s" % sub.basecalls())
	f2.close()

	ax.set_xlim([min(minx-1, box_xmin), max(maxx+1, box_xmax)])
	ax.set_ylim([min(miny-1, box_ymin), max(maxy+1, box_ymax)])
	plt.savefig(plot_fn)
	
results = np.loadtxt("reads.raw.comp.2D.gt15kb", dtype="float")
labels  = np.loadtxt("reads.raw.labels.gt15kb",  dtype="str")
names   = np.loadtxt("reads.raw.names.gt15kb",   dtype="str")
plot_fn = "pub.rsphaer.cbeij.reads.comp.png"
title   = "read-level cBinning"

fig     = plt.figure(figsize=[15,12])
ax      = fig.add_axes([0.1, 0.1, 0.6, 0.6])
ax.axis("off")
extent            = [-60,60,-60,60]
bins              = 75
H, xedges, yedges = np.histogram2d(results[:,0], results[:,1], bins=bins, range=[extent[:2], extent[2:]])
H = np.rot90(H)
H = np.flipud(H)
Hmasked = np.ma.masked_where(H==0,H)         # Mask pixels with a value of zero
plt.hot()                                    # set 'hot' as default colour map
im = plt.imshow(H, interpolation='bilinear', # creates background image
                origin='lower', cmap=cm.Blues, 
                extent=extent)
plt.subplots_adjust(bottom=0.15, left=0.15)
levels            = [35,30,25,20]
cols              = ["k","0.4","0.6","0.8"]
cset              = plt.contour(H, levels, origin="lower",colors=cols,linewidths=1.4,extent=extent)
for c in cset.collections:
	c.set_linestyle("solid")
minx = min(results[:,0])
maxx = max(results[:,0])
miny = min(results[:,1])
maxy = max(results[:,1])

box_xmin = 36
box_xmax = 40
box_ymin = 1
box_ymax = 5
ax.plot([box_xmin,box_xmax,box_xmax,box_xmin,box_xmin], [box_ymax,box_ymax,box_ymin,box_ymin,box_ymax], linestyle="--", color="k", linewidth=2, dashes=(3,2))

box_xmin = -10
box_xmax = -4
box_ymin = 6
box_ymax = 12
ax.plot([box_xmin,box_xmax,box_xmax,box_xmin,box_xmin], [box_ymax,box_ymax,box_ymin,box_ymin,box_ymax], linestyle="--", color="k", linewidth=2, dashes=(3,2))
ax.set_xlim([min(minx-1, box_xmin-1), max(maxx+1, box_xmax+1)])
ax.set_ylim([min(miny-1, box_ymin-1), max(maxy+1, box_ymax+1)])
plt.savefig("pub.contours.reads.comp.png")

scatterplot(results, labels, plot_fn, title, names)