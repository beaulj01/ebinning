import os,sys
import numpy as np

sites = np.loadtxt(sys.argv[1], dtype='int')

gaps = []
for i,site in enumerate(sites):
	if i==0: continue
	d = site - sites[i-1]
	gaps.append( (d,sites[i-1],site) )

gaps.sort(key=lambda x: x[0])
for gap in gaps[-20:]:
		print "%sbp (%s-%s)" % (gap[0], gap[1], gap[2])