import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from methyl_FDR_sensitivity_ipdRatios import PowerCurves as Methyl_PowerCurves
import logging
import optparse

bash5_barcodes = sys.argv[1]
cmph5_barcodes = sys.argv[2]

class FDR_processor:
	def __init__ ( self ):
		"""
		Parse the options and arguments, then instantiate the logger. 
		"""
		self.__parseArgs( )
		self.__initLog( )

	def __parseArgs( self ):
		"""Handle command line argument parsing"""

		usage = """%prog [--help] [options] <inputs_file>"""

		parser = optparse.OptionParser( usage=usage, description=__doc__ )

		parser.add_option( "-d", "--debug",       action="store_true", help="Increase verbosity of logging" )
		parser.add_option( "-i", "--info",        action="store_true", help="Add basic logging" )
		parser.add_option( "--logFile",           type="str", help="Write logging to file [ROC.out]" )
		parser.add_option( "--FDRthresh",         type="float", help="For this FDR level, return corresponding SMp value [0.01]" )
		parser.add_option( "--oppStrandOffset",   type="int", help="Number of positions mods are offset on opposite strands [1]" )
		parser.set_defaults( logFile="ROC.out",       \
							 debug=False,             \
							 info=False,              \
							 oppStrandOffset=1,       \
							 FDRthresh=0.01)

		self.opts, self.args = parser.parse_args( )

		self.bash5_exp  = self.args[0]
		self.bash5_null = self.args[1]
		self.cmph5_exp  = self.args[2]
		self.cmph5_null = self.args[3]

	def __initLog( self ):
		"""Sets up logging based on command line arguments. Allows for three levels of logging:
		logging.error( ): always emitted
		logging.info( ) : emitted with --info or --debug
		logging.debug( ): only with --debug"""

		if os.path.exists(self.opts.logFile):
			os.remove(self.opts.logFile)

		logLevel = logging.DEBUG if self.opts.debug \
					else logging.INFO if self.opts.info \
					else logging.ERROR

		self.logger = logging.getLogger("")
		self.logger.setLevel(logLevel)
		
		# create file handler which logs even debug messages
		fh = logging.FileHandler(self.opts.logFile)
		fh.setLevel(logLevel)
		
		# create console handler with a higher log level
		ch = logging.StreamHandler()
		ch.setLevel(logLevel)
		
		# create formatter and add it to the handlers
		logFormat = "%(asctime)s [%(levelname)s] %(message)s"
		formatter = logging.Formatter(logFormat)
		ch.setFormatter(formatter)
		fh.setFormatter(formatter)
		
		# add the handlers to logger
		self.logger.addHandler(ch)
		self.logger.addHandler(fh)

		logging.info("========== Configuration ===========")
		logging.info("bas_exp_fn:            %s" % self.bash5_exp)
		logging.info("bas_null_fn:           %s" % self.bash5_null)
		logging.info("cmp_exp_fn:            %s" % self.cmph5_exp)
		logging.info("cmp_null_fn:           %s" % self.cmph5_null)
		logging.info("FDRthresh:              %s" % self.opts.FDRthresh)
		logging.info("logFile:               %s" % self.opts.logFile)
		logging.info("debug:                 %s" % self.opts.debug)
		logging.info("info:                  %s" % self.opts.info)

	def run( self ):

		def read_ebarcodes( fn ):
			vals = []
			for i,line in enumerate(open(fn).xreadlines()):
				if i==0: continue # Header
				delim = "\t"
				line  =       line.strip()
				lab   =       line.split(delim)[0]
				read  =       line.split(delim)[1]
				sites =   int(line.split(delim)[2])
				score = float(line.split(delim)[3])
				vals.append( (read, sites, score, lab) )
			return vals

		bash5_exp_vals  = read_ebarcodes(self.bash5_exp)
		bash5_null_vals = read_ebarcodes(self.bash5_null)
		cmph5_exp_vals  = read_ebarcodes(self.cmph5_exp)
		cmph5_null_vals = read_ebarcodes(self.cmph5_null)

		methyl         = Methyl_PowerCurves( bash5_exp_vals, bash5_null_vals, cmph5_exp_vals, cmph5_null_vals, self.opts.FDRthresh, self.logger )
		methyl_results = methyl.run( plot=True )

if __name__ == "__main__":
	app = FDR_processor()
	sys.exit( app.run() )