import os,sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import random

def scatterplot(results, labels, plot_fn, sizes, title):
	label_set = list(set(labels))
	label_set.sort()
	colors    = plt.get_cmap('rainbow')(np.linspace(0, 1.0, len(label_set)))
	shapes    = ["o", "v", "^", "s", "D", "p", "d"]
	fig       = plt.figure(figsize=[15,12])
	ax        = fig.add_subplot(111)
	# sizes[sizes<100000] = 100000
	# scaled_sizes = sizes**1.5 / max(sizes**1.5) * 2000
	scaled_sizes = (sizes - min(sizes)) / (max(sizes) - min(sizes))
	max_size     = 1000
	min_size     = 30
	range2       = max_size - min_size
	scaled_sizes = (scaled_sizes*range2) + min_size
	to_plot      = []
	for k,target_lab in enumerate(label_set):
		idxs             = [j for j,label in enumerate(labels) if label==target_lab]
		X                = results[idxs,0]
		Y                = results[idxs,1]
		# scaled_sizes_idx = np.array(scaled_sizes)[idxs]
		scaled_sizes_idx = (np.zeros(len(labels))+1)*10
		for i,(x,y) in enumerate(results[idxs,:]):
			to_plot.append( (x,y,target_lab, colors[k], shapes[k%len(shapes)], scaled_sizes_idx[i]) )
	np.random.shuffle(to_plot)
	plotted_labs = set()
	legend_plots = []
	legend_labs  = []
	for i,(x,y,lab,color,shape,scaled_size) in enumerate(to_plot):
		# if i==5000: break
		if lab=="unknown":
			# pass
			plot = ax.scatter(x, y, edgecolors="None", label=lab, marker="o", facecolors="k", lw=3, alpha=0.2, s=scaled_size)
		else:
			plot = ax.scatter(x, y, edgecolors="None", label=lab, marker="o", facecolors="k", lw=3, alpha=0.2, s=scaled_size)
		if lab not in plotted_labs:
			plotted_labs.add(lab)
			legend_plots.append(plot)
			legend_labs.append(lab)
	box = ax.get_position()
	ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
	leg_tup = zip(legend_labs,legend_plots)
	leg_tup.sort(key=lambda x: x[0])
	legend_labs, legend_plots = zip(*leg_tup)
	ax.legend(legend_plots, legend_labs, loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':14})
	ax.set_title(title)
	plt.savefig(plot_fn)

output_fn  =     sys.argv[1]
labels_fn  =     sys.argv[2]
scatter_fn =     sys.argv[3]
title      =     sys.argv[4]

labels  = np.loadtxt(labels_fn, dtype="str")
results = np.loadtxt(output_fn, dtype="float")
if len(sys.argv)==6:
	sizes_fn =     sys.argv[5]
	sizes    = np.loadtxt(sizes_fn,  dtype="float")
else:
	sizes   = np.zeros(len(labels))+1
scatterplot(results, labels, scatter_fn, sizes, title)