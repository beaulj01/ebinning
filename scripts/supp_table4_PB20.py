import os,sys
from pbcore.io.BasH5IO import BasH5Reader
import numpy as np
from collections import Counter

log_wl           = set(np.loadtxt("log_abundance_whitelist.txt", dtype="str"))
read_species_map = np.loadtxt("read_species_mapping.txt", dtype="str")
input_fn         = np.loadtxt("input.fofn", dtype="str")

genome_sizes     = {"A_baumannii_ATCC_17978" :               3976747, \
		   			"A_odontolyticus_ATCC_17982_Scfld021" :  2391230, \
		   			"B_cereus_ATCC_10987" :                  5224283, \
		   			"B_vulgatus_ATCC_8482" :                 5163189, \
		   			"C_beijerinckii_NCIMB_8052" :            6000632, \
		   			"D_radiodurans_R1" :                     3060986, \
		   			"E_coli_str_K_12_substr_MG1655" :        4641652, \
		   			"E_faecalis_OG1RF" :                     2739625, \
		   			"H_pylori_26695" :                       1667867, \
		   			"L_gasseri_ATCC_33323" :                 1894360, \
		   			"L_monocytogenes_EGD_e" :                2944528, \
		   			"N_meningitidis_MC58" :                  2272360, \
		   			"P_acnes_KPA171202" :                    2560265, \
		   			"P_aeruginosa_PAO1" :                    6264404, \
		   			"R_sphaeroides_2_4_1" :                  4131542, \
		   			"S_agalactiae_2603V_R" :                 2160267, \
		   			"S_aureus_subsp_aureus_USA300_TCH1516" : 2872915, \
		   			"S_epidermidis_ATCC_12228" :             2499279, \
		   			"S_mutans_UA159" :                       2032925, \
		   			"S_pneumoniae_TIGR4" :                   2160842}

n_reads   = Counter()
n_sreads  = Counter()
n_bases   = Counter()
rlengths  = {}
slengths  = {}
read_map  = {}
for entry in read_species_map:
	read  = entry[0]
	spec  = entry[1]
	read_map[read] = spec
	rlengths[spec] = []
	slengths[spec] = []

for i,f in enumerate(input_fn):
	print "bax %s" % i
	reader = BasH5Reader(f)
	for z in reader:
		zl   = 0
		read = z.zmwName
		if read_map.get(read) and read in log_wl:
			spec = read_map[read]
			for s in z.subreads:
				sl = len(s.basecalls())
				slengths[spec].append(sl)
				zl += sl
				n_sreads[spec] += 1
			rlengths[spec].append(zl)
			n_bases[spec] += zl
			n_reads[spec] += 1
		else:
			pass

for spec in n_reads.keys():
	print "%s\t%s\t%s\t%.2f\t%.2f" % (spec, n_bases[spec], n_reads[spec], np.mean(rlengths[spec]), float(n_bases[spec])/genome_sizes[spec])
