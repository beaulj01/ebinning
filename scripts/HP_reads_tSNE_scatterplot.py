import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os,sys
import math
from collections import Counter
import operator
import random
import pub_figs

pub_figs.setup_math_fonts(font_size=24, font="Computer Modern Sans serif")

labels_d = {"26695": r"${H. pylori}$ 26695", \
			"J99":   r"${H. pylori}$ J99"}

def scatterplot(results, labels, plot_fn, title, read_names):
	label_set = list(set(labels))
	label_set.sort()
	# colors    = plt.get_cmap('rainbow')(np.linspace(0, 1.0, len(label_set)))
	colors    = ["k", "r"]
	shapes    = ["o", "v", "^", "s", "D", "p", "d"]
	fig       = plt.figure(figsize=[15,12])
	ax        = fig.add_subplot(111)
	res = []
	to_plot      = []
	for k,target_lab in enumerate(label_set):
		idxs             = [j for j,label in enumerate(labels) if label==target_lab]
		X                = results[idxs,0]
		Y                = results[idxs,1]
		scaled_sizes_idx = (np.zeros(len(labels))+1)*10
		for i,(x,y) in enumerate(results[idxs,:]):
			to_plot.append( (x,y,target_lab, colors[k], shapes[k%len(shapes)], scaled_sizes_idx[i]) )
	np.random.shuffle(to_plot)
	plotted_labs = set()
	legend_plots = []
	legend_labs  = []
	for i,(x,y,lab,color,shape,scaled_size) in enumerate(to_plot):
		if i==20000: break
		plot = ax.scatter(x, y, edgecolors=color, label=lab, marker=shape, facecolors="None", lw=3, alpha=0.7, s=scaled_size)
		if lab not in plotted_labs:
			plotted_labs.add(lab)
			legend_plots.append(plot)
			legend_labs.append(labels_d[lab])
	box = ax.get_position()
	ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
	leg_tup = zip(legend_labs,legend_plots)
	leg_tup.sort(key=lambda x: x[0])
	legend_labs, legend_plots = zip(*leg_tup)
	leg = ax.legend(legend_plots, legend_labs, loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':24}, scatterpoints=1,frameon=False)
	for i in range(len(legend_labs)):
		leg.legendHandles[i]._sizes = [100]
	ax.set_title(title)
	pub_figs.remove_top_right_axes(ax)
	
	plt.savefig(plot_fn)

SMp_coords  = np.loadtxt(sys.argv[1], dtype="float")
read_labels = np.loadtxt(sys.argv[2], dtype="str")
read_names  = np.loadtxt(sys.argv[3], dtype="str")
plot_fn     = "reads.raw.SMp.tsne.png"
title       = ""

scatterplot(SMp_coords, read_labels, plot_fn, title, read_names)