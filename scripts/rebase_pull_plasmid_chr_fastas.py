import os,sys
import re
import urllib2
from bs4 import BeautifulSoup
import string
from Bio import Entrez
from collections import defaultdict
import numpy as np
import math
sys.path.append( "/hpc/users/beaulj01/gitRepo/eBinning/src" )
import read_scanner
from itertools import groupby
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import scipy.stats as stats
import subprocess

all_fastas_fn = "all_plasmids.fasta"

def run_OS_command( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts != 0:
		print "Failed command: %s" % CMD
	return sts, stdOutErr

bioproject_urls = set()
specs_key       = {}

class Opts:
	def __init__(self):
		self.comp_kmer = 5

def fasta_iter( fasta_name ):
	fh = open(fasta_name)
	# ditch the boolean (x[0]) and just keep the header or sequence since
	# we know they alternate.
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		# drop the ">"
		header = header.next()[1:].strip()
		# join all sequence lines to one.
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

for k,letter in enumerate(string.ascii_uppercase):
	# if k>0:
	# 	break

	response = urllib2.urlopen("http://tools.neb.com/genomes/index.php?page=%s" % letter)
	html     = response.read()
	soup     = BeautifulSoup(html, "html.parser")

	trs = soup.find_all("tr")
	for j,tr in enumerate(trs):
		if tr.td != None:
			if re.search("lasmid", tr.td.prettify()):
				# This tr element describes a plasmid
				m = re.search(r'http://www.ncbi.nlm.nih.gov/bioproject/\d+', tr.td.prettify())
				if m != None:
					ncbi = m.group()
					bioproject_urls.add(ncbi)
					a_elements = tr.td.children
					for i,a_element in enumerate(a_elements):
						if a_element.name != None:
							i_elements = list(a_element.descendants)
							if len(i_elements)!=0:
								spec        = list(i_elements)[1]
								plas        = list(i_elements)[2][1:]
								plasmid_str = "%s %s" % (spec, plas)
								
								bioproject_urls.add(ncbi)
								specs_key[ncbi] = spec

print len(bioproject_urls), "Organisms with plasmids identified"

spec_accession_urls = set()
spec_accessions_key = {}
for i,url in enumerate(list(bioproject_urls)):
	response = urllib2.urlopen(url)
	html     = response.read()
	soup     = BeautifulSoup(html, "html.parser")
	all_as = soup.find_all("a")
	for a in all_as:
		if a != None:
			if a.attrs.get("href") and a.attrs.get("title"):
				if a.attrs["href"].find("nuccore")>-1 and \
				   a.attrs["title"].find("Nucleotide")<0 and \
				   a.attrs["title"].find("BioProject")<0 and \
				   a.attrs["title"].find("organelles")>-1 and \
				   re.search(r'nuccore/\d+', a.attrs["href"]):
					accession_url = ".gov".join([url.split(".gov")[0], a.attrs["href"]])
					if a.attrs["href"].find(",")>0:
						# We don't want it if there is only one sequence (i.e. no plasmid)
						print "%s / %s" % ((i+1), len(bioproject_urls))
						spec_accession_urls.add(accession_url)
						spec_accessions_key[accession_url] = specs_key[url]
						print accession_url

spec_acc_lists = defaultdict(list)
spec_acc_key   = {}
dist_list      = []
spec_map       = {}
f_out          = open("dists.out", "wb")
f_comps        = open("comp_vectors.out", "wb")
spec_n         = 0
errors_n       = 0
error_urls     = []
for s,url in enumerate(list(spec_accession_urls)):
	spec     = spec_accessions_key[url].replace(" ","_")
	print url, spec
	f        = open("GI_spec_type_length.map", "wb")
	accs     = []
	try:
		response = urllib2.urlopen(url)
	except urllib2.HTTPError as err:
		if err.code==404:
			print "**** HTTPError 404! ****"
			errors_n += 1
			error_urls.append(err.geturl())
		else:
			raise
	html     = response.read()
	soup     = BeautifulSoup(html, "html.parser")	

	all_divs = soup.find_all("div")
	for div in all_divs:
		if div != None:
			if div.attrs.get("class") == [u'rprt']:
				rprt_ps = div.find_all("p")
				for p in rprt_ps:
					if p != None:
						if p.attrs.get("class") == [u"desc"]:
							length = p.text.split("bp")[0].replace(",","")[:-1]
						elif p.attrs.get("class") == [u"title"]:
							if list(p.children)[0].text.find("plasmid")>-1:
								tp = "plasmid"
							else:
								tp = "chr"
				rprt_dls = div.find_all("dl")
				for dl in rprt_dls:
					if dl != None:
						if dl.attrs.get("class") == [u'rprtid']:
							children_l = list(dl.children)
							for j,child in enumerate(children_l):
								if child.name!=None:
									if child.name=="dt" and child.text=="Accession:":
										# print spec_accessions_key[url], children_l[j+2].text
										acc = children_l[j+2].text
										accs.append(acc)
										spec_map[acc] = spec
										f.write("%s\t%s\t%s\t%s\n" % (acc, spec, tp, length))
	f.close()

	# Download all the fastas from this NCBI page
	db           = "nuccore"
	Entrez.email = "john.beaulaurier@mssm.edu"
	batchSize    = 100
	retmax       = 10**9

	# all_accs = list(np.loadtxt("accessions.txt", dtype="str"))
	all_accs = accs
	tmp_fasta_name = "tmp.fasta"
	try:
		for k,start in enumerate(range( 0,len(all_accs),batchSize )):
			#first get GI for query accesions
			batch_accs = all_accs[start:(start+batchSize)]
			sys.stderr.write( "Fetching %s entries from GenBank: %s\n" % (len(batch_accs), ", ".join(batch_accs[:10])))
			query  = " ".join(batch_accs)
			handle = Entrez.esearch( db=db,term=query,retmax=retmax )
			giList = Entrez.read(handle)['IdList']
			sys.stderr.write( "Found %s GI: %s\n" % (len(giList), ", ".join(giList[:10])))
			#post NCBI query
			search_handle     = Entrez.epost(db=db, id=",".join(giList))
			search_results    = Entrez.read(search_handle)
			webenv,query_key  = search_results["WebEnv"], search_results["QueryKey"]
			#fetch entries in batch
			# handle = Entrez.efetch(db=db, rettype="fasta", retstart=start, retmax=batchSize, webenv=webenv, query_key=query_key)
			handle = Entrez.efetch(db=db, rettype="fasta", webenv=webenv, query_key=query_key)
			# sys.stdout.write(handle.read())
			f = open(tmp_fasta_name, "wb")
			f.write(handle.read())
			f.close()
	except urllib2.HTTPError as err:
		if err.code==404:
			print "**** HTTPError 404! ****"
			error_urls.append(err.geturl())
			errors_n += 1
		else:
			raise

	contigs_comp_vectors = {}
	opts_obj      = Opts()
	len_map       = {}
	tp_map        = {}
	full_name_map = {}
	org_list      = []
	for i,(header,contig_seq) in enumerate(fasta_iter(tmp_fasta_name)):
		contig_comp  = []
		contig_kmers = read_scanner.kmer_freq( "cmp", contig_seq, 0, opts_obj )
		length       = len(contig_seq)
		for kmer,count in contig_kmers.iteritems():
			kmer_normed_comp = math.log(float(count) / sum(contig_kmers.values()))
			contig_comp.append(kmer_normed_comp)
		comp_vector = np.array(contig_comp)
		print header
		acc       = header.split(" ")[0].split(".")[0]
		full_name = " ".join(header.split(" ")[1:])
		if header.find("lasmid")>-1:
			# It's a plasmid sequence
			tp     = "pla"
		else:
			# It's a chromosome sequence
			tp     = "chr" 
		tp_map[acc]               = tp
		len_map[acc]              = length
		full_name_map[acc]        = full_name
		contigs_comp_vectors[acc] = comp_vector
		org_list.append( (acc, tp) )
		f_comps.write("%s\t%s\t%s\n" % (s, acc, "\t".join(map(lambda x: str(round(x,4)), comp_vector))) )

	chr_accs = [x[0] for x in org_list if x[1]=="chr"]
	pls_accs = [x[0] for x in org_list if x[1]=="pla"]
	
	if len(chr_accs)>1:
		# Take mean chr composition vector for pls <--> chr distance
		# all_chrs = []
		# for chr_acc in chr_accs:
		# 	all_chrs.append( contigs_comp_vectors[chr_acc] )
		# all_chrs   = np.array(all_chrs)
		# chr_vector = np.mean(all_chrs, axis=0)
		# Only take the largest chromosome for pls <--> chr distance
		biggest_acc = max(len_map.iterkeys(), key=(lambda key: len_map[key]))
		chr_vector  = contigs_comp_vectors[biggest_acc]
	elif len(chr_accs)==1:
		chr_vector = contigs_comp_vectors[chr_accs[0]]
	elif len(chr_accs)==0:
		continue
	
	# Calc euclidian distance
	for pls_acc in pls_accs:
		pls_vector = contigs_comp_vectors[pls_acc]
		dist = np.linalg.norm(chr_vector - pls_vector)
		spec = full_name_map[pls_acc]
		print spec, tp_map[pls_acc], pls_acc, dist
		out_str = "%s\t%s\t%s\t%s\t%.4f\n" % (s, spec, tp_map[pls_acc], pls_acc, dist)
		f_out.write(out_str)
		dist_list.append( (spec, tp_map[pls_acc], pls_acc, dist) )

	spec_n += 1

	# Output all the fasta sequences for the plasmids
	if s==0:
		mv_CMD  = "mv %s %s" % (tmp_fasta_name, all_fastas_fn)
		sts,stdOutErr = run_OS_command( mv_CMD )
	else:
		cat_CMD = "cat %s %s > tmp.combo.fasta" % (all_fastas_fn, tmp_fasta_name)
		sts,stdOutErr = run_OS_command( cat_CMD )

		mv_CMD  = "mv tmp.combo.fasta %s" % all_fastas_fn
		sts,stdOutErr = run_OS_command( mv_CMD )

os.remove(tmp_fasta_name)

dists = map(lambda x: x[3], dist_list)

plt.figure()
plt.hist( dists, 50)
plt.grid()

plt.legend(loc=2)
plt.xlabel("Distance")
plt.ylabel("Count")
plt.title("Distribution of plasmid/chromosome composition distance")
plt.savefig("dist_hist.png")

f_out.close()
f_comps.close()

print "Finished. Encountered %s HTTPErrors" % errors_n