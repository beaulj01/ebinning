import os,sys
import numpy as np
from itertools import groupby
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import re
import urllib2
from bs4 import BeautifulSoup
from collections import defaultdict
import pub_figs
from Bio import motifs as BioMotifs
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
from Bio import SeqUtils

rebase_data_fn   = "pb_rebase_seqs_and_motifs.6mA_only.out"
plasmid_fasta_fn = "all_plasmids.fasta"

motifs_d  = {}
tp_d      = {}
org_d     = {}
spec_id_d = {}
for i,line in enumerate(open(rebase_data_fn, "r").xreadlines()):
	line       = line.strip()
	vals       = line.split("\t")
	spec_id    = int(vals[0])
	acc        =     vals[1]
	tp         =     vals[2]
	org        =     vals[3]
	spec       = " ".join(org.split(" ")[:2])
	spec_id_d[acc] = spec_id
	tp_d[acc]      = tp
	org_d[acc]     = org
	if len(vals)==5:
		motifs        = vals[4].split(";")
		motifs_d[acc] = motifs
		# print "%s\t%s\t%s\t%s" % (acc, tp, spec, motifs)
	else:
		# No motifs
		motifs_d[acc] = []

f = open(rebase_data_fn+".pls_wCounts", "wb")
for seq_record in SeqIO.parse(plasmid_fasta_fn, "fasta"):
	seq_acc = seq_record.id.split(" ")[0].split(".")[0]
	
	if tp_d[seq_acc]=="plasmid":

		length  = len(str(seq_record.seq))
		motif_counts = []
		
		seq    = Seq(str(seq_record.seq), IUPAC.unambiguous_dna)
		rc_seq = seq.reverse_complement()

		# For each motif, find instances in this sequence
		for i,motif in enumerate(motifs_d[seq_acc]):

			# Search in forward strand
			f_hits = SeqUtils.nt_search(str(seq_record.seq), motif)
			query  = f_hits[0]
			f_pos  = f_hits[1:]

			# Search in reverse complement strand
			rc_hits = SeqUtils.nt_search(str(rc_seq), motif)
			rc_pos  = rc_hits[1:]

			all_pos = f_pos + rc_pos
			motif_counts.append(len(all_pos))
			# print seq_acc, len(str(seq_record.seq)), motif, len(all_pos)

		m_str = ";".join(motifs_d[seq_acc])
		n_str = ";".join(map(str,motif_counts))

		print "%s\t%s\t%s\t%s\t%s\t%s\t%s" % (spec_id_d[seq_acc], seq_acc, tp_d[seq_acc], length, org_d[seq_acc], m_str, n_str)
		f.write("%s\t%s\t%s\t%s\t%s\t%s\t%s\n" % (spec_id_d[seq_acc], seq_acc, tp_d[seq_acc], length, org_d[seq_acc], m_str, n_str))
f.close()
