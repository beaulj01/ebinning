import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import math
import glob
import subprocess

species = ["bcaccae", \
		   "bovatus", \
		   "btheta", \
		   "bvulgatus", \
		   "caero", \
		   "cbolteae", \
		   "ecoli", \
		   "rgnavus"]

FIG_SIZE = [15,15]
fig = plt.figure(figsize=FIG_SIZE)
dims = int(math.ceil(math.sqrt(len(species))))

for i,spec in enumerate(species):
	spec_fns = glob.glob("%s.*.txt" % spec)
	out_fn   = "%s.all.tmp" % spec
	cat_CMD   = "cat %s | cut -d\" \" -f3,4 > %s" % (" ".join(spec_fns), out_fn)
	p         = subprocess.Popen(cat_CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts != 0:
		raise Exception("Failed cat command: %s" % cat_CMD)

	array  = np.loadtxt(out_fn, dtype="int")
	rls    = array[:,0]
	subrls = array[:,1]

	ax = plt.subplot(dims,dims,i+1)
	ax.hist( [rls, subrls], bins=100, linewidth=0, label=["Reads", "Subreads"])
	ax.set_xlim([0, 30000])
	ax.set_ylim([0, 20000])
	ax.set_title("%s RLs" % spec)

	ax.legend(loc=1, prop={'size':12})
	os.remove(out_fn)

fig.savefig("readlength_hists.png")
