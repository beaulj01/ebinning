import os,sys
import numpy as np
from itertools import groupby

unmapped_fasta = "unmappedSubreads.combo.fasta"
names_105      = np.loadtxt("reads.raw.105.names",   dtype="str")
names_439      = np.loadtxt("reads.raw.439.names",   dtype="str")
labels_105     = np.loadtxt("reads.raw.105.labels",  dtype="str")
labels_439     = np.loadtxt("reads.raw.439.labels",  dtype="str")
lengths_105    = np.loadtxt("reads.raw.105.lengths", dtype="int")
lengths_439    = np.loadtxt("reads.raw.439.lengths", dtype="int")
comp_105       = np.loadtxt("reads.raw.105.comp",    dtype="float")
comp_439       = np.loadtxt("reads.raw.439.comp",    dtype="float")

def fasta_iter( fasta_name ):
	fh = open(fasta_name)
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		header = header.next()[1:].strip()
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

unmapped_reads = set()
for name,seq in fasta_iter(unmapped_fasta):
	read = "/".join(name.split("/")[:-1])
	unmapped_reads.add(read)

idx_105  = []
labs_105 = []
for i,name in enumerate(names_105):
	if name in unmapped_reads:
		idx_105.append(i)
		labs_105.append("105")
labs_105 = np.array(labs_105)


idx_439 = []
labs_439 = []
for i,name in enumerate(names_439):
	if name in unmapped_reads:
		idx_439.append(i)
		labs_439.append("439")
labs_439 = np.array(labs_439)

idx_105 = np.array(idx_105)
idx_439 = np.array(idx_439)

np.savetxt("reads.raw.105.names.unmapped",   names_105[idx_105],   fmt="%s", delimiter="\t")
np.savetxt("reads.raw.439.names.unmapped",   names_439[idx_439],   fmt="%s", delimiter="\t")
np.savetxt("reads.raw.105.labels.unmapped",  labs_105,             fmt="%s", delimiter="\t")
np.savetxt("reads.raw.439.labels.unmapped",  labs_439,             fmt="%s", delimiter="\t")
np.savetxt("reads.raw.105.lengths.unmapped", lengths_105[idx_105], fmt="%i", delimiter="\t")
np.savetxt("reads.raw.439.lengths.unmapped", lengths_439[idx_439], fmt="%i", delimiter="\t")
np.savetxt("reads.raw.105.comp.unmapped",    comp_105[idx_105],    fmt="%.6f", delimiter="\t")
np.savetxt("reads.raw.439.comp.unmapped",    comp_439[idx_439],    fmt="%.6f", delimiter="\t")