import os,sys
import numpy as np
import math
import sklearn.metrics as metrics

labels = np.loadtxt(sys.argv[1], dtype="str")
coords = np.loadtxt(sys.argv[2], dtype="float", delimiter="\t")

dim   = len(labels)
dists = np.zeros([dim, dim])
for i in range(dim):
	for j in range(dim):
		X1 = coords[i][0]
		Y1 = coords[i][1]
		
		X2 = coords[j][0]
		Y2 = coords[j][1]
		
		d = math.sqrt( (X2-X1)**2 + (Y2-Y1)**2 )
		dists[i,j] = d

coefs = metrics.silhouette_samples(dists, labels)
for coef in coefs:
	print coef