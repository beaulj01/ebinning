import os,sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt

def scatterplot(results, labels, plot_fn, sizes, title):
	label_set = list(set(labels))
	label_set.sort()
	colors    = plt.get_cmap('rainbow')(np.linspace(0, 1.0, len(label_set)))
	shapes    = ["o", "v", "^", "s"]
	fig       = plt.figure(figsize=[15,12])
	ax        = fig.add_subplot(111)
	sizes[sizes<100000] = 100000
	scaled_sizes = sizes**1.5 / max(sizes**1.5) * 2000
	for k,target_lab in enumerate(label_set):
		idxs             = [j for j,label in enumerate(labels) if label==target_lab]
		X                = results[idxs,0]
		Y                = results[idxs,1]
		scaled_sizes_idx = np.array(scaled_sizes)[idxs]
		if target_lab=="unknown":
			ax.scatter(X, Y, edgecolors="r", label=target_lab, marker="x", facecolors="r", lw=3, alpha=0.3, s=scaled_sizes_idx)
		else:
			ax.scatter(X, Y, edgecolors=colors[k], label=target_lab, marker=shapes[k%4], facecolors="None", lw=3, alpha=0.7, s=scaled_sizes_idx)
	box = ax.get_position()
	ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
	ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':14})
	ax.set_title(title)
	plt.savefig(plot_fn)

output_fn  =     sys.argv[1]
labels_fn  =     sys.argv[2]
sizes_fn   =     sys.argv[3]
scatter_fn =     sys.argv[4]
title      =     sys.argv[5]
ncontigs   = int(sys.argv[6])

labels  = np.loadtxt(labels_fn, dtype="str")[:ncontigs]
sizes   = np.loadtxt(sizes_fn,  dtype="float")[:ncontigs]
results = np.loadtxt(output_fn, dtype="float")[:ncontigs]
scatterplot(results, labels, scatter_fn, sizes, title)