import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pub_figs
import numpy as np
import matplotlib.gridspec as gridspec
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

font_size      = 25
adjust         = 0.15
fontProperties = pub_figs.setup_math_fonts(font_size)
fig1           = pub_figs.init_fig(x=15,y=15)
# ax1            = fig1.add_subplot(2,1,1)
gs             = gridspec.GridSpec(3, 3)
gs.update(hspace=0.2)
ax1            = plt.subplot(gs[:-1, :])
# ax1            = plt.subplot2grid((3,1), (0, 0), colspan=1, rowspan=2)
# fig2           = pub_figs.init_fig(x=15,y=15)
# ax2            = fig1.add_subplot(2,1,2)
ax2            = plt.subplot(gs[-1, :])
# ax2            = plt.subplot2grid((3,1), (2, 0), colspan=1, rowspan=1)

baseline_fn = "/hpc/users/beaulj01/projects/ebinning/pacbio20/hmp_sets5_6/baseline_HGAP/data/polished_assembly.fasta.lengths"
log_fn      = "/hpc/users/beaulj01/projects/ebinning/pacbio20/hmp_sets5_6/log_HGAP/data/polished_assembly.fasta.lengths"
even_fn     = "/hpc/users/beaulj01/projects/ebinning/pacbio20/hmp_sets5_6/even_HGAP/data/polished_assembly.fasta.lengths"
k           = 50
for j,fn in enumerate([baseline_fn, log_fn, even_fn]):
	qual_fn = fn.replace("data/polished_assembly.fasta.lengths", "results/polished_coverage_vs_quality.csv")
	qual    = np.loadtxt(qual_fn, delimiter=",", dtype="str")
	bases   = 0
	Y1      = [bases]
	Y2      = []
	labs    = []
	ind     = np.arange(k+1)
	for i,line in enumerate(open(fn, "r").xreadlines()):
		if i==k: break
		# labs.append(i)
		name    = line.split()[1].split("|")[0]
		length  = line.split()[0]
		quality = qual[qual[:,0]==name][0][2].astype(np.float)
		bases  += int(length)
		Y1.append(float(bases)/1000000)
		Y2.append(quality)
	if j==0:
		bases1 = ax1.plot(ind, Y1, "bo-", linewidth=7, markersize=10)
		quals1 = ax2.scatter(ind[1:], Y2, color="b", marker="o", s=30)
	elif j==1:
		bases2 = ax1.plot(ind, Y1, "ro-", linewidth=7, markersize=10)
		quals2 = ax2.scatter(ind[1:], Y2, color="r", marker="o", s=30)
	elif j==2:
		bases3 = ax1.plot(ind, Y1, "ko-", linewidth=7, markersize=10)
		quals3 = ax2.scatter(ind[1:], Y2, color="k", marker="o", s=30)

majorLocator   = MultipleLocator(5)
majorFormatter = FormatStrFormatter('%d')
minorLocator   = MultipleLocator(1)

# ax1.set_xlabel("Contigs (%s largest)" % k)
ax1.set_ylabel("Total assembly (Mbp)")
ax1.set_xticks(ind)
ax1.set_xticklabels( labs, rotation=90)
ax1.xaxis.labelpad = 30
ax1.yaxis.labelpad = 30
ax1.set_xlim([0,k+1])
ax1.xaxis.set_major_locator(majorLocator)
ax1.xaxis.set_major_formatter(majorFormatter)
ax1.xaxis.set_minor_locator(minorLocator)
# plt.tick_params(which='major', length=15)
# plt.tick_params(which='minor', length=10)
legend = ax1.legend( (bases1[0], bases2[0], bases3[0]), ('Baseline', 'Log', 'Even (19x)'), loc=2 )
legend.draw_frame(False)
pub_figs.change_font_size(ax1, font_size=font_size, bottom_adjust=adjust, left_adjust=adjust)
pub_figs.remove_top_right_axes(ax1)
# fn = "top_k_contig_bases_assembled"
# fig1.savefig(fn+".png")

ax2.set_xlabel("Contigs (%s largest)" % k)
ax2.set_ylabel("Mean QV")
ax2.set_xticks(ind)
ax2.set_xticklabels( labs, rotation=90)
ax2.xaxis.labelpad = 30
ax2.yaxis.labelpad = 30
ax2.set_xlim([0,k+1])
ax2.set_ylim([0,60])
ax2.xaxis.set_major_locator(majorLocator)
ax2.xaxis.set_major_formatter(majorFormatter)
ax2.xaxis.set_minor_locator(minorLocator)
# plt.tick_params(which='major', length=15)
# plt.tick_params(which='minor', length=10)
# legend = ax2.legend( (quals1, quals2, quals3), ('Baseline', 'Log', 'Even (19x)'), loc=4)
# legend.draw_frame(False)
pub_figs.change_font_size(ax2, font_size=font_size, bottom_adjust=adjust, left_adjust=adjust)
pub_figs.remove_top_right_axes(ax2)
fn = "top_k_contig_mean_qv"
fig1.savefig(fn+".png")

