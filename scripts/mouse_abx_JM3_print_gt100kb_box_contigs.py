import os,sys
from Bio import SeqIO
import glob

nums  = range(1,10)
boxes = ["box%s" % n for n in nums]

for box in boxes:
	new    = []
	new_fn = "%s_gt100kb.fasta" % box
	fasta  = os.path.join(box, "data", "polished_assembly.fasta")
	for record in SeqIO.parse(fasta, "fasta"):
		if len(record.seq)>100000:
			new.append(record)
			SeqIO.write(new, new_fn, "fasta")

for box in boxes:
	new    = []
	new_fn = "%s_lt100kb.fasta" % box
	fasta  = os.path.join(box, "data", "polished_assembly.fasta")
	for record in SeqIO.parse(fasta, "fasta"):
		if len(record.seq)<100000:
			new.append(record)
			SeqIO.write(new, new_fn, "fasta")