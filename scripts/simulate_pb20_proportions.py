import numpy as np
import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from itertools import groupby

read_species_map_fn = "/hpc/users/beaulj01/projects/ebinning/pacbio20/all7_sets/aligns_to_refs/read_species_mapping.txt"
read_species_map    = np.loadtxt(read_species_map_fn, dtype="str")

# read_species_map contains 3135626 reads
# aligned_reads.cmp.h5 for all7 sets contains 2346560 alignments

def fasta_iter( fasta_name ):
	"""
	Given a fasta file, yield tuples of (header, sequence).
	"""
	fh = open(fasta_name)
	# ditch the boolean (x[0]) and just keep the header or sequence since
	# we know they alternate.
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		# drop the ">"
		header = header.next()[1:].strip()
		# join all sequence lines to one.
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

def run_OS_command( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	return sts, stdOutErr

species = {"A_baumannii_ATCC_17978": "/hpc/users/beaulj01/projects/data_repo/refs/A_baumannii_ATCC_17978.fasta",                             \
		   "A_odontolyticus_ATCC_17982_Scfld021": "/hpc/users/beaulj01/projects/data_repo/refs/A_odontolyticus_ATCC_17982_Scfld021.fasta",   \
		   "B_cereus_ATCC_10987": "/hpc/users/beaulj01/projects/data_repo/refs/B_cereus_ATCC_10987.fasta",                                   \
		   "B_vulgatus_ATCC_8482": "/hpc/users/beaulj01/projects/data_repo/refs/B_vulgatus_ATCC_8482.fasta",                                 \
		   "C_beijerinckii_NCIMB_8052": "/hpc/users/beaulj01/projects/data_repo/refs/C_beijerinckii_NCIMB_8052.fasta",                       \
		   "D_radiodurans_R1": "/hpc/users/beaulj01/projects/data_repo/refs/D_radiodurans_R1.fasta",                                         \
		   "E_coli_str_K_12_substr_MG1655": "/hpc/users/beaulj01/projects/data_repo/refs/E_coli_str_K-12_substr_MG1655.fasta",               \
		   "E_faecalis_OG1RF": "/hpc/users/beaulj01/projects/data_repo/refs/E_faecalis_OG1RF.fasta",                                         \
		   "H_pylori_26695": "/hpc/users/beaulj01/projects/data_repo/refs/H_pylori_26695.fasta",                                             \
		   "L_gasseri_ATCC_33323": "/hpc/users/beaulj01/projects/data_repo/refs/L_gasseri_ATCC_33323.fasta",                                 \
		   "L_monocytogenes_EGD_e": "/hpc/users/beaulj01/projects/data_repo/refs/L_monocytogenes_EGD-e.fasta",                               \
		   "N_meningitidis_MC58": "/hpc/users/beaulj01/projects/data_repo/refs/N_meningitidis_MC58.fasta",                                   \
		   "P_acnes_KPA171202": "/hpc/users/beaulj01/projects/data_repo/refs/P_acnes_KPA171202.fasta",                                       \
		   "P_aeruginosa_PAO1": "/hpc/users/beaulj01/projects/data_repo/refs/P_aeruginosa_PAO1.fasta",                                       \
		   "R_sphaeroides_2_4_1": "/hpc/users/beaulj01/projects/data_repo/refs/R_sphaeroides_2.4.1.fasta",                                   \
		   "S_agalactiae_2603V_R": "/hpc/users/beaulj01/projects/data_repo/refs/S_agalactiae_2603V_R.fasta",                                 \
		   "S_aureus_subsp_aureus_USA300_TCH1516": "/hpc/users/beaulj01/projects/data_repo/refs/S_aureus_subsp_aureus_USA300_TCH1516.fasta", \
		   "S_epidermidis_ATCC_12228": "/hpc/users/beaulj01/projects/data_repo/refs/S_epidermidis_ATCC_12228.fasta",                         \
		   "S_mutans_UA159": "/hpc/users/beaulj01/projects/data_repo/refs/S_mutans_UA159.fasta",                                             \
		   "S_pneumoniae_TIGR4": "/hpc/users/beaulj01/projects/data_repo/refs/S_pneumoniae_TIGR4.fasta"}

# Average read length of alignments in all_sets aligned_reads.cmp.h5: 4297 bases
for spec in species.keys():
	n_reads = len(read_species_map[read_species_map[:,1]==spec])
	fasta      = species[spec]
	ref_size   = 0
	read_bases = 4297 * n_reads
	for header,seq in fasta_iter( fasta ):
		ref_size += len(seq)
	print spec, ref_size, round( (float(read_bases)/ref_size), 1)

