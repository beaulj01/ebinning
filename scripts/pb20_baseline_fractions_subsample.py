import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os,sys
import math
from collections import Counter

baseline_readlist_fn  = sys.argv[1]
spec_tot_bases_fn     = sys.argv[2]

rank             = np.array(range(1,21))
genome_sizes     = {"A_baumannii_ATCC_17978" :               3976747, \
		   			"A_odontolyticus_ATCC_17982_Scfld021" :  2391230, \
		   			"B_cereus_ATCC_10987" :                  5224283, \
		   			"B_vulgatus_ATCC_8482" :                 5163189, \
		   			"C_beijerinckii_NCIMB_8052" :            6000632, \
		   			"D_radiodurans_R1" :                     3060986, \
		   			"E_coli_str_K_12_substr_MG1655" :        4641652, \
		   			"E_faecalis_OG1RF" :                     2739625, \
		   			"H_pylori_26695" :                       1667867, \
		   			"L_gasseri_ATCC_33323" :                 1894360, \
		   			"L_monocytogenes_EGD_e" :                2944528, \
		   			"N_meningitidis_MC58" :                  2272360, \
		   			"P_acnes_KPA171202" :                    2560265, \
		   			"P_aeruginosa_PAO1" :                    6264404, \
		   			"R_sphaeroides_2_4_1" :                  4131542, \
		   			"S_agalactiae_2603V_R" :                 2160267, \
		   			"S_aureus_subsp_aureus_USA300_TCH1516" : 2872915, \
		   			"S_epidermidis_ATCC_12228" :             2499279, \
		   			"S_mutans_UA159" :                       2032925, \
		   			"S_pneumoniae_TIGR4" :                   2160842}

tot_bases = {}
cov       = {}
for line in open(spec_tot_bases_fn, "r").xreadlines():
	line         =     line.strip()
	s            =     line.split("\t")[0]
	s_bases      = int(line.split("\t")[1])
	tot_bases[s] = s_bases
	cov[s]       = float(s_bases) / genome_sizes[s]

species_baseline      = []
target_bases_div2     = {}
target_bases_div5     = {}
target_bases_div10    = {}
target_bases_div20    = {}

for s,size in genome_sizes.iteritems():
	abund = cov[s] / np.sum(cov.values())
	species_baseline.append( (s, size, tot_bases[s], round(cov[s],3), round(abund,3)) )
	target_bases_div2[s]  = tot_bases[s] / 2
	target_bases_div5[s]  = tot_bases[s] / 5
	target_bases_div10[s] = tot_bases[s] / 10
	target_bases_div20[s] = tot_bases[s] / 20

species_baseline.sort(key=lambda x: x[3], reverse=True)
wl_name2  = "baseline_div2_read_info.txt"
wl_name5  = "baseline_div5_read_info.txt"
wl_name10 = "baseline_div10_read_info.txt"
wl_name20 = "baseline_div20_read_info.txt"
f2        = open(wl_name2,  "w")
f5        = open(wl_name5,  "w")
f10       = open(wl_name10, "w")
f20       = open(wl_name20, "w")
tot_bases_div2  = Counter()
tot_bases_div5  = Counter()
tot_bases_div10 = Counter()
tot_bases_div20 = Counter()
total_basesline_bases = np.sum(tot_bases.values())
for i,line in enumerate(open(baseline_readlist_fn, "r").xreadlines()):
	s     =     line.split("\t")[1]
	bases = int(line.split("\t")[2])
	if tot_bases_div2[s] < target_bases_div2[s]:
		tot_bases_div2[s] += bases
		f2.write(line)
	if tot_bases_div5[s] < target_bases_div5[s]:
		tot_bases_div5[s] += bases
		f5.write(line)
	if tot_bases_div10[s] < target_bases_div10[s]:
		tot_bases_div10[s] += bases
		f10.write(line)
	if tot_bases_div20[s] < target_bases_div20[s]:
		tot_bases_div20[s] += bases
		f20.write(line)
f2.close()
f5.close()
f10.close()
f20.close()

# Calculate the relative abundances
baseline_cov       = {}
baseline_div2_cov  = {}
baseline_div5_cov  = {}
baseline_div10_cov = {}
baseline_div20_cov = {}
for s in genome_sizes.keys():
	baseline_cov[s]       = float(tot_bases[s])       / genome_sizes[s]
	baseline_div2_cov[s]  = float(tot_bases_div2[s])  / genome_sizes[s]
	baseline_div5_cov[s]  = float(tot_bases_div5[s])  / genome_sizes[s]
	baseline_div10_cov[s] = float(tot_bases_div10[s]) / genome_sizes[s]
	baseline_div20_cov[s] = float(tot_bases_div20[s]) / genome_sizes[s]
baseline_abund       = []
baseline_div2_abund  = {}
baseline_div5_abund  = {}
baseline_div10_abund = {}
baseline_div20_abund = {}
for s in genome_sizes.keys():
	baseline_abund.append( (s, baseline_cov[s]        / np.sum(baseline_cov.values())) )
	baseline_div2_abund[s]  =   baseline_div2_cov[s]  / np.sum(baseline_div2_cov.values())
	baseline_div5_abund[s]  =   baseline_div5_cov[s]  / np.sum(baseline_div5_cov.values())
	baseline_div10_abund[s] =   baseline_div10_cov[s] / np.sum(baseline_div10_cov.values())
	baseline_div20_abund[s] =   baseline_div20_cov[s] / np.sum(baseline_div20_cov.values())
baseline_abund.sort(key=lambda x: x[1], reverse=True)

# Sort based on the baseline abundance ranking
base  = []
div2  = []
div5  = []
div10 = []
div20 = []
for s in map(lambda x: x[0], baseline_abund):
	# base.append(baseline_cov[s])
	div2.append(baseline_div2_abund[s])
	div5.append(baseline_div5_abund[s])
	div10.append(baseline_div10_abund[s])
	div20.append(baseline_div20_abund[s])

fig = plt.figure(figsize=[6,9])
ax  = fig.add_subplot(111)
ax.plot(rank, map(lambda x: x[1], baseline_abund),       "k-o", label="Baseline")
# ax.plot(rank, base,  "k-o", label="Baseline")
ax.plot(rank, div2,  "r-o", label="Baseline/2")
ax.plot(rank, div5,  "b-o", label="Baseline/5")
ax.plot(rank, div10, "m-o", label="Baseline/10")
ax.plot(rank, div20, "g-o", label="Baseline/20")
bottom_adjust=0.50
left_adjust=0.15
hspace=0.5
wspace=0.2 
plt.gcf().subplots_adjust(bottom=bottom_adjust, left=left_adjust, hspace=hspace, wspace=wspace)
ax.set_ylabel("Relative abundance")
# ax.set_ylabel("Coverage")
ax.set_xticks(range(1,21))
xlabs = []
for abund in baseline_abund:
	s = abund[0]
	xlabs.append(s)
ax.set_xticklabels(xlabs, rotation=90 )
ax.legend(loc='upper right', prop={'size':14})
plt.savefig("rank_abundances_baseline_and_lower_cov.png")
