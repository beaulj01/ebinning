import os,sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import random

label_dict = {"HP26695": r"${H. pylori}$ 26695", \
			  "HPJ99": r"${H. pylori}$ J99", \
			  "EcoliC227": r"${E. coli}$ C227", \
			  "Steno": r"${S. maltophilia}$"}

def scatterplot(results, labels, plot_fn, title, read_names):
	label_set = list(set(labels))
	label_set.sort()
	colors    = plt.get_cmap('rainbow')(np.linspace(0, 1.0, len(label_set)))
	shapes    = ["o", "v", "^", "s", "D", "p", "d"]
	fig       = plt.figure(figsize=[15,12])
	ax        = fig.add_axes([0.1, 0.1, 0.8, 0.8])
	ax.axis("off")
	res = []
	to_plot      = []
	for k,target_lab in enumerate(label_set):
		idxs             = [j for j,label in enumerate(labels) if label==target_lab]
		X                = results[idxs,0]
		Y                = results[idxs,1]
		scaled_sizes_idx = (np.zeros(len(labels))+1)*10
		for i,(x,y) in enumerate(results[idxs,:]):
			to_plot.append( (x,y,target_lab, colors[k], shapes[k%len(shapes)], scaled_sizes_idx[i]) )
	np.random.shuffle(to_plot)
	plotted_labs = set()
	legend_plots = []
	legend_labs  = []
	for i,(x,y,lab,color,shape,scaled_size) in enumerate(to_plot):
		# if i==5000: break
		if lab=="unknown":
			# pass
			plot = ax.scatter(x, y, edgecolors="r", label=lab, marker="x", facecolors="r", lw=3, alpha=0.7, s=scaled_size)
		else:
			plot = ax.scatter(x, y, edgecolors=color, label=label_dict[lab], marker=shape, facecolors="None", lw=3, alpha=0.7, s=scaled_size)
		if lab not in plotted_labs:
			plotted_labs.add(lab)
			legend_plots.append(plot)
			legend_labs.append(label_dict[lab])
	box = ax.get_position()
	ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
	leg_tup = zip(legend_labs,legend_plots)
	leg_tup.sort(key=lambda x: x[0])
	legend_labs, legend_plots = zip(*leg_tup)
	ax.legend(legend_plots, legend_labs, bbox_to_anchor=(1.05, 1.05), prop={'size':24}, scatterpoints=1, frameon=False)
	# ax.legend(legend_plots, legend_labs, loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':14})
	# ax.set_title(title)
	
	x1 = np.array(range(-60,60))  
	y1 = eval("2.5*x1+0")
	# plt.plot(x1, y1, linestyle="--", color="k")
	x2 = np.array(range(-60,60))  
	y2 = eval("x2*(-0.2)+5")
	# plt.plot(x2, y2, linestyle="--", color="k")
	plt.plot([51,1,15,51,51], [-6,4,36,36,-6], linestyle="--", color="k", linewidth=3)
	xmin = min(results[:,0])
	xmax = max(results[:,0])
	ymin = min(results[:,1])
	ymax = max(results[:,1])
	ax.set_xlim([xmin-1, xmax+5])
	ax.set_ylim([ymin-1, ymax+5])
	for i in range(results.shape[0]):
		x = results[i,0]
		y = results[i,1]
		if y<(2.5*x+0) and y>(x*-0.2 + 5):
			print read_names[i]

	plt.savefig(plot_fn)

SMp_coords  = np.loadtxt(sys.argv[1], dtype="float")
read_labels = np.loadtxt(sys.argv[2], dtype="str")
read_names  = np.loadtxt(sys.argv[3], dtype="str")
plot_fn     = "reads.raw.SMp.26695_gte8kb.png"
title       = ""

scatterplot(SMp_coords, read_labels, plot_fn, title, read_names)