import os,sys
import numpy as np
from pbcore.io.BasH5IO import BasH5Reader

names        = "/hpc/users/beaulj01/projects/ebinning/pacbio20/all7_sets/HGAP_naive/whitelist_log_abund/bas_binning/reads.rspaeroides.names"
input_fofn   = "bas.h5.fofn"
sam_fn       = "aligned_reads.sam"
rsphaer_all  = set(np.loadtxt(names, dtype="str"))

# # First gather all the relevant reads and subread lengths
# sublens = {}
# for k,line in enumerate(open(input_fofn, "r").xreadlines()):
# 	# print k,"..."
# 	# if k==1: break
# 	line     = line.strip()
# 	movie    = line.split()[0]
# 	reader   = BasH5Reader(movie)
# 	to_check = [z for z in reader if z.zmwName in rsphaer_all]
# 	for z in to_check:
# 		mean_sublength = np.mean([len(sub.basecalls()) for sub in z.subreads])
# 		sublens[z.zmwName] = mean_sublength

# nams = np.loadtxt(names, dtype="str")
# for i,n in enumerate(nams):
# 	sublen = sublens[n]
# 	print sublen

read_start = {}
read_chrom = {}
for k,line in enumerate(open(sam_fn, "r").xreadlines()):
	if line[0] != "@":
		line     =     line.strip()
		readname =     line.split("\t")[0]
		chrom    =     line.split("\t")[2]
		start    = int(line.split("\t")[3])
		if readname in rsphaer_all:
			read_chrom[readname] = chrom
			read_start[readname] = start

nams = np.loadtxt(names, dtype="str")
f1   = open("reads.rspaeroides.chrom_lab", "w")
f2   = open("reads.rspaeroides.startpos",  "w")
for i,n in enumerate(nams):
	f1.write("%s\n" % read_chrom[n])
	f2.write("%s\n" % read_start[n])
f1.close()
f2.close()