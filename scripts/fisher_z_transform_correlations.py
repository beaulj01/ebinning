import os,sys
import math
import numpy as np
import matplotlib as mpl; mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

corr_matrix_case = np.loadtxt(sys.argv[1], dtype="float")
readnames_case   = np.loadtxt(sys.argv[2], dtype="str")
corr_matrix_cont = np.loadtxt(sys.argv[3], dtype="float")
readnames_cont   = np.loadtxt(sys.argv[4], dtype="str")
motif_names      = np.loadtxt(sys.argv[5], dtype="str")

min_z = 0.08

Z_cont = np.zeros(corr_matrix_cont.shape)
for i in range(corr_matrix_cont.shape[0]):
	for j in range(corr_matrix_cont.shape[1]):
		r = corr_matrix_cont[i,j]
		if i==j: r = 0
		Z1 = 0.5*math.log( (1+r)/(1-r) )
		Z_cont[i,j] = Z1

Z_case = np.zeros(corr_matrix_case.shape)
for i in range(corr_matrix_case.shape[0]):
	for j in range(corr_matrix_case.shape[1]):
		r = corr_matrix_case[i,j]
		if i==j: r = 0
		Z2 = 0.5*math.log( (1+r)/(1-r) )
		Z_case[i,j] = Z2

z_mat = np.zeros(corr_matrix_case.shape)
n1    = readnames_case.shape[0]
n2    = readnames_cont.shape[0]
s     = math.sqrt( (1/(n1-3)) + (1/(n2-3)) )
for i in range(Z_case.shape[0]):
	for j in range(Z_case.shape[1]):
		z          = (Z_case[i,j] - Z_cont[i,j])# / s
		z_mat[i,j] = z

fn = "binning.corr_diff"
np.savetxt(fn, z_mat, "%.4f", delimiter="\t")

fn1    = "7mock.motifs.corr_diff.txt"
motifs = ["AATCC-1",
		  "AGATCC-2", \
		  "AGATCT-2", \
		  "GGATCC-2", \
		  "GGATCT-2", \
		  "GATGG-1",  \
		  "CCATC-2",  \
		  "GATC-1",   \
		  "AGATC-2",  \
		  "TGATC-2",  \
		  "CGAGC-2",  \
		  "GGAGC-2",  \
		  "CGATC-2",  \
		  "GGATC-2",  \
		  "CAGGAG-4"]
idxs = []
for motif in motifs:
	idxs.append(list(motif_names).index(motif))
z7_motifs = np.zeros([len(motifs), len(motifs)])

motifs_keep = set()
for i in range(z_mat.shape[0]):
	for j in range(z_mat.shape[1]):
		if np.abs(z_mat[i,j]) >= min_z:
			motifs_keep.add( motif_names[i] )
			motifs_keep.add( motif_names[j] )
		if i in idxs and j in idxs:
			i_idx = motifs.index(motif_names[i])
			j_idx = motifs.index(motif_names[j])
			z7_motifs[i_idx,j_idx] = z_mat[i,j]

np.savetxt(fn1, z7_motifs, "%.4f", delimiter="\t", header="\t".join(motifs))

column_labels = motifs
row_labels    = motifs
fig           = plt.figure()
ax            = fig.add_subplot(111)
ax.heatmap    = ax.pcolor(z7_motifs)
cbar          = plt.colorbar(ax.heatmap)
plt.xticks(np.arange(len(motifs))+0.5, motifs, rotation='vertical')
plt.yticks(np.arange(len(motifs))+0.5, motifs)
ax.invert_yaxis()
plt.gcf().subplots_adjust(bottom=0.2, right=0.75)
plt.savefig("7member_motif_corrs_heatmap.png")

for entry in sorted(list(motifs_keep)):
	print entry