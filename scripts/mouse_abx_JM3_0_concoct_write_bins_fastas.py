import os,sys
from Bio import SeqIO
from collections import defaultdict

bin_map_fn = sys.argv[1]
assembly   = sys.argv[2]

bin_map = {}
for line in open(bin_map_fn, "rb").xreadlines():
	line   = line.strip()
	contig = line.split(",")[0]
	bin_id = line.split(",")[1]
	bin_map[contig] = bin_id

bin_records = defaultdict(list)
for Seq in SeqIO.parse(assembly, "fasta"):
	
	if bin_map.get(Seq.id):
		bin_id = bin_map[Seq.id]
		bin_records[bin_id].append(Seq)
	else:
		print Seq.id, len(Seq.seq), "clustering not found!"

for bin_id,records in bin_records.iteritems():
	SeqIO.write(records, "bins/bin%s.fasta" % bin_id, "fasta")