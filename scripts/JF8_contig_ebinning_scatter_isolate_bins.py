import sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import random
import pub_figs
from Bio import SeqIO

label_dict      =  {"B_caccae":     r"\textit{B. caccae}", \
				    "B_ovatus":     r"\textit{B. ovatus}", \
				    "B_vulgatus":   r"\textit{B. vulgatus}", \
				    "C_aerofaciens":r"\textit{C. aerofaciens}", \
				    "R_gnavus":     r"\textit{R. gnavus}", \
				    "B_theta":      r"\textit{B. thetaiotaomicron}", \
				    "E_coli":       r"\textit{E. coli}", \
				    "C_bolteae":    r"\textit{C. bolteae}"}

pub_figs.setup_math_fonts(font_size=20, font="Computer Modern Sans serif")

def scatterplot(results, labels, sizes, plot_fn, title, boxes):
	label_set = list(set(labels))
	label_set.sort()
	colors    = plt.get_cmap('spectral')(np.linspace(0, 1.0, len(label_set)+1))
	# colors    = plt.get_cmap('spectral')(range(len(label_set)))
	shapes    = ["o", "v", "^", "s", "D", "p", "d"]
	fig       = plt.figure(figsize=[10,7])
	ax        = fig.add_axes([0.1, 0.1, 0.8, 0.8])
	leg_font  = 16
	# ax.axis("off")
	pub_figs.remove_top_right_axes( ax )
	if len(sizes)>0:
		sizes[sizes<100000] = 100000
		scaled_sizes = sizes**1.5 / max(sizes**1.5) * 2000
	res       = []
	for k,target_lab in enumerate(label_set):
		idxs             = [j for j,label in enumerate(labels) if label==target_lab]
		scaled_sizes_idx = np.array(scaled_sizes)[idxs]
		X                = results[idxs,0]
		Y                = results[idxs,1]
		color            = colors[k]
		for i,x in enumerate(X):
			res.append( (x, Y[i], target_lab, color, scaled_sizes_idx[i], shapes[k%len(shapes)]) ) 
	
	np.random.shuffle(res)
	plotted_labs = set()
	legend_plots = []
	legend_labs  = []
	for (x,y, target_lab, color, size, shape) in res:
		plot = ax.scatter(x,y, edgecolors=color, \
							   facecolors="None", \
							   marker=shape, \
							   label=label_dict[target_lab], \
							   alpha=0.8, \
							   s=size, \
							   lw=3)
		if label_dict[target_lab] not in plotted_labs:
			plotted_labs.add(label_dict[target_lab])
			legend_plots.append(plot)
			legend_labs.append(label_dict[target_lab])
	# box = ax.get_position()
	# ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
	leg_tup = zip(legend_labs,legend_plots)
	leg_tup.sort(key=lambda x: x[0])
	legend_labs, legend_plots = zip(*leg_tup)
	# leg = ax.legend(legend_plots, legend_labs, loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':24}, scatterpoints=1, frameon=False)
	leg = ax.legend(legend_plots, legend_labs, loc='upper left', prop={'size':leg_font}, scatterpoints=1, frameon=True)
	for i in range(len(legend_labs)):
		# leg.legendHandles[i]._sizes = [200]
		leg.legendHandles[i]._sizes = [150]
	# ax.set_title(title)
	xmin = min(results[:,0])
	xmax = max(results[:,0])
	ymin = min(results[:,1])
	ymax = max(results[:,1])
	# ax.set_xlim([xmin-3, xmax+3])
	# ax.set_ylim([ymin-3, ymax+3])
	ax.set_xlim([-60,30])
	ax.set_ylim([-30,50])

	for k,box_dims in enumerate(boxes):
		box_xmin = box_dims[0]
		box_xmax = box_dims[1]
		box_ymin = box_dims[2]
		box_ymax = box_dims[3]
		ax.plot([box_xmin,box_xmax,box_xmax,box_xmin,box_xmin], [box_ymax,box_ymax,box_ymin,box_ymin,box_ymax], linestyle="--", color="k", linewidth=2)
		# labX = box_xmin + (box_xmax - box_xmin)/2
		# labY = box_ymax + 1
		labX = box_xmax + 1
		labY = box_ymax - (box_ymax - box_ymin)/2
		ax.text(labX,labY, "bin%s" % (k+1), fontsize=18, horizontalalignment="left", verticalalignment="center")
		# ax.set_xlim([min(minx-4, box_xmin-2), max(maxx+4, box_xmax+2)])
		# ax.set_ylim([min(miny-4, box_ymin-2), max(maxy+4, box_ymax+2)])
		ax.set_xlim([-60,30])
		ax.set_ylim([-35,50])

	plt.savefig(plot_fn, dpi=300)

boxes   = [(-38,-27,-14,8), \
		   (-7,3,-31,-15), \
		   (12,25,-31,-16), \
		   (11,21,-13,-2), \
		   (21,29,-4,7), \
		   (-1,21,10,25), \
		   (-18,-6,32,45), \
		   (-13,-1,-11,5)]

box_id_map = {0:"B_theta", \
			  1:"E_coli", \
			  2:"B_vulgatus", \
			  3:"R_gnavus", \
			  4:"C_aero", \
			  5:"B_ovatus", \
			  6:"B_caccae", \
			  7:"C_bolteae", \
			  }

results    = np.loadtxt("contigs.SCp.2D",  dtype="float")
results_nD = np.loadtxt("contigs.SCp",     dtype="float")
labels     = np.loadtxt("contigs.labels",  dtype="str")
sizes      = np.loadtxt("contigs.lengths", dtype="int")
names      = np.loadtxt("contigs.names",   dtype="str")
cov_bed    = np.loadtxt("coverage.bed",    dtype="str", skiprows=1)
motifs     = np.loadtxt("ordered_motifs.txt", dtype="str")

# remove digested subcontigs
results = results[sizes!=50000]
labels  = labels[sizes!=50000]
sizes   = sizes[sizes!=50000]

plot_fn = "JF8.contigs.SCp.bins.png"
title   = "contig-level eBinning"

def get_contig_mean_cov( name, cov_bed ):
	name      = name.split(".")[0]
	cov_names = cov_bed[:,0]
	mask      = cov_names==name
	covs      = cov_bed[mask,4].astype(float)
	# Ignore edges of contigs
	covs      = covs[2:-2]
	return covs.mean()

for i,box in enumerate(boxes):
	box_xmin = box[0]
	box_xmax = box[1]
	box_ymin = box[2]
	box_ymax = box[3]
	box_contigs = []
	box_labels  = {}
	box_SCp     = []
	for j in range(results.shape[0]):
		x    = results[j,0]
		y    = results[j,1]
		if y>box_ymin and y<box_ymax and x>box_xmin and x<box_xmax:
			box_contigs.append(names[j])
			box_SCp.append(results_nD[j])
			box_labels[names[j]] = labels[j]

	box_SCp       = np.matrix(box_SCp)
	box_SCp_means = box_SCp.mean(0).A1
	mask          = box_SCp_means > 1.0
	sig_motifs    = motifs[mask]
	mean_SCps     = box_SCp_means[mask]
	for j,motif in enumerate(sig_motifs):
		print "%s %s\t%.4f" % (box_id_map[i], motif, mean_SCps[j])

	fasta_fn = "contigs.box.%s.fasta" % box_id_map[i]
	f = open(fasta_fn, "wb")
	cluster_size = 0
	contigs_n    = 0
	for contig in SeqIO.parse("polished_assembly.fasta", "fasta"):
		name_str = contig.id.split("|")[0]
		if name_str in box_contigs:
			contig.description = "%s.%s" % (name_str, box_labels[name_str])
			contig.id          = "%s.%s" % (name_str, box_labels[name_str])
			print "%s %s\t%s\t%.2f" % (box_id_map[i],contig.id, len(contig.seq), get_contig_mean_cov(contig.id, cov_bed))
			SeqIO.write(contig, f, "fasta")
			cluster_size += len(contig.seq)
			contigs_n    += 1
	f.close()
	print "Total contigs = %s" % contigs_n
	print "Total bases = %s bp" % cluster_size
	print ""

scatterplot(results, labels, sizes, plot_fn, title, boxes)