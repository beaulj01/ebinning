import os,sys
import glob
import numpy as np
from pbcore.io.BasH5IO import BasH5Reader
from collections import Counter

movie_mapping = {}
specs        = set()
for line in open("/hpc/users/beaulj01/projects/ebinning/2HP_1EC_1SM/bas_binning/downsample_HP/movie_mapping.txt", "r").xreadlines():
	line  = line.strip()
	spec  = line.split(":")[0].split(".")[0]
	movie = line.split(":")[1].split(".")[0]
	movie_mapping[movie] = spec
	specs.add(spec)

baxh5_files   = np.loadtxt("/hpc/users/beaulj01/projects/ebinning/2HP_1EC_1SM/bas_binning/downsample_HP/input.fofn", dtype="str")
bases_counter = Counter()
for i,baxh5_file in enumerate(baxh5_files):
	reader = BasH5Reader(baxh5_file)
	for r in reader:
		if r.productivity==1:
			readname = r.zmwName
			movie    = readname.split("/")[0]
			spec = movie_mapping[movie]
			read_bases = 0
			for sub in r.subreads:
				bases                = sub.readEnd - sub.readStart
				read_bases          += bases
				bases_counter[spec] += bases
			# Print readname for whitelist
			print "%s\t%s\t%s" % (readname, spec, read_bases)

f = open("baseline_base_counts_2HP_1EC_1SM.out", "w")
for spec in list(specs):
	f.write("%s\t%s\n" % (spec, bases_counter[spec]))
f.close()
