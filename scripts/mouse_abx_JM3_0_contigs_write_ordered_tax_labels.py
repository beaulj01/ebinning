import os,sys
import numpy as np

contigs_tax_fn = "contigs.tax.unordered"
labels_digest  = np.loadtxt("contigs.labels.digest", dtype="str")
labels         = np.loadtxt("contigs.labels",        dtype="str")
names_digest   = np.loadtxt("contigs.names.digest",  dtype="str")
names          = np.loadtxt("contigs.names",         dtype="str")

contig_label = {}
for line in open(contigs_tax_fn, "r").xreadlines():
	line = line.strip()
	name = line.split("\t")[0].split("|")[0]
	if len(line.split("\t"))!=2:
		continue
	tax    = line.split("\t")[1]
	levels = tax.split("|")
	
	# Try for a species-level annotation
	# spec = [lev for lev in levels if lev[:3]=="s__"]
	# if len(spec)==1:
	# 	spec = spec[0].strip("s__")
	# 	contig_label[name] = spec

	# Try for an order-level annotation
	order = [lev for lev in levels if lev[:3]=="o__"]
	if len(order)==1:
		order = order[0].strip("o__")
		contig_label[name] = order

	# Try for an family-level annotation
	# family = [lev for lev in levels if lev[:3]=="f__"]
	# if len(family)==1:
	# 	family = family[0].strip("f__")
	# 	contig_label[name] = family

f = open("contigs.kraken.digest", "w")
for i,name in enumerate(names_digest):
	if contig_label.get(name):
		f.write("%s\n" % contig_label[name])
	else:
		f.write("Unlabeled\n")
f.close()

f = open("contigs.kraken", "w")
for i,name in enumerate(names):
	if contig_label.get(name):
		f.write("%s\n" % contig_label[name])
	else:
		f.write("Unlabeled\n")
f.close()