import numpy as np
import sys
import scipy.stats as stats

x = np.loadtxt(sys.argv[1], dtype="float")

x_minmax = np.zeros(x.shape)
if len(x.shape)==2:
	for col in range(x.shape[1]):
		x_minmax[:,col] = (x[:,col] - min(x[:,col])) / (max(x[:,col]) - min(x[:,col]))
elif len(x.shape)==1:
	x_minmax[:] = (x[:] - min(x[:])) / (max(x[:]) - min(x[:]))
minmax_fn = sys.argv[1] + ".minmax"
np.savetxt(minmax_fn, x_minmax, fmt="%.4f", delimiter="\t")

m     = np.loadtxt(sys.argv[1], dtype="float")
m_std = stats.mstats.zscore(m, axis=0)
z_fn  = sys.argv[1] + ".zscores"
np.savetxt(z_fn, m_std, fmt='%.4f', delimiter="\t")