import os,sys,glob
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import re
import math
from pbcore.io.BasH5IO import BasH5Reader
import subprocess
import numpy as np
from collections import OrderedDict,Counter,defaultdict
from operator import itemgetter
from itertools import groupby
from itertools import izip

contigs_noBD_labels  = "no_bdorei.contigs.unordered.labels"
subreads_noBD_labels = "no_bdorei.subreads.unordered.labels"

# Build set of non-B. dorei subread + contig names
specs = {}
labs  = Counter()
for line in open(contigs_noBD_labels, "r").xreadlines():
	line = line.strip()
	contig = line.split("\t")[0].split("|")[0]
	tax  = line.split("\t")[1]
	spec = " ".join(tax.split(";")[-1].split(" ")[:2])
	if len(spec.split(" "))==1:
		continue

	# spec = spec.split(" ")[0]

	specs[contig] = spec
	labs[spec] += 1

subread_iter = [ ("/".join(line.split("\t")[0].split("/")[:2]), line.strip().split("\t")[1]) for line in open(subreads_noBD_labels).xreadlines()]
for read,group in groupby(subread_iter, lambda x: x[0]):
	read_spec_counter = Counter()
	for tax in group:
		spec = " ".join(tax[1].split(";")[-1].split(" ")[:2])
		if len(spec.split(" "))==1:
			continue
		
		# spec = spec.split(" ")[0]

		read_spec_counter[spec] += 1
	if len(read_spec_counter)>0:
		read_spec   = max(read_spec_counter, key=read_spec_counter.get)
		specs[read] = read_spec
		labs[read_spec] += 1

both_names_fn   = "both.noBD.names"
both_labels_fn  = "both.noBD.labels"
both_comp_fn    = "both.noBD.comp"
both_lengths_fn = "both.noBD.lengths"
f_lab = open(both_labels_fn,  "w")
f_nam = open(both_names_fn,   "w")
f_len = open(both_lengths_fn, "w")
f_com = open(both_comp_fn,    "w")
for i,(name,length,comp) in enumerate(izip( open("both.names"), open("both.lengths"), open("both.comp"))):
	name   = name.strip("\n")
	length = int(length.strip())
	comp   = comp.strip("\n")
	if specs.get(name):
		f_lab.write("%s\n" % "_".join(specs[name].split(" ")))
		f_nam.write("%s\n" % name)
		f_len.write("%s\n" % length)
		f_com.write("%s\n" % comp)
f_lab.close()
f_nam.close()
f_len.close()
f_com.close()