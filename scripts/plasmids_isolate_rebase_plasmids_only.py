import os,sys
from Bio import SeqIO

map_fn   = "rebase_acc_type.map"
fasta_fn = "rebase_all.fasta"

pls_accs = set()
for line in open(map_fn, "rb").xreadlines():
	line = line.strip()
	acc  = line.split("\t")[1]
	tp   = line.split("\t")[2]
	if tp=="plasmid":
		pls_accs.add(acc)

pls_records = []
for record in SeqIO.parse(fasta_fn, "fasta"):
	acc = record.id.split(" ")[0].split(".")[0]
	if acc in pls_accs:
		pls_records.append(record)

out_fn = "rebase_plasmids.fasta"
SeqIO.write(pls_records, out_fn, "fasta")