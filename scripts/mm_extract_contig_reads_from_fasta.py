import os,sys
from itertools import groupby

def fasta_iter( fasta_name ):
	fh = open(fasta_name)
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		header = header.next()[1:].strip()
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

ccs_fn     = "/hpc/users/beaulj01/projects/ebinning/mouse_abx/pilot_unmatched/abx/CCS/MIRA/pilot_noabx_CCS_assembly/c1241_IGV_analysis/reads_of_insert.fasta"
readtags   = "/hpc/users/beaulj01/projects/ebinning/mouse_abx/pilot_unmatched/abx/CCS/MIRA/pilot_noabx_CCS_assembly/c1241_IGV_analysis/pilot_noabx_CCS_info_readtaglist.txt"
target_ctg = sys.argv[1]

hits = set()
for line in open(readtags).xreadlines():
	line = line.strip()
	ctg  = line.split("\t")[0]
	if ctg==target_ctg:
		read = line.split("\t")[6]
		hits.add(read)

for name,seq in fasta_iter(ccs_fn):
	if name in hits:
		print ">%s" % name
		print seq

