import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import scipy
from scipy.stats import gaussian_kde
import sklearn.cluster as cluster
import pub_figs

data = np.loadtxt(sys.argv[1], dtype="float")
bins = 40

fig               = plt.figure()
# ax                = fig.add_subplot(111)
# extent            = [-60,60,-60,60]
extent            = [-40,40,-40,40]
H, xedges, yedges = np.histogram2d(data[:,0], data[:,1], bins=bins, range=[extent[:2], extent[2:]])
# H needs to be rotated and flipped
H = np.rot90(H)
H = np.flipud(H)
Hmasked = np.ma.masked_where(H==0,H) # Mask pixels with a value of zero
plt.hot()                                    # set 'hot' as default colour map
im = plt.imshow(H, interpolation='bilinear', # creates background image
                origin='lower', cmap=cm.Blues, 
                extent=extent)
plt.subplots_adjust(bottom=0.15, left=0.15)
# levels            = np.arange(1000, 50, -50)
levels            = [300,250,170,120]
# cols              = plt.get_cmap('spectral')(np.linspace(0, 1.0, len(levels)))
cols              = ["k","0.4","0.6","0.8"]
cset              = plt.contour(H, levels, origin="lower",colors=cols,linewidths=1.4,extent=extent)
plt.clabel(cset, inline=1, fontsize=10, fmt="%1.0i")
for c in cset.collections:
	c.set_linestyle("solid")
x1 = np.array(range(-4,1))
y1 = eval("-0.8*x1-1.5")
plt.plot(x1, y1, linestyle="--", color="k")
plt.gca().set_aspect('equal', adjustable='box')
plt.savefig("2d_density_plot.png")

fig = plt.figure()
plt.scatter(data[:,0], data[:,1])
plt.savefig("scatter_plot.png")