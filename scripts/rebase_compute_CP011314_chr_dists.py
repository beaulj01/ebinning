import os,sys
import numpy as np
import urllib2
from bs4 import BeautifulSoup
from collections import Counter

all_comps_fn  = "comp_vectors.out"
pls_info_fn   = "dists.out"
pb_acc_org_fn = "pb_acc_org_mapping.txt"

mix_orgs    = set(["Escherichia coli ECONIH1 chr.", \
				   "Enterobacter cloacae 34978 chr.", \
				   "Salmonella enterica subsp. enterica serovar Tennessee CFSAN001387 chr.", \
				   "Citrobacter freundii CFNIH1 chr.", \
				   "Klebsiella oxytoca KONIH1 chr.", \
				   "Escherichia coli O157:H7 EDL933 chr.", \
				   "Salmonella bongori N268-08 chr.", \
				   "Yersinia pestis PBM19 chr.", \
				   "Vibrio cholerae 2012EL-2176 chr. 1", \
				   "Vibrio cholerae 2012EL-2176 chr. 2", \
				   "Klebsiella pneumoniae subsp. pneumoniae 234-12 chr.", \
				   "Klebsiella pneumoniae subsp. pneumoniae 234-12 plasmid pKpn23412-362"])

org_motifs  = {}
pb_acc_orgs = {}
for line in open(pb_acc_org_fn).xreadlines():
	line   = line.strip()
	acc    = line.split("\t")[0]
	org    = line.split("\t")[1]
	if len(line.split("\t"))==3:
		motifs = line.split("\t")[2].split(" ")
	if acc =="CP007634":
		org = "Vibrio cholerae 2012EL-2176 chr. 1"
	elif acc=="CP007635":
		org = "Vibrio cholerae 2012EL-2176 chr. 2"
	elif acc=="CP011314":
		pass
	else:
		org += " chr."
	org_motifs[org]  = motifs
	pb_acc_orgs[acc] = org

plasmids = set()
for line in open(pls_info_fn).xreadlines():
	line = line.strip()
	acc  = line.split("\t")[2].split(".")[0]
	plasmids.add(acc)

for line in open(all_comps_fn).xreadlines():
	line = line.strip()
	acc  = line.split("\t")[1].split(".")[0]
	if acc=="CP011314":
		CP011314_comp = np.array(map(lambda x: float(x), line.split("\t")[2:]))

kp_motifs = ["CCAYNNNNNTCC", "GATC"]
kp_accs   = set(["CP011313","CP011314"])
names     = {"CP011313" : "Klebsiella pneumoniae subsp. pneumoniae 234-12 chr.", \
			 "CP011314" : "Klebsiella pneumoniae subsp. pneumoniae 234-12 plasmid pKpn23412-362", \
			 "CP009859" : "Escherichia coli ECONIH1 chr.", \
			 "CP012165" : "Enterobacter cloacae 34978 chr.", \
			 "CP014994" : "Salmonella enterica subsp. enterica serovar Tennessee CFSAN001387 chr.", \
			 "CP007557" : "Citrobacter freundii CFNIH1 chr.", \
			 "CP008788" : "Klebsiella oxytoca KONIH1 chr.", \
			 "CP008957" : "Escherichia coli O157:H7 EDL933 chr.", \
			 "CP006608" : "Salmonella bongori N268-08 chr.", \
			 "CP009492" : "Yersinia pestis PBM19 chr.", \
			 "CP007634" : "Vibrio cholerae 2012EL-2176 chr. 1", \
			 "CP007635" : "Vibrio cholerae 2012EL-2176 chr. 2"}

similar_accs = set()
for line in open(all_comps_fn).xreadlines():
	line = line.strip()
	acc  = line.split("\t")[1].split(".")[0]
	if acc not in plasmids or acc in kp_accs:
		# Chromosome
		comp = np.array(map(lambda x: float(x), line.split("\t")[2:]))
		dist = np.linalg.norm(comp - CP011314_comp)
		if dist<=10.61041:
			similar_accs.add(acc)
			try:
				org    = names[acc]
				if acc in kp_accs:
					motifs = kp_motifs
				else:
					motifs = org_motifs[org]
				motifs.sort()
				if org in mix_orgs or acc in kp_accs:
					print "%.4f\t%s\t%s\t%s\t%s" % (dist, acc, names[acc], len(motifs), " ".join(motifs))
			except KeyError:
				# Not in the Pacbio Rebase list
				pass