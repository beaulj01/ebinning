import sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import random
import pub_figs
from pbcore.io.BasH5IO import BasH5Reader
import textwrap

label_dict      =  {"S_mutans_UA159" :                       r"${S. mutans}$ UA159", \
		   			"L_monocytogenes_EGD_e" :                r"${L. monocytogenes}$ EGD-e", \
		   			"S_epidermidis_ATCC_12228" :             r"${S. epidermidis}$ ATCC 12228", \
		   			"L_gasseri_ATCC_33323" :                 r"${L. gasseri}$ ATCC 33323", \
		   			"P_acnes_KPA171202" :                    r"${P. acnes}$ KPA171202", \
		   			"S_aureus_subsp_aureus_USA300_TCH1516" : r"${S. aureus}$ subsp aureus USA300 TCH1516", \
		   			"N_meningitidis_MC58" :                  r"${N. meningitidis}$ MC58", \
		   			"P_aeruginosa_PAO1" :                    r"${P. aeruginosa}$ PAO1", \
					"H_pylori_26695" :                       r"${H. pylori}$ 26695", \
					"D_radiodurans_R1" :                     r"${D. radiodurans}$ R1", \
		   			"A_odontolyticus_ATCC_17982_Scfld021" :  r"${A. odontolyticus}$ ATCC 17982 Scfld021", \
		   			"B_vulgatus_ATCC_8482" :                 r"${B. vulgatus}$ ATCC 8482", \
		   			"E_faecalis_OG1RF" :                     r"${E. faecalis}$ OG1RF", \
		   			"E_coli_str_K_12_substr_MG1655" :        r"${E. coli}$ str K-12 substr MG1655", \
		   			"S_agalactiae_2603V_R" :                 r"${S. agalactiae}$ 2603V R", \
					"A_baumannii_ATCC_17978" :               r"${A. baumannii}$ ATCC 17978", \
					"R_sphaeroides_2_4_1" :                  r"${R. sphaeroides}$ 2.4.1", \
		   			"S_pneumoniae_TIGR4":                    r"${S. pneumoniae}$ TIGR4", \
		   			"C_beijerinckii_NCIMB_8052" :            r"${C. beijerinckii}$ NCIMB 8052", \
		   			"B_cereus_ATCC_10987" :                  r"${B. cereus}$ ATCC 10987"}

def scatterplot(results, labels, plot_fn, title, read_names, extra, box_dims):
	label_set = list(set(labels))
	label_set.sort()
	colors    = plt.get_cmap('spectral')(np.linspace(0, 1.0, len(label_set)))
	fig       = plt.figure(figsize=[15,12])
	ax        = fig.add_axes([0.1, 0.1, 0.6, 0.6])
	# ax.axis("off")
	res       = []
	for k,target_lab in enumerate(label_set):
		if target_lab == "unknown":
			continue
		idxs  = [j for j,label in enumerate(labels) if label==target_lab]
		X     = results[idxs,0]
		Y     = results[idxs,1]
		color = colors[k]
		# colors = plt.get_cmap('spectral')(extra[idxs]/max(extra[idxs]))
		for i,x in enumerate(X):
			res.append( (x, Y[i], target_lab, color) ) 
			# res.append( (x, Y[i], target_lab, colors[i]) ) 
	
	np.random.shuffle(res) 
	res          = random.sample(res, 5000)
	plotted_labs = set()
	legend_plots = []
	legend_labs  = []
	for (x,y, target_lab, color) in res:
		# if target_lab != "R_sphaeroides_2_4_1":
		# 	continue
		plot = ax.scatter(x,y, marker="o", s=15 , edgecolors="None", label=label_dict[target_lab], facecolors=color, alpha=0.8)
		if label_dict[target_lab] not in plotted_labs:
			plotted_labs.add(label_dict[target_lab])
			legend_plots.append(plot)
			legend_labs.append(label_dict[target_lab])
	box = ax.get_position()
	ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
	leg_tup = zip(legend_labs,legend_plots)
	leg_tup.sort(key=lambda x: x[0])
	legend_labs, legend_plots = zip(*leg_tup)
	leg = ax.legend(legend_plots, legend_labs, loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':18}, frameon=False)
	for i in range(len(legend_labs)):
		leg.legendHandles[i]._sizes = [50]
	minx = min(map(lambda x: x[0], res))
	maxx = max(map(lambda x: x[0], res))
	miny = min(map(lambda x: x[1], res))
	maxy = max(map(lambda x: x[1], res))

	box_xmin = box_dims[0]
	box_xmax = box_dims[1]
	box_ymin = box_dims[2]
	box_ymax = box_dims[3]
	ax.set_xlim([min(minx-1, box_xmin), max(maxx+1, box_xmax)])
	ax.set_ylim([min(miny-1, box_ymin), max(maxy+1, box_ymax)])
	ax.plot([box_xmin,box_xmax,box_xmax,box_xmin,box_xmin], [box_ymax,box_ymax,box_ymin,box_ymin,box_ymax], linestyle="--", color="k", linewidth=3)
	plt.savefig(plot_fn)

	box_readnames = set()
	for i in range(results.shape[0]):
		x = results[i,0]
		y = results[i,1]
		if y>box_ymin and y<box_ymax and x>box_xmin and x<box_xmax:
			# if len(box_readnames) == 500:
			# 	break
			box_readnames.add(read_names[i])

	for line in open("bas.h5.fofn", "r").xreadlines():
		line     = line.strip()
		baxh5    = line.split()[0]
		reader   = BasH5Reader(baxh5)
		zmws     = [z for z in reader if z.zmwName in box_readnames]
		for z in zmws:
			for sub in z.subreads:
				print ">%s" % sub.readName
				print textwrap.fill(sub.basecalls(), 70)

results = np.loadtxt("reads.raw.comp.2D", dtype="float")
labels  = np.loadtxt("reads.raw.labels",  dtype="str")
names   = np.loadtxt("reads.raw.names",   dtype="str")
extra   = np.array([]) 
plot_fn = "pub.reads.raw.comp.png"
title   = "read-level cBinning"
box_dims = [int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4]),]

scatterplot(results, labels, plot_fn, title, names, extra, box_dims)