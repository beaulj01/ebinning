import os,sys
import numpy as np
import urllib2
from bs4 import BeautifulSoup
from collections import Counter

# Read all Rebase PacBio organisms
pb_org_accs = set()
main_url    = "http://rebase.neb.com/cgi-bin/pacbiolist?0"
response    = urllib2.urlopen(main_url)
html        = response.read()
soup        = BeautifulSoup(html, "html.parser")

org_accs_count = Counter()
f              = open("pb_acc_org_mapping.txt", "wb")
acc_org_map    = {}

tables         = soup.find_all("table")
for i,table in enumerate(tables):
	if table.attrs.get("bgcolor")=="beige":
		all_as = table.find_all("a")
		for j,a in enumerate(all_as):

			if a.attrs.get("href").find("pacbioget")>-1:
				org      = a.text
				org_url  = "http://rebase.neb.com"+a.attrs["href"]
				org_accs = 0

				response = urllib2.urlopen(org_url)
				html     = response.read()
				soup     = BeautifulSoup(html, "html.parser")

				center   = soup.body.center

				motifs = set()
				
				# First get all the known motifs w/ known MTases
				known_motifs_table = list(soup.find_all("table"))[7]
				trs = known_motifs_table.td.find_all("tr")
				for tr in trs[2:]: #####
					tds       = tr.find_all("td")
					
					all_fonts = tds[5].find_all("font")
					for font in all_fonts:
						if font.attrs.get('size')!="3":
							motifs.add(font.text.strip())

				# Next get all the motifs with unmatch MTases
				unknown_motifs_table = list(soup.find_all("table"))[10]
				trs = unknown_motifs_table.td.find_all("tr")
				for tr in trs[2:]: #####
					tds       = tr.find_all("td")
					all_fonts = tds[0].find_all("font")
					for font in all_fonts:
						if font.attrs.get('size')!="3":
							motifs.add(font.text.strip())

				# The first table entry contains the organism info
				chr_table   = list(center.children)[3].find("table")
				acc_section = list(chr_table.children)[2]
				all_as      = acc_section.td.font.find_all("a")
				for a in all_as:
					if a.attrs.get("href").find("seqsget")>-1:
							org_accs_count[org]  += 1
							acc_org_map[a.string] = org
							acc                   = a.string
							f.write("%s\t%s\t%s\n" % (acc, org, " ".join(list(motifs))))
							pb_org_accs.add(a.string.strip())
				print org, org_accs_count[org], ",".join(list(motifs))
f.close()