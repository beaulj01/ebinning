import os,sys
import subprocess
import urllib2
from bs4 import BeautifulSoup
import re

gbk_GCFs = ["GCA_001701165.1", \
			"GCA_001689405.1", \
			"GCA_001689415.1", \
			"GCA_001689425.1", \
			"GCA_001689445.1", \
			"GCA_001689485.1", \
			"GCA_001689495.1", \
			"GCA_001689515.1", \
			"GCA_001689535.1", \
			"GCA_001689565.1", \
			"GCA_001689575.1", \
			"GCA_001689585.1", \
			"GCA_001689615.1", \
			"GCA_001689645.1", \
			"GCA_001689655.1", \
			"GCA_001689665.1", \
			"GCA_001689685.1", \
			"GCA_001701065.1", \
			"GCA_001701075.1", \
			"GCA_001701105.1", \
			"GCA_001701115.1", \
			"GCA_001701135.1", \
			"GCA_001701175.1", \
			"GCA_001701195.1", \
			"GCA_001701225.1", \
			"GCA_001701235.1", \
			"GCA_001701255.1", \
			"GCA_001701285.1", \
			"GCA_001701295.1", \
			"GCA_001701305.1"]

def run_OS_command( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts != 0:
		print "Failed command: %s" % CMD
	return sts, stdOutErr

base_url = "https://www.ncbi.nlm.nih.gov/assembly/"
for GCF in gbk_GCFs:
	url      = base_url+GCF
	response = urllib2.urlopen(url)
	html     = response.read()
	soup     = BeautifulSoup(html, "html.parser")
	REGEX    = r"ASM\w\w\w\w\w\wv\d"
	m        = re.search(REGEX, soup.prettify())
	ASM      =  m.group()

	# ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/001/701/165/GCA_001701165.1_ASM170116v1
	ftp_dir = "ftp://ftp.ncbi.nlm.nih.gov/genomes/all/%s/%s/%s/%s/%s_%s" % (GCF[:3], GCF[4:7], GCF[7:10], GCF[10:13], GCF, ASM)
	fna_fn  = "%s_%s_genomic.fna.gz" % (GCF, ASM)
	path    = os.path.join(ftp_dir, fna_fn)

	wget_CMD      = "wget %s" % path
	sts,stdOutErr = run_OS_command(wget_CMD)

	gunzip_CMD    = "gunzip %s" % fna_fn
	sts,stdOutErr = run_OS_command(gunzip_CMD)

