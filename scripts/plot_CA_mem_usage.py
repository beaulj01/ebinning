import os,sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib
from matplotlib import pyplot as plt

log_fn = sys.argv[1]

kb       = []
entry_kb = 0
for line in open(log_fn).xreadlines():
	if line.find("Nov ")>-1: continue
	if line!="\n":
		if line.split()[1] == "beaulj01":
			val = int(line.split()[0])
			entry_kb += val
	else:
		kb.append(entry_kb)
		entry_kb = 0

kb  = np.array(kb)
gb  = kb/1000000
fig = plt.figure(figsize=[9,6])
ax  = fig.add_subplot(111)
ax.plot(gb, "k")
ax.set_ylabel("GB RAM consumed")
ax.set_xlabel("Minutes")
plt.savefig("HGAP_memory_usage.png")
