import os,sys
import numpy as np
from Bio import SeqIO

cov_bed = np.loadtxt(sys.argv[1], dtype="str", skiprows=1)
fasta   = sys.argv[2]
names   = np.loadtxt("contigs.names", dtype="str")
SCp     = np.loadtxt(sys.argv[3], dtype="float")

SCp_norm = np.zeros(SCp.shape)
for j in range(SCp.shape[1]):
	motif_ipds = SCp[:,j]
	
	motif_max = max(motif_ipds)
	motif_min = min(motif_ipds)
	span      = motif_max - motif_min
	for i in range(SCp.shape[0]):
		# I'll normalize and multiply by 100 to make it look like coverage
		SCp_norm[i,j] = 100 * (motif_ipds[i] - motif_min) / span
np.savetxt(sys.argv[3]+".norm", SCp_norm, delimiter="\t", fmt="%.4f")

SCp_dict = {}
for i,name in enumerate(names):
	scp_str        = "\t".join(map(str, 10**SCp_norm[i,:]))
	SCp_dict[name] = scp_str

def get_contig_mean_cov( name, cov_bed ):
	name      = name.split("|")[0]
	cov_names = cov_bed[:,0]
	mask      = cov_names==name
	covs      = cov_bed[mask,4].astype(float)
	# covs      = covs[2:-2]
	return covs.mean()

entries = []
for entry in SeqIO.parse(fasta, "fasta"):
	entry.cov = get_contig_mean_cov( entry.name, cov_bed )
	entries.append( (entry.name, len(entry.seq), entry.cov) )

low_cov_bp = 0
i = 0
s = sorted(entries, key=lambda x: x[2])
SCp_header = "\t".join(["val"] * len(SCp_norm[i,:]))
print "Contig\tCoverage\t%s" % SCp_header
for (contig,size,cov) in s:
	if SCp_dict.get(contig):
		print "%s\t%.2f\t%s" % (contig, cov, SCp_dict[contig])
		if cov < 100:
			low_cov_bp += size
			i += 1
