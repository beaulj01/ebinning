import os,sys
from Bio import Entrez
from itertools import groupby
import urllib2
sys.path.append("/hpc/users/beaulj01/gitRepo/eBinning/src")
import read_scanner
from collections import Counter
from urllib2 import urlopen
import subprocess
import glob

assemblies = ["GCF_000335055.2", \
			  "CP009072.1", \
			  "GCF_000019385.1", \
			  "GCF_000690815.1"]

def pull_fasta_get_comp( accs ):
	# Download all the fastas from this NCBI page
	db           = "nuccore"
	Entrez.email = "john.beaulaurier@mssm.edu"
	batchSize    = 100
	retmax       = 10**9

	try:
		for k,start in enumerate(range( 0,len(accs),batchSize )):
			fasta_name = "tmp.%s.fasta" % k
			#first get GI for query accesions
			batch_accs = accs[start:(start+batchSize)]
			sys.stderr.write( "Fetching %s entries from GenBank: %s\n" % (len(batch_accs), ", ".join(batch_accs[:10])))
			query  = " ".join(batch_accs)
			handle = Entrez.esearch( db=db,term=query,retmax=retmax )
			giList = Entrez.read(handle)['IdList']
			sys.stderr.write( "Found %s GI: %s\n" % (len(giList), ", ".join(giList[:10])))
			#post NCBI query
			search_handle     = Entrez.epost(db=db, id=",".join(giList))
			search_results    = Entrez.read(search_handle)
			webenv,query_key  = search_results["WebEnv"], search_results["QueryKey"]
			#fetch entries in batch
			# handle = Entrez.efetch(db=db, rettype="fasta", retstart=start, retmax=batchSize, webenv=webenv, query_key=query_key)
			handle = Entrez.efetch(db=db, rettype="fasta", webenv=webenv, query_key=query_key)
			# sys.stdout.write(handle.read())
			f = open(fasta_name, "wb")
			f.write(handle.read())
			f.close()
	except urllib2.HTTPError as err:
		if err.code==404:
			print "**** HTTPError 404! ****"
			error_urls.append(err.geturl())
			errors_n += 1
		else:
			raise

def cat_list_of_files( in_fns, out_fn, del_ins=True ):
	"""
	Given a list of filenames, cat them together into a single file. Then cleans up pre-catted
	single files.
	"""
	if len(in_fns)==0:
		raise Exception("No files to cat!")
		
	cat_CMD   = "cat %s > %s" % (" ".join(in_fns), out_fn)
	p         = subprocess.Popen(cat_CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts != 0:
		raise Exception("Failed cat command: %s" % cat_CMD)
	if del_ins:
		for fn in in_fns:
			try:
				os.remove(fn)
			except OSError:
				pass

for assembly in assemblies:
	os.chdir(assembly)
	accs = []
	if assembly[:3]=="GCF":
		u    = urlopen("ftp://ftp.ncbi.nlm.nih.gov/genomes/ASSEMBLY_REPORTS/All/%s.assembly.txt" % assembly)
		for line in u:
			line = line.strip()
			if line[0]!="#":
				gb   = line.split("\t")[4]
				accs.append(gb)
		
	else:
		accs.append(assembly)
	pull_fasta_get_comp( accs )
	to_cat = glob.glob("tmp.*.fasta")
	cat_list_of_files(to_cat, "%s.fasta" % assembly)
	os.chdir("../")
