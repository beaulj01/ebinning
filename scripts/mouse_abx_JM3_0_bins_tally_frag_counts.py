import os,sys
import glob
from Bio import SeqIO
import numpy as np
from collections import Counter

boxes_dir    = "/hpc/users/beaulj01/projects/ebinning/mouse_abx/JM3/0/10kb/contig_binning/rerun_motif_discov/debug/box_assemblies/"
concoct_bins = np.loadtxt(sys.argv[1], delimiter=",", dtype="str")

bin_map = dict(zip(concoct_bins[:,0], concoct_bins[:,1]))

bin_counter = {}
for i in range(concoct_bins.shape[0]):
	frag   = concoct_bins[i,0]
	contig = frag.split(".")[0]
	if not bin_counter.get(contig):
		# initialize counter
		bin_counter[contig] = Counter()

	bin_id = concoct_bins[i,1]
	# add a vote for this bin
	bin_counter[contig][bin_id] += 1

bin_contigs = {1: ["unitig_6577"], \
		   	   2: ["unitig_58"], \
		       3: ["unitig_6591"], \
		       4: ["unitig_92"], \
		       5: ["unitig_6604"], \
		       6: ["unitig_6573"], \
		       7: ["unitig_6588"], \
		       8: ["unitig_6477"], \
		       9: ["unitig_103"]} 

for i in range(1,10):
	print i, bin_contigs[i], bin_counter[bin_contigs[i][0]]


sys.exit()

voted_bin_map = {}
# pick the winning bin for each contig
for contig,counter in bin_counter.iteritems():
	tot_votes    = np.sum(counter.values())
	top_vote_bin = counter.most_common()[0][0]
	top_vote     = counter.most_common()[0][1]
	voted_bin_map[contig] = top_vote_bin

for i in range(1,10):
	box_fn = os.path.join(boxes_dir, "box%s.fasta" % i)
	for Seq in SeqIO.parse(box_fn, "fasta"):
		name = Seq.id.replace("|quiver", "")
		if voted_bin_map.get(name):
			print i, name, voted_bin_map[name], len(Seq.seq), bin_counter[name]
		else:
			print i, name, "NOT FOUND", len(Seq.seq)