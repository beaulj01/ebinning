import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import scipy
from scipy.stats import gaussian_kde
import sklearn.cluster as cluster
import pub_figs
import random
from pbcore.io.BasH5IO import BasH5Reader
from itertools import groupby

data     = np.loadtxt("both.comp.2D", dtype="float")
labels   = np.loadtxt("both.labels",  dtype="str")
names    = np.loadtxt("both.names",   dtype="str")
unmapped = "unmappedSubreads.fasta"
# plot_fn  = "pub.reads.comp.unmapped.png"
# plot_fn  = "pub.reads.comp.mapped.png"
plot_fn  = "pub.reads.comp.mapped_unmapped.png"
title    = "read-level cBinning"
box_dims = [float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3]), float(sys.argv[4]),]
pre      = sys.argv[5]

def fasta_iter( fasta_name ):
	fh = open(fasta_name)
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		header = header.next()[1:].strip()
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

unmapped_reads = set()
for name,seq in fasta_iter(unmapped):
	read = "/".join(name.split("/")[:-1])
	unmapped_reads.add(read)

for i,name in enumerate(names):
	if name in unmapped_reads:
		labels[i] = "unmapped"
	else:
		labels[i] = "mapped"

label_set = list(set(labels))
label_set.sort()
colors    = plt.get_cmap('spectral')(np.linspace(0.2, 0.8, len(label_set)))
fig       = plt.figure(figsize=[15,12])
ax        = fig.add_axes([0.1, 0.1, 0.7, 0.7])
# ax.axis("off")
res       = []
for k,target_lab in enumerate(label_set):
	idxs  = [j for j,label in enumerate(labels) if label==target_lab]
	X     = data[idxs,0]
	Y     = data[idxs,1]
	color = colors[k]
	for i,x in enumerate(X):
		res.append( (x, Y[i], target_lab, color) ) 

np.random.shuffle(res) 
res          = random.sample(res, 10000)
plotted_labs = set()
legend_plots = []
legend_labs  = []
for (x,y, target_lab, color) in res:
	plot = ax.scatter(x,y, marker="o", s=15 , edgecolors="None", label=target_lab, facecolors=color, alpha=0.8)
	if target_lab not in plotted_labs:
		plotted_labs.add(target_lab)
		legend_plots.append(plot)
		legend_labs.append(target_lab)
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
leg_tup = zip(legend_labs,legend_plots)
leg_tup.sort(key=lambda x: x[0])
legend_labs, legend_plots = zip(*leg_tup)
leg = ax.legend(legend_plots, legend_labs, loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':18}, frameon=False)
for i in range(len(legend_labs)):
	leg.legendHandles[i]._sizes = [50]
plt.savefig(plot_fn)

print data[labels=="unmapped",0].shape
print data[labels=="mapped",0].shape

bins              = 200
fig               = plt.figure()
ax                = fig.add_subplot(111)
extent            = [-25,25,-25,25]
H_map, xedges, yedges   = np.histogram2d(data[labels=="mapped",0],   data[labels=="mapped",1],   bins=bins, range=[extent[:2], extent[2:]])
H_unmap, xedges, yedges = np.histogram2d(data[labels=="unmapped",0], data[labels=="unmapped",1], bins=bins, range=[extent[:2], extent[2:]])
H = H_unmap/float(np.amax(H_unmap)) - H_map/float(np.amax(H_map))
print np.amin(H)
print np.amax(H)
H = np.rot90(H)
H = np.flipud(H)
Hmasked = np.ma.masked_where(H==0,H)
plt.hot()
# im = plt.imshow(H, interpolation='bilinear', origin='lower', cmap=cm.Blues, extent=extent)
im = plt.imshow(H, interpolation='bilinear', origin='lower', cmap=cm.bwr, extent=extent, vmin=-1, vmax=1)
plt.subplots_adjust(bottom=0.15, left=0.15)
# levels            = [500,400,300,200]
# cols              = ["k","0.4","0.6","0.8"]
# cset              = plt.contour(H, levels, origin="lower",colors=cols,linewidths=1.4,extent=extent)
# plt.clabel(cset, inline=1, fontsize=10, fmt="%1.0i")
# for c in cset.collections:
# 	c.set_linestyle("solid")
cbar     = plt.colorbar(im)
cbar.set_ticks([-1,1])
cbar.ax.set_yticklabels(["Mapped","Unmapped"])
minx = min(data[:,0])
maxx = max(data[:,0])
miny = min(data[:,1])
maxy = max(data[:,1])
box_xmin = box_dims[0]
box_xmax = box_dims[1]
box_ymin = box_dims[2]
box_ymax = box_dims[3]
ax.set_xlim([min(minx-1, box_xmin-1), max(maxx+1, box_xmax+1)])
ax.set_ylim([min(miny-1, box_ymin-1), max(maxy+1, box_ymax+1)])
# ax.plot([box_xmin,box_xmax,box_xmax,box_xmin,box_xmin], [box_ymax,box_ymax,box_ymin,box_ymin,box_ymax], linestyle="--", color="k", linewidth=3)
plt.gca().set_aspect('equal', adjustable='box')
# plt.savefig("2d_density_plot_unmapped.png")
# plt.savefig("2d_density_plot_mapped.png")
plt.savefig("2d_density_plot_mapped_unmapped.png")

sys.exit()

box_readnames = set()
for i in range(data.shape[0]):
	x = data[i,0]
	y = data[i,1]
	if y>box_ymin and y<box_ymax and x>box_xmin and x<box_xmax:
		box_readnames.add(names[i])

print "Found %s reads in box..." % len(box_readnames)
f = open("box_reads_%s.fasta" % pre, "w")
for name,seq in fasta_iter(unmapped):
	if "/".join(name.split("/")[:-1]) in box_readnames:
		f.write(">%s\n" % name)
		f.write("%s\n" % seq)
f.close()