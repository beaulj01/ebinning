import os,sys
import matplotlib as mpl; mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from collections import defaultdict
import re

fourmer_fn            = sys.argv[1]
fivemer1_fn           = sys.argv[2]
fivemer2_fn           = sys.argv[3]
fivemer3_fn           = sys.argv[4]
sixmer_contiguous_fn  = sys.argv[5]
sixmer_bipartite1_fn  = sys.argv[6]
sixmer_bipartite2_fn  = sys.argv[7]
sixmer_bipartite3_fn  = sys.argv[8]

fourmers = defaultdict(list)
for line in open(fourmer_fn).xreadlines():
    line = line.strip()
    cnt  =   int(line.split("\t")[1])
    SMp  = float(line.split("\t")[2])
    fourmers[cnt].append(SMp)

fivemers1 = defaultdict(list)
for line in open(fivemer1_fn).xreadlines():
    line = line.strip()
    cnt  =   int(line.split("\t")[1])
    SMp  = float(line.split("\t")[2])
    fivemers1[cnt].append(SMp)

fivemers2 = defaultdict(list)
for line in open(fivemer2_fn).xreadlines():
    line = line.strip()
    cnt  =   int(line.split("\t")[1])
    SMp  = float(line.split("\t")[2])
    fivemers2[cnt].append(SMp)

fivemers3 = defaultdict(list)
for line in open(fivemer3_fn).xreadlines():
    line = line.strip()
    cnt  =   int(line.split("\t")[1])
    SMp  = float(line.split("\t")[2])
    fivemers3[cnt].append(SMp)

sixmer_contig = defaultdict(list)
for line in open(sixmer_contiguous_fn).xreadlines():
	line = line.strip()
	cnt  =   int(line.split("\t")[1])
	SMp  = float(line.split("\t")[2])
	sixmer_contig[cnt].append(SMp)

sixmer_bipartite1 = defaultdict(list)
for line in open(sixmer_bipartite1_fn).xreadlines():
    line = line.strip()
    cnt  =   int(line.split("\t")[1])
    SMp  = float(line.split("\t")[2])
    sixmer_bipartite1[cnt].append(SMp)

sixmer_bipartite2 = defaultdict(list)
for line in open(sixmer_bipartite2_fn).xreadlines():
	line = line.strip()
	cnt  =   int(line.split("\t")[1])
	SMp  = float(line.split("\t")[2])
	sixmer_bipartite2[cnt].append(SMp)

sixmer_bipartite3 = defaultdict(list)
for line in open(sixmer_bipartite3_fn).xreadlines():
    line = line.strip()
    cnt  =   int(line.split("\t")[1])
    SMp  = float(line.split("\t")[2])
    sixmer_bipartite3[cnt].append(SMp)

# function for setting the colors of the box plots pairs
def setBoxColors(bp):
    plt.setp(bp['boxes'][0], color='black')
    plt.setp(bp['caps'][0], color='black')
    plt.setp(bp['caps'][1], color='black')
    plt.setp(bp['whiskers'][0], color='black')
    plt.setp(bp['whiskers'][1], color='black')
    plt.setp(bp['fliers'][0], color='black')
    plt.setp(bp['medians'][0], color='black')

    plt.setp(bp['boxes'][1], color='green')
    plt.setp(bp['caps'][2], color='green')
    plt.setp(bp['caps'][3], color='green')
    plt.setp(bp['whiskers'][2], color='green')
    plt.setp(bp['whiskers'][3], color='green')
    plt.setp(bp['fliers'][1], color='green')
    plt.setp(bp['medians'][1], color='green')

    plt.setp(bp['boxes'][2], color='magenta')
    plt.setp(bp['caps'][4], color='magenta')
    plt.setp(bp['caps'][5], color='magenta')
    plt.setp(bp['whiskers'][4], color='magenta')
    plt.setp(bp['whiskers'][5], color='magenta')
    plt.setp(bp['fliers'][2], color='magenta')
    plt.setp(bp['medians'][2], color='magenta')

    plt.setp(bp['boxes'][3], color='blue')
    plt.setp(bp['caps'][6], color='blue')
    plt.setp(bp['caps'][7], color='blue')
    plt.setp(bp['whiskers'][6], color='blue')
    plt.setp(bp['whiskers'][7], color='blue')
    plt.setp(bp['fliers'][3], color='blue')
    plt.setp(bp['medians'][3], color='blue')

    plt.setp(bp['boxes'][4], color='cyan')
    plt.setp(bp['caps'][8], color='cyan')
    plt.setp(bp['caps'][9], color='cyan')
    plt.setp(bp['whiskers'][8], color='cyan')
    plt.setp(bp['whiskers'][9], color='cyan')
    plt.setp(bp['fliers'][4], color='cyan')
    plt.setp(bp['medians'][4], color='cyan')

    plt.setp(bp['boxes'][5], color='red')
    plt.setp(bp['caps'][10], color='red')
    plt.setp(bp['caps'][11], color='red')
    plt.setp(bp['whiskers'][10], color='red')
    plt.setp(bp['whiskers'][11], color='red')
    plt.setp(bp['fliers'][5], color='red')
    plt.setp(bp['medians'][5], color='red')

    plt.setp(bp['boxes'][6], color='yellow')
    plt.setp(bp['caps'][12], color='yellow')
    plt.setp(bp['caps'][13], color='yellow')
    plt.setp(bp['whiskers'][12], color='yellow')
    plt.setp(bp['whiskers'][13], color='yellow')
    plt.setp(bp['fliers'][6], color='yellow')
    plt.setp(bp['medians'][6], color='yellow')

    plt.setp(bp['boxes'][7], color='0.75')
    plt.setp(bp['caps'][14], color='0.75')
    plt.setp(bp['caps'][15], color='0.75')
    plt.setp(bp['whiskers'][14], color='0.75')
    plt.setp(bp['whiskers'][15], color='0.75')
    plt.setp(bp['fliers'][7], color='0.75')
    plt.setp(bp['medians'][7], color='0.75')

# Some fake data to plot
A = [fourmers[3], fivemers1[3], fivemers2[3], fivemers3[3], sixmer_contig[3],  sixmer_bipartite1[3], sixmer_bipartite2[3], sixmer_bipartite3[3]]
B = [fourmers[5], fivemers1[5], fivemers2[5], fivemers3[5], sixmer_contig[5],  sixmer_bipartite1[5], sixmer_bipartite2[5], sixmer_bipartite3[5]]
C = [fourmers[7], fivemers1[7], fivemers2[7], fivemers3[7], sixmer_contig[7],  sixmer_bipartite1[7], sixmer_bipartite2[7], sixmer_bipartite3[7]]
D = [fourmers[9], fivemers1[9], fivemers2[9], fivemers3[9], sixmer_contig[9],  sixmer_bipartite1[9], sixmer_bipartite2[9], sixmer_bipartite3[9]]
E = [fourmers[11], fivemers1[11], fivemers2[11], fivemers3[11], sixmer_contig[11],  sixmer_bipartite1[11], sixmer_bipartite2[11], sixmer_bipartite3[11]]

fig = plt.figure()
ax  = plt.axes()
plt.hold(True)

pos = np.array(range(1,len(A)+1))
bp = plt.boxplot(A, positions = pos, widths = 0.6)
setBoxColors(bp)

pos += len(A)+1
bp = plt.boxplot(B, positions = pos, widths = 0.6)
setBoxColors(bp)

pos += len(A)+1
bp = plt.boxplot(C, positions = pos, widths = 0.6)
setBoxColors(bp)

pos += len(A)+1
bp = plt.boxplot(D, positions = pos, widths = 0.6)
setBoxColors(bp)

pos += len(A)+1
bp = plt.boxplot(E, positions = pos, widths = 0.6)
setBoxColors(bp)

plt.grid(True)

# set axes limits and labels
plt.xlim(0,max(pos)+2)
plt.ylim(-4,4)
ax.set_xticklabels(['3', '5', '7', '9', '11'])
ax.set_xticks([4, 12, 20, 28, 36])

# draw temporary red and blue lines and use them to create a legend
hK, = plt.plot([1,1],'k-')
hG, = plt.plot([1,1],'g-')
hM, = plt.plot([1,1],'m-')
hB, = plt.plot([1,1],'b-')
hC, = plt.plot([1,1],'c-')
hR, = plt.plot([1,1],'r-')
hY, = plt.plot([1,1],'y-')
hGr, = plt.plot([1,1],'0.7')
plt.legend((hK, hG, hM, hB, hC, hR, hY, hGr),('4mer (GATC-1)', \
                                         '5mer (AATCC-1)', \
                                         '5mer (GATGG-1)', \
                                         '5mer (RGATCY-2)', \
                                         '6mer (CAGGAG-4)', \
                                         '6mer (GATGNAG-5)', \
                                         '6mer (CCAN6CAT-2)', \
                                         '6mer (CAGN5GGA-1)'), \
                                         loc=4, prop={'size':6})
plt.xlabel("# motif sites")
plt.ylabel("SMp score")
hK.set_visible(False)
hG.set_visible(False)
hM.set_visible(False)
hB.set_visible(False)
hC.set_visible(False)
hR.set_visible(False)
hY.set_visible(False)
hGr.set_visible(False)

plt.savefig('boxcompare.png')