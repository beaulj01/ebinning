import os,sys
from Bio import SeqIO
import glob

fns = glob.glob("MGS_*.fasta")

for fn in fns:
	records = []
	for Seq in SeqIO.parse(fn, "fasta"):
		mgs = fn.split(".")[0]
		print mgs
		Seq.id          = "%s_%s" % (mgs, Seq.id)
		Seq.description = "%s_%s" % (mgs, Seq.id)
		records.append(Seq)
	SeqIO.write(records, fn.replace(".fasta", ".renamed.fasta"), "fasta")
