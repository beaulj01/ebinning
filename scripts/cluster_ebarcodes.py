import os,sys
import glob
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import re
import math
from itertools import product
import operator

cluster_dir     = sys.argv[1]
flat_files_fofn = sys.argv[2]

def tryint(s):
	try:
		return int(s)
	except:
		return s
	 
def alphanum_key(s):
	""" Turn a string into a list of string and number chunks.
		"z23a" -> ["z", 23, "a"]
	"""
	return [ tryint(c) for c in re.split('([0-9]+)', s) ]

def sort_nicely(l):
	""" Sort the given list in the way that humans expect.
	"""
	l.sort(key=alphanum_key)

def build_motif_dict( max_kmer ):
	"""
	Generate a set of all possible motifs.
	"""
	mod_bases = ["A"]
	bipartite = False

	motifs = set()
	NUCS   = "ACGT"
	total_kmers = 0
	print "Initiating dictionary of all possible motifs..."
	for kmer in range(4,max_kmer+1):
		total_kmers += len(NUCS)**kmer
		print "  - Adding %s %s-mer motifs..." % (len(NUCS)**kmer, kmer)
		for seq in product("ATGC",repeat=kmer):
			string = "".join(seq)
			for base in mod_bases:
				indexes = [m.start() for m in re.finditer(base, string)]
				for index in indexes:
					motif = "%s-%s" % (string, index)							
					motifs.add(motif)

	if bipartite:
		print "  - Adding bipartite motifs to search space..."
		part1_lens = [3]
		part2_lens = [4]
		N_lens     = [1]
		for part1_len in part1_lens:
			for part2_len in part2_lens:
				for seq1 in product("ATGC",repeat=part1_len):
					for seq2 in product("ATGC",repeat=part2_len):
						for N_len in N_lens:
							string = "".join(seq1) + ("N"*N_len) + "".join(seq2)
							for base in mod_bases:
								indexes = [m.start() for m in re.finditer(base, string)]
								for index in indexes:
									motif = "%s-%s" % (string, index)							
									motifs.add(motif)
	print "Done: %s possible motifs\n" % len(motifs)
	return motifs

def sub_bases( motif ):
	subs = {"W":"[AT]",  \
			"S":"[CG]",  \
			"M":"[AC]",  \
			"K":"[GT]",  \
			"R":"[AG]",  \
			"Y":"[CT]",  \
			"B":"[CGT]", \
			"D":"[AGT]", \
			"H":"[ACT]", \
			"V":"[ACG]", \
			"N":"[ACGT]"}
	for symbol,sub in subs.iteritems():
		if motif.find(symbol) > -1:
			motif = motif.replace(symbol, sub)
	return motif

def rev_comp_motif( motif ):
	"""
	
	"""
	COMP = {"A":"T", \
			"T":"A", \
			"C":"G", \
			"G":"C", \
			"W":"S", \
			"S":"W", \
			"M":"K", \
			"K":"M", \
			"R":"Y", \
			"Y":"R", \
			"B":"V", \
			"D":"H", \
			"H":"D", \
			"V":"B", \
			"N":"N", \
			"X":"X", \
			"*":"*"}
	rc_motif = []
	for char in motif[::-1]:
		rc_motif.append( COMP[char] )
	return "".join(rc_motif)

def find_motif_matches( motif, read_str ):
	q_motif = sub_bases( rev_comp_motif(motif) )
	matches_iter = re.finditer(q_motif, read_str)

	matches_list = []
	for match in matches_iter:
		matches_list.append(match)
	return matches_list

def motif_ipds( read_str, read_ipds, motifs, max_kmer ):
	"""
	"""
	motifs_file = None
	skip_motifs = None
	mod_bases   = ["A"]

	subread_ipds = {}
	if motifs_file != None:
		for motif in motifs:
			if motif.find("-") == -1:
				raise Exception("Specify the position of the modified base in your supplied motifs (0-based)")
			ref_index = int(motif[-1])
			motif     =     motif[:-2]
			rc_index  = len(motif) - 1 - ref_index

			matches_list = find_motif_matches( motif, read_str )

			motif_ipds = []
			for match in matches_list:
				motif_start = match.span()[0]
				motif_end   = match.span()[1]
				motif_ipds.append( read_ipds[motif_start:motif_end] )
			
			ipds = map(lambda x: x[rc_index], motif_ipds)
			ref_motif_str = "%s-%s" % (motif, ref_index)
			subread_ipds[ref_motif_str] = ipds
	else:
		# Instatiate a motif IPD dict where the default value is 0
		for motif in motifs:
			subread_ipds[motif] = []

		if skip_motifs != None:
			# We want to first remove these bases in the read and ref (and associated IPDs)
			motifs_to_del = []
			for line in open(skip_motifs).readlines():
				motif = line.strip()
				motifs_to_del.append(motif)
				motifs_to_del = map(lambda x: x.split("-")[0], motifs_to_del)
				try:
					del subread_ipds[motif]
				except KeyError:
					pass
			for motif in motifs_to_del:
				matches_list = find_motif_matches( motif, read_str )
				motif_ipds = []
				for match in matches_list:
					# Remove the data associated with this motif instance
					motif_start = match.span()[0]
					motif_end   = match.span()[1]
					read_str    = "".join([read_str[:motif_start], "X"*len(motif), read_str[motif_end:]])
					read_ipds   = read_ipds[:motif_start] + ([100.0]*len(motif)) + read_ipds[motif_end:]

		# Loop over all possible motif sizes
		for k in range( 4, max_kmer+1 ):
			print "  - Scanning %s-mers..." % k
			# Loop over each position in the read string
			for j in range( len(read_str)-k ):
				# if j%1000000==0: print "    ...%.1f Mb" % (float(j)/1000000)
				read_motif  = read_str[j:j+k]
				ref_motif   = rev_comp_motif( read_motif )
				ipds        = read_ipds[j:j+k]

				if ref_motif.find("*") == -1 and ref_motif.find("X") == -1:
					for base in mod_bases:
						ref_indexes = [m.start() for m in re.finditer(base, ref_motif)]
						for ref_index in ref_indexes:
							rc_index = len(ref_motif) - 1 - ref_index
							
							IPD = ipds[rc_index]

							ref_motif_str = "%s-%s" % (ref_motif, ref_index)
							subread_ipds[ref_motif_str].append( IPD )
	return subread_ipds

def subread_remove_lowQV( seq, ipds, qvs ):
	minQV      = 4
	base_calls = np.array(list(seq))
	fps        = 75.0
	ipds       = np.array(ipds) / fps

	for i,val in enumerate(qvs):
		if val < minQV:
			base_calls[i] = "*"
			ipds[i]       = 99999
	return "".join(list(base_calls)), ipds, qvs

def subread_normalize( ipds ):
	"""
	Every IPD entry needs to be normalized by the mean IPD of its subread.
	"""
	# rawIPDs = np.array(map(lambda x: math.log(x + 0.001), ipds))
	rawIPDs = np.array([math.log(x + 0.001) for x in ipds if x!=99999])
	nfs     = rawIPDs.mean()

	normed_ipds = []
	for ipd in ipds:
		if ipd!=99999:
			normed_ipds.append(math.log(ipd + 0.001) - nfs)
		else:
			normed_ipds.append(99999)
	return normed_ipds

def compute_score( ipds ):
	mean = np.mean(ipds)
	return mean

max_kmer   = 6
motifs     = build_motif_dict( max_kmer )
basedir    = os.getcwd()
flat_files = np.loadtxt(flat_files_fofn, dtype="str")[:,0]

os.chdir(cluster_dir)
cluster_dir = os.getcwd()

k_dirs = glob.glob("k_*_tmp")
sort_nicely(k_dirs)
for k_dir in k_dirs:
	print "Isolating cluster %s reads..." % k_dir
	os.chdir(k_dir)
	mols_fn = "%s.readnames" % k_dir
	mols    = open(mols_fn, "r").xreadlines()
	mols    = set(map(lambda line: line.strip("\n"), mols))

	# Pull the reads from the flat reads files to create a cluster-specific version
	print "  - Writing cluster-specific flat file..."
	fn = "%s_data.txt" % k_dir
	# f  = open(fn, "w")
	# for flat_file in flat_files:
	# 	for line in open(os.path.join(basedir,flat_file)).xreadlines():
	# 		mol = "/".join(line.split("/")[:2])
	# 		if mol in mols:
	# 			f.write(line)
	# f.close()

	cluster_seq_l  = []
	cluster_ipds_l = []
	cluster_qvs_l  = []
	for i,line in enumerate(open(fn, "r").xreadlines()):
		seq          = line.split(" ")[4]
		ipds         = map(lambda x: float(x), line.split(" ")[5].split(","))
		qvs          = map(lambda x: int(x),   line.split(" ")[6].split(","))
		
		seq, ipds, qvs = subread_remove_lowQV( seq, ipds, qvs)
		normed_ipds    = subread_normalize( ipds )
		cluster_seq_l.append(seq)
		cluster_ipds_l += normed_ipds
	print "  - Scanning cluster motif scores..."
	cluster_ipds = motif_ipds( "".join(cluster_seq_l), cluster_ipds_l, motifs, max_kmer )

	cluster_barcode = {}
	cluster_counts  = {}
	for motif,ipds in cluster_ipds.iteritems():
		if len(ipds) == 0:
			score = 0.0
		else:
			score = compute_score( ipds )
		cluster_barcode[motif] = score
		cluster_counts[motif]  = len(ipds)
	
	sorted_x = sorted(cluster_barcode.items(), key=operator.itemgetter(1))
	for (motif,score) in sorted_x[-15:]:
		print motif, score, cluster_counts[motif]

	os.chdir(cluster_dir)