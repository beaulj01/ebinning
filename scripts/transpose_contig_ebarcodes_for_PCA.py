import os,sys
import numpy as np
from itertools import groupby

ebarcodes_fn = sys.argv[1]

motifs = set()
for line in open(ebarcodes_fn).xreadlines():
	motif = line.split("\t")[3]
	motifs.add(motif)
motifs = list(motifs)

# ref000178       C227_contig_0   100.0   ACCACC-3        13      1.75231873509
# ref000178       C227_contig_0   100.0   GATC-1  6       2.23899249317
# ref000175       J109_contig_0   96.8    CTGCAG-1        119     -0.72378582863
# ref000175       J109_contig_0   96.8    ACCACC-3        167     0.293812562592
# ref000175       J109_contig_0   96.8    GATC-1  1081    2.2795222784
# ref000175       J109_contig_0   96.8    CTGCAG-4        118     -0.564446989729
# ref000171       J109_contig_0   74.6    GATC-1  43      2.06139027248

contig_iter = [ (line.split("\t")[0], line.strip().split("\t")[1:]) for line in open(ebarcodes_fn).xreadlines()]

header = "label"
for motif in motifs:
	header += "\t%s" % motif
print header

for contig,group in groupby(contig_iter, lambda x: x[0]):
	motif_SCp = {}
	for motif_entry in group:
		vals   = motif_entry[1]
		label  =       vals[0]
		conf   = float(vals[1])
		motif  =       vals[2]
		count  =   int(vals[3])
		SCp    = float(vals[4])
		if motif in motifs:
			motif_SCp[motif] = SCp
	entry    = label
	has_vals = False
	for motif in motifs:
		try:
			entry += "\t%s" % motif_SCp[motif]
			has_vals = True
		except KeyError:
			# pass
			entry += "\t%s" % 0
	if has_vals:
		print entry
