import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt

read_cnt_fn = sys.argv[1]

contig_unamb_cts = {}
contig_amb_cts   = {}
contig_lens      = {}
tot_reads        = {}
unamb_pcts       = {}
for i,line in enumerate(open(read_cnt_fn).xreadlines()):
	if i==0:
		header       = line.strip().split("\t")[1:]
		contig_ids   = map(lambda x: "_".join(x.split("_")[3:]), header)
		contig_names = [contig for contig in contig_ids[:len(contig_ids)/2]]
	else:
		line         = line.strip()
		contig       = line.split("\t")[0]
		counts       = line.split("\t")[1:]
		unamb_counts = map(lambda x: int(x), counts[:len(contig_ids)/2])
		amb_counts   = map(lambda x: int(x), counts[len(contig_ids)/2:])
		tot_reads[contig] = sum(map(lambda x: int(x), line.split("\t")[1:]))
		if tot_reads[contig] == 0:
			continue
		contig_unamb_cts[contig] = {}
		contig_amb_cts[contig]   = {}
		contig_len   = int(contig.split("_")[3])
		if contig_len>1000:
		# if contig_len>1:
			contig_lens[contig] = contig_len
		for j,name in enumerate(contig_names):
			contig_unamb_cts[contig][name] = unamb_counts[j]
			contig_amb_cts[contig][name]   = amb_counts[j]
		max_unamb = max(contig_unamb_cts[contig].values())
		pct_unamb = float(max_unamb)/tot_reads[contig]
		unamb_pcts[contig] = pct_unamb

FIG_SIZE = (8,8)
fig      = plt.figure(figsize=FIG_SIZE)

ax = fig.add_subplot(211)
X  = []
Y  = []
for contig in contig_lens.keys():
	X.append(contig_lens[contig])
	Y.append(unamb_pcts[contig])
ax.scatter(X,Y)
ax.set_xlim([0,max(X)])
# ax.set_xscale("log")
ax.set_xlabel("Contig length")
ax.set_ylabel("Mapping confidence")

ax       = fig.add_subplot(212)
nbins    = 50
ax.hist(contig_lens.values(), bins=nbins, color="r", linewidth=0)
ax.set_xlabel("Contig length")
ax.set_ylabel("Count")
plt.savefig("contig_stats.png")