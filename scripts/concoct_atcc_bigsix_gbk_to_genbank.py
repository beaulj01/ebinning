import os,sys
from Bio import SeqIO

gbk_fn = sys.argv[1]

strand_convert = {1:"+", -1:"-"}
for record in SeqIO.parse(open(gbk_fn,"r"), "genbank") :
	for feature in record.features:
		if feature.type=="CDS":
			print "%s\t%s\t%s\t%s\t%s" % (record.id, feature.location.nofuzzy_start, feature.location.nofuzzy_end, strand_convert[feature.strand], feature.qualifiers["product"][0])