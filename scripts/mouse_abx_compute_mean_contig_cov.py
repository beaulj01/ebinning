import os,sys
import numpy as np
from Bio import SeqIO

cov_bed = np.loadtxt(sys.argv[1], dtype="str", skiprows=1)
fasta   = sys.argv[2]

def get_contig_mean_cov( name, cov_bed ):
	name      = name.split("|")[0]
	cov_names = cov_bed[:,0]
	mask      = cov_names==name
	covs      = cov_bed[mask,4].astype(float)
	# covs      = covs[2:-2]
	return covs.mean()

entries = []
for entry in SeqIO.parse(fasta, "fasta"):
	entry.cov = get_contig_mean_cov( entry.name, cov_bed )
	entries.append( (entry.name, len(entry.seq), entry.cov) )

low_cov_bp = 0
i = 0
s = sorted(entries, key=lambda x: x[2]) 
for (contig,size,cov) in s:
	# print "%s\t%s\t%.2f" % (contig, size, cov)
	print "%s\t%.2f" % (contig, cov)
	if cov < 100:
		low_cov_bp += size
		i += 1
