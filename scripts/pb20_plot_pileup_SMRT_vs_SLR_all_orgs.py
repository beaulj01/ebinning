import pysam
import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from collections import defaultdict
import numpy as np
import pub_figs
import itertools
import operator
import subprocess
import matplotlib.gridspec as gridspec

def run_OS( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts != 0:
		print "Failed command: %s" % CMD
	return sts, stdOutErr

def get_tot_bases( bam ):
	f      = pysam.AlignmentFile(bam, "rb")
	pileup = f.pileup()
	return np.sum([p.n for p in pileup])

def downsample_bam( orig_fn, target_frac, tech, seed=1 ):
	down_fn         = "%s.%s.down.%s.bam" % (orig_fn.split(".sorted")[0], tech, target_frac)
	target_frac_str = str(target_frac).split(".")[1]
	CMD             = "samtools view -s %s.%s -h -b %s > %s" % (seed, target_frac_str, orig_fn, down_fn)
	print CMD
	if not os.path.exists(down_fn):
		sts, stdOutErr  = run_OS( CMD )
	if not os.path.exists(down_fn+".bai"):
		pysam.index(down_fn, "%s.bai" % down_fn)
	
	down_y = get_tot_bases(down_fn)
	
	print "downsampling:",tech, "frac:",target_frac, "new_yield:",down_y, orig_fn.split(os.sep)[-1]
	return down_fn, down_y

def format_axes( ax, bounds=[0,1000000000] ):
	tick_labs  = []
	for n in range(bounds[0],bounds[1]+500000, 500000):
		tick_labs.append( "%.2f" % (float(n)/1000000) )
	ax.set_xticks(range(bounds[0],bounds[1]+500000, 500000))
	ax.set_xticklabels(tick_labs)
	xticks = ax.xaxis.get_major_ticks()
	xticks[0].set_visible(False)
	ax.set_xlim([(bounds[0]-2000),(bounds[1]+2000)])
	ax.locator_params(axis="y", nbins=4)

org_fns_SMRT = {"A_baumannii":     "A_baumannii_ATCC_17978", \
				"A_odontolyticus": "A_odontolyticus_ATCC_17982_Scfld021", \
				"B_cereus":        "B_cereus_ATCC_10987", \
				"B_vulgatus":      "B_vulgatus_ATCC_8482", \
				"C_beijerinckii":  "C_beijerinckii_NCIMB_8052", \
				"D_radiodurans":   "D_radiodurans_R1", \
				"E_faecalis":      "E_faecalis_OG1RF", \
				"E_coli":          "E_coli_str_K_12_substr_MG1655", \
				"H_pylori":        "H_pylori_26695", \
				"L_gasseri":       "L_gasseri_ATCC_33323", \
				"L_monocytogenes": "L_monocytogenes_EGD_e", \
				"N_meningitidis":  "N_meningitidis_MC58", \
				"P_acnes":         "P_acnes_KPA171202", \
				"P_aeruginosa":    "P_aeruginosa_PAO1", \
				"R_sphaeroides":   "R_sphaeroides_2_4_1", \
				"S_aureus":        "S_aureus_subsp_aureus_USA300_TCH1516", \
				"S_epidermidis":   "S_epidermidis_ATCC_12228", \
				"S_agalactiae":    "S_agalactiae_2603V_R", \
				"S_mutans":        "S_mutans_UA159", \
				"S_pneumoniae":    "S_pneumoniae_TIGR4"} 

org_fns_SLR  = {"A_baumannii":     "A_baumannii_ATCC_17978", \
				"A_odontolyticus": "A_odontolyticus_ATCC_17982_Scfld021", \
				"B_cereus":        "B_cereus_ATCC_10987", \
				"B_vulgatus":      "B_vulgatus_ATCC_8482", \
				"C_beijerinckii":  "C_beijerinckii_NCIMB_8052", \
				"D_radiodurans":   "D_radiodurans_R1", \
				"E_faecalis":      "E_faecalis_OG1RF", \
				"E_coli":          "E_coli_str_K-12_substr_MG1655", \
				"H_pylori":        "H_pylori_26695", \
				"L_gasseri":       "L_gasseri_ATCC_33323", \
				"L_monocytogenes": "L_monocytogenes_EGD-e", \
				"N_meningitidis":  "N_meningitidis_MC58", \
				"P_acnes":         "P_acnes_KPA171202", \
				"P_aeruginosa":    "P_aeruginosa_PAO1", \
				"R_sphaeroides":   "R_sphaeroides_2.4.1", \
				"S_aureus":        "S_aureus_subsp_aureus_USA300_TCH1516", \
				"S_epidermidis":   "S_epidermidis_ATCC_12228", \
				"S_agalactiae":    "S_agalactiae_2603V_R", \
				"S_mutans":        "S_mutans_UA159", \
				"S_pneumoniae":    "S_pneumoniae_TIGR4"} 

labels = {"A_odontolyticus":r"\textit{A. odontolyticus} ATCC 17982", \
		  "D_radiodurans":	r"\textit{D. radiodurans} R1", \
		  "P_aeruginosa":	r"\textit{P. aeruginosa} PAO1", \
		  "R_sphaeroides":	r"\textit{R. sphaeroides} 2.4.1", \
		  "A_baumannii": 	r"\textit{A. baumannii} ATCC 17978", \
		  "B_cereus": 		r"\textit{B. cereus} ATCC 10987", \
		  "B_vulgatus": 	r"\textit{B. vulgatus} ATCC 8482", \
		  "C_beijerinckii": r"\textit{C. beijerinckii} NCIMB 8052", \
		  "E_coli": 		r"\textit{E. coli} str. K-12 substr. MG1655", \
		  "E_faecalis": 	r"\textit{E. faecalis} OG1RF", \
		  "H_pylori": 		r"\textit{H. pylori} 26695", \
		  "L_gasseri": 		r"\textit{L. gasseri} ATCC 33323", \
		  "L_monocytogenes":r"\textit{L. monocytogenes} EGD-e", \
		  "N_meningitidis": r"\textit{N. meningitidis} MC58", \
		  "P_acnes": 		r"\textit{P. acnes KPA171202", \
		  "S_agalactiae": 	r"\textit{S. agalactiae} 2603V R", \
		  "S_aureus": 		r"\textit{S. aureus} subsp. aureus USA300 TCH1516", \
		  "S_epidermidis": 	r"\textit{S. epidermidis} ATCC 12228", \
		  "S_mutans": 		r"\textit{S. mutans} UA159", \
		  "S_pneumoniae": 	r"\textit{S. pneumoniae} TIGR4"}

orgs = org_fns_SMRT.keys()
orgs.sort()

SLR_cov = {"A_baumannii" : 	10894845, \
	    "A_odontolyticus" : 3730113, \
	    "B_cereus" : 		26483461, \
	    "B_vulgatus" : 		3579005, \
	    "C_beijerinckii" : 	11135316, \
	    "D_radiodurans" : 	3983663, \
	    "E_faecalis" : 		11270120, \
	    "E_coli" : 			2133476693, \
	    "H_pylori" : 		26015813, \
	    "L_gasseri" : 		10760149, \
	    "L_monocytogenes" : 24364014, \
	    "N_meningitidis" : 	15910092, \
	    "P_acnes" : 		26717866, \
	    "P_aeruginosa" : 	170029436, \
	    "R_sphaeroides" : 	29901273, \
	    "S_aureus" : 		61148568, \
	    "S_epidermidis" : 	173408151, \
	    "S_agalactiae" : 	49104157, \
	    "S_mutans" : 		252711874, \
	    "S_pneumoniae" : 	23107608}

SMRT_cov = {"A_baumannii" : 458337257, \
	    "A_odontolyticus" : 390178052, \
	    "B_cereus" : 		101134305, \
	    "B_vulgatus" : 		744355850, \
	    "C_beijerinckii" : 	518725397, \
	    "D_radiodurans" : 	112809810, \
	    "E_faecalis" : 		388729626, \
	    "E_coli" : 			607875898, \
	    "H_pylori" : 		1637499690, \
	    "L_gasseri" : 		436941561, \
	    "L_monocytogenes" : 770815426, \
	    "N_meningitidis" : 	448123214, \
	    "P_acnes" : 		533794211, \
	    "P_aeruginosa" : 	1072961775, \
	    "R_sphaeroides" : 	363229532, \
	    "S_aureus" : 		616183706, \
	    "S_epidermidis" : 	444655487, \
	    "S_agalactiae" : 	277523113, \
	    "S_mutans" : 		506964576, \
	    "S_pneumoniae" : 	76800567}

SMRT_base = "/hpc/users/beaulj01/projects/ebinning/pacbio20/all7_sets/aligns_to_refs"
SLR_base  = "/hpc/users/beaulj01/projects/ebinning/pacbio20/SLR/align_to_refs"

SMRT_even_yield_bams = {}
SLR_even_yield_bams  = {}
for i,org in enumerate(orgs):
	SLR_bam  = os.path.join(SLR_base, "%s.sorted.bam" % org_fns_SLR[org])
	SMRT_bam = os.path.join(SMRT_base, org_fns_SMRT[org], "%s.sorted.bam" % org_fns_SMRT[org])
	
	min_idx      = np.argmin([SMRT_cov[org], SLR_cov[org]])
	target_yield = [SMRT_cov[org], SLR_cov[org]][min_idx]

	if min_idx==0:
		# Downsample SLR
		tech                      = "SLR"
		target_frac               = round(float(target_yield) / SLR_cov[org], 4)
		down_bam_fn,down_y        = downsample_bam( SLR_bam, target_frac, tech )
		SLR_cov[org]              = down_y
		SLR_even_yield_bams[org]  = down_bam_fn
		SMRT_even_yield_bams[org] = os.path.join(SMRT_base, org_fns_SMRT[org], "%s.sorted.bam" % org_fns_SMRT[org])
	else:
		# Downsample SMRT
		tech                      = "SMRT"
		target_frac               = round(float(target_yield) / SMRT_cov[org], 4)
		down_bam_fn,down_y        = downsample_bam( SMRT_bam, target_frac, tech )
		# Update yield with downsampled value
		SMRT_cov[org]             = down_y
		SLR_even_yield_bams[org]  = os.path.join(SLR_base, "%s.sorted.bam" % org_fns_SLR[org])
		SMRT_even_yield_bams[org] = down_bam_fn

def get_window_covs(bamfile):
	pileup = bamfile.pileup()
	# Build bit array to track non-zero cov
	pos_array        = np.zeros(np.sum(bamfile.lengths))
	x                = []
	y                = []	
	windowsize       = 1
	window_vals      = defaultdict(list)
	for p in pileup:
		pos  = p.pos
		cov  = p.n
		rpos = p.reference_pos
		rid  = p.reference_id
		if p.reference_id==1:
			pos += bamfile.lengths[0]
		window_id = pos/windowsize
		window_vals[window_id].append(cov)
		# print rid, pos, window_id, cov
		if cov>0:
			pos_array[pos] = 1

	cov_pct   = 100*float(pos_array.sum()) / np.sum(bamfile.lengths)


	# Compile list of no-coverage intervals
	no_cov_intervals = []
	no_cov_blocks    = [[i for i,value in it] for key,it in \
						itertools.groupby(enumerate(pos_array), key=operator.itemgetter(1)) if key==0]
	for block in no_cov_blocks:
		no_cov_intervals.append( (block[0], block[-1]) )

	for i in range(np.sum(bamfile.lengths)/windowsize+1):
		if len(window_vals[i])==0:
			mean = 0
		elif len(window_vals[i])==1:
			mean = window_vals[i][0]
		else:
			mean = np.mean(window_vals[i])
		pos  = i * windowsize
		# print i, pos, mean
		x.append(pos)
		y.append(mean)

	return np.array(x),np.array(y),no_cov_intervals

font_size  = 14
fig        = pub_figs.init_fig(x=18,y=24)
outer_grid = gridspec.GridSpec(10, 2)
outer_grid.update(left=0.05, right=0.98, top=0.95, bottom=0.04, hspace=0.6, wspace=0.2)

print "Generating plots..."
bot_SMRTs  = []
mean_SMRTs = []
top_SMRTs  = []
bot_SLRs   = []
mean_SLRs  = []
top_SLRs   = []
for i,org in enumerate(orgs):
	print i, org

	row        = i/2
	col        = i%2
	subcell    = outer_grid[row,col]
	inner_grid = gridspec.GridSpecFromSubplotSpec(2,1, subcell, hspace=0.6)
	ax1        = plt.subplot(inner_grid[0,0])
	ax2        = plt.subplot(inner_grid[1,0])

	# SLR
	bam         = SLR_even_yield_bams[org]
	bamfile     = pysam.AlignmentFile(os.path.join(SLR_base, bam), "rb")
	genome_size = np.sum(bamfile.lengths)
	bounds      = [0,genome_size]
	tot_bases   = SLR_cov[org]
	x_SLR,y_SLR,no_cov_intervals = get_window_covs(bamfile)
	ax1.plot(x_SLR, y_SLR, linestyle="-", color="r", linewidth=1.5)
	for x in no_cov_intervals:
		ax1.axvspan(x[0], x[1], color='pink')
	format_axes( ax1, bounds )
	ax1.set_title(labels[org])
	if i==0:
		# draw temporary red and blue lines and use them to create a legend
		hR,  = ax1.plot([1,1],'r-', linewidth=3)
		hB,  = ax1.plot([1,1],'b-', linewidth=3)
		hR1, = ax1.plot([1,1], color="pink",      linewidth=20)
		hB1, = ax1.plot([1,1], color="lightblue", linewidth=20)
		ax1.legend((hR, hR1, hB, hB1 ),('SLR', 'No SLR coverage', 'SMRT', 'No SMRT coverage'), ncol=2, loc='lower left', bbox_to_anchor=(0.8, 1.3), frameon=False, fontsize=font_size)
		hB.set_visible(False)
		hR.set_visible(False)
		hB1.set_visible(False)
		hR1.set_visible(False)

	# SMRT
	bam       = SMRT_even_yield_bams[org]
	bamfile   = pysam.AlignmentFile(os.path.join(SMRT_base, org_fns_SMRT[org], bam), "rb")
	tot_bases = SMRT_cov[org]
	x_SMRT,y_SMRT,no_cov_intervals = get_window_covs(bamfile)
	ax2.plot(x_SMRT, y_SMRT, linestyle="-", color="b", linewidth=1.5)
	for x in no_cov_intervals:
		ax2.axvspan(x[0], x[1], color='lightblue')
	format_axes( ax2, bounds )
	ax2.set_xlabel("Genome position (Mb)")
	ax2.set_ylabel("Coverage")
	ax2.yaxis.set_label_coords(-0.08, 1.4)

	# Plot top/bottom N% quantiles for each
	pct = 0.1
	# bot_SLRs.append(y_SLR[ y_SLR <= np.percentile(y_SLR, pct)  ].mean())
	# mean_SLRs.append( y_SLR.mean() )
	# top_SLRs.append(y_SLR[ y_SLR >= np.percentile(y_SLR, (100-pct)) ].mean())
	# bot_SMRTs.append(y_SMRT[ y_SMRT <= np.percentile(y_SMRT, pct)  ].mean())
	# mean_SMRTs.append( y_SMRT.mean() )
	# top_SMRTs.append(y_SMRT[ y_SMRT >= np.percentile(y_SMRT, (100-pct)) ].mean())
	bot_SLRs.append(np.percentile(y_SLR, pct))
	mean_SLRs.append( y_SLR.mean() )
	top_SLRs.append(np.percentile(y_SLR, (100-pct)))
	bot_SMRTs.append(np.percentile(y_SMRT, pct))
	mean_SMRTs.append( y_SMRT.mean() )
	top_SMRTs.append(np.percentile(y_SMRT, (100-pct)))

fontProperties = pub_figs.setup_math_fonts(font_size)
plt.locator_params(axis='y',nbins=3)
plot_fn = "SLR_vs_SMRT_cov_lines.png"
plt.savefig(plot_fn)

fig_quants = pub_figs.init_fig(x=15,y=12)
ax_x = 0.20
ax_y = 0.45
ax_w = 0.7
ax_h = 0.37
ax_q       = fig_quants.add_axes([ax_x, ax_y, ax_w, ax_h])
pub_figs.setup_math_fonts(24)
idx        = np.arange(len(orgs))

# Sort by SMRT top - bottom differences
SMRT_diffs = np.array(top_SMRTs) - np.array(bot_SMRTs)
didx       = SMRT_diffs.argsort()
bot_SLRs   = np.array(bot_SLRs)[didx]
mean_SLRs  = np.array(mean_SLRs)[didx]
top_SLRs   = np.array(top_SLRs)[didx]
bot_SMRTs  = np.array(bot_SMRTs)[didx]
mean_SMRTs = np.array(mean_SMRTs)[didx]
top_SMRTs  = np.array(top_SMRTs)[didx]
orgs       = np.array(orgs)[didx]

slr_tops  = ax_q.scatter(idx-0.1, top_SLRs,  color="r", marker="s", s=100)
slr_bots  = ax_q.scatter(idx-0.1, bot_SLRs,  color="r", marker="o", s=100)
smrt_tops = ax_q.scatter(idx+0.1, top_SMRTs, color="b", marker="s", s=100)
smrt_bots = ax_q.scatter(idx+0.1, bot_SMRTs, color="b", marker="o", s=100)
SLR_Xs  = idx - 0.1
SMRT_Xs = idx + 0.1 
for i in range(len(orgs)):
	slr_mids,  = ax_q.plot([SLR_Xs[i], SLR_Xs[i]],  [bot_SLRs[i],top_SLRs[i]],   color="r", linestyle="--", linewidth=3)
	smrt_mids, = ax_q.plot([SMRT_Xs[i],SMRT_Xs[i]], [bot_SMRTs[i],top_SMRTs[i]], color="b", linestyle="--", linewidth=3)
ax_q.set_ylabel("Coverage")
ax_q.set_xticks(range(len(orgs)))
ax_q.set_xticklabels(map(lambda x: labels[x], orgs), rotation=45, ha="right")
ax_q.set_xlim([-1, len(orgs)])
ax_q.set_ylim([-2, 1.05*max(max(top_SLRs), max(top_SMRTs))])
ax_q.grid(True)
pub_figs.remove_top_right_axes(ax_q)
# ax_q.legend(loc='lower left', bbox_to_anchor=(1.0, 0.5), frameon=False, fontsize=16, scatterpoints=1)
ax_q.legend([slr_tops, slr_mids, slr_bots, smrt_tops, smrt_mids, smrt_bots ], \
			[r"SLR: 99.9th percentile", pub_figs.tex_escape(r"SLR: middle 99.8%"),  r"SLR: 0.1st percentile", \
			r"SMRT: 99.9th percentile", pub_figs.tex_escape(r"SMRT: middle 99.8%"), r"SMRT: 0.1st percentile"], 
			ncol=2, loc='lower center', bbox_to_anchor=(0.5, 0.96), frameon=False, scatterpoints=1, fontsize=24)
fontProperties = pub_figs.setup_math_fonts(24)
fig_quants.savefig("SLR_vs_SMRT_quant.%s.png" % pct)