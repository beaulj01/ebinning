import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.ticker import MultipleLocator
import subprocess
import numpy as np
from collections import defaultdict,Counter
import operator
import pub_figs
from pbcore.io.align.CmpH5IO import CmpH5Reader
import math

font_s = 20
pub_figs.setup_math_fonts(font_size=font_s, font="Computer Modern Sans serif")
# fontProperties = pub_figs.setup_math_fonts(16)

combined_cmph5_fns = ["/hpc/users/beaulj01/projects/ebinning/2HP/combined_HGAP/J99_150x_and_26695_150x/gt5kb_only/data/aligned_reads.cmp.h5"]

assembly_names  = ["J99_150x_and_26695_150x"]

assembly_labels = {"J99_150x_and_26695_150x": "150x 26695, 150x J99"}

movies_spec = {"m140130_050607_42177R_c100623362550000001823111308061481_s1_p0" : "J99", \
			   "m140130_082505_42177R_c100623362550000001823111308061482_s1_p0" : "26695"}

# First row
fig = plt.figure(figsize=[6,6])
ax  = fig.add_axes([0.16, 0.16, 0.8, 0.8])
for i,cmph5 in enumerate(combined_cmph5_fns):
	contigs_counters = {}
	contig_lens      = {}
	seen_contigs     = set()
	reader           = CmpH5Reader(cmph5)

	for entry in reader.referenceInfoTable:
		contig_lens[entry[3]] = entry[4]

	for r in reader:
		movie     = r.movieName
		read_spec = movies_spec[movie]
		contig    = r.referenceName
		if contig not in seen_contigs:
			seen_contigs.add(contig)
			contigs_counters[contig] = Counter()
		contigs_counters[contig][read_spec] += 1

	sorted_contig_lens = sorted(contig_lens.items(), key=operator.itemgetter(1), reverse=True)

	x     = []
	y     = []
	color = []
	area  = []
	for j,(contig,length) in enumerate(sorted_contig_lens):
		tot_reads       = np.sum(contigs_counters[contig].values())
		reads_pct_J99   = float(contigs_counters[contig]["J99"])/tot_reads
		reads_pct_26695 = float(contigs_counters[contig]["26695"])/tot_reads
		# print "%s  \t%.3f\t%.3f\t%s\t%s" % (contig, reads_pct_J99, reads_pct_26695, tot_reads, length)
		x.append(100*reads_pct_J99)
		y.append(j+1)
		color.append(plt.get_cmap('cool')(reads_pct_J99))
		area.append(math.sqrt(length)*2)
	
	sct = ax.scatter(x, y, color="gray", s=area, linewidths=2, edgecolor='k', alpha=0.6)
	# sct = ax.scatter(x, y, c=color, s=area, linewidths=2, edgecolor='w')
	sct.set_alpha(0.75)
	ax.set_xlim([-10,110])
	ax.set_ylim([0,4])
	ax.set_xticks(range(0,105,25))
	ax.set_yticks(range(0,5,1))
	pub_figs.remove_top_right_axes(ax)
	ax.minorticks_on()
	ax.tick_params('both', length=10, width=2, which='major')
	ax.tick_params('both', length=5, width=1, which='minor')
	ax.tick_params(pad=10, labelsize=font_s)
	ax.set_xlabel(r"$\%$ J99 reads in contig", fontsize=font_s)
	ax.set_ylabel('Contig count', fontsize=font_s)
	xticks = ax.xaxis.get_minor_ticks()
	yticks = ax.yaxis.get_minor_ticks()
	xticks[0].set_visible(False)
	xticks[-1].set_visible(False)
	yticks[0].set_visible(False)
	yticks[-1].set_visible(False)
	plt.grid()

plt.savefig("HP_mixed_assemblies_summary.png", dpi=300)

# Second row
fig = plt.figure(figsize=[6,6])
ax  = fig.add_axes([0.16, 0.16, 0.8, 0.8])

PCA      = "reads.SMp.gt5000.PCA"
labels   = "reads.raw.labels.gt5000"
names    = "reads.raw.names.gt5000"

combined_dirs    = ["/hpc/users/beaulj01/projects/ebinning/2HP/combined_HGAP/J99_150x_and_26695_150x/binning_bas"]

label_dict = {"26695": r"${H. pylori}$ 26695", \
			  "J99":   r"${H. pylori}$ J99"}
plt.hot()
for d,dir_name in enumerate(combined_dirs):
	pca_fn      = os.path.join(dir_name, PCA)
	read_labs   = os.path.join(dir_name, labels)
	read_names  = os.path.join(dir_name, names)
	SMp_coords  = np.loadtxt(pca_fn,     dtype="float")
	read_labels = np.loadtxt(read_labs,  dtype="str")
	read_names  = np.loadtxt(read_names, dtype="str")
	m    = 0.8
	b    = -1.5
	xmin = 0
	xmax = 4
	ymin = -2
	ymax = 2.2
	extent            = [xmin, xmax, ymin, ymax]
	H, xedges, yedges = np.histogram2d(SMp_coords[:,0], SMp_coords[:,1], bins=25, range=[extent[:2], extent[2:]])
	H       = np.rot90(H)
	H       = np.flipud(H)
	Hmasked = np.ma.masked_where(H==0,H)         # Mask pixels with a value of zero
	im      = plt.imshow(H, interpolation='bilinear', origin='lower', cmap=cm.Greys, extent=extent) # Sets background image
	levels  = [25,50,100,200]
	cols    = ["k","0.4","0.6","0.8"]
	cset    = plt.contour(H, levels, origin="lower",colors=cols,linewidths=1.4,extent=extent)
	# pl t.clabel(cset, inline=1, fontsize=10, fmt="%1.0i")
	for c in cset.collections:
		c.set_linestyle("solid")
	x1 = np.array(range(xmin,xmax+1)) 
	y1 = eval("m*x1 + b")
	plt.plot(x1, y1, linestyle="--", color="k", linewidth=3.0)
	ax.set_xlabel("PC1", fontsize=font_s)
	ax.set_ylabel("PC2", fontsize=font_s)
	ax.set_xticks(range(xmin,xmax+1,1))
	ax.set_yticks(range(-2,3,1))
	ax.tick_params(pad=10, labelsize=font_s)
	pub_figs.remove_top_right_axes(ax)

# plt.tight_layout()
plt.savefig("HP_PCA_splitting.png", dpi=300)

# Third row
iso_cmph5_base_dirs = ["/hpc/users/beaulj01/projects/ebinning/2HP/combined_HGAP/J99_150x_and_26695_150x/iso_HGAPs"]

fig = plt.figure(figsize=[6,6])
for i,iso_cmph5_base_dir in enumerate(iso_cmph5_base_dirs):
	ax  = fig.add_axes([0.16, 0.16, 0.8, 0.8])

	contigs_counters = {}
	contig_lens      = {}
	seen_contigs     = set()
	for strain in ["26695", "J99"]:
		cmph5 = os.path.join(iso_cmph5_base_dir, strain, "data", "aligned_reads.cmp.h5")
		print cmph5
		reader           = CmpH5Reader(cmph5)

		for entry in reader.referenceInfoTable:
			contig_lens[entry[3] + ".%s" % strain] = entry[4]

		for r in reader:
			movie     = r.movieName
			read_spec = movies_spec[movie]
			contig    = r.referenceName + ".%s" % strain
			if contig not in seen_contigs:
				seen_contigs.add(contig)
				contigs_counters[contig] = Counter()
			contigs_counters[contig][read_spec] += 1

	sorted_contig_lens = sorted(contig_lens.items(), key=operator.itemgetter(1), reverse=True)

	x     = []
	y     = []
	color = []
	area  = []
	for j,(contig,length) in enumerate(sorted_contig_lens):
		tot_reads       = np.sum(contigs_counters[contig].values())
		reads_pct_J99   = float(contigs_counters[contig]["J99"])/tot_reads
		reads_pct_26695 = float(contigs_counters[contig]["26695"])/tot_reads
		# print "%s  \t%.3f\t%.3f\t%s\t%s" % (contig, reads_pct_J99, reads_pct_26695, tot_reads, length)
		x.append(100*reads_pct_J99)
		y.append(j+1)
		color.append(plt.get_cmap('cool')(reads_pct_J99))
		area.append(math.sqrt(length)*2)
	sct = ax.scatter(x, y, color="gray", s=area, linewidths=2, edgecolor='k', alpha=0.6)
	# sct = ax.scatter(x, y, c=color, s=area, linewidths=2, edgecolor='w')
	sct.set_alpha(0.75)
	ax.set_xlim([-10,110])
	ax.set_ylim([0,4])
	ax.set_xticks(range(0,105,25))
	ax.set_yticks(range(0,5,1))
	pub_figs.remove_top_right_axes(ax)
	ax.minorticks_on()
	ax.tick_params('both', length=10, width=2, which='major')
	ax.tick_params('both', length=5, width=1, which='minor')
	ax.tick_params(pad=10, labelsize=font_s)
	ax.set_xlabel(r"$\%$ J99 reads in contig", fontsize=font_s)
	ax.set_ylabel('Contig count', fontsize=font_s)
	xticks = ax.xaxis.get_minor_ticks()
	yticks = ax.yaxis.get_minor_ticks()
	xticks[0].set_visible(False)
	xticks[-1].set_visible(False)
	yticks[0].set_visible(False)
	yticks[-1].set_visible(False)
	plt.grid()

plt.savefig("HP_split_assemblies_summary.png", dpi=300)