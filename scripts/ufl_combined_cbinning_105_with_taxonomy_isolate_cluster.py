import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import scipy
from scipy.stats import gaussian_kde
import sklearn.cluster as cluster
import pub_figs
import random
from pbcore.io.BasH5IO import BasH5Reader
from itertools import groupby
from collections import Counter

data     = np.loadtxt("both.comp.2D", dtype="float")
labels   = np.loadtxt("both.labels",  dtype="S40")
names    = np.loadtxt("both.names",   dtype="str")
lengths  = np.loadtxt("both.lengths", dtype="int")
title    = "contig-level cBinning"
reads_tax_fn  = "reads.tax.unordered"
conigs_tax_fn = "contigs.tax.unordered"
corrected_fn  = "corrected.fasta"
contigs_fn    = "polished_assembly.fasta"

def fasta_iter( fasta_name ):
	fh = open(fasta_name)
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		header = header.next()[1:].strip()
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

def move_unknowns_to_bottom( leg_tup ):
	for tup in leg_tup:
		if tup[0]=="Unknown contig":
			unknown_tup = tup
	leg_tup.remove(unknown_tup)
	leg_tup.append(unknown_tup)
	return leg_tup

corrected = set()
for name,seq in fasta_iter(corrected_fn):
	read = "/".join(name.split("/")[:2])
	corrected.add(read)

read_specs = {}
for line in open(reads_tax_fn, "r").xreadlines():
	line = line.strip()
	name = "/".join(line.split("\t")[0].split("/")[:2])
	if len(line.split("\t"))==1:
		spec  = "Unknown read"
	else:
		tax  = line.split("\t")[1]
		spec = " ".join(tax.split(";")[-1].split(" ")[:2])
	read_specs[name] = spec

contig_spec = {}
for line in open(conigs_tax_fn, "r").xreadlines():
	line = line.strip()
	name = line.split("|")[0]
	tax  = line.split("\t")[1]
	spec = " ".join(tax.split(";")[-1].split(" ")[:2])
	contig_spec[name] = spec

for i,name in enumerate(names):
	if name.find("unitig")>-1:
		# labels[i] = "Contig"
		if contig_spec.get(name):
			labels[i] = contig_spec[name]
		else:
			labels[i] = "Unknown contig"
			contig_spec[name] = "Unknown"
	elif name in corrected:
		if read_specs.get(name):
			# labels[i] = read_specs[name]
			labels[i] = "Known read"
		else:
			labels[i] = "Unknown read"
	else:
		labels[i] = "Uncorrected read"

# Filter contig labels; if label only present on one contig, call "Unknown contig"
lab_counter = Counter()
for i,label in enumerate(labels):
	if names[i].find("unitig")>-1:
		if label == "[Eubacterium] eligens":
			labels[i] = "Eubacterium eligens"
		lab_counter[label] += 1

for i,label in enumerate(labels):
	if lab_counter[label]==1 or len(labels[i].split(" "))==1:
		labels[i] = "Unknown contig"

print "All sequences",     len(labels)
print "Unlabeled reads",   len(labels[labels=="Unknown read"])
print "Uncorrected reads", len(labels[labels=="Uncorrected read"])
print "Contigs",           len(labels[labels=="Contig"])

fig        = plt.figure(figsize=[15,12])
ax         = fig.add_axes([0.1, 0.1, 0.8, 0.8])
ax.axis("off")

for i,name in enumerate(names):
	if name.find("m1")>-1:
		labels[i] = "Raw read"
reads_comp        = data[labels=="Raw read"]           
extent            = [-30,30,-30,35]
# extent            = [-19.330229081900001, 22.264959403300001, -27.434908619400002, 17.7252792084]
bins              = 40
H, xedges, yedges = np.histogram2d(reads_comp[:,0], reads_comp[:,1], bins=bins, range=[extent[:2], extent[2:]])
H = np.rot90(H)
H = np.flipud(H)
Hmasked = np.ma.masked_where(H==0,H)         # Mask pixels with a value of zero
plt.hot()                                    # set 'hot' as default colour map
im = plt.imshow(H, interpolation='bilinear', # creates background image
                origin='lower', cmap=cm.Greys, 
                extent=extent)


lab_set    = set(labels)
label_list = list(lab_set)
label_list.sort()
label_list.remove("Unknown contig")
label_list.append("Unknown contig")
colors    = plt.get_cmap('spectral')(np.linspace(0.0, 0.8, len(label_list)))
# shapes    = ["o", "v", "^", "s", "D"]
shapes    = ["o", "v", "^", "D"]
res       = []
lengths[lengths<50000] = 50000
scaled_sizes = lengths**1.5 / max(lengths**1.5) * 2000
for k,target_lab in enumerate(label_list):
	if target_lab=="Uncorrected read" or target_lab=="Known read" or target_lab=="Unknown read" or target_lab=="Raw read":
		continue
	idxs             = [j for j,label in enumerate(labels) if label==target_lab]
	scaled_sizes_idx = np.array(scaled_sizes)[idxs]
	X     = data[idxs,0]
	Y     = data[idxs,1]
	color = colors[k]
	for i,x in enumerate(X):
		res.append( (x, Y[i], target_lab, color, shapes[k%len(shapes)], scaled_sizes_idx[i]) ) 
np.random.shuffle(res) 
plotted_labs = set()
legend_plots = []
legend_labs  = []
for i,(x,y, target_lab, color, shape, size) in enumerate(res):
	if target_lab=="Unknown contig":
		shape = "*"
		color = "r"
	plot = ax.scatter(x,y, marker=shape, s=size , edgecolors=color, label=target_lab, facecolors="None", linewidth=1.2, alpha=0.8)
	if target_lab not in plotted_labs:
		plotted_labs.add(target_lab)
		legend_plots.append(plot)
		legend_labs.append(target_lab)
box     = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
leg_tup = zip(legend_labs,legend_plots)
leg_tup.sort(key=lambda x: x[0])
leg_tup = move_unknowns_to_bottom( leg_tup )
legend_labs, legend_plots = zip(*leg_tup)
leg = ax.legend(legend_plots, legend_labs, loc='center left', prop={'size':18}, bbox_to_anchor=(1, 0.5), frameon=False, scatterpoints=1)
for i in range(len(legend_labs)):
	leg.legendHandles[i]._sizes = [150]
minx = min(map(lambda x: x[0], res))
maxx = max(map(lambda x: x[0], res))
miny = min(map(lambda x: x[1], res))
maxy = max(map(lambda x: x[1], res))

fig_main = plt.gcf()
ax_main  = plt.gca()

def plot_zoom( ax_main, box_dims, labels, data, lengths, z ):
	box_xmin = box_dims[0]
	box_xmax = box_dims[1]
	box_ymin = box_dims[2]
	box_ymax = box_dims[3]
	ax_main.plot([box_xmin,box_xmax,box_xmax,box_xmin,box_xmin], [box_ymax,box_ymax,box_ymin,box_ymin,box_ymax], linestyle="--", color="k", linewidth=2, dashes=(3,2))
	
	fig        = plt.figure(figsize=[12,12])
	ax         = fig.add_axes([0, 0, 1, 1])
	ax.axis("off")
	ax.set_xlim([box_xmin, box_xmax])
	ax.set_ylim([box_ymin, box_ymax])
	lab_set    = set(labels)
	label_list = list(lab_set)
	label_list.sort()
	label_list.remove("Unknown contig")
	label_list.append("Unknown contig")
	colors    = plt.get_cmap('spectral')(np.linspace(0.0, 0.9, len(label_list)))
	scaled_sizes = lengths**2 / max(lengths**1.5) * 100000
	# shapes    = ["o", "v", "^", "s", "D"]
	shapes    = ["o", "v", "^", "D"]
	res       = []
	for k,target_lab in enumerate(label_list):
		# if target_lab=="Uncorrected read" or target_lab=="Contig":
		if target_lab=="Uncorrected read" or target_lab=="Known read" or target_lab=="Unknown read" or target_lab=="Raw read":
			continue
		idxs  = [j for j,label in enumerate(labels) if label==target_lab]
		scaled_sizes_idx = np.array(scaled_sizes)[idxs]
		X     = data[idxs,0]
		Y     = data[idxs,1]
		color = colors[k]
		for i,x in enumerate(X):
			res.append( (x, Y[i], target_lab, color, shapes[k%len(shapes)], scaled_sizes_idx[i]) ) 
	np.random.shuffle(res) 
	plotted_labs = set()
	legend_plots = []
	legend_labs  = []
	for i,(x,y, target_lab, color, shape, size) in enumerate(res):
		if target_lab=="Unknown contig":
			shape = "*"
			color = "r"
		plot = ax.scatter(x,y, marker=shape, s=8000 , edgecolors=color, label=target_lab, facecolors="None", linewidth=4, alpha=0.8)
		if target_lab not in plotted_labs:
			plotted_labs.add(target_lab)
			legend_plots.append(plot)
			legend_labs.append(target_lab)
	# box     = ax.get_position()
	# ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
	# leg_tup = zip(legend_labs,legend_plots)
	# leg_tup.sort(key=lambda x: x[0])
	# leg_tup = move_unknowns_to_bottom( leg_tup )
	# legend_labs, legend_plots = zip(*leg_tup)
	# leg = ax.legend(legend_plots, legend_labs, loc='center left', prop={'size':14}, bbox_to_anchor=(1, 0.5), frameon=False, scatterpoints=1)
	# for i in range(len(legend_labs)):
	# 	leg.legendHandles[i]._sizes = [150]
	ax.set_xlim([box_xmin, box_xmax])
	ax.set_ylim([box_ymin, box_ymax])
	fig.savefig("contigs.comp.tax.zoom.%s.png" % z)

	box_contig_names = set()
	for i in range(data.shape[0]):
		if names[i] in contig_spec.keys():
			x = data[i,0]
			y = data[i,1]
			if y>box_ymin and y<box_ymax and x>box_xmin and x<box_xmax:
				box_contig_names.add(names[i])
				print names[i]

	print "Found %s contigs in box %s..." % (len(box_contig_names), z)
	f = open("box_contigs.%s.fasta" % z, "w")
	for name,seq in fasta_iter(contigs_fn):
		if name.split("|")[0] in box_contig_names:
			f.write(">%s\n" % name)
			f.write("%s\n" % seq)
	f.close()

box_dims = [-6.5, -3.2, 2.9, 5.5]
plot_zoom( ax_main, box_dims, labels, data, lengths, 1 )

box_dims = [20.0, 21.5, 11.5, 13.5]
plot_zoom( ax_main, box_dims, labels, data, lengths, 2 )

box_dims = [20.0, 22.0, 13.5, 15.5]
plot_zoom( ax_main, box_dims, labels, data, lengths, 3 )

fig_main.savefig("contigs.comp.tax.png")