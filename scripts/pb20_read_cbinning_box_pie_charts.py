import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})
import numpy as np

plt.figure(1, figsize=(8,8))
ax = plt.axes([0.15, 0.15, 0.6, 0.6])
# The slices will be ordered and plotted counter-clockwise.
colors    = plt.get_cmap('spectral')(np.linspace(0.0, 0.9, 25))
labels = []
labels.append(r"${R. sphaeroides}$ 2.4.1")
labels.append("Other")
frac  = 0.980769230769
fracs = [frac, (1-frac)]
explode=(0, 0)
patches, texts = ax.pie(fracs, explode=explode, shadow=True, colors=[colors[16],'0.75'])
# for t in texts:
	# t.set_horizontalalignment('center')
ax.legend(patches, labels, loc='lower center', bbox_to_anchor=(0.5, 0.9), prop={'size':36}, frameon=False)
pie_fn = "pie_rsphaeroides_box_percents.png"
plt.savefig(pie_fn)

plt.figure(1, figsize=(8,8))
ax = plt.axes([0.15, 0.15, 0.6, 0.6])
# The slices will be ordered and plotted counter-clockwise.
labels = []
labels.append(r"${B. cereus}$ ATCC 10987")
labels.append("Other")
frac  = 0.891774891775
fracs = [frac, (1-frac)]
explode=(0, 0)
patches, texts = ax.pie(fracs, explode=explode, shadow=True, colors=[colors[2],'0.75'])
# for t in texts:
	# t.set_horizontalalignment('center')
ax.legend(patches, labels, loc='lower center', bbox_to_anchor=(0.5, 0.9), prop={'size':36}, frameon=False)
pie_fn = "pie_bcereus_box_percents.png"
plt.savefig(pie_fn)