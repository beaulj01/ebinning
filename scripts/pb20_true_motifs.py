import os,sys
from collections import Counter
import re

true_motifs = set(["RAGCNNNNNNCGT", \
"CTCGAG", \
"CRTANNNNNNRTC", \
"GAYNNNNNNTAYG", \
"ACGVNNNNNGCTY", \
"CAYNNNNNGTG", \
"CACNNNNNATG", \
"CYHANNNNNNNCTTG", \
"CAAGNNNNNNNTDRG", \
"GATC", \
"GCACNNNNNNGTT", \
"CATG", \
"GATC", \
"GCAG", \
"GANTC", \
"GAGG", \
"ATTAAT", \
"GAAGA", \
"DGAAGG", \
"CTANNNNNNNNTGT", \
"VCGDAD", \
"TACNNNNNCTC", \
"GAGNNNNNGTA", \
"GACGC", \
"AGCAGY", \
"GATCNNNNNNGTC", \
"GACNNNNNNGATC", \
"GATCNNNNNNGTC", \
"GACNNNNNNGATC", \
"ATCNNNNNCCT", \
"AGGNNNNNGAT", \
"ACANNNNNNRTGG", \
"CCAYNNNNNNTGT", \
"GATC", \
"CGANNNNNNNTCY", \
"RGANNNNNNNTCG", \
"CTGVAG", \
"CTYCAG"])

configs = Counter()
for motif in true_motifs:
	print motif
	if motif.find("N") > -1:
		# Bipartite motif
		m      = re.search(r"([ACGTWSMKRYBDHV]+)(N+)([ACGTWSMKRYBDHV]+)", motif)
		first  = m.groups()[0]
		Ns     = m.groups()[1]
		second = m.groups()[2]
		config = ( len(first), len(Ns), len(second) )
	else:
		# Contiguous motif
		m      = re.search(r"[ACGTWSMKRYBDHV]+", motif)
		bases  = m.group()
		config = ( len(bases) )
	configs[config] += 1

for config in configs.keys():
	print configs[config], config
print ""

# # "motifString","centerPos","modificationType","fraction","nDetected","nGenome","groupTag","partnerMotifString","meanScore","meanIpdRatio","meanCoverage","objectiveScore"
# # A_odontolyticus_ATCC
# "RAGCNNNNNNCGT","1","m6A","0.3560606","282","792","RAGCNNNNNNCGT","","37.975178","6.9925556","18.166666","4172.227"
# "CTCGAG","4","m6A","0.3482024","1046","3004","CTCGAG","CTCGAG","38.43021","6.2676215","18.796368","15554.392"
# "CRTANNNNNNRTC","3","m6A","0.33594978","214","637","CRTANNNNNNRTC/GAYNNNNNNTAYG","GAYNNNNNNTAYG","37.443924","6.064114","18.939253","3002.2168"
# "GAYNNNNNNTAYG","1","m6A","0.3155416","201","637","CRTANNNNNNRTC/GAYNNNNNNTAYG","CRTANNNNNNRTC","38.482586","5.4036317","19.502487","2739.1213"
# "ACGVNNNNNGCTY","0","m6A","0.2836767","179","631","ACGVNNNNNGCTY","","37.122906","5.9119","18.597765","2138.1406"
# # B_cereus_ATCC_10987
# # B_vulgatus_ATCC_8482
# "CAYNNNNNGTG","1","m6A","0.3363202","1126","3348","CAYNNNNNGTG","","37.62078","5.5518966","17.984015","15887.071"
# "CACNNNNNATG","1","m6A","0.31574395","730","2312","CACNNNNNATG","","37.516438","5.5255747","18.332876","9703.893"
# "CYHANNNNNNNCTTG","3","m6A","0.2090909","115","550","CYHANNNNNNNCTTG/CAAGNNNNNNNTDRG","CAAGNNNNNNNTDRG","37.373913","6.1832166","18.382608","1050.916"
# "CAAGNNNNNNNTDRG","2","m6A","0.16545455","91","550","CYHANNNNNNNCTTG/CAAGNNNNNNNTDRG","CYHANNNNNNNCTTG","37.692307","6.374505","18.615385","679.3678"
# # C_beijerinckii_NCIMB_8052
# # D_radiodurans_R1
# # E_faecalis_OG1RF
# # E_coli_str_K_12_substr_MG1655
# "GATC","1","m6A","0.29651225","11341","38248","GATC","GATC","37.72313","6.079228","16.102901","143250.92"
# "GCACNNNNNNGTT","2","m6A","0.20168068","120","595","GCACNNNNNNGTT","","36.191666","5.8193326","17.133333","1027.9872"
# # H_pylori_26695
# "CATG","1","m6A","0.98998785","14634","14782","CATG","CATG","137.43365","5.6926703","98.36531","1993072.1"
# "GATC","1","m6A","0.9874469","10698","10834","GATC","GATC","148.93532","5.3766503","98.5229","1575297.8"
# "GCAG","2","m6A","0.9802817","4176","4260","GCAG","","134.34938","5.8759346","97.412834","551076.56"
# "GANTC","1","m6A","0.97809696","5448","5570","GANTC","GANTC","137.15913","5.2165604","98.3605","732496.56"
# "GAGG","1","m6A","0.97461617","4761","4885","GAGG","","126.05377","4.944178","97.8975","586413.94"
# "ATTAAT","4","m6A","0.9722222","945","972","ATTAAT","ATTAAT","143.05185","5.16313","95.110054","131799.66"
# "GAAGA","4","m6A","0.968277","4670","4823","GAAGA/TCTTC","TCTTC","130.07666","5.1245017","99.12334","476104.62"
# "TCTTC","1","m4C","0.9668256","4663","4823","GAAGA/TCTTC","GAAGA","84.30238","3.5552614","91.48188","381345.47"
# "DGAAGG","3","m6A","0.960371","1139","1186","DGAAGG","","126.0878","5.455677","93.26602","138481.55"
# "CTANNNNNNNNTGT","2","m6A","0.95151514","314","330","CTANNNNNNNNTGT","","137.84714","5.686847","99.71656","41390.582"
# "VCGDAD","4","m6A","0.5674717","5757","10145","VCGDAD","","130.42679","5.092508","96.9333","382918.34"
# # L_gasseri_ATCC_33323
# "TACNNNNNCTC","1","m6A","0.63982105","286","447","TACNNNNNCTC/GAGNNNNNGTA","GAGNNNNNGTA","41.251747","4.971014","28.517483","7893.3447"
# "GAGNNNNNGTA","1","m6A","0.61744964","276","447","TACNNNNNCTC/GAGNNNNNGTA","TACNNNNNCTC","40.304348","5.2996387","27.322464","7207.794"
# # L_monocytogenes_EGD_e
# # N_meningitidis_MC58
# "GACGC","1","m6A","0.46555698","1818","3905","GACGC","","40.11056","5.9071527","22.311882","36646.137"
# # P_acnes_KPA171202
# "AGCAGY","3","m6A","0.6131036","2171","3541","AGCAGY","","40.400738","7.300599","20.419622","56471.535"
# "GATCNNNNNNGTC","1","m6A","0.39357647","723","1837","GATCNNNNNNGTC/GACNNNNNNGATC","GACNNNNNNGATC","38.330566","5.8368073","17.4509","11973.187"
# "GACNNNNNNGATC","1","m6A","0.27109417","498","1837","GATCNNNNNNGTC/GACNNNNNNGATC","GATCNNNNNNGTC","36.35743","5.6198416","17.771084","5592.8184"
# # P_aeruginosa_PAO1
# "GATCNNNNNNGTC","1","m6A","0.39357647","723","1837","GATCNNNNNNGTC/GACNNNNNNGATC","GACNNNNNNGATC","38.330566","5.8368073","17.4509","11973.187"
# "GACNNNNNNGATC","1","m6A","0.27109417","498","1837","GATCNNNNNNGTC/GACNNNNNNGATC","GATCNNNNNNGTC","36.35743","5.6198416","17.771084","5592.8184"
# # R_sphaeroides_2_4_1
# # S_aureus_subsp_aureus_USA300_TCH1516
# "ATCNNNNNCCT","0","m6A","0.7438495","514","691","ATCNNNNNCCT/AGGNNNNNGAT","AGGNNNNNGAT","43.227627","5.8300753","23.947472","17023.979"
# "AGGNNNNNGAT","0","m6A","0.6685962","462","691","ATCNNNNNCCT/AGGNNNNNGAT","ATCNNNNNCCT","40.761906","6.169419","23.967533","13108.228"
# "ACANNNNNNRTGG","2","m6A","0.72032195","358","497","ACANNNNNNRTGG/CCAYNNNNNNTGT","CCAYNNNNNNTGT","41.854748","5.7183247","22.969275","11153.259"
# "CCAYNNNNNNTGT","2","m6A","0.7082495","352","497","ACANNNNNNRTGG/CCAYNNNNNNTGT","ACANNNNNNRTGG","42.59943","5.4822726","22.707386","10992.947"
# # S_epidermidis_ATCC_12228
# # S_agalactiae_2603V_R
# # S_mutans_UA159
# "GATC","1","m6A","0.850475","8236","9684","GATC","GATC","51.351383","5.9608374","28.13781","365564.4"
# "CGANNNNNNNTCY","2","m6A","0.79296345","586","739","CGANNNNNNNTCY/RGANNNNNNNTCG","RGANNNNNNNTCG","48.1314","5.49374","29.585323","20190.82"
# "RGANNNNNNNTCG","2","m6A","0.78078485","577","739","CGANNNNNNNTCY/RGANNNNNNNTCG","CGANNNNNNNTCY","48.01213","5.6802096","28.906412","20070.1"
# "CTGVAG","4","m6A","0.57754856","1456","2521","CTGVAG","","45.752747","6.2385864","29.543957","40645.1"
# "CTYCAG","4","m6A","0.5015461","811","1617","CTYCAG","","44.525276","5.7984123","31.167694","19404.713"

