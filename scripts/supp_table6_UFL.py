import os,sys
from pbcore.io.BasH5IO import BasH5Reader
import numpy as np
from collections import Counter

read_species_map = np.loadtxt("read_species_mapping.txt", dtype="str")
input_fn         = np.loadtxt("input.fofn", dtype="str")

n_reads   = Counter()
n_sreads  = Counter()
n_bases   = Counter()
rlengths  = {}
slengths  = {}
read_map  = {}
for entry in read_species_map:
	read  = entry[0]
	spec  = entry[1]
	read_map[read] = spec
	rlengths[spec] = []
	slengths[spec] = []

for i,f in enumerate(input_fn):
	print "bas %s" % i
	reader = BasH5Reader(f)
	for z in reader:
		zl   = 0
		read = z.zmwName
		if read_map.get(read):
			spec = read_map[read]
			for s in z.subreads:
				sl = len(s.basecalls())
				slengths[spec].append(sl)
				zl += sl
				n_sreads[spec] += 1
			rlengths[spec].append(zl)
			n_bases[spec] += zl
			n_reads[spec] += 1
		else:
			pass

for spec in n_reads.keys():
	print "%s\t%s\t%s\t%.2f" % (spec, n_bases[spec], n_reads[spec], np.mean(rlengths[spec]))