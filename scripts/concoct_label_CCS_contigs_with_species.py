import os,sys
import numpy as np

labs  = np.loadtxt(sys.argv[1], delimiter=",",  dtype="str")
names = np.loadtxt(sys.argv[2], delimiter="\t", dtype="str")

labs_dict = dict([(labs[i,0],labs[i,1]) for i in range(labs.shape[0])])

f = open("contigs.labels", "wb")
for name in names:
	if labs_dict.get(name):
		lab = labs_dict[name].replace(" ", "_")
	else:
		lab = "Unlabeled"
	f.write("%s\n" % lab)
f.close()	