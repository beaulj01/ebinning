import os,sys
import pysam
from collections import Counter,defaultdict
import glob
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt

sample_bams = glob.glob("map/sample_*/bowtie2/*.bam")
aln_counter = defaultdict(Counter)

ref_map = {"unitig_0|quiver"         : "H. pylori 26695", \
		   "unitig_1|quiver"         : "H. pylori 26695", \
		   "scf7180000000008|quiver" : "H. pylori J99", \
		   "scf7180000000009|quiver" : "H. pylori J99", \
		   "scf7180000000010|quiver" : "H. pylori J99", \
		   "scf7180000000011|quiver" : "H. pylori J99", \
		   "scf7180000000002|quiver" : "H. pylori JP26", \
		   "scf7180000000022|quiver" : "E. coli C227-11", \
		   "NC_000913.3" : "E. coli str. K-12 substr. MG1655", \
		   "NC_004663.1" : "B. thetaiotaomicron VPI-5482", \
		   "NC_008497.1" : "L. brevis ATCC 367", \
		   "NC_009614.1" : "B. vulgatus ATCC 8482", \
		   "NC_010655.1" : "A. muciniphila ATCC BAA-835", \
		   "NC_012781.1" : "E. rectale ATCC 33656", \
		   "NC_014656.1" : "B. longum subsp. longum BBMN68", \
		   "NC_014833.1" : "R. albus 7", \
		   "NC_014933.1" : "B. helcogenes P 36-108", \
		   "NC_015164.1" : "B. salanitronis DSM 18170", \
		   "NC_015977.1" : "R. hominis A2-183", \
		   "NC_016776.1" : "B. fragilis 638", \
		   "NC_017179.1" : "C. difficile BI1", \
		   "NC_018221.1" : "E. faecalis D32", \
		   "NC_018937.1" : "H. pylori Rif1", \
		   "NC_021042.1" : "F. prausnitzii L2-6", \
}


# sample_bams = sample_bams[0]

for bam in sample_bams:
	samfile = pysam.AlignmentFile(bam, "rb")
	for i,aln in enumerate(samfile.fetch()):
		ref = "_".join(aln.query_name.split("_")[2:])
		ref = ref.split("_ln")[0]
		if aln.mapq==42:
			aln_counter[aln.reference_name][ref_map[ref]] += 1

		# if i==10000:
			# break

contigs       = []
fracs_to_plot = []
contig_list = sys.argv[1]
for line in open(contig_list, "r").xreadlines():
	line   = line.strip()
	contig = line.split(",")[0]
	total  = float(np.sum(aln_counter[contig].values()))
	orgs   = aln_counter[contig].keys()
	if any("E. coli" in o for o in orgs):
		K12_n   = aln_counter[contig]["E. coli str. K-12 substr. MG1655"]
		C227_n  = aln_counter[contig]["E. coli C227-11"]
		other_n = total - (K12_n+C227_n)
		if (other_n/total)<0.10: # Less than 10% non-E. coli reads
			contigs.append(contig)
			fracs_to_plot.append( (K12_n/total, C227_n/total, other_n/total) )
fig = plt.figure(figsize=[15,6])
ax  = fig.add_axes([0.05, 0.05, 0.8, 0.8])
ax.grid()
fracs_to_plot.sort(key=lambda x: (x[0], x[1]))
N           = len(fracs_to_plot)
k12_fracs   = np.array(map(lambda x: x[0], fracs_to_plot))
c227_fracs  = np.array(map(lambda x: x[1], fracs_to_plot))
other_fracs = np.array(map(lambda x: x[2], fracs_to_plot))
ind         = np.arange(N)
width       = 1.0
p1 = ax.bar(ind, k12_fracs,   width, color='r', linewidth=0)
p2 = ax.bar(ind, c227_fracs,  width, color='b', linewidth=0, bottom=k12_fracs)
p3 = ax.bar(ind, other_fracs, width, color='y', linewidth=0, bottom=k12_fracs+c227_fracs)
ax.set_xlabel('Contigs')
ax.set_ylabel('Fraction aligned reads')
# ax.set_xticks(ind + width/2.)
ax.set_ylim([0,1.0])
ax.set_xlim([0,N])
ax.set_yticks(np.arange(0, 1.1, 0.1))
leg = ax.legend((p1[0], p2[0], p3[0]), ('K12', 'C227', 'Other'), loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':14}, frameon=False)
plt.savefig("contig_chimerism_ecoli_barchart.png")

contigs       = []
fracs_to_plot = []
contig_list = sys.argv[1]
for line in open(contig_list, "r").xreadlines():
	line   = line.strip()
	contig = line.split(",")[0]
	total  = float(np.sum(aln_counter[contig].values()))
	orgs   = aln_counter[contig].keys()
	if any("H. pylori" in o for o in orgs):
		n_26695   = aln_counter[contig]["H. pylori 26695"]
		n_J99  = aln_counter[contig]["H. pylori J99"]
		n_JP26  = aln_counter[contig]["H. pylori JP26"]
		n_Rif1  = aln_counter[contig]["H. pylori Rif1"]
		other_n = total - (n_26695+n_J99+n_JP26+n_Rif1)
		if (other_n/total)<0.10: # Less than 10% non-E. coli reads
			contigs.append(contig)
			fracs_to_plot.append( (n_26695/total, n_J99/total, n_JP26/total, n_Rif1/total, other_n/total) )
fig = plt.figure(figsize=[15,6])
ax  = fig.add_axes([0.05, 0.05, 0.8, 0.8])
ax.grid()
fracs_to_plot.sort(key=lambda x: (x[0], x[1], x[2], x[3]))
N           = len(fracs_to_plot)
fracs_26695 = np.array(map(lambda x: x[0], fracs_to_plot))
fracs_J99   = np.array(map(lambda x: x[1], fracs_to_plot))
fracs_JP26  = np.array(map(lambda x: x[2], fracs_to_plot))
fracs_Rif1  = np.array(map(lambda x: x[3], fracs_to_plot))
fracs_other = np.array(map(lambda x: x[4], fracs_to_plot))
ind         = np.arange(N)
width       = 1.0
p1 = ax.bar(ind, fracs_26695, width, color='r', linewidth=0)
p2 = ax.bar(ind, fracs_J99,   width, color='b', linewidth=0, bottom=fracs_26695)
p3 = ax.bar(ind, fracs_JP26,  width, color='g', linewidth=0, bottom=fracs_26695+fracs_J99)
p4 = ax.bar(ind, fracs_Rif1,  width, color='m', linewidth=0, bottom=fracs_26695+fracs_J99+fracs_JP26)
p5 = ax.bar(ind, fracs_other, width, color='y', linewidth=0, bottom=fracs_26695+fracs_J99+fracs_JP26+fracs_Rif1)
ax.set_xlabel('Contigs')
ax.set_ylabel('Fraction aligned reads')
# ax.set_xticks(ind + width/2.)
ax.set_yticks(np.arange(0, 1.1, 0.1))
ax.set_ylim([0,1.0])
ax.set_xlim([0,N])
leg = ax.legend((p1[0], p2[0], p3[0], p4[0], p5[0]), ('26695', 'J99', 'JP26', 'Rif1', 'Other'), loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':14}, frameon=False)
plt.savefig("contig_chimerism_hpylori_barchart.png")

