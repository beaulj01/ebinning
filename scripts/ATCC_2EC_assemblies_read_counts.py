import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.ticker import MultipleLocator
import subprocess
import numpy as np
from collections import defaultdict,Counter
import operator
import pub_figs
from pbcore.io.align.CmpH5IO import CmpH5Reader
import math

nContigs = 50

fontProperties = pub_figs.setup_math_fonts(24)

combined_cmph5_fns = ["/hpc/users/beaulj01/projects/ebinning/concoct_integration/bigsix_atcc/10kb_bas_binning/2215_and_2440/k12_align/cmp_binning/combined_HGAP_4.6Mb/data/aligned_reads.cmp.h5"]

assembly_names  = ["2215_2440"]

assembly_labels = {"2215_2440" : "BAA-2215 and BAA-2440"}

movies_spec = {"m170323_053009_42156_c101187332550000001823278408081760_s1_p0" : "BAA_2215", \
			   "m170324_002632_42156_c101187332550000001823278408081763_s1_p0" : "BAA_2440"}

fig = plt.figure(figsize=[15,12])
for i in range(len(assembly_names)):
	ax       = fig.add_subplot(3,3,7)
	ax.axis("off")
	
	ind     = np.arange(2)+0.2
	width   = 0.65
	
	# Coverage based on total subread lengths and assembly size
	BAA2215 = 34.6
	BAA2440 = 42.3
	
	y        = np.array([0, 1])
	plot     = ax.scatter(y, y, c=y, cmap='winter')
	plt.cla()
	cbaxes   = fig.add_axes([0.10, 0.50, 0.18, 0.03])
	cbar     = plt.colorbar(plot, cax=cbaxes, orientation="horizontal")
	cbar.set_ticks([0,1])
	cbar.ax.set_xticklabels([r"BAA-2440",r"BAA-2215"])#, rotation=45, ha="right")

	ax       = fig.add_subplot(3,3,1)
	rects1   = ax.bar(ind,   [BAA2440,0], width, color=plt.get_cmap('winter')(0), linewidth=0)
	rects2   = ax.bar(ind+1, [BAA2215,0],   width, color=plt.get_cmap('winter')(0.99), linewidth=0)
	ax.set_xlim([0,2])
	plt.xticks([0.5,1.5], ["BAA-2440", "BAA-2215"], rotation=45, ha="right")
	plt.yticks([25,50], ["25x","50x"])
	pub_figs.remove_top_right_axes(ax)
plt.subplots_adjust(wspace=1.3)
plt.savefig("2EC_mixed_assemblies_proportions.png")

# fig = plt.figure(figsize=[15,12])
# ax  = fig.add_subplot(111) 
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.tick_params(labelcolor='w', top='off', bottom='off', left='off', right='off')


# First row
fig = plt.figure(figsize=[15,12])
for i,cmph5 in enumerate(combined_cmph5_fns):
	ax     = fig.add_subplot(3,3,1)
	
	contigs_counters = {}
	contig_lens      = {}
	seen_contigs     = set()
	reader           = CmpH5Reader(cmph5)

	for entry in reader.referenceInfoTable:
		contig_lens[entry[3]] = entry[4]

	for r in reader:
		movie     = r.movieName
		read_spec = movies_spec[movie]
		contig    = r.referenceName
		if contig not in seen_contigs:
			seen_contigs.add(contig)
			contigs_counters[contig] = Counter()
		contigs_counters[contig][read_spec] += 1

	sorted_contig_lens = sorted(contig_lens.items(), key=operator.itemgetter(1), reverse=True)

	x     = []
	y     = []
	color = []
	area  = []
	for j,(contig,length) in enumerate(sorted_contig_lens):
		tot_reads      = np.sum(contigs_counters[contig].values())
		reads_pct_2215 = float(contigs_counters[contig]["BAA_2215"])/tot_reads
		reads_pct_2440 = float(contigs_counters[contig]["BAA_2440"])/tot_reads
		print "%s  \t%.3f\t%.3f\t%s\t%s" % (contig, reads_pct_2215, reads_pct_2440, tot_reads, length)
		x.append(100*reads_pct_2440)
		y.append(j)
		color.append(plt.get_cmap('winter')(reads_pct_2440))
		area.append(math.sqrt(length))
	sct = ax.scatter(x[:nContigs], y[:nContigs], c=color, s=area, linewidths=2, edgecolor='w')
	sct.set_alpha(0.75)
	ax.set_xlim([-10,110])
	ax.set_ylim([-2,55])
	ax.set_xticks(range(0,105,25))
	ax.set_yticks(range(0,51,10))
	pub_figs.remove_top_right_axes(ax)
	ax.minorticks_on()
	ax.tick_params('both', length=10, width=2, which='major')
	ax.tick_params('both', length=5, width=1, which='minor')
	ax.tick_params(pad=10)
	if i==(len(assembly_names)-2):
		ax.set_xlabel(r"$\%$ BAA-2440 reads in contig")
	if i==0:
		ax.set_ylabel('Contig count')
	xticks = ax.xaxis.get_minor_ticks()
	yticks = ax.yaxis.get_minor_ticks()
	xticks[0].set_visible(False)
	xticks[-1].set_visible(False)
	yticks[0].set_visible(False)
	yticks[-1].set_visible(False)
	plt.grid()

# Second row
PCA      = "reads.SMp.5kb.no0s.PCA"
labels   = "reads.labels.5kb.no0s"
names    = "reads.names.5kb.no0s"
combined_dirs   = ["/hpc/users/beaulj01/projects/ebinning/concoct_integration/bigsix_atcc/10kb_bas_binning/2215_and_2440/k12_align/cmp_binning"]

assembly_names  = ["2215_2440"]

assembly_labels = {"2215_2440" : "BAA-2215 and BAA-2440"}

label_dict = {"BAA_2215": r"${E. coli}$ BAA-2215", \
			  "BAA_2440": r"${E. coli}$ BAA-2440"}
plt.hot()
for d,dir_name in enumerate(combined_dirs):
	ax          = fig.add_subplot(3,3,4)
	
	pca_fn      = os.path.join(dir_name, PCA)
	read_labs   = os.path.join(dir_name, labels)
	read_names  = os.path.join(dir_name, names)
	SMp_coords  = np.loadtxt(pca_fn,     dtype="float")
	read_labels = np.loadtxt(read_labs,  dtype="str")
	read_names  = np.loadtxt(read_names, dtype="str")
	xmin = 0
	xmax = 6
	ymin = -3
	ymax = 3

	split_x = 2.8

	extent            = [xmin, xmax, ymin, ymax]
	H, xedges, yedges = np.histogram2d(SMp_coords[:,0], SMp_coords[:,1], bins=25, range=[extent[:2], extent[2:]])
	H       = np.rot90(H)
	H       = np.flipud(H)
	Hmasked = np.ma.masked_where(H==0,H)         # Mask pixels with a value of zero
	im      = plt.imshow(H, interpolation='bilinear', origin='lower', cmap=cm.Greys, extent=extent) # Sets background image
	levels  = [30,50,70,90]
	cols    = ["k","0.4","0.6","0.8"]
	cset    = plt.contour(H, levels, origin="lower",colors=cols,linewidths=1.4,extent=extent)
	plt.clabel(cset, inline=1, fontsize=10, fmt="%1.0i")
	for c in cset.collections:
		c.set_linestyle("solid")
	# x1 = np.array(range(xmin,xmax+1)) 
	# y1 = eval("m*x1 + b")
	# plt.plot(x1, y1, linestyle="--", color="k")
	ax.axvline(x=split_x, ymin=-3, ymax=3, linestyle="--", color="k")
	ax.set_xlabel("PC1")
	ax.set_ylabel("PC2")
	ax.set_xticks(range(xmin,xmax+1,1))
	ax.set_yticks(range(ymin,ymax+1,1))
	ax.tick_params(pad=10)
	pub_figs.remove_top_right_axes(ax)

# Third row
iso_cmph5_base_dirs = ["/hpc/users/beaulj01/projects/ebinning/concoct_integration/bigsix_atcc/10kb_bas_binning/2215_and_2440/k12_align/cmp_binning"]

for i,iso_cmph5_base_dir in enumerate(iso_cmph5_base_dirs):
	ax     = fig.add_subplot(3,3,7)

	contigs_counters = {}
	contig_lens      = {}
	seen_contigs     = set()
	for strain in ["2215", "2440"]:
		cmph5 = os.path.join(iso_cmph5_base_dir, strain+"_whitelist", "data", "aligned_reads.cmp.h5")
		print cmph5
		reader = CmpH5Reader(cmph5)

		for entry in reader.referenceInfoTable:
			contig_lens[entry[3] + ".%s" % strain] = entry[4]

		for r in reader:
			movie     = r.movieName
			read_spec = movies_spec[movie]
			contig    = r.referenceName + ".%s" % strain
			if contig not in seen_contigs:
				seen_contigs.add(contig)
				contigs_counters[contig] = Counter()
			contigs_counters[contig][read_spec] += 1

	sorted_contig_lens = sorted(contig_lens.items(), key=operator.itemgetter(1), reverse=True)

	x     = []
	y     = []
	color = []
	area  = []
	for j,(contig,length) in enumerate(sorted_contig_lens):
		tot_reads       = np.sum(contigs_counters[contig].values())
		reads_pct_2215 = float(contigs_counters[contig]["BAA_2215"])/tot_reads
		reads_pct_2440 = float(contigs_counters[contig]["BAA_2440"])/tot_reads
		print "%s  \t%.3f\t%.3f\t%s\t%s" % (contig, reads_pct_2215, reads_pct_2440, tot_reads, length)
		x.append(100*reads_pct_2440)
		y.append(j)
		color.append(plt.get_cmap('winter')(reads_pct_2440))
		area.append(math.sqrt(length))
	sct = ax.scatter(x[:nContigs], y[:nContigs], c=color, s=area, linewidths=2, edgecolor='w')
	sct.set_alpha(0.75)
	ax.set_xlim([-10,110])
	ax.set_ylim([-2,55])
	ax.set_xticks(range(0,105,25))
	ax.set_yticks(range(0,51,10))
	pub_figs.remove_top_right_axes(ax)
	ax.minorticks_on()
	ax.tick_params('both', length=10, width=2, which='major')
	ax.tick_params('both', length=5, width=1, which='minor')
	ax.tick_params(pad=10)
	if i==(len(assembly_names)-2):
		ax.set_xlabel(r"$\%$ BAA-2440 reads in contig")
	if i==0:
		ax.set_ylabel('Contig count')
	xticks = ax.xaxis.get_minor_ticks()
	yticks = ax.yaxis.get_minor_ticks()
	xticks[0].set_visible(False)
	xticks[-1].set_visible(False)
	yticks[0].set_visible(False)
	yticks[-1].set_visible(False)
	plt.grid()

# plt.tight_layout()
fig.subplots_adjust(top=0.98, hspace=0.5)
plt.savefig("2EC_mixed_assemblies_summary.png")