import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.ticker import MultipleLocator
import subprocess
import numpy as np
from collections import defaultdict,Counter
import operator
import pub_figs
from pbcore.io.align.CmpH5IO import CmpH5Reader
import math
import ternary

nContigs = 50

font_s = 20
pub_figs.setup_math_fonts(font_size=font_s, font="Computer Modern Sans serif")
# fontProperties = pub_figs.setup_math_fonts(fontsize)

combined_cmph5_fns = ["/hpc/users/beaulj01/projects/ebinning/concoct_integration/bigsix_atcc/10kb_bas_binning/2215_2440_2196/k12_align/cmp_binning/combined_HGAP_4.6Mb/data/aligned_reads.cmp.h5"]

assembly_names  = ["2215_2440_2196"]

label_dict = {"2196": r"${E. coli}$ BAA-2196 O26:H11", \
			  "2215": r"${E. coli}$ BAA-2215 O103:H11", \
			  "2440": r"${E. coli}$ BAA-2440 O111"}

movies_spec = {"m170323_053009_42156_c101187332550000001823278408081760_s1_p0" : "BAA_2215", \
			   "m170324_002632_42156_c101187332550000001823278408081763_s1_p0" : "BAA_2440", \
			   "m170323_114806_42156_c101187332550000001823278408081761_s1_p0" : "BAA_2196"}

# First row
scale = 100
fig   = plt.figure(figsize=[8,8])
ax    = fig.add_axes([0.15, 0.15, 0.75, 0.7])
fig, ax = ternary.figure(scale=scale, ax=ax)
def color_point(x, y, z, scale):
	w = 255
	x_color = x * w / float(scale)
	y_color = y * w / float(scale)
	z_color = z * w / float(scale)
	r = math.fabs(w - y_color) / w
	g = math.fabs(w - x_color) / w
	b = math.fabs(w - z_color) / w
	return (r, g, b, 1.)

for i,cmph5 in enumerate(combined_cmph5_fns):
	contigs_counters = {}
	contig_lens      = {}
	seen_contigs     = set()
	reader           = CmpH5Reader(cmph5)

	for entry in reader.referenceInfoTable:
		contig_lens[entry[3]] = entry[4]

	for r in reader:
		movie     = r.movieName
		read_spec = movies_spec[movie]
		contig    = r.referenceName
		if contig not in seen_contigs:
			seen_contigs.add(contig)
			contigs_counters[contig] = Counter()
		contigs_counters[contig][read_spec] += 1

	sorted_contig_lens = sorted(contig_lens.items(), key=operator.itemgetter(1), reverse=True)

	X     = []
	Y     = []
	Z     = []
	color = []
	area  = []
	for j,(contig,length) in enumerate(sorted_contig_lens):
		tot_reads      = np.sum(contigs_counters[contig].values())
		reads_pct_2215 = float(contigs_counters[contig]["BAA_2215"])/tot_reads
		reads_pct_2440 = float(contigs_counters[contig]["BAA_2440"])/tot_reads
		reads_pct_2196 = float(contigs_counters[contig]["BAA_2196"])/tot_reads
		# print "%s  \t%.3f\t%.3f\t%.3f\t%s\t%s" % (contig, reads_pct_2215, reads_pct_2440, reads_pct_2196, tot_reads, length)
		x = 100*reads_pct_2215
		y = 100*reads_pct_2440
		z = 100*reads_pct_2196
		X.append(x)
		Y.append(y)
		Z.append(z)
		# color.append(plt.get_cmap('plasma')(reads_pct_2440))
		color.append(color_point(x,y,z,scale))
		area.append(math.sqrt(length))
	
	### Scatter Plot
	ax.boundary(linewidth=1.0)
	ax.gridlines(multiple=10, color="k")
	ax.scatter(zip(X,Y,Z), marker='o', c=color, s=area, linewidths=1, edgecolor='k')
	ax.legend()
	ax.ticks(axis='lbr', linewidth=1, multiple=10, offset=0.03,fsize=20)
	ax.left_axis_label(r"$\leftarrow$  "+label_dict["2196"], fontsize=font_s, offset=0.17)
	ax.right_axis_label(r"$\leftarrow$  "+label_dict["2440"], fontsize=font_s, offset=0.17)
	ax.bottom_axis_label(label_dict["2215"]+r"  $\rightarrow$", fontsize=font_s, offset=-0.08)
	ax._redraw_labels()
	ax.clear_matplotlib_ticks(axis="both")
	drawing_ax = ax.get_axes()
	pub_figs.remove_axes_border_and_spines(drawing_ax)

plt.savefig("3EC_mixed_assemblies_summary.png", dpi=300)


# Second row
PCA      = "reads.SMp.5kb.no0s.PCA"
labels   = "reads.labels.5kb.no0s"
names    = "reads.names.5kb.no0s"
combined_dirs   = ["/hpc/users/beaulj01/projects/ebinning/concoct_integration/bigsix_atcc/10kb_bas_binning/2215_2440_2196/k12_align/cmp_binning"]

assembly_names  = ["2215_2440_2196"]

plt.hot()
fig = plt.figure(figsize=[8,8])
for d,dir_name in enumerate(combined_dirs):
	ax  = fig.add_axes([0.15, 0.15, 0.75, 0.7])
	
	pca_fn      = os.path.join(dir_name, PCA)
	read_labs   = os.path.join(dir_name, labels)
	read_names  = os.path.join(dir_name, names)
	SMp_coords  = np.loadtxt(pca_fn,     dtype="float")
	read_labels = np.loadtxt(read_labs,  dtype="str")
	read_names  = np.loadtxt(read_names, dtype="str")
	xmin = 0
	xmax = 5
	ymin = -3.5
	ymax = 3

	extent            = [xmin, xmax, ymin, ymax]
	H, xedges, yedges = np.histogram2d(SMp_coords[:,0], SMp_coords[:,1], bins=25, range=[extent[:2], extent[2:]])
	H       = np.rot90(H)
	H       = np.flipud(H)
	Hmasked = np.ma.masked_where(H==0,H)         # Mask pixels with a value of zero
	im      = plt.imshow(H, interpolation='bilinear', origin='lower', cmap=cm.Greys, extent=extent) # Sets background image
	levels  = [30,45,60,80]
	cols    = ["k","0.4","0.6","0.8"]
	cset    = plt.contour(H, levels, origin="lower",colors=cols,linewidths=1.4,extent=extent)
	# plt.clabel(cset, inline=1, fontsize=10, fmt="%1.0i")
	for c in cset.collections:
		c.set_linestyle("solid")
	m1    = 0.15
	b1    = -1.25
	x1    = np.array(range(xmin,xmax+1)) 
	y1    = eval("m1*x1 + b1")
	ax.plot(x1, y1, linestyle="--", color="k", linewidth=3.0)

	m2    = -2.8
	b2    = 8.7
	x2    = np.array(range(xmin,xmax+1)) 
	y2    = eval("m2*x2 + b2")
	ax.plot(x2, y2, linestyle="--", color="k", linewidth=3.0)
	ax.set_xlabel("PC1",fontsize=font_s)
	ax.set_ylabel("PC2",fontsize=font_s)
	ax.set_xticks(range(xmin,xmax+1,1))
	ax.set_yticks(range(-3,3+1,1))
	ax.tick_params(pad=10, labelsize=font_s)
	ax.set_xlim([xmin,xmax])
	ax.set_ylim([ymin,ymax])
	pub_figs.remove_top_right_axes(ax)
	plt.savefig("3EC_PCA_splitting.png", dpi=300)

# Third row
iso_cmph5_base_dirs = ["/hpc/users/beaulj01/projects/ebinning/concoct_integration/bigsix_atcc/10kb_bas_binning/2215_2440_2196/k12_align/cmp_binning"]

scale = 100
scale = 100
fig   = plt.figure(figsize=[8,8])
ax    = fig.add_axes([0.15, 0.15, 0.75, 0.7])
fig, ax = ternary.figure(scale=scale, ax=ax)
for i,iso_cmph5_base_dir in enumerate(iso_cmph5_base_dirs):
	contigs_counters = {}
	contig_lens      = {}
	seen_contigs     = set()
	for strain in ["2215", "2440","2196"]:
		cmph5 = os.path.join(iso_cmph5_base_dir, strain+"_whitelist", "data", "aligned_reads.cmp.h5")
		print cmph5
		reader = CmpH5Reader(cmph5)

		for entry in reader.referenceInfoTable:
			contig_lens[entry[3] + ".%s" % strain] = entry[4]

		for r in reader:
			movie     = r.movieName
			read_spec = movies_spec[movie]
			contig    = r.referenceName + ".%s" % strain
			if contig not in seen_contigs:
				seen_contigs.add(contig)
				contigs_counters[contig] = Counter()
			contigs_counters[contig][read_spec] += 1

	sorted_contig_lens = sorted(contig_lens.items(), key=operator.itemgetter(1), reverse=True)

	X     = []
	Y     = []
	Z     = []
	color = []
	area  = []
	for j,(contig,length) in enumerate(sorted_contig_lens):
		tot_reads       = np.sum(contigs_counters[contig].values())
		reads_pct_2215 = float(contigs_counters[contig]["BAA_2215"])/tot_reads
		reads_pct_2440 = float(contigs_counters[contig]["BAA_2440"])/tot_reads
		reads_pct_2196 = float(contigs_counters[contig]["BAA_2196"])/tot_reads
		print "%s  \t%.3f\t%.3f\t%.3f\t%s\t%s" % (contig, reads_pct_2215, reads_pct_2440, reads_pct_2196, tot_reads, length)
		x = 100*reads_pct_2215
		y = 100*reads_pct_2440
		z = 100*reads_pct_2196
		X.append(x)
		Y.append(y)
		Z.append(z)
		color.append(color_point(x,y,z,scale))
		area.append(math.sqrt(length))

	### Scatter Plot
	ax.boundary(linewidth=1.0)
	ax.gridlines(multiple=10, color="k")
	ax.scatter(zip(X,Y,Z), marker='o', c=color, s=area, linewidths=1, edgecolor='k')
	ax.legend()
	ax.ticks(axis='lbr', linewidth=1, multiple=10, offset=0.03,fsize=font_s)
	ax.left_axis_label(r"$\leftarrow$  "+label_dict["2196"], fontsize=font_s, offset=0.17)
	ax.right_axis_label(r"$\leftarrow$  "+label_dict["2440"], fontsize=font_s, offset=0.17)
	ax.bottom_axis_label(label_dict["2215"]+r"  $\rightarrow$", fontsize=font_s, offset=-0.08)
	ax._redraw_labels()
	ax.clear_matplotlib_ticks(axis="both")
	drawing_ax = ax.get_axes()
	pub_figs.remove_axes_border_and_spines(drawing_ax)

plt.savefig("3EC_split_assemblies_summary.png", dpi=300)