import os,sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import pub_figs
import matplotlib.pyplot as plt
import matplotlib.cm as cm

names   = np.loadtxt("contigs.names",   dtype="str")
lengths = np.loadtxt("contigs.lengths", dtype="int")
covs    = np.loadtxt("contigs.normcovs",    dtype="float")
labels  = np.loadtxt("contigs.kraken",  dtype="str")

font_s = 28
pub_figs.setup_math_fonts(font_size=font_s, font="Computer Modern Sans serif")
fig       = plt.figure(figsize=[24,12])
ax        = fig.add_axes([0.06, 0.1, 0.82, 0.8])

# Take the top two largest contigs from each SCp bin
# bin_contigs = {1: ["unitig_6577", "unitig_66"], \
# 		   	   2: ["unitig_58",   "unitig_40"], \
# 		       3: ["unitig_6591", "unitig_6555"], \
# 		       4: ["unitig_92"], \
# 		       5: ["unitig_6604", "unitig_6579"], \
# 		       6: ["unitig_6573", "unitig_6609"], \
# 		       7: ["unitig_6588", "unitig_6615"], \
# 		       8: ["unitig_6477", "unitig_6595"], \
# 		       9: ["unitig_103"], \
# 				}

# Take the single largest contigs from each SCp bin
bin_contigs = {1: ["unitig_6577"], \
		   	   2: ["unitig_58"], \
		       3: ["unitig_6591"], \
		       4: ["unitig_92"], \
		       5: ["unitig_6604"], \
		       6: ["unitig_6573"], \
		       7: ["unitig_6588"], \
		       8: ["unitig_6477"], \
		       9: ["unitig_103"], \
				}

bin_ids_s = bin_contigs.keys()
bin_ids_s.sort()
# annots  = ["Eggerthellaceae", "Rikenellaceae", "Porphyromonadaceae"]


# colors  = ["k", "b", "r", "m", "c", "g", ""]
colors    = plt.get_cmap('spectral')(np.linspace(0, 0.9, len(bin_contigs.keys())))

plotted_labs = set()
legend_plots = []
legend_labs  = []
for i,bin_id in enumerate(bin_ids_s):
	for contig in bin_contigs[bin_id]:
		idx     = names==contig
		profile = covs[idx,:][0]

		# There are 100 samples but 101 columns because that includes the sum coverage
		lab = "bin%s" % bin_id
		X   = np.arange(1,102)
		plot, = ax.plot(X,profile, color=colors[i], label=lab, linewidth=1.5)
		if lab not in plotted_labs:
			plotted_labs.add(lab)
			legend_plots.append(plot)
			legend_labs.append(lab)
# for i,anno in enumerate(annots):
# 	idx    = labels==anno
# 	zipped = zip( names[idx], lengths[idx], covs[idx,:] )
# 	zipped.sort(key=lambda x: x[1])

# 	# Pick the ten largest contigs from each annotation group
# 	to_plot  = zipped[-10:]
# 	profiles = map(lambda x: x[2], to_plot)

# 	# There are 100 samples but 101 columns because that includes the sum coverage
# 	X = np.arange(1,102)
# 	for profile in profiles:
# 		plot, = ax.plot(X,profile, color=colors[i], label=anno, linewidth=1.2)
# 		if anno not in plotted_labs:
# 			plotted_labs.add(anno)
# 			legend_plots.append(plot)
# 			legend_labs.append(anno)
leg = ax.legend(legend_plots, legend_labs, loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':font_s}, frameon=False)
ax.set_xlim([1,102])
ax.set_xticks(np.arange(0,102,10))
plot_fn = "coverage_profiles.png"
plt.savefig(plot_fn)