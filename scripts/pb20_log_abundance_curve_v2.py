import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os,sys
import math
from collections import Counter

baseline_readlist_fn  = sys.argv[1]
spec_tot_bases_fn     = sys.argv[2]

rank           = np.array(range(1,21))
log            = -np.log(rank)
shift          = 0-(log[-1]) + 0.02
nonnormed      = log+shift
log_abundances = nonnormed/np.sum(nonnormed)

genome_sizes     = {"A_baumannii_ATCC_17978" :               3976747, \
		   			"A_odontolyticus_ATCC_17982_Scfld021" :  2391230, \
		   			"B_cereus_ATCC_10987" :                  5224283, \
		   			"B_vulgatus_ATCC_8482" :                 5163189, \
		   			"C_beijerinckii_NCIMB_8052" :            6000632, \
		   			"D_radiodurans_R1" :                     3060986, \
		   			"E_coli_str_K_12_substr_MG1655" :        4641652, \
		   			"E_faecalis_OG1RF" :                     2739625, \
		   			"H_pylori_26695" :                       1667867, \
		   			"L_gasseri_ATCC_33323" :                 1894360, \
		   			"L_monocytogenes_EGD_e" :                2944528, \
		   			"N_meningitidis_MC58" :                  2272360, \
		   			"P_acnes_KPA171202" :                    2560265, \
		   			"P_aeruginosa_PAO1" :                    6264404, \
		   			"R_sphaeroides_2_4_1" :                  4131542, \
		   			"S_agalactiae_2603V_R" :                 2160267, \
		   			"S_aureus_subsp_aureus_USA300_TCH1516" : 2872915, \
		   			"S_epidermidis_ATCC_12228" :             2499279, \
		   			"S_mutans_UA159" :                       2032925, \
		   			"S_pneumoniae_TIGR4" :                   2160842}

tot_bases = {}
cov       = {}
for line in open(spec_tot_bases_fn, "r").xreadlines():
	line         =     line.strip()
	s            =     line.split("\t")[0]
	s_bases      = int(line.split("\t")[1])
	tot_bases[s] = s_bases
	cov[s]       = float(s_bases) / genome_sizes[s]

species_baseline = []
for s,size in genome_sizes.iteritems():
	if s=="R_sphaeroides_2_4_1":
		continue
	abund = cov[s] / np.sum(cov.values())
	species_baseline.append( (s, size, tot_bases[s], round(cov[s],3), round(abund,3)) )

species_baseline.sort(key=lambda x: x[3], reverse=True)
#####
s     = "R_sphaeroides_2_4_1"
abund = cov[s] / np.sum(cov.values())
species_baseline.append( (s, genome_sizes[s], tot_bases[s], round(cov[s],3), round(abund,3)) )
#####

target_abund  = {}
target_bases  = {}
actual_cov    = {}
fraction      = 1.0
done_looking  = False
while not done_looking:
	print ""
	print "---- %s ----" % fraction
	enough_bases = []
	cov_sum      = np.sum(cov.values()) * fraction
	for i,entry in enumerate(species_baseline):
		s               = entry[0]
		target_abund[s] = log_abundances[i]
		target_bases[s] = int(genome_sizes[s] * target_abund[s] * cov_sum)
		actual_cov[s]   = float(min(tot_bases[s], target_bases[s])) / genome_sizes[s]
		actual_abund    = actual_cov[s] / cov_sum
		print "%s\tAvailable: %s\tNeed: %s\tTarget_abund: %.3f\tActual_abund:%.3f" % (s[:15], tot_bases[s], target_bases[s], target_abund[s], actual_abund)
		if target_bases[s] > tot_bases[s]:
			print "   * Need more bases!"
			enough_bases.append(False)
			fraction -= 0.01
		else:
			enough_bases.append(True)
	if all(enough_bases):
		done_looking = True

found_bases = Counter()
spec_enough = {}
for s in genome_sizes.keys():
	spec_enough[s] = False
f = open("log_abundance_read_info_v2.txt", "w")
for line in open(baseline_readlist_fn, "r").xreadlines():
	if all(spec_enough.values()):
		break
	line            =     line.strip()
	read            =     line.split("\t")[0]
	s               =     line.split("\t")[1]
	bases           = int(line.split("\t")[2])
	found_bases[s] += bases
	if not spec_enough[s]:
		f.write("%s\n" % line)
	if found_bases[s] > target_bases[s]:
		spec_enough[s] = True
f.close()

# Calculate the baseline relative abundances
avail_cov = {}
for s in genome_sizes.keys():
	avail_cov[s] = float(tot_bases[s]) / genome_sizes[s]
avail_abund = []
for s in genome_sizes.keys():
	if s=="R_sphaeroides_2_4_1":
		continue
	abund = avail_cov[s] / np.sum(avail_cov.values())
	avail_abund.append( (s, abund) )
avail_abund.sort(key=lambda x: x[1], reverse=True)
#######
s = "R_sphaeroides_2_4_1"
abund = avail_cov[s] / np.sum(avail_cov.values())
avail_abund.append( (s, abund) )
#######

# Calculate the even relative abundances
# target_abund_species = avail_abund[-1][0]
# even_cov             = avail_cov[target_abund_species]
# even_bases           = {}
# for s in genome_sizes.keys():
# 	even_bases[s] = int(genome_sizes[s] * even_cov)
# even_abund           = []
# for s in genome_sizes.keys():
# 	abund = even_cov / (even_cov*len(genome_sizes))
# 	even_abund.append(abund)
# found_bases = Counter()
# spec_enough = {}
# for s in genome_sizes.keys():
# 	spec_enough[s] = False
# f = open("even_abundance_read_info_v2.txt", "w")
# for line in open(baseline_readlist_fn, "r").xreadlines():
# 	if all(spec_enough.values()):
# 		break
# 	line            =     line.strip()
# 	read            =     line.split("\t")[0]
# 	s               =     line.split("\t")[1]
# 	bases           = int(line.split("\t")[2])
# 	found_bases[s] += bases
# 	if not spec_enough[s]:
# 		f.write("%s\n" % line)
# 	if found_bases[s] > even_bases[s]:
# 		spec_enough[s] = True
# f.close()

fig = plt.figure(figsize=[6,9])
ax  = fig.add_subplot(111)
# ax.plot(rank, map(lambda x: x[1], avail_abund), "b-o", label="Baseline")
ax.plot(rank,log_abundances, "r-o", label="Log-curve (%s%% seq. depth)" % (round(fraction*100)))
# ax.plot(rank,even_abund,     "k-o", label="Even (%sx)" % (int(even_cov)))
bottom_adjust=0.50
left_adjust=0.15
hspace=0.5
wspace=0.2 
plt.gcf().subplots_adjust(bottom=bottom_adjust, left=left_adjust, hspace=hspace, wspace=wspace)
ax.set_ylabel("Relative abundance")
ax.set_xticks(range(1,21))
xlabs = []
for abund in avail_abund:
	s = abund[0]
	xlabs.append(s)
ax.set_xticklabels(xlabs, rotation=90 )
# ax.legend(loc='upper right', prop={'size':14})
plt.savefig("rank_abundances_baseline_even_and_log_v2.png", dpi=300)