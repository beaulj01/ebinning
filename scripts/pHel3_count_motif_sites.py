import sys
from Bio import Entrez
from itertools import groupby
import urllib2
sys.path.append("/hpc/users/beaulj01/gitRepo/eBinning/src")
import read_scanner
from collections import Counter
import numpy as np

fn = "pHel3.fa"

m_J99 = set(["GCCTA", \
"GATC", \
"GANTC", \
"GTAC", \
"RTAYNNNNNRTAY", \
"GAGHNNNNNCTT", \
"CYANNNNNNTGA", \
"TCANNNNNNTRG", \
"CTTTANNNNNNCTT", \
"AAGNNNNNNTAAAG", \
"TCNNGA", \
"GAGG", \
"ATTAAT", \
"TCGA", \
"AAGNNNNNNCTC", \
"AAGNNNNNCTT", \
"CATG", \
"CCGG", \
"CCNNGG", \
"ACGTD", \
"GCGC"])

m_26695 = set(["GAGG", \
"CCTC", \
"CCTTC", \
"GATC", \
"TCGA", \
"ATTAAT", \
"GCGA", \
"GTNNAC", \
"GCGC", \
"CATG", \
"GANTC", \
"GAAGA", \
"TCTTC"])

m_JP26 = set(["GTAC", \
"TCGA" , \
"CRTANNNNNNNTC", \
"GANNNNNNNTAYG", \
"TCNNGA" , \
"GAGG", \
"GAATTC", \
"CYANNNNNNTTC", \
"GAANNNNNNTRG", \
"TGCA" , \
"CTRYAG", \
"CCTCTAGB", \
"GMRGA" , \
"CATG", \
"TCTTC", \
"CCGG", \
"GGCCH", \
"GCGC"])

m_coli = set(["GATC", \
"AACNNNNNNGTGC", \
"GCACNNNNNNGTT", \
"CCWGG"])

m_coli_CFT073 = set(["GATC", \
"CACAG", \
"GAGNNNNNNNGTCA"])

JP26_only  = [m for m in m_JP26 if m not in m_26695 and m not in m_J99 and m not in m_coli and m not in m_coli_CFT073]
J99_only   = [m for m in m_J99 if m not in m_26695 and m not in m_JP26 and m not in m_coli and m not in m_coli_CFT073]
only_26695 = [m for m in m_26695 if m not in m_J99 and m not in m_JP26 and m not in m_coli and m not in m_coli_CFT073]
coli_only  = [m for m in m_coli if m not in m_J99 and m not in m_JP26 and m not in m_26695 and m not in m_coli_CFT073]
coli_CFT_only = [m for m in m_coli_CFT073 if m not in m_J99 and m not in m_JP26 and m not in m_26695 and m not in m_coli]
print "JP26_only", JP26_only
print "J99_only", J99_only
print "26995_only", only_26695
print "coli_only", coli_only
print "coli_CFT_only", coli_CFT_only

def count_motifs( fasta_fn, motif ):
	motifs_n  = Counter()
	sizes_d   = {}
	motif     = motif
	for i,(name,seq) in enumerate(fasta_iter(fasta_fn)):
		name = name.split("|")[0]
		sizes_d[name]   = len(seq)
		matches_list = read_scanner.find_motif_matches("bas", motif, seq.upper(), 0)
		for match in matches_list:
			motifs_n[name] += 1
	return np.sum(motifs_n.values())

def fasta_iter( fasta_name ):
	fh = open(fasta_name)
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		header = header.next()[1:].strip()
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

for motif in JP26_only:
	n = count_motifs( fn, motif )
	print "JP26", fn, motif, n

for motif in J99_only:
	n = count_motifs( fn, motif )
	print "J99", fn, motif, n

for motif in only_26695:
	n = count_motifs( fn, motif )
	print "26695", fn, motif, n

for motif in coli_only:
	n = count_motifs( fn, motif )
	print "coli_K12", fn, motif, n

for motif in coli_CFT_only:
	n = count_motifs( fn, motif )
	print "coli_CFT", fn, motif, n