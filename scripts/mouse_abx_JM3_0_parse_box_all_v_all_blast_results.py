import os,sys
import numpy as np
from Bio import SeqIO

out = np.loadtxt("all_vs_all_boxes.out", dtype="str", delimiter="\t")
gbk_dir = "/hpc/users/beaulj01/projects/ebinning/mouse_abx/JM3/0/10kb/contig_binning/rerun_motif_discov/debug/box_assemblies/RAST_gbks"

qseqid   = 0
sseqid   = 1
pident   = 2
length   = 3
qlen     = 4
qstart   = 5
qend     = 6
slen     = 7
sstart   = 8
send     = 9
mismatch = 10
gapopen  = 11
evalue   = 12
bitscore = 13

for i in range(out.shape[0]):
	if out[i,qseqid]!=out[i,sseqid]:
		# if float(out[i,pident]) > 99.9:
		if float(out[i,pident]) > 99.5:
			if int(out[i,length]) > 5000:
				# if int(out[i,qlen]) > 100000:

				# if int(out[i,slen])<200000:

					# Get the gene products in the regions of interest
					q_contig = "_".join(out[i,qseqid].split("_")[1:])
					q_box    = out[i,qseqid].split("_")[0]
					q_gbk_fn = os.path.join(gbk_dir, "%s.gbk" % q_box) 

					s_contig = "_".join(out[i,sseqid].split("_")[1:])
					s_box    = out[i,sseqid].split("_")[0]

					# if q_box=="box7" and q_contig=="unitig_85" and s_box=="box3":
					print "\t".join(out[i,:])

					for record in SeqIO.parse(open(q_gbk_fn,"r"), "genbank"):
						record_contig = record.annotations["accessions"][0].split("|")[0]

						if q_contig == record_contig:
							for feature in record.features:
								if feature.type=="CDS":
									protein = feature.qualifiers["product"][0]
									start   = feature.location.nofuzzy_start
									end     = feature.location.nofuzzy_end
									if start>=int(out[i,qstart]) and end<=int(out[i,qend]):
										print "     ", protein, start, end
