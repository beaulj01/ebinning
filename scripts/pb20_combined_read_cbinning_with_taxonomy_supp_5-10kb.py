import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import scipy
from scipy.stats import gaussian_kde
import sklearn.cluster as cluster
import pub_figs
import random
from pbcore.io.BasH5IO import BasH5Reader
from itertools import groupby
from collections import Counter
import locale
locale.setlocale(locale.LC_ALL, 'en_US')

pub_figs.setup_math_fonts(font_size=18, font="Computer Modern Sans serif")

glyphs_d = {"Acinetobacter baumannii": 		([ 0., 0., 0., 1.], 			   			"o"), \
			"Actinomyces odontolyticus": 	([ 0.40264314, 0., 0.46010196, 1.],			"v"), \
			"Bacillus cereus": 				([ 0.51501765, 0., 0.5816902, 1.], 			"^"), \
			"Bacteroides vulgatus": 		([ 0.21959412, 0., 0.63923529, 1.],			"s"), \
			"Clostridium beijerinckii": 	([ 0., 0., 0.75689608, 1.],        			"D"), \
			"Deinococcus radiodurans": 		([ 0., 0.49022353, 0.8667, 1.],    			"o"), \
			"Enterococcus faecalis": 		([ 0., 0.60261569, 0.85885686, 1.],			"v"), \
			"Escherichia coli": 			([ 0., 0.66016078, 0.68630784, 1.],			"^"), \
			"Helicobacter pylori": 			([ 0., 0.6667,  0.56468824, 1.],   			"s"), \
			"Lactobacillus gasseri": 		([ 0., 0.62484902, 0.19868039, 1.],			"D"), \
			"Listeria monocytogenes": 		([ 0., 0.66534314, 0., 1,],        			"o"), \
			"Neisseria meningitidis": 		([ 0., 0.78038235, 0., 1.],        			"v"), \
			"Propionibacterium acnes": 		([ 0., 0.89545098, 0., 1.],        			"^"), \
			"Pseudomonas aeruginosa":  		([ 0.05751373, 1., 0., 1.],        			"s"), \
			"Rhodobacter sphaeroides": 		([0.690164705882,1.0,0.0,1.0],     			"D"), \
			"Staphylococcus aureus": 		([0.894084313725,0.946378431373,0.0,1.0],	"o"), \
			"Staphylococcus epidermidis": 	([0.977766666667,0.844433333333,0.0,1.0],	"v"), \
			"Streptococcus agalactiae": 	([1.0,0.694117647059,0.0,1.0],     			"^"), \
			"Streptococcus mutans": 		([1.0,0.317647058824,0.0,1.0],     			"s"), \
			"Streptococcus pneumoniae": 	([0.955566666667,0.0,0.0,1.0],     			"D"), \
			"Unlabeled":		 			("grey", "*"), \
			}

data     = np.loadtxt("both.comp.2D.5-10kb", dtype="float")
labels   = np.loadtxt("both.labels.5-10kb",  dtype="S40")
names    = np.loadtxt("both.names.5-10kb",   dtype="str")
title    = "combined cBinning"
reads_tax_fn = "reads.tax.unordered"
corrected_fn = "filtered_subreads.fasta"

def fasta_iter( fasta_name ):
	fh = open(fasta_name)
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		header = header.next()[1:].strip()
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

def move_unknowns_to_bottom( leg_tup ):
	for tup in leg_tup:
		if tup[0].split(":")[0]=="Unlabeled":
			unknown_tup = tup
	leg_tup.remove(unknown_tup)
	leg_tup.append(unknown_tup)
	return leg_tup

corrected = set()
for name,seq in fasta_iter(corrected_fn):
	read = "/".join(name.split("/")[:2])
	corrected.add(read)

read_specs = {}
for line in open(reads_tax_fn, "r").xreadlines():
	line     = line.strip()
	if len(line.split("\t"))!=2:
		continue
	subread  = line.split("\t")[0]
	tax      = line.split("\t")[1]

	readname = "/".join(subread.split("/")[:2])
	levels   = tax.split("|")

	# Try for a species-level annotation
	spec = [lev for lev in levels if lev[:3]=="s__"]
	if len(spec)==1:
		spec = spec[0][3:]
		if spec=="Cutibacterium_acnes":
			spec = "Propionibacterium_acnes"
		read_specs[readname] = spec.replace("_", " ")

both_names    = set(names)

for i,name in enumerate(names):
	readname = "/".join(name.split("/")[:2])

	if readname.find("unitig")>-1:
		labels[i] = "Contig"
	elif readname in corrected:
		if read_specs.get(readname):
			labels[i] = read_specs[readname]
		else:
			labels[i] = "Unlabeled"
	else:
		labels[i] = "Uncorrected read"

# Filter contig labels; if label only present on one contig, call "Unlabeled contig"
lab_counter = Counter()
for i,label in enumerate(labels):
	if names[i].find("m1")>-1:
		lab_counter[label] += 1

for i,label in enumerate(labels):
	if names[i].find("m1")>-1:
		if lab_counter[label]<25 or len(labels[i].split(" "))==1:
			labels[i] = "Unlabeled"

lab_counter = Counter()
for i,label in enumerate(labels):
	if names[i].find("m1")>-1:
		lab_counter[label] += 1

labs_ord = lab_counter.keys()
labs_ord.sort()
for label in labs_ord:
	print label, lab_counter[label]

print "All sequences",     len(labels)
print "Unlabeled",         len(labels[labels=="Unlabeled"])
print "Uncorrected reads", len(labels[labels=="Uncorrected read"])
print "Contigs",           len(labels[labels=="Contig"])

fig        = plt.figure(figsize=[15,12])
ax         = fig.add_axes([0.05, 0.15, 0.85, 0.85])
# ax.axis("off")
pub_figs.remove_top_right_axes( ax )

background_labels = []
for i,name in enumerate(names):
	if name.find("m1")>-1:
		background_labels.append("Raw read")
	else:
		background_labels.append(labels[i])
background_labels = np.array(background_labels)
reads_comp        = data[background_labels=="Raw read"]           
extent            = [-41,38,-36,40]
bins              = 50
H, xedges, yedges = np.histogram2d(reads_comp[:,0], reads_comp[:,1], bins=bins, range=[extent[:2], extent[2:]])
# H = np.rot90(H)
# H = np.flipud(H)
# Hmasked = np.ma.masked_where(H==0,H)         # Mask pixels with a value of zero
# plt.hot()                                    # set 'hot' as default colour map
# im = plt.imshow(H, interpolation='bilinear', # creates background image
#                 origin='lower', cmap=cm.Greys, 
#                 extent=extent)

lab_set    = set(labels)
label_list = list(lab_set)
label_list.sort()
label_list.remove("Unlabeled")
label_list.append("Unlabeled")
colors    = plt.get_cmap('spectral')(np.linspace(0.0, 0.95, len(label_list)))
shapes    = ["o", "v", "^", "s", "D"]
res       = []
for k,target_lab in enumerate(label_list):
	if target_lab=="Uncorrected read" or target_lab=="Contig" or target_lab=="Raw read":
		continue
	idxs  = [j for j,label in enumerate(labels) if label==target_lab]
	X     = data[idxs,0]
	Y     = data[idxs,1]
	# color = colors[k]
	# shape = shapes[k%len(shapes)]
	color = glyphs_d[target_lab][0]
	shape = glyphs_d[target_lab][1]
	for i,x in enumerate(X):
		res.append( (x, Y[i], target_lab, color, shape) ) 
np.random.shuffle(res) 
plotted_labs = set()
legend_plots = []
legend_labs  = []
for i,(x,y, target_lab, color, shape) in enumerate(res):
	if target_lab=="Unlabeled":
		shape = "*"
		color = "grey"
	plot = ax.scatter(x,y, marker=shape, s=15 , edgecolors=color, label=target_lab, facecolors="None", linewidth=1.2, alpha=0.8)
	if target_lab not in plotted_labs:
		plotted_labs.add(target_lab)
		legend_plots.append(plot)
		N_str = locale.format("%s", lab_counter[target_lab], grouping=True)
		if target_lab!="Unlabeled":
			target_str = r"\textit{%s}" % target_lab
		else:
			target_str = target_lab
		legend_labs.append((target_str+": %s reads" % N_str))
box     = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.6, box.height * 0.7])
leg_tup = zip(legend_labs,legend_plots)
leg_tup.sort(key=lambda x: x[0])
leg_tup = move_unknowns_to_bottom( leg_tup )
legend_labs, legend_plots = zip(*leg_tup)
leg = ax.legend(legend_plots, legend_labs, loc='center left', prop={'size':18}, bbox_to_anchor=(1, 0.5), frameon=False, scatterpoints=1)
for i in range(len(legend_labs)):
	leg.legendHandles[i]._sizes = [100]

minx = min(map(lambda x: x[0], res))
maxx = max(map(lambda x: x[0], res))
miny = min(map(lambda x: x[1], res))
maxy = max(map(lambda x: x[1], res))
# ax.set_xlim([minx-3, maxx+3])
# ax.set_ylim([miny-3, maxy+3])
ax.set_xlim([-30, 30])
ax.set_ylim([-30, 30])
fig_main = plt.gcf()
ax_main  = plt.gca()

def plot_zoom( ax_main, box_dims, labels, data, z ):
	box_xmin = box_dims[0]
	box_xmax = box_dims[1]
	box_ymin = box_dims[2]
	box_ymax = box_dims[3]
	ax_main.plot([box_xmin,box_xmax,box_xmax,box_xmin,box_xmin], [box_ymax,box_ymax,box_ymin,box_ymin,box_ymax], linestyle="--", color="k", linewidth=2, dashes=(3,2))
	
	fig        = plt.figure(figsize=[12,12])
	ax         = fig.add_axes([0, 0, 1, 1])
	ax.axis("off")
	ax.set_xlim([box_xmin, box_xmax])
	ax.set_ylim([box_ymin, box_ymax])
	lab_set    = set(labels)
	label_list = list(lab_set)
	label_list.sort()
	label_list.remove("Unlabeled")
	label_list.append("Unlabeled")
	colors    = plt.get_cmap('spectral')(np.linspace(0.0, 0.95, len(label_list)))
	# shapes    = ["o", "v", "^", "s", "D"]
	shapes    = ["o", "v", "^", "D"]
	res       = []
	for k,target_lab in enumerate(label_list):
		if target_lab=="Uncorrected read" or target_lab=="Contig":
			continue
		idxs  = [j for j,label in enumerate(labels) if label==target_lab]
		X     = data[idxs,0]
		Y     = data[idxs,1]
		color = colors[k]
		for i,x in enumerate(X):
			res.append( (x, Y[i], target_lab, color, shapes[k%len(shapes)]) ) 
	np.random.shuffle(res) 
	plotted_labs = set()
	legend_plots = []
	legend_labs  = []
	for i,(x,y, target_lab, color, shape) in enumerate(res):
		if target_lab=="Unlabeled":
			shape = "*"
			color = "r"
		plot = ax.scatter(x,y, marker=shape, s=8000 , edgecolors=color, label=target_lab, facecolors="None", linewidth=4, alpha=0.8)
		if target_lab not in plotted_labs:
			plotted_labs.add(target_lab)
			legend_plots.append(plot)
			legend_labs.append(target_lab)
	# box     = ax.get_position()
	# ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
	# leg_tup = zip(legend_labs,legend_plots)
	# leg_tup.sort(key=lambda x: x[0])
	# leg_tup = move_unknowns_to_bottom( leg_tup )
	# legend_labs, legend_plots = zip(*leg_tup)
	# leg = ax.legend(legend_plots, legend_labs, loc='center left', prop={'size':14}, bbox_to_anchor=(1, 0.5), frameon=False, scatterpoints=1)
	# for i in range(len(legend_labs)):
	# 	leg.legendHandles[i]._sizes = [150]
	ax.set_xlim([box_xmin, box_xmax])
	ax.set_ylim([box_ymin, box_ymax])
	fig.savefig("reads.comp.tax.zoom.%s.png" % z)

	# box_corrected_names = set()
	# for i in range(data.shape[0]):
	# 	if names[i] in corrected:
	# 		x = data[i,0]
	# 		y = data[i,1]
	# 		if y>box_ymin and y<box_ymax and x>box_xmin and x<box_xmax:
	# 			box_corrected_names.add(names[i])
	# 			print names[i]

	# print "Found %s reads in box..." % len(box_corrected_names)
	# # f = open("box_corrected_reads.fasta", "w")
	# f = open("box_reads.%s.fasta" % z, "w")
	# for name,seq in fasta_iter(corrected_fn):
	# 	if "/".join(name.split("/")[:2]) in box_corrected_names:
	# 		f.write(">%s\n" % name)
	# 		f.write("%s\n" % seq)
	# f.close()

box_dims = [-9.5, -6.5, -20.5, -23.5]
# plot_zoom( ax_main, box_dims, labels, data, 1 )

box_dims = [-6.0, 3, -21.5, -15.0]
# plot_zoom( ax_main, box_dims, labels, data, 2 )

fig_main.savefig("reads.comp.tax.5-10kb.png", dpi=300)