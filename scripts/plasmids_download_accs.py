import os,sys
import numpy as np
from itertools import groupby
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Bio import Entrez
import subprocess
import re
import string
import urllib2
from bs4 import BeautifulSoup
from collections import defaultdict,Counter
import pub_figs

all_accs      = np.loadtxt(sys.argv[1], dtype="str")
all_fastas_fn = "ebi_plasmids.fasta"

def run_OS_command( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts != 0:
		print "Failed command: %s" % CMD
	return sts, stdOutErr

# Download all the fastas from this NCBI page
db           = "nuccore"
Entrez.email = "john.beaulaurier@mssm.edu"
batchSize    = 100
retmax       = 10**9

tmp_fasta_name = "tmp.fasta"
for k,start in enumerate(range( 0,len(all_accs),batchSize )):
	#first get GI for query accesions
	batch_accs = all_accs[start:(start+batchSize)]
	sys.stderr.write( "Fetching %s entries from GenBank: %s...\n" % (len(batch_accs), ", ".join(batch_accs[:10])))
	query  = " ".join(batch_accs)
	handle = Entrez.esearch( db=db,term=query,retmax=retmax )
	giList = Entrez.read(handle)['IdList']
	sys.stderr.write( "Found %s GI: %s\n" % (len(giList), ", ".join(giList[:10])))
	#post NCBI query
	search_handle     = Entrez.epost(db=db, id=",".join(giList))
	search_results    = Entrez.read(search_handle)
	webenv,query_key  = search_results["WebEnv"], search_results["QueryKey"]
	#fetch entries in batch
	# handle = Entrez.efetch(db=db, rettype="fasta", retstart=start, retmax=batchSize, webenv=webenv, query_key=query_key)
	handle = Entrez.efetch(db=db, rettype="fasta", webenv=webenv, query_key=query_key)
	# sys.stdout.write(handle.read())
	fa = open(tmp_fasta_name, "wb")
	fa.write(handle.read())
	fa.close()

	# Output all the fasta sequences for the plasmids
	cat_CMD = "cat %s %s > tmp.combo.fasta" % (all_fastas_fn, tmp_fasta_name)
	sts,stdOutErr = run_OS_command( cat_CMD )

	mv_CMD  = "mv tmp.combo.fasta %s" % all_fastas_fn
	sts,stdOutErr = run_OS_command( mv_CMD )

	os.remove(tmp_fasta_name)

