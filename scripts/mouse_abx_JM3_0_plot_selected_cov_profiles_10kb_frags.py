import os,sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import pub_figs
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import random

uniq_frags = set(np.loadtxt(sys.argv[1], dtype="str"))
in_mat     = np.loadtxt("original_data_gt1000.csv", delimiter=",", dtype="str", skiprows=1)

lengths = np.loadtxt("contigs.lengths.orig", dtype="int")
names   = np.loadtxt("contigs.names.orig",   dtype="str")

font_s = 24
pub_figs.setup_math_fonts(font_size=font_s, font="Computer Modern Sans serif")
fig       = plt.figure(figsize=[20,10])
ax        = fig.add_axes([0.06, 0.1, 0.7, 0.8])

# Take the single largest contigs from each SCp bin
bin_contigs = {1: ["unitig_6577"], \
		   	   2: ["unitig_58"], \
		       3: ["unitig_6591"], \
		       4: ["unitig_92"], \
		       5: ["unitig_6604"], \
		       6: ["unitig_6573"], \
		       7: ["unitig_6588"], \
		       8: ["unitig_6477"], \
		       9: ["unitig_103"], \
				}

len_dict = dict(zip(names, lengths))


bin_ids_s = bin_contigs.keys()
bin_ids_s.sort()

colors    = plt.get_cmap('spectral')(np.linspace(0, 0.9, len(bin_ids_s)))

plotted_labs = set()
legend_plots = []
legend_labs  = []
for i,bin_id in enumerate(bin_ids_s):
	for contig in bin_contigs[bin_id]:
		frag_idxs = [k for k in range(in_mat.shape[0]) if in_mat[k,0].split(".")[0]==contig and in_mat[k,0] in uniq_frags]
		profiles  = in_mat[frag_idxs,137:].astype(float)
		
		# Calc the mean of the coverage profiles for a contig's frags
		mean_profile = profiles.mean(axis=0)
		
		# There are 100 samples but 101 columns because that includes the sum coverage
		X   = np.arange(1,102)
		lab = "Bin%s: %.2f Mp contig" % (bin_id, float(len_dict[contig])/1000000 )
		
		plot, = ax.plot(X,mean_profile, color=colors[i], label=lab, linewidth=1.5)
		if lab not in plotted_labs:
			plotted_labs.add(lab)
			legend_plots.append(plot)
			legend_labs.append(lab)
leg = ax.legend(legend_plots, legend_labs, loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':font_s}, frameon=False)
ax.set_xlim([1,102])
ax.set_title("Largest contigs from each bin: unique sequences only")
ax.set_xticks(np.arange(0,102,10))
ax.set_xlabel("Sample from Xiao et al")
ax.set_ylabel("Coverage (norm)")
plot_fn = "coverage_profiles.uniq.png"
plt.savefig(plot_fn)