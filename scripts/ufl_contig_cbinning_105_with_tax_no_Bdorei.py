import os,sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import scipy
from scipy.stats import gaussian_kde
import sklearn.cluster as cluster
import pub_figs
import random
from pbcore.io.BasH5IO import BasH5Reader
from itertools import groupby,izip
from collections import Counter

contigs_noBD_labels  = "no_bdorei.contigs.unordered.labels"
# Build set of non-B. dorei subread + contig names
specs = {}
labs  = Counter()
for line in open(contigs_noBD_labels, "r").xreadlines():
	line = line.strip()
	contig = line.split("\t")[0].split("|")[0]
	tax  = line.split("\t")[1]
	spec = " ".join(tax.split(";")[-1].split(" ")[:2])
	if len(spec.split(" "))==1:
		continue
	specs[contig] = spec
	labs[spec] += 1

contigs_names_fn   = "contigs.noBD.names"
contigs_labels_fn  = "contigs.noBD.labels"
contigs_comp_fn    = "contigs.noBD.comp"
contigs_lengths_fn = "contigs.noBD.lengths"
f_lab = open(contigs_labels_fn,  "w")
f_nam = open(contigs_names_fn,   "w")
f_len = open(contigs_lengths_fn, "w")
f_com = open(contigs_comp_fn,    "w")
for i,(name,length,comp) in enumerate(izip( open("contigs.names"), open("contigs.lengths"), open("contigs.comp"))):
	name   = name.strip("\n")
	length = int(length.strip())
	comp   = comp.strip("\n")
	if specs.get(name):
		f_lab.write("%s\n" % "_".join(specs[name].split(" ")))
		f_nam.write("%s\n" % name)
		f_len.write("%s\n" % length)
		f_com.write("%s\n" % comp)
f_lab.close()
f_nam.close()
f_len.close()
f_com.close()


