import os,sys
import matplotlib as mpl; mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from collections import defaultdict
import re

flat_fn  = sys.argv[1]
n_reads  = 1000
q_motif  = "GATC-1"

FIG_SIZE = [8,8]

all_reads_ipd_means = []
all_reads_QV_means = []

def sub_bases( motif ):
	subs = {"W":"[AT]",  \
			"S":"[CG]",  \
			"M":"[AC]",  \
			"K":"[GT]",  \
			"R":"[AG]",  \
			"Y":"[CT]",  \
			"B":"[CGT]", \
			"D":"[AGT]", \
			"H":"[ACT]", \
			"V":"[ACG]", \
			"N":"[ACGT]"}
	for symbol,sub in subs.iteritems():
		if motif.find(symbol) > -1:
			motif = motif.replace(symbol, sub)
	return motif

def rev_comp_motif( motif ):
	COMP = {"A":"T", \
			"T":"A", \
			"C":"G", \
			"G":"C", \
			"W":"S", \
			"S":"W", \
			"M":"K", \
			"K":"M", \
			"R":"Y", \
			"Y":"R", \
			"B":"V", \
			"D":"H", \
			"H":"D", \
			"V":"B", \
			"N":"N", \
			"X":"X", \
			"*":"*"}
	rc_motif = []
	for char in motif[::-1]:
		rc_motif.append( COMP[char] )
	return "".join(rc_motif)

def find_motif_matches( motif, read_str ):
	q_motif      = sub_bases( rev_comp_motif(motif) )
	matches_iter = re.finditer(q_motif, read_str)
	matches_list = []
	for match in matches_iter:
		matches_list.append(match)
	return matches_list

for q_motif in ["GATC-1", "CATC-1"]:
	plt.figure(figsize=FIG_SIZE)
	QV_ipd_dists = defaultdict(list)
	for i,line in enumerate(open(flat_fn, "r").xreadlines()):
		if i==n_reads: break
		
		read_str = line.split(" ")[4]
		seq      = list(read_str)
		ipds     = map(lambda x: float(x), line.split(" ")[5].split(","))
		QVs      = map(lambda x: int(x),   line.split(" ")[6].split(","))
		
		ref_index    = int(q_motif.split("-")[1])
		motif        =     q_motif.split("-")[0]
		rc_index     = len(motif) - 1 - ref_index
		matches_list = find_motif_matches( motif, read_str )
		motif_ipds   = []
		motif_QVs    = []
		for match in matches_list:
			motif_start = match.span()[0]
			motif_end   = match.span()[1]
			motif_ipds.append( ipds[motif_start:motif_end] )
			motif_QVs.append(   QVs[motif_start:motif_end] )
		
		site_ipds = map(lambda x: x[rc_index], motif_ipds)
		
		site_qvs  = map(lambda x: x[rc_index], motif_QVs)
		if len(site_ipds) == 0 or len(site_qvs) == 0:
			continue
		all_reads_ipd_means.append( np.mean(site_ipds) )
		all_reads_QV_means.append(  np.mean(site_qvs) )

		for j,QV in enumerate(site_qvs):
			QV_ipd_dists[QV].append(site_ipds[j])

	data = []
	for QV in range(max(QV_ipd_dists.keys())+1):
		data.append(QV_ipd_dists[QV])

	plt.boxplot(data)
	plt.xlabel("Base QV")
	plt.ylabel("IPD (frames)")
	plt.ylim([0,500])
	fn = "ipd_vs_QV_%s.png" % q_motif
	plt.savefig(fn)
	print "Mean IPD of %s = %.3f" % (q_motif, np.mean(all_reads_ipd_means))
	print "Mean QV of  %s = %.3f" % (q_motif, np.mean(all_reads_QV_means))
