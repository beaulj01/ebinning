import sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import random
import pub_figs

pub_figs.setup_math_fonts(24, "Arial")

label_dict = {"HP26695": r"${H. pylori}$ 26695", \
			  "HPJ99": r"${H. pylori}$ J99", \
			  "EcoliC227": r"${E. coli}$ C227", \
			  "Steno": r"${S. maltophilia}$"}

def scatterplot(results, labels, plot_fn, title):
	label_set = list(set(labels))
	label_set.sort()
	c1 = cm.spectral(float(0)  / 20)
	c2 = cm.spectral(float(15) / 20)
	c3 = cm.spectral(float(17) / 20)
	c4 = cm.spectral(float(6)  / 20)
	colors    = [c1, c2, c3, c4]
	fig       = plt.figure(figsize=[15,12])
	ax        = fig.add_axes([0.1, 0.1, 0.8, 0.8])
	ax.axis("off")
	res       = []
	for k,target_lab in enumerate(label_set):
		idxs  = [j for j,label in enumerate(labels) if label==target_lab]
		X     = results[idxs,0]
		Y     = results[idxs,1]
		color = colors[k]
		for i,x in enumerate(X):
			res.append( (x, Y[i], target_lab, color) ) 

	np.random.shuffle(res)
	plotted_labs = set()
	legend_plots = []
	legend_labs  = []
	for (X,Y,target_lab, color) in res:
		plot = ax.scatter(X, Y, marker="o", s=25 , edgecolors="None", label=label_dict[target_lab], facecolors=color)
		# plot = ax.scatter(X, Y, marker="o", s=15 , edgecolors="None", label=label_dict[target_lab], facecolors="k",alpha=0.1)
		if target_lab not in plotted_labs:
			plotted_labs.add(target_lab)
			legend_plots.append(plot)
			legend_labs.append(label_dict[target_lab])

	# box = ax.get_position()
	# ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
	leg_tup = zip(legend_labs,legend_plots)
	leg_tup.sort(key=lambda x: x[0])
	legend_labs, legend_plots = zip(*leg_tup)
	# ax.legend(legend_plots, legend_labs, bbox_to_anchor=(1.1, 1.10), prop={'size':24}, scatterpoints=1, frameon=False)
	ax.legend(legend_plots, legend_labs, bbox_to_anchor=(1.1, 1.10), scatterpoints=1, frameon=False)
	# ax.legend(legend_plots, legend_labs, bbox_to_anchor=(0.95, 1.05), prop={'size':24}, scatterpoints=1, frameon=False)
	# ax.legend(legend_plots, legend_labs, loc='lower right', prop={'size':24}, frameon=False)
	# ax.legend(legend_plots, legend_labs, loc='upper right', prop={'size':24}, frameon=False)
	xmin = min(results[:,0])
	xmax = max(results[:,0])
	ymin = min(results[:,1])
	ymax = max(results[:,1])
	ax.set_xlim([xmin-1, xmax+1])
	ax.set_ylim([ymin-1, ymax+1])
	plt.savefig(plot_fn)

results = np.loadtxt(sys.argv[1], dtype="float")
labels  = np.loadtxt(sys.argv[2], dtype="str")
plot_fn = "pub.reads.raw.SMp.png"
title   = "read-level eBinning"

scatterplot(results, labels, plot_fn, title)