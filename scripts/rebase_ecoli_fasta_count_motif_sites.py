import sys
from Bio import Entrez
from itertools import groupby
import urllib2
# sys.path.append("/Users/beaulj01/mothra_home/gitRepo/eBinning/src")
sys.path.append("/hpc/users/beaulj01/gitRepo/eBinning/src")
import read_scanner
from collections import Counter
import numpy as np

fns       = ["CP015229.1.fa", \
			 "CP013662.1.fa", \
			 "CP015228.1.fa", \
			 "CP015240.1.fa", \
			 "CP015241.1.fa", \
			 "CP010371.1.fa", \
			 "CP009106.2.fa", \
			 "CP014268.2.fa", \
			 "CP014269.1.fa", \
			 "CP011416.1.fa", \
			 "NC_004431.1.fa", \
			 "CP016404.1.fa", \
			 "CP016182.1.fa", \
			 "CP015159.1.fa", \
			 "CP009859.1.fa", \
			 "CP014667.1.fa", \
			 "CP009578.1.fa", \
			 "CP013663.1.fa", \
			 "CP014272.1.fa", \
			 "CP014270.1.fa", \
			 "CP014197.1.fa", \
			 "CP011495.1.fa", \
			 "CP011331.1.fa", \
			 "CP007133.1.fa", \
			 "CP015832.1.fa", \
			 "CP015831.1.fa", \
			 "HG941718.1.fa", \
			 "CP009104.1.fa", \
			 "CP011061.1.fa", \
			 "JMIZ01000004_JMIZ01000006_JMIZ01000008_JMIZ01000009.1.fa", \
			 "JMJA01000001_JMJA01000002.1.fa", \
			 "JMJB01000001.1.fa", \
			 "JMJC01000003_JMJC01000005_JMJC01000010_JMJC01000011_JMJC01000012_JMJC01000014.1.fa", \
			 "JMJD01000001_JMJD01000003.1.fa", \
			 "JMJE01000004_JMJE01000006_JMJE01000011_JMJE01000013_JMJE01000015_JMJE01000025.1.fa", \
			 "CP011134.1.fa", \
			 "NC_000913.3.fa"]

motifs = ["CCWGG TTCANNNNNNNNCTGG GATC", \
		  "CCWGG ACGNNNNNNTGGT CGTANNNNNGTC GATC SAGCTS", \
		  "CCWGG CCANNNNNNNNNTTGG CTGCAG GATC TGANNNNNNTCCA", \
		  "RGACAG CCWGG GATC", \
		  "CACNNNNNNNCTGG GAAABCC CCWGG GATC", \
		  "CCANNNNNNNNTGAA GATC DHCCTGGBB CCWGG", \
		  "CAGCTB CCWGG GATC", \
		  "GATC", \
		  "GATC", \
		  "BTTGGTAVY BRGTGTCGA CCWGG GATC", \
		  "GAGNNNNNNNGTCA CACAG GATC", \
		  "GATC TTAGNNNNGRTC CCWGG", \
		  "CCWGG GATC", \
		  "AACNNNNCTTT ACYNNNNNNGTTC GAGACC GATC", \
		  "GTANNNNNCGATC CCTGGBV GATGNNNNNNTAC AACNNNNNNGTGC YTCANNNNNNGTTY GATC", \
		  "CCWGG GATC GGANNNNNNNNATGC GACNNNNNNGTC", \
		  "GATC", \
		  "CCWGG CTGCAG CCANNNNNNNNNTTGG GATC TGANNNNNNTCCA", \
		  "AACNNNNNNGTGC GATC", \
		  "AACNNNNNNGTGC CCWGG GATC", \
		  "RCCGGCRYD RCCGGY CCWGG GATC", \
		  "GATC AACNNNNNNGTGC", \
		  "GATC CCWGG CTGCAG RTCANNNNNNNNGTGG ACCACC", \
		  "CAGCTCYNRT ANCCTRGTNNNTA CCWGG GATC", \
		  "GAAABCC CACNNNNNNNCTGG CCAYNNNNNGTTY CCWGG GATC", \
		  "GAAABCC CCWGG CACNNNNNNNCTGG CCAYNNNNNGTTY GATC", \
		  "CANCATC AACNNNNCTTT GGTCTC CACNNNNGTAY GATC", \
		  "CCWGG GATC", \
		  "YGGTACCR TGANNNNNNCTTC CCAGG GATC", \
		  "GATC GATGNNNNNCTG CCWGG TGGCCA", \
		  "GAGNNNNNNNGTCA CACAG CGGYCG CCWGG GATC", \
		  "GAGNNNNNNNGTCA CACAG GATC", \
		  "TGGCCA AACNNNNCTTT GAGACC CACNNNNGTAY GATC", \
		  "CAGCAG AGCANNNNNNTGA CCWGG GATC", \
		  "CCANNNNNNNCTTC GATC", \
		  "TGANNNNNNNNTGCT CCWGG GATC", \
		  "AACNNNNNNGTGC CCWGG GATC"]

def count_motifs( fasta_fn, motif ):
	motifs_n  = Counter()
	sizes_d   = {}
	for i,(name,seq) in enumerate(fasta_iter(fasta_fn)):
		name = name.split("|")[0]
		sizes_d[name]   = len(seq)
		matches_list = read_scanner.find_motif_matches("bas", motif, seq, 0)
		for match in matches_list:
			motifs_n[name] += 1
	return np.sum(motifs_n.values())

def fasta_iter( fasta_name ):
	fh = open(fasta_name)
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		header = header.next()[1:].strip()
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

motifs_set = set()
fn_motifs  = {}
for i,fn in enumerate(fns):
	fn_motifs[fn] = motifs[i].split(" ")
	for motif in fn_motifs[fn]:
		n = count_motifs( fn, motif )
		print fn, motif, n
		motifs_set.add(motif)
	print ""

m = list(motifs_set)
m.sort()
print ",".join(m)

