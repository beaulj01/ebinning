import os,sys
from itertools import groupby
import numpy as np
import re
sys.path.append("/hpc/users/beaulj01/gitRepo/eBinning/src")
import read_scanner
from collections import Counter
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import pub_figs
import scipy.stats as stats

contigs_fasta  = "polished_assembly.fasta"
contigs_labels = np.loadtxt("contigs.labels", dtype="str")
contigs_names  = np.loadtxt("contigs.names",  dtype="str")

contigs_cbolteae = set(contigs_names[contigs_labels=="C_bolteae"])

def fasta_iter( fasta_name ):
	fh = open(fasta_name)
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		header = header.next()[1:].strip()
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

motifs = ["CAGGAG-4", \
		  "AATCC-1", \
		  "GATC-1", \
		  "AGATCT-2", \
		  "CCANNNNNNCAT-2", \
		  "AGATCC-2", \
		  "GGAGC-2"]

contigs   = []
counter_d = {}
sizes_d   = {}
for i,(name,seq) in enumerate(fasta_iter(contigs_fasta)):
	name = name.split("|")[0]
	if name in contigs_cbolteae:
		contigs.append(name)
		counter_d[name] = Counter()
		sizes_d[name]   = len(seq)
		for j,motif in enumerate(motifs):
			motif        = motif.split("-")[0]
			matches_list = read_scanner.find_motif_matches("bas", motif, seq, 0)
			for match in matches_list:
				counter_d[name][motif] += 1
			# print name, motif, counter_d[name][motif]
		# print len(seq), len(counter_d.keys())
		# print ""

motifs.sort()
colors    = plt.get_cmap('spectral')(np.linspace(0, 0.9, len(motifs)))
fig       = plt.figure(figsize=[15,12])
plots     = []
ax        = fig.add_axes([0.15, 0.15, 0.6, 0.8])
font_size = 18
adjust    = 0.15
for i,motif in enumerate(motifs):
	motif     = motif.split("-")[0]
	X         = []
	Y         = []
	cols      = []
	labels    = []
	for name in contigs:
		X.append(sizes_d[name])
		Y.append(counter_d[name][motif])
		cols.append(colors[i])
		labels.append(motif)
	plot = ax.scatter(X, Y, color=cols, label=labels)
	plots.append(plot)
ax.set_xlim([0,max(sizes_d.values())+5000])
ax.set_ylim([0,300])
ax.set_xlabel("Contig length (bp)")
ax.set_ylabel("Motif sites")
ax.set_xticklabels(range(0,121000,20000), rotation=45, ha="right")
ax.minorticks_on()
ax.tick_params('both', length=10, width=2, which='major')
ax.tick_params('both', length=5, width=1, which='minor')
xticks = ax.xaxis.get_minor_ticks()
yticks = ax.yaxis.get_minor_ticks()
xticks[1].label1.set_visible(False)
ax.legend(plots, motifs, loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':18}, frameon=False, scatterpoints=1)
pub_figs.change_font_size(ax, font_size=font_size, bottom_adjust=adjust, left_adjust=adjust*1.3)
pub_figs.remove_top_right_axes(ax)
ax.plot([0,10000,10000,0,0], [20,20,0,0,20], linestyle="-", color="k", linewidth=4)
fig.savefig("cbolteae_contig_motif_Ns.png")

fig  = plt.figure(figsize=[15,12])
ax   = fig.add_axes([0.15, 0.15, 0.6, 0.4])
vals = sizes_d.values()
if len(vals)>10:
	kde        = stats.kde.gaussian_kde( vals )
	dist_space = np.linspace( min(vals), max(vals), 100 )
	plt.plot( dist_space, kde(dist_space), color="k", linewidth=4)

ax.set_xlabel("Contig length (bp)")
ax.set_ylabel("Density")
ax.set_xlim([0,max(sizes_d.values())+5000])
ax.set_xticklabels(range(0,121000,20000), rotation=45, ha="right")
pub_figs.change_font_size(ax, font_size=font_size, bottom_adjust=adjust, left_adjust=adjust*1.3)
pub_figs.remove_top_right_axes(ax)
fig.savefig("cbolteae_contig_lengths.png")

fig       = plt.figure(figsize=[15,12])
plots     = []
ax        = fig.add_axes([0.3, 0.5, 0.25, 0.25])
font_size = 18
adjust    = 0.15
for i,motif in enumerate(motifs):
	motif     = motif.split("-")[0]
	X         = []
	Y         = []
	cols      = []
	labels    = []
	for name in contigs:
		X.append(sizes_d[name])
		Y.append(counter_d[name][motif])
		cols.append(colors[i])
		labels.append(motif)
	plot = ax.scatter(X, Y, color=cols, label=labels)
	plots.append(plot)
ax.set_xlim([0,10000])
ax.set_ylim([0,20])
# ax.set_xlabel("Contig length (bp)")
# ax.set_ylabel("Motif sites")
ax.set_xticklabels(range(0,10001,1000), rotation=45, ha="right")
# ax.legend(plots, motifs, loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':18}, frameon=False)
pub_figs.change_font_size(ax, font_size=font_size, bottom_adjust=adjust, left_adjust=adjust*1.3)
fig.savefig("cbolteae_contig_motif_Ns_ZOOM.png")