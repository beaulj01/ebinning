import os,sys
from itertools import groupby
import numpy as np
import re
from collections import Counter
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from Bio import SeqIO
import pub_figs
import scipy.stats as stats

labs_fn    = "contigs.labels"
names_fn   = "contigs.names"
sizes_fn   = "contigs.lengths"
fasta_fn   = "polished_assembly.fasta"
cov_fn     = "coverage.bed"

pub_figs.setup_math_fonts(font_size=24, font="Computer Modern Sans serif")

label_dict      =  {"B_caccae":     r"\textit{B. caccae}", \
				    "B_ovatus":     r"\textit{B. ovatus}", \
				    "B_vulgatus":   r"\textit{B. vulgatus}", \
				    "C_aerofaciens":r"\textit{C. aerofaciens}", \
				    "R_gnavus":     r"\textit{R. gnavus}", \
				    "B_theta":      r"\textit{B. thetaiotaomicron}", \
				    "E_coli":       r"\textit{E. coli}", \
				    "C_bolteae":    r"\textit{C. bolteae}"}

def gc_calc(seq):
	tot      = len(seq)
	gc_count = 0
	for base in ["G", "C"]:
		match = re.findall(base, str(seq), flags=re.IGNORECASE)
		for m in match:
			gc_count += 1

	gc_content = 100 * (float(gc_count) / tot)
	return gc_content

def get_contig_mean_cov( name, cov_bed ):
	name      = name.split("|")[0]
	cov_names = cov_bed[:,0]
	mask      = cov_names==name
	covs      = cov_bed[mask,4].astype(float)
	# Ignore edges of contigs
	# covs      = covs[2:-2]
	return covs.mean()

labels   = np.loadtxt(labs_fn,  dtype="str")
names    = np.loadtxt(names_fn, dtype="str")
sizes    = np.loadtxt(sizes_fn, dtype="int")
cov_bed  = np.loadtxt(cov_fn,   dtype="str", skiprows=1)

lab_dict = dict( zip(names,labels) )
len_dict = dict( zip(names,sizes) )

gc_dict  = {}
cov_dict = {}
for entry in SeqIO.parse(fasta_fn, "fasta"):
	name = entry.id.split("|")[0]
	if lab_dict.get(name):
		gc             = gc_calc(entry.seq)
		cov            = get_contig_mean_cov( entry.id, cov_bed)
		gc_dict[name]  = gc
		cov_dict[name] = cov
		lab            = lab_dict[name]
		length         = len_dict[name]
		print name, lab, gc, cov, length

fig     = plt.figure(figsize=[12,8])
ax      = fig.add_axes([0.1, 0.1, 0.8, 0.8])
# ax.axis("off")

label_set    = list(set(lab_dict.values()))
label_set.sort()
colors       = plt.get_cmap('spectral')(np.linspace(0, 1.0, len(label_set)+1))
shapes       = ["o","s","^"]
sizes[sizes<100000] = 100000
scaled_sizes = sizes**1.5 / max(sizes**1.5) * 2000
res          = []
f            = open("contigs.cov_vs_GC", "wb")
flab         = open("contigs.cov_vs_GC.labels", "wb")
for k,target_lab in enumerate(label_set):
	idxs             = [j for j,label in enumerate(labels) if label==target_lab]
	scaled_sizes_idx = np.array(scaled_sizes)[idxs]
	X                = map(lambda x: cov_dict[x], names[idxs])
	Y                = map(lambda x: gc_dict[x],  names[idxs])
	color            = colors[k]
	for i,x in enumerate(X):
		print x,Y[i]
		res.append( (x, Y[i], target_lab, color, scaled_sizes_idx[i], shapes[k%len(shapes)]) ) 
		f.write("%s\t%s\n" % (x, Y[i]))
		flab.write("%s\n" % target_lab)
f.close()
flab.close()

np.random.shuffle(res) 
plotted_labs = set()
legend_plots = []
legend_labs  = []
for m,(x,y, target_lab, color, size, shape) in enumerate(res):
	plot = ax.scatter(x,y, edgecolors=color, \
							   facecolors="None", \
							   marker=shape, \
							   label=label_dict[target_lab], \
							   alpha=0.8, \
							   s=size, \
							   lw=3)
	if label_dict[target_lab] not in plotted_labs:
		plotted_labs.add(label_dict[target_lab])
		legend_plots.append(plot)
		legend_labs.append(label_dict[target_lab])
ax.set_xlabel("Coverage")
ax.set_ylabel("GC (\\%)")
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.75, box.height])
leg_tup = zip(legend_labs,legend_plots)
leg_tup.sort(key=lambda x: x[0])
legend_labs, legend_plots = zip(*leg_tup)
leg = ax.legend(legend_plots, legend_labs, loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':18}, frameon=False, scatterpoints=1)
for i in range(len(legend_labs)):
	leg.legendHandles[i]._sizes = [200]

ax.set_ylim([30,65])
ax.set_xlim([0,350])

plt.grid()
pub_figs.remove_top_right_axes(ax)
font_size = 20
adjust    = 0.15
pub_figs.change_font_size(ax, font_size=font_size, bottom_adjust=adjust, left_adjust=adjust*1.3)
plt.savefig("contig_cov_vs_GC_content.png", dpi=300)