import os,sys

mapping_fn = sys.argv[1]

contig_cts  = {}
tot_reads   = {}
for i,line in enumerate(open(mapping_fn).xreadlines()):
	if i==0:
		header       = line.strip().split("\t")[1:]
		contig_ids   = map(lambda x: "_".join(x.split("_")[3:]), header)
		contig_names = [contig for contig in contig_ids[:len(contig_ids)/2]]
	else:
		line         = line.strip()
		contig       = line.split("\t")[0]
		counts       = line.split("\t")[1:]
		unamb_counts = map(lambda x: int(x), counts[:len(contig_ids)/2])
		tot_reads[contig]   = sum(map(lambda x: int(x), line.split("\t")[1:]))
		contig_cts[contig] = {}
		for j,name in enumerate(contig_names):
			if tot_reads[contig] == 0:
				contig_cts[contig][name] = 0
			else:
				contig_cts[contig][name] = unamb_counts[j]

for contig,cts_dict in contig_cts.iteritems():
	has_match = False
	for ref,count in cts_dict.iteritems():
		if tot_reads[contig] == 0:
			pct = 0.0
			continue
		pct = float(count) / tot_reads[contig] * 100
		if pct > 50 and count >= 10:
			has_match = True
			print "%s\t%s\t%s\t%s\t%.1f" % (contig, ref, count, tot_reads[contig], pct)
	if not has_match:
		print "%s\t%s\t%s\t%s\t%.1f" %     (contig, "ambiguous", count, tot_reads[contig], pct)