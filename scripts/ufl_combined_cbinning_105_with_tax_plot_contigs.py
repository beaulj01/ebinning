import os,sys,glob
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import re
import math
from pbcore.io.BasH5IO import BasH5Reader
import subprocess
import numpy as np
from collections import OrderedDict,Counter,defaultdict
from operator import itemgetter
from itertools import groupby
from itertools import izip
import pub_figs
import locale
locale.setlocale(locale.LC_ALL, 'en_US')

pub_figs.setup_math_fonts(font_size=18, font="Computer Modern Sans serif")

def move_unknowns_to_bottom( leg_tup ):
	for tup in leg_tup:
		if tup[0]=="Unlabeled":
			unknown_tup = tup
	leg_tup.remove(unknown_tup)
	leg_tup.append(unknown_tup)
	return leg_tup

both_names_fn   = "both.noBD.names"
both_comp_fn    = "both.noBD.comp.2D"
both_labels_fn  = "both.noBD.labels"
both_lengths_fn = "both.noBD.lengths"

names      = np.loadtxt(both_names_fn,   dtype="str")
data       = np.loadtxt(both_comp_fn,    dtype="float")
labels     = np.loadtxt(both_labels_fn,  dtype="str")
lengths    = np.loadtxt(both_lengths_fn, dtype="int")
fig        = plt.figure(figsize=[15,12])
ax         = fig.add_axes([0.05, 0.15, 0.62, 0.62])
# ax.axis("off")
pub_figs.remove_top_right_axes( ax )
for i,name in enumerate(names):
	if name.find("m1")>-1:
		labels[i] = "Raw read"
# reads_comp        = data[labels=="Raw read"]           
# extent            = [-40,30,-40,40]
# bins              = 40
# H, xedges, yedges = np.histogram2d(reads_comp[:,0], reads_comp[:,1], bins=bins, range=[extent[:2], extent[2:]])
# H = np.rot90(H)
# H = np.flipud(H)
# Hmasked = np.ma.masked_where(H==0,H)         # Mask pixels with a value of zero
# plt.hot()                                    # set 'hot' as default colour map
# im = plt.imshow(H, interpolation='bilinear', # creates background image
# 				origin='lower', cmap=cm.Greys, 
# 				extent=extent)

# Re-set all contig labels where contig species appears <=N times in assembly
label_list = list(set(labels))
for k,target_lab in enumerate(label_list):
	if target_lab=="Raw read":
		continue
	idxs = [j for j,label in enumerate(labels) if label==target_lab]
	lens = lengths[idxs]
	if len(idxs)<=3 and sum(lens)<50000:
		for idx in idxs:
			labels[idx] = "Unlabeled contig"

labels     = ["Eubacterium_eligens" if x=="[Eubacterium]_eligens" else x for x in labels]

len_counter = Counter()
for i,label in enumerate(labels):
	if names[i].find("unitig")>-1:
		len_counter[label] += lengths[i]

label_list = list(set(labels))
label_list.sort()
colors    = plt.get_cmap('spectral')(np.linspace(0.0, 0.95, len(label_list)))
shapes    = ["o", "v", "^", "D"]
res       = []
lengths[lengths<25000] = 25000
scaled_sizes = lengths**1.5 / max(lengths**1.5) * 2000
for k,target_lab in enumerate(label_list):
	if target_lab=="Raw read":
		continue
	idxs             = [j for j,label in enumerate(labels) if label==target_lab]
	scaled_sizes_idx = np.array(scaled_sizes)[idxs]
	X     = data[idxs,0]
	Y     = data[idxs,1]
	lens  = lengths[idxs]
	color = colors[k]
	shape = shapes[k%len(shapes)]
	for i,x in enumerate(X):
		res.append( (x, Y[i], target_lab, color, shape, scaled_sizes_idx[i], len_counter[target_lab]) ) 
np.random.shuffle(res) 
plotted_labs = set()
legend_plots = []
legend_labs  = []
for i,(x,y, target_lab, color, shape, size, tot_length) in enumerate(res):
	target_lab = target_lab.replace("_", " ")
	if target_lab=="Unlabeled contig":
		shape = "*"
		color = "r"
	plot = ax.scatter(x,y, marker=shape, s=size , edgecolors=color, label=target_lab, facecolors="None", linewidth=1.2, alpha=0.8)
	if target_lab not in plotted_labs:
		plotted_labs.add(target_lab)
		legend_plots.append(plot)
		bp_str = locale.format("%.1f", float(tot_length)/1000, grouping=True)
		print target_lab, float(tot_length)/1000
		if target_lab!="Unlabeled contig":
			target_str = r"\textit{%s}" % target_lab
		else:
			target_str = target_lab
		legend_labs.append((target_str+": %s Kbp" % bp_str))
box     = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
leg_tup = zip(legend_labs,legend_plots)
leg_tup.sort(key=lambda x: x[0])
# leg_tup = move_unknowns_to_bottom( leg_tup )
legend_labs, legend_plots = zip(*leg_tup)
leg = ax.legend(legend_plots, legend_labs, loc='center left', prop={'size':18}, bbox_to_anchor=(1, 0.5), frameon=False, scatterpoints=1)
for i in range(len(legend_labs)):
	leg.legendHandles[i]._sizes = [150]
ax.set_xlim([-24,24])
ax.set_ylim([-35,40])
fig.savefig("contigs.comp.tax.png")