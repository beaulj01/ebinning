import os,sys
import numpy as np

subreads_tax_fn = "subreads.tax.unordered"
labels          = np.loadtxt("reads.labels", dtype="str")
mols            = np.loadtxt("reads.names",  dtype="str")

subread_specs = {}
for line in open(subreads_tax_fn, "r").xreadlines():
	line = line.strip()
	name = line.split("\t")[0].split("|")[0]
	mol  = "/".join(name.split("/")[:-1])
	tax  = line.split("\t")[1]
	if len(tax.split(";")[-1].split(" "))>1 and tax.split(";")[-1].split(" ")[1]=="sp.":
		spec = " ".join(tax.split(";")[-1].split(" ")[:3])
	else:
		spec = " ".join(tax.split(";")[-1].split(" ")[:2])
	subread_specs[mol] = "_".join(spec.split(" "))

f = open("subreads.kraken", "w")
for i,mol in enumerate(mols):
	if subread_specs.get(mol):
		f.write("%s\n" % subread_specs[mol])
	else:
		f.write("Unlabeled\n")
f.close()