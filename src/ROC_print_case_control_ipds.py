import os,sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib
from matplotlib import pyplot as plt
from collections import defaultdict, OrderedDict, Counter
import math
import operator
from pbcore.io.align.CmpH5IO import CmpH5Reader
import bisect
import logging
import cmph5_read

class Opts:
	def __init__(self, cmph5 ):
		self.h5_labels                = {}
		self.cmph5_contig_lens        = {}
		self.cmph5_contig_lens[cmph5] = {}
		self.debug                    = False
		self.info                     = False
		self.logFile                  = "log.out"
		self.readlength_min           = 500
		self.minMapQV                 = 240
		self.minContigLength          = 1000
		self.tmp                      = "tmp"
		self.h5_labels[cmph5]         = "mix"
		self.motifs_file              = "motif.txt"
		self.skip_motifs              = None
		self.comp_kmer                = 4
		self.contig_type              = None
		self.printIPDs                = True


class ROC_runner:
	def __init__( self ):
		"""
		Parse the options and arguments, then instantiate the logger. 
		"""
		self.cmph5_file     = "/hpc/users/beaulj01/projects/ebinning/gnoto/isolates/synthetic_mix/8mock/HGAP_naive/aligned_reads.cmp.h5"
		self.opts           = Opts(self.cmph5_file)
		# self.case_contig    = "unitig_414"  # C. aero
		self.case_contig    = "unitig_17"  # E. coli
		# self.case_contig    = "unitig_407"  # B. vulgatus
		self.control_contig = "unitig_61"   # R. gnavus
		self.opts.cmph5_contig_lens[self.cmph5_file][self.case_contig]    = 4668944
		self.opts.cmph5_contig_lens[self.cmph5_file][self.control_contig] = 744973
		self.__initLog( )

	def __initLog( self ):
		"""Sets up logging based on command line arguments. Allows for three levels of logging:
		logging.error( ): always emitted
		logging.info( ) : emitted with --info or --debug
		logging.debug( ): only with --debug"""

		if os.path.exists(self.opts.logFile):
			os.remove(self.opts.logFile)

		logLevel = logging.DEBUG if self.opts.debug \
					else logging.INFO if self.opts.info \
					else logging.ERROR

		self.logger = logging.getLogger("")
		self.logger.setLevel(logLevel)
		
		# create file handler which logs even debug messages
		fh = logging.FileHandler(self.opts.logFile)
		fh.setLevel(logLevel)
		
		# create console handler with a higher log level
		ch = logging.StreamHandler()
		ch.setLevel(logLevel)
		
		# create formatter and add it to the handlers
		logFormat = "%(asctime)s [%(levelname)s] %(message)s"
		formatter = logging.Formatter(logFormat)
		ch.setFormatter(formatter)
		fh.setFormatter(formatter)
		
		# add the handlers to logger
		self.logger.addHandler(ch)
		self.logger.addHandler(fh)

	def run( self ):
		chunk_id              = 0
		reader                = CmpH5Reader(self.cmph5_file)
		# motifs                = ["CAGGAG-4"]
		motifs                = ["GATC-1"]
		bi_motifs             = []

		idx                   = [i for i,r in enumerate(reader) if r.referenceInfo[3]==self.case_contig]
		np.random.shuffle(idx)
		# idx  = idx[:min(len(idx), 15000)]
		idx  = idx[:min(len(idx), 100)]
		n                     = len(idx)
		self.opts.contig_type = "case"
		motif_processor       = cmph5_read.subread_motif_processor( self.cmph5_file, chunk_id, idx, n, motifs, bi_motifs, self.opts)
		tmp_files             = motif_processor.__call__()

		idx                   = [i for i,r in enumerate(reader) if r.referenceInfo[3]==self.control_contig]
		np.random.shuffle(idx)
		# idx  = idx[:min(len(idx), 15000)]
		idx  = idx[:min(len(idx), 100)]
		n                     = len(idx)
		self.opts.contig_type = "control"
		motif_processor       = cmph5_read.subread_motif_processor( self.cmph5_file, chunk_id, idx, n, motifs, bi_motifs, self.opts)
		tmp_files             = motif_processor.__call__()


if __name__=="__main__":
	app     = ROC_runner()
	results = app.run()