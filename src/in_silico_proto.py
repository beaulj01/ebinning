from kineticsTools.ipdModel import IpdModel
from pbcore.io.FastaIO import FastaReader
import cmph5_read
from pbcore.io.align.CmpH5IO import CmpH5Reader
import os,sys,glob
import re
import shutil
import math
import multiprocessing
import subprocess
import numpy as np
from collections import OrderedDict,Counter,defaultdict
import logging
import read_scanner
from operator import itemgetter
from tempfile import TemporaryFile
import pickle
import unicodedata
"""
Use the surrounding sequence context of the reference positions to predict
IPD values to be used as the control.
"""

ref    = sys.argv[1]
lut    = sys.argv[2]
cmp_fn = sys.argv[3]

class ipd_entry:
	def __init__( self, tup ):
		"""
		"""
		self.ref_base  = tup[0]
		self.ipd       = tup[1]
		# self.call      = tup[2]
		# self.read_base = tup[3]
		self.ref_pos   = tup[2]

class subread:
	def __init__( self, cmph5, alignment, label, subread_id, opts ):
		leftAnchor   = 1
		rightAnchor  = 1
		self.entries = {}
		self.opts    = opts

		self.subname    = alignment.readName
		movieID         = alignment.movieInfo[0]
		alignedLength   = alignment.referenceSpan
		fps             = alignment.movieInfo[2]
		self.refName    = alignment.referenceInfo[3]
		zmw             = alignment.HoleNumber
		self.mol        = alignment.MoleculeID
		if alignment.isForwardStrand:
			self.strand = 0
		else:
			self.strand = 1
		self.ref_bases  = alignment.reference()
		# self.read_bases = alignment.read()
		
		read_calls      = alignment.transcript()
		ref_pos         = list(alignment.referencePositions())
		IPD             = list(alignment.IPD())
		self.label      = self.opts.h5_labels[cmph5]

		error_mk = []
		for read_call in read_calls:
			# Go through all entries and flag which positions are MM/indels
			if read_call != "M":
				# Mismatch or indel at this position!
				error_mk.append(1)
			else:
				error_mk.append(0)
		
		# Get the indices of all the non-matches
		error_idx = [i for (i,val) in enumerate(error_mk) if val == 1]
		for error_id in error_idx:
			try:
				for j in range(leftAnchor):
					error_mk[error_id - (j+1)] = 1
				for j in range(rightAnchor):
					error_mk[error_id + (j+1)] = 1
			except IndexError:
				pass
		error_mk = np.array(error_mk)

		ipds     = np.array(IPD) / fps
		
		strands  = np.array([self.strand] * len(read_calls))
		subread  = np.array([subread_id]  * len(read_calls))

		self.ref_bases  = np.array(list(self.ref_bases))
		# self.read_bases = np.array(list(self.read_bases))
		self.ref_pos    = np.array(ref_pos)
		read_calls      = np.array(list(read_calls))

		# Mark the error positions, but leave them in the sequence so
		# we can pull out intact motifs from contiguous correct bases
		self.ref_bases[error_mk==1]  = "*"
		# self.read_bases[error_mk==1] = "*"
		read_calls[error_mk==1] = "*"
		ipds[error_mk==1]       = -9
		strands[error_mk==1]    = -9
		subread[error_mk==1]    = -9

		# Attach these IPD entries to the subread object
		# for i,tup in enumerate(zip(self.ref_bases, ipds, read_calls, self.read_bases, self.ref_pos)):
		for i,tup in enumerate(zip(self.ref_bases, ipds, self.ref_pos)):
			entry = ipd_entry(tup)
			self.entries[ self.ref_pos[i] ] = ipd_entry(tup)

		self.cap_outliers()

		self.subread_normalize()

	def cap_outliers( self, max_ipd=10 ):
		"""
		Cap the outlier IPDs at max_ipd seconds.
		"""
		subread_vals = []
		for entry in self.entries.values():
			# Only do if this IPD is NOT from an error position
			if entry.ipd != -9:
				subread_vals.append(entry.ipd)

		capValue = min( 10 , np.percentile(subread_vals,99) ) 
		for read_pos,entry in self.entries.iteritems():
			entry.ipd = min(entry.ipd, min(10, capValue))

	def subread_normalize( self ):
		"""
		Every IPD entry needs to be normalized by the mean IPD of its subread.
		"""
		if len(self.entries) == 0:
			# Nothing to do here.
			return self.entries

		# First populate list of all IPDs per subread. Will use to get normalization factor.
		subread_vals = []
		for entry in self.entries.values():
			# Only do if this IPD is NOT from an error position
			if entry.ipd != -9:
				subread_vals.append(entry.ipd)

		# rawIPDs = np.array(map(lambda x: math.log(x + 0.001), subread_vals))
		rawIPDs = np.array(subread_vals)
		nfs     = rawIPDs.mean()

		for pos, entry in self.entries.iteritems():
			if entry.ipd == -9:
				newIPD = -9
			else:
				# newIPD = math.log(entry.ipd + 0.001) - nfs
				newIPD = entry.ipd / nfs
			
			entry.ipd = newIPD

	def zip_bases_and_IPDs( self ):
		"""
		Reassemble the read and IPD values using the subread normalized IPDs
		"""
		od        = OrderedDict(sorted(self.entries.items()))
		ref       = []
		ref_pos   = []
		self.ipds = []
		for read_pos, entry in od.items():
			ref.append(entry.ref_base)
			ref_pos.append(entry.ref_pos)
			self.ipds.append(entry.ipd)
		self.ref_str  = "".join(ref)
		self.ref_pos  = ref_pos

class ReferenceEntryObj:
	"""
	Generate a barebones reference entry that this kinetics code needs:
	(1) self.contig.sequence
	(2) self.contig.id
	"""
	def __init__( self, fasta ):

		class Contig:
			def __init__( self, entry ):
				self.id       = entry.name
				self.sequence = entry.sequence

		self.contigs = []
		for entry in FastaReader(fasta):



			if entry.name=="unitig_92|quiver":
				contig = Contig(entry)
				self.contigs.append(contig)




insilico_vals = {}
refObj = ReferenceEntryObj( ref )
model  = IpdModel( fastaRecords=refObj.contigs, modelFile=lut )
for contig in refObj.contigs:
	print "Using model to calculate in silico IPDs for contig %s..." % contig.id
	insilico_vals[contig.id] = { 0:{}, 1:{} }
	meanIpdFunc     = model.predictIpdFunc( refId=contig.id )
	cognateBaseFunc = model.cognateBaseFunc(contig.id)
	for strand in [0,1]:
		for pos in range(model.refLengthDict[contig.id]): # This will iterate over all positions in the contig
			ref_base                   = cognateBaseFunc(pos,strand)
			ipd                        = meanIpdFunc( pos, strand )
			print "%s\t%s\t%s\t%.5f\t%s" % (contig.id, strand, pos, ipd, ref_base)
			insilico_vals[contig.id][strand][pos] = (ipd, ref_base)
			if pos==1000:
				break

			pickle.dump(insilico_vals, open("insilico_0-10kb_unitig_92.pkl","wb"))

class opts:
	def __init__( self ):
		pass

opts                  = opts()
opts.h5_labels        = {}
opts.h5_labels[cmp_fn] = "whatever"

reader = CmpH5Reader(cmp_fn)
nat_ipds = []
con_ipds = []
for i,alignment in enumerate(reader):
	sub = subread( cmp_fn, alignment, "whatever", 1, opts )
	sub.zip_bases_and_IPDs()
	ref = alignment.referenceInfo[3]
	nat_ipds = []
	con_ipds = []
	for j,ipd in enumerate(sub.ipds):
		if ipd!=-9 and j<1000:
			print ref, sub.ref_pos[j], ipd, sub.ref_str[j], sub.strand, insilico_vals[ref+"|quiver"][sub.strand][sub.ref_pos[j]]
			if sub.ref_str[j]!=insilico_vals[ref+"|quiver"][sub.strand][sub.ref_pos[j]][1]:
				print "    ********** %s != %s", sub.ref_str[j], insilico_vals[ref+"|quiver"][sub.strand][sub.ref_pos[j]][1]
			nat_ipds.append(ipd)
			con_ipds.append(insilico_vals[ref+"|quiver"][sub.strand][sub.ref_pos[j]])

	sub_ratio = np.mean(nat_ipds) / np.mean(con_ipds)
	print "subread ratio:", sub_ratio
	if i==5:
		sys.exit()
