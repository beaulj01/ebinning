import os,sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import cm as cm
import scipy.cluster.hierarchy as hier
import scipy.spatial.distance as dist
import collections
import pub_figs
from Bio import SeqIO,SeqUtils
import numpy as np
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
import hierarchical_cluster_JM3_boxes
import glob
from collections import defaultdict

names_fn   = "contigs.names"
SCp_fn     = "contigs.SCp"
sizes_fn   = "contigs.lengths"
motifs_fn  = "ordered_motifs.txt"

SCp_scores = np.loadtxt(SCp_fn,    dtype="float", delimiter="\t")
names      = np.loadtxt(names_fn,  dtype="str",   delimiter="\t")
sizes      = np.loadtxt(sizes_fn, dtype="int",   delimiter="\t")
motifs     = np.loadtxt(motifs_fn, dtype="str",   delimiter="\t")

minlen = 0

bin_contigs = ["unitig_133", \
		   	   "unitig_186", \
		   	   "unitig_6577", \
		   	   "unitig_58", \
		   	   "unitig_6591", \
		   	   "unitig_92", \
		   	   "unitig_6604", \
		   	   "unitig_6573", \
		   	   "unitig_6588", \
		   	   "unitig_6477", \
		   	   "unitig_103", \
		   	   ]

SCp_scores = SCp_scores[sizes>minlen,:]
names      = names[sizes>minlen]
sizes      = sizes[sizes>minlen]

# Find the entries for these specific contigs
idx = [i for i,name in enumerate(names) if name in set(bin_contigs)]
SCp_scores = SCp_scores[idx]
names      = names[idx]
sizes      = sizes[idx]

# sys.exit()


# Limit contigs to those in the boxes and add their box ID
box_fastas  = glob.glob("contigs.box.*.fasta")
box_contigs = {}
contig_lens = {}
for box_fasta in box_fastas:
	box_id = int(box_fasta.split(".")[2])
	for seq_record in SeqIO.parse(box_fasta, "fasta"):
		name = seq_record.id.split("|")[0]
		box_contigs[name] = box_id
		contig_lens[name] = len(seq_record.seq)

box_contigs["unitig_1671"] = "None"

f = open("contigs.box", "wb")
for name in names:
	if box_contigs.get(name):
		box = box_contigs[name]
	else:
		box = "None"
	f.write("%s\n" % box)
f.close()

clust_mat_fn = SCp_fn+".forClust"
f            = open(clust_mat_fn, "w")
f.write("names\t%s\n" % "\t".join(map(lambda x: str(x), motifs)))
for i in range(SCp_scores.shape[0]):
	name     = names[i]
	# if box_contigs.get(name):
	SCp      = SCp_scores[i,:]
	SCp_str  = "\t".join(map(lambda x: str(x), SCp))
	name_str = "box%s %s %s" % (box_contigs[name], sizes[i], name.replace("_","-"))
	f.write("%s\t%s\n" % (name_str, SCp_str))
	# else:
	# 	# Contig not in one of the boxes
	# 	pass
f.close()

hierarchical_cluster_JM3_boxes.run(clust_mat_fn)