import os,sys
import subprocess
import logging
import optparse
import random
import glob
from collections import Counter
from pbcore.io.BasH5IO import BasH5Reader
import bash5_read_processor as read_processor

class simulator:
	def __init__ ( self ):
		"""
		Parse the options and arguments, then instantiate the logger. 
		"""
		self.__parseArgs( )
		self.__initLog( )
	
	def run( self ):
		self.abund       = self.read_abundances()
		logging.info("Finished reading %s -- will simulate the following:" % self.abundance_fn)
		for spec,ab in self.abund.iteritems():
			logging.info("  %s:\t%s reads" % (spec, ab))

		self.baxh5_paths = self.get_baxh5_paths()
		logging.info("Finished fetching the desired bax.h5 paths in %s" % self.baxh5_basedir)

		if self.opts.checkCount:
			logging.info("Ensuring enough available reads for subsampling...")
			for baxh5_dir,fns in self.baxh5_paths.iteritems():
				logging.info("Checking %s (%s movies)..." % (baxh5_dir, len(fns)))
				total_reads = self.check_species_avail_reads( baxh5_dir, fns, self.abund[baxh5_dir] )
				logging.info("  -- Found %s acceptable subreads" % total_reads[baxh5_dir])

		logging.info("Picking random reads from each bax.h5 file...")
		seq_fn = "%s.fasta" % self.opts.out
		seq_f  = open(seq_fn, "w")
		ipd_fn = "%s.ipds" % seq_fn
		ipd_f  = open(ipd_fn, "w")
		tot_subreads = 1
		tot_bases    = 0
		for baxh5_dir,fns in self.baxh5_paths.iteritems():
			
			logging.info("Sampling %s subreads from %s..." % (self.abund[baxh5_dir], baxh5_dir))
			to_sample, movie_map = self.get_rand_read_ids( baxh5_dir, fns, self.abund[baxh5_dir] )
			sampled_subreads     = self.sample_read_ids( to_sample, movie_map, baxh5_dir )
			logging.info("  -- Finished: sampled %s subreads" % len(sampled_subreads))
			
			logging.info("  -- Writing subreads seq and IPD data...")
			tot_subreads, tot_bases = self.write_subreads_to_file( seq_f,            \
																   ipd_f,            \
																   sampled_subreads, \
																   baxh5_dir,        \
																   tot_subreads,     \
																   tot_bases )
		seq_f.close()
		ipd_f.close()
		logging.info("Finished: simulated %s total bases" % tot_bases)

	def read_abundances( self ):
		abund = {}
		for line in open(self.abundance_fn).xreadlines():
			line       =       line.strip("\n")
			baxh5_dir  =       line.split("\t")[0]
			ab_frac    = float(line.split("\t")[1])

			frac_reads       = int(self.opts.numReads * ab_frac)
			abund[baxh5_dir] = frac_reads
		return abund

	def get_baxh5_paths( self ):
		baxh5_paths = {}
		for baxh5_dir in self.abund.keys():
			full_baxh5_path = os.path.join(self.baxh5_basedir, baxh5_dir)
			baxh5_paths[baxh5_dir] = glob.glob("%s/*bax.h5" % full_baxh5_path)
		return baxh5_paths

	def check_species_avail_reads( self, baxh5_dir, baxh5_fns, need_count ):
		total_reads = Counter()
		for baxh5 in baxh5_fns:
			reader = BasH5Reader(baxh5)
			for r in reader:
				if r.zmwMetric("Productivity") == 1 and r.zmwMetric("ReadScore") > self.opts.minRS and r.zmwMetric("Pausiness") < self.opts.maxP:
					for subread in r.subreads:
						if len(subread.basecalls()) > self.opts.minSRlength:
							total_reads[baxh5_dir] += 1
							if total_reads[baxh5_dir] == need_count:
								return total_reads

	def get_rand_read_ids( self, baxh5_dir, baxh5_fns, need_count):
		holeNumbers = set()
		movie_map   = {}
		for i,baxh5 in enumerate(baxh5_fns):
			movie_map[i] = baxh5
			reader = BasH5Reader(baxh5)
			for r in reader:
				if r.zmwMetric("Productivity") == 1 and r.zmwMetric("ReadScore") > self.opts.minRS and r.zmwMetric("Pausiness") < self.opts.maxP:
					for j,subread in enumerate(r.subreads):
						if len(subread.basecalls()) > self.opts.minSRlength:
							holeNumbers.add( (i,r.holeNumber,j) )
							break
		
		to_sample     = set(random.sample(holeNumbers, need_count))
		return to_sample, movie_map

	def sample_read_ids( self, to_sample, movie_map, baxh5_dir ):
		movies_to_use    = set([movie for (movie,zmw,sr_id) in to_sample])
		sampled_subreads = []
		for movie in movies_to_use:
			holes_to_use = dict([ (zmw,sr_id) for (m,zmw,sr_id) in to_sample if m==movie])
			reader = BasH5Reader(movie_map[movie])
			for r in reader:
				if r.holeNumber in holes_to_use.keys():
					for j,subread in enumerate(r.subreads):
						sr = holes_to_use[r.holeNumber]
						if sr == j:
							sampled_subreads.append(subread)
		return sampled_subreads

	def write_subreads_to_file( self, seq_f, ipd_f, subreads, baxh5_dir, tot_subreads, tot_bases ):
		"""
		This will write two files: one for the basecalls (fasta) and one for the IPDs (fasta-like).
		"""
		for i,sub in enumerate(subreads):
			subread = read_processor.subread( sub )
			# Generate sorted subread.ipds (normalized) and subread.read_str
			subread.zip_bases_and_IPDs()
			header = ">%s_%s/%s_%s\n" % (tot_subreads, i+1, self.abund[baxh5_dir], baxh5_dir)

			seq_f.write(header)
			seq_f.write("%s\n" % subread.read_str)

			ipd_f.write(header)
			ipd_str = ",".join(map(lambda x: str(round(x, 3)), subread.ipds))
			ipd_f.write("%s\n" % ipd_str)

			tot_subreads += 1
			tot_bases    += len(subread.read_str)
		return tot_subreads, tot_bases

	def __parseArgs( self ):
		"""Handle command line argument parsing"""

		usage = """%prog [--help] [options] <baxh5_basedir> <abundances_file>

		This program will simulate a set of metagenomic reads from a set of species/strain-specific
		PacBio bax.h5 reads and a file specifying the desired abundance of each species/strain type.
		The baxh5_basedir should be a path pointing to a directory that contains a directory for each
		species/strain to be included in the mock community. The structure should be as follows:

		.
		L-- baxh5_basedir
		    -- species1
		    	-- SMRTcell_name.1.bax.h5
		    	-- SMRTcell_name.2.bax.h5
		    	-- SMRTcell_name.3.bax.h5
		    -- species2
		    	-- SMRTcell_name.1.bax.h5
		    	-- SMRTcell_name.2.bax.h5
		    	-- SMRTcell_name.3.bax.h5
		    
		The abundances_file should be a tab-delimited list of the desired (fractional) abundances
		for each species included in the mock, as follows:

		species1	0.6
		species2	0.4
		"""

		parser = optparse.OptionParser( usage=usage, description=__doc__ )

		parser.add_option( "-d", "--debug", action="store_true", help="Increase verbosity of logging" )
		parser.add_option( "-i", "--info", action="store_true", help="Add basic logging" )
		parser.add_option( "--logFile", type="str", help="Write logging to file [log.out]" )
		parser.add_option( "--out", type="str", help="Prefix to use with the output reads [meta_sim_longreads]" )
		parser.add_option( "-l", "--minSRlength", type="float", help="Min subread length when picking reads for mock [5000]" )
		parser.add_option( "-p", "--maxP", type="float", help="Pausiness threshold when picking reads for mock [0.05]" )
		parser.add_option( "-r", "--minRS", type="float", help="Min readscore (pred accuracy) when picking reads for mock [0.80]" )
		parser.add_option( "-n", "--numReads", type="int", help="Total number of reads to include in mock [10000]" )
		parser.add_option( "-c", "--checkCount", action="store_true", help="First count available subreads for each species/sample [False]" )
		parser.set_defaults( logFile="log.out",                \
							 debug=False,                      \
							 info=False,                       \
							 out="meta_sim_longreads",         \
							 maxP=0.05,                        \
							 minRS=0.80,                       \
							 minSRlength=5000,                 \
							 numReads=10000)

		self.opts, args = parser.parse_args( )

		if len(args) != 2:
			parser.error( "Expected 2 arguments." )

		self.baxh5_basedir = args[0]
		self.abundance_fn  = args[1]

		total_abund = 0
		for line in open(self.abundance_fn).xreadlines():
			line = line.strip("\n")
			ab   = float(line.split("\t")[1])
			total_abund += ab
		if total_abund != 1.0:
			raise Exception("Make sure your desired abundances add up to 1!")

	def __initLog( self ):
		"""Sets up logging based on command line arguments. Allows for three levels of logging:
		logging.error( ): always emitted
		logging.info( ) : emitted with --info or --debug
		logging.debug( ): only with --debug"""

		if os.path.exists(self.opts.logFile):
			os.remove(self.opts.logFile)

		logLevel = logging.DEBUG if self.opts.debug \
					else logging.INFO if self.opts.info \
					else logging.ERROR

		self.logger = logging.getLogger("")
		self.logger.setLevel(logLevel)
		
		# create file handler which logs even debug messages
		fh = logging.FileHandler(self.opts.logFile)
		fh.setLevel(logLevel)
		
		# create console handler with a higher log level
		ch = logging.StreamHandler()
		ch.setLevel(logLevel)
		
		# create formatter and add it to the handlers
		logFormat = "%(asctime)s [%(levelname)s] %(message)s"
		formatter = logging.Formatter(logFormat)
		ch.setFormatter(formatter)
		fh.setFormatter(formatter)
		
		# add the handlers to logger
		self.logger.addHandler(ch)
		self.logger.addHandler(fh)

		logging.info("========== Configuration ===========")
		logging.info("logFile:                 %s" % self.opts.logFile)
		logging.info("debug:                   %s" % self.opts.debug)
		logging.info("info:                    %s" % self.opts.info)
		logging.info("numReads:                %s" % self.opts.numReads)
		logging.info("minSRlength:             %s" % self.opts.minSRlength)
		logging.info("maxP:                    %s" % self.opts.maxP)
		logging.info("minRS:                   %s" % self.opts.minRS)
		logging.info("out:                     %s" % self.opts.out)
		logging.info("checkCount:              %s" % self.opts.checkCount)

if __name__=="__main__":
	app     = simulator()
	results = app.run()
