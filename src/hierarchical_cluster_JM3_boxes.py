import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as pylab
import string
import scipy
import scipy.cluster.hierarchy as sch
import scipy.spatial.distance as dist
import fastcluster
import time
import pub_figs
import numpy as np
from collections import Counter
import logging

font_size = 30
style     = "bold"
fontProperties = pub_figs.setup_math_fonts(font_size)

def run( barcodes_fn ):
	################  Default Methods ################
	row_method     = 'ward'
	row_metric     = 'euclidean' #cosine
	column_method  = 'ward'
	column_metric  = 'euclidean'
	color_gradient = 'Greys'

	""" Running with cosine or other distance metrics can often produce negative Z scores
		during clustering, so adjustments to the clustering may be required.
		
	see: http://docs.scipy.org/doc/scipy/reference/cluster.hierarchy.html
	see: http://docs.scipy.org/doc/scipy/reference/spatial.distance.htm  
	color_gradient = red_white_blue|red_black_sky|red_black_blue|red_black_green|yellow_black_blue|green_white_purple'
	"""
	matrix, column_header, row_header = importData(barcodes_fn)

	if len(matrix)>0:
		try:
			heatmap(matrix, row_header, column_header, row_method, column_method, row_metric, column_metric, color_gradient, barcodes_fn)
		except Exception:
			print 'Error using %s ... trying euclidean instead' % row_metric
			row_metric = 'euclidean'
			try:
				heatmap(matrix, row_header, column_header, row_method, column_method, row_metric, column_metric, color_gradient, barcodes_fn)
			except IOError:
				print 'Error with clustering encountered'

def heatmap(x, row_header, column_header, row_method,
			column_method, row_metric, column_metric,
			color_gradient, filename):
	
	print "\nPerforming hiearchical clustering using %s for columns and %s for rows" % (column_metric,row_metric)
		
	"""
	This below code is based in large part on the protype methods:
	http://old.nabble.com/How-to-plot-heatmap-with-matplotlib--td32534593.html
	http://stackoverflow.com/questions/7664826/how-to-get-flat-clustering-corresponding-to-color-clusters-in-the-dendrogram-cre

	x is an m by n ndarray, m observations, n genes
	"""
	
	### Define the color gradient to use based on the provided name
	n = len(x[0]); m = len(x)
	if color_gradient == 'red_white_blue':
		cmap=pylab.cm.bwr
	if color_gradient == 'red_black_sky':
		cmap=RedBlackSkyBlue()
	if color_gradient == 'red_black_blue':
		cmap=RedBlackBlue()
	if color_gradient == 'red_black_green':
		cmap=RedBlackGreen()
	if color_gradient == 'yellow_black_blue':
		cmap=YellowBlackBlue()
	if color_gradient == 'seismic':
		cmap=pylab.cm.seismic
	if color_gradient == 'green_white_purple':
		cmap=pylab.cm.PiYG_r
	if color_gradient == 'coolwarm':
		cmap=pylab.cm.coolwarm
	if color_gradient == "Greys":
		cmap=pylab.cm.Greys

	### Scale the max and min colors so that 0 is white/black
	vmin = x.min()
	vmax = x.max()
	vmax = max([vmax,abs(vmin)])
	# vmin = vmax*-1
	vmin = 0
	norm = matplotlib.colors.Normalize(vmin/2, vmax/2) ### adjust the max and min to scale these colors

	### Scale the Matplotlib window size
	default_window_hight = 20
	# default_window_width = 50
	default_window_width = 20
	# fig = pylab.figure(figsize=(default_window_width,default_window_hight)) ### could use m,n to scale here
	fig = pylab.figure(figsize=[25,15])
	color_bar_w = 0.015 ### Sufficient size to show

	#######################
	# CUSTOMIZED LAYOUT
	color_bar_w = 0.0

	# ax1, placement of dendrogram 1, on the left of the heatmap
	ax1_x = 0.05
	ax1_y = 0.15
	ax1_w = 0.02
	ax1_h = 0.80
	width_between_ax1_axr  = 0.004
	height_between_ax1_axc = 0.004 ### distance between the top color bar axis and the matrix
	
	# axr, placement of row side colorbar
	axr_w = color_bar_w ### controls the width of the side color bar - 0.015 when showing
	axr_x = ax1_x + ax1_w + width_between_ax1_axr
	axr_y = ax1_y
	axr_h = ax1_h
	width_between_axr_axm = 0.0

	# axm, placement of heatmap for the data matrix
	axm_x = axr_x + axr_w + width_between_axr_axm
	axm_y = ax1_y
	axm_h = ax1_h
	axm_w = 0.7

	# axc, placement of column side colorbar
	axc_x = axr_x + axr_w + width_between_axr_axm
	axc_y = ax1_y + ax1_h + height_between_ax1_axc
	axc_w = axm_w
	axc_h = color_bar_w
	height_between_axc_ax2 = 0.004

	# ax2, placement of dendrogram 2, on the top of the heatmap
	ax2_h = 0.05 ### controls hight of the dendrogram
	ax2_x = axr_x + axr_w + width_between_axr_axm
	ax2_y = ax1_y + ax1_h + height_between_ax1_axc + axc_h + height_between_axc_ax2
	ax2_w = axc_w

	# axcb, placement of the color legend
	axcb_w = 0.10
	axcb_h = 0.03
	axcb_x = ax1_x + ax1_w + (axm_w/2) - (axcb_w/2)
	axcb_y = ax1_y + ax1_h + ax2_h + 0.03
	#######################

	# Compute and plot top dendrogram
	if column_method != None:
		start_time = time.time()
		d2   = dist.pdist(x.T)
		D2   = dist.squareform(d2)
		ax2  = fig.add_axes([ax2_x, ax2_y, ax2_w, ax2_h], frame_on=True)
		pylab.axis('off')
		# Y2   = sch.linkage(D2, method=column_method, metric=column_metric) ### array-clustering metric - 'average', 'single', 'centroid', 'complete'
		Y2   = fastcluster.linkage(D2, method=column_method, metric=column_metric, preserve_input=False)
		Z2   = sch.dendrogram(Y2, color_threshold=0.0, above_threshold_color="k")
		# ind2 = sch.fcluster(Y2,0.3*max(Y2[:,2]),'distance') ### This is the default behavior of dendrogram
		ind2 = sch.fcluster(Y2,0.3*max(Y2[:,2]),'distance') ### This is the default behavior of dendrogram
		# ax2.set_xticks([]) ### Hides ticks
		# ax2.set_yticks([])
		time_diff = str(round(time.time()-start_time,1))
		logging.info('Column clustering completed in %s seconds' % time_diff)
	else:
		ind2 = ['NA']*len(column_header) ### Used for exporting the flat cluster data
		
	axm  = fig.add_axes([axm_x, axm_y, axm_w, axm_h])  # axes for the data matrix
	axm.axis("off")
	xt   = x

	idx2 = Z2['leaves'] ### apply the clustering for the array-dendrograms to the actual matrix data
	ind2 = ind2[idx2] ### reorder the flat cluster to match the order of the leaves the dendrogram
	xt   = xt[:,idx2]

	# Compute and plot left dendrogram.
	if row_method != None:
		start_time = time.time()
		d1   = dist.pdist(x)
		D1   = dist.squareform(d1)  # full matrix
		ax1  = fig.add_axes([ax1_x, ax1_y, ax1_w, ax1_h], frame_on=False) # frame_on may be False
		Y1   = sch.linkage(D1, method=row_method, metric=row_metric) ### gene-clustering metric - 'average', 'single', 'centroid', 'complete'
		Z1   = sch.dendrogram(Y1, orientation='left', color_threshold=0.0, above_threshold_color="k")
		ind1 = sch.fcluster(Y1,0.4*max(Y1[:,2]),'distance') ### This is the default behavior of dendrogram
		ax1.set_xticks([]) ### Hides ticks
		ax1.set_yticks([])
		time_diff = str(round(time.time()-start_time,1))
		logging.info('Row clustering completed in %s seconds' % time_diff)
	else:
		ind1 = ['NA']*len(row_header) ### Used for exporting the flat cluster data
		
	# Plot distance matrix.
	# axm = fig.add_axes([axm_x, axm_y, axm_w, axm_h])  # axes for the data matrix
	# xt = x
	# if column_method != None:
		# idx2 = Z2['leaves'] ### apply the clustering for the array-dendrograms to the actual matrix data
		# xt   = xt[:,idx2]
		# ind2 = ind2[:,idx2] ### reorder the flat cluster to match the order of the leaves the dendrogram
		# ind2 = ind2[idx2] ### reorder the flat cluster to match the order of the leaves the dendrogram
	if row_method != None:
		idx1 = Z1['leaves'] ### apply the clustering for the gene-dendrograms to the actual matrix data
		xt   = xt[idx1,:]   # xt is transformed x
		# ind1 = ind1[idx1,:] ### reorder the flat cluster to match the order of the leaves the dendrogram
		ind1 = ind1[idx1] ### reorder the flat cluster to match the order of the leaves the dendrogram
	### taken from http://stackoverflow.com/questions/2982929/plotting-results-of-hierarchical-clustering-ontop-of-a-matrix-of-data-in-python/3011894#3011894
	im = axm.matshow(xt, aspect='auto', origin='lower', cmap=cmap, norm=norm) ### norm=norm added to scale coloring of expression with zero = white or black
	# im = axm.matshow(xt, aspect='auto', origin='lower', cmap=cmap) ### norm=norm added to scale coloring of expression with zero = white or black
	axm.set_xticks([]) ### Hides x-ticks
	axm.set_yticks([])

	# Add text
	new_row_header=[]
	new_column_header=[]
	spec_labels = {"CFT073-chrom-contigs.SCp"	: 	r"${E.}$ ${coli}$ CFT073 chr.", \
				   "CFT073-plasmid-contigs.SCp"	: 	r"pHel3 plasmid (${E.}$ ${coli}$ CFT073-hosted)", \
				   "DH5a-chrom-contigs.SCp"	: 		r"${E.}$ ${coli}$ DH5a chr.", \
				   "DH5a-plasmid-contigs.SCp"	: 	r"pHel3 plasmid (${E.}$ ${coli}$ DH5a-hosted)", \
				   "JP26-chrom-contigs.SCp"	: 		r"${H.}$ ${pylori}$ JP26 chr.", \
				   "JP26-plasmid-contigs.SCp"	: 	r"pHel3 plasmid (${H.}$ ${pylori}$ JP26-hosted)"}
	
	box_colors = {"1":  "k", \
				   "2": "indigo", \
				   "3": "darkcyan", \
				   "4": "green", \
				   "5": "r", \
				   "6": "dodgerblue", \
				   "7": "gold", \
				   "8": "lime", \
				   "9": "magenta", \
				   "None": "gray"}

	def motif_methyl_underliner( motif ):
		idx       = int(motif.split("-")[1])
		motif_seq = motif.split("-")[0]
		return r"%s\textbf{%s}%s" % (motif_seq[:idx], motif_seq[idx], motif_seq[(idx+1):])

	# Rows
	for i in range(x.shape[0]):
		if row_method != None:
			if len(row_header)<100: ### Don't visualize gene associations when more than 100 rows
				# row_name    = row_header[idx1[i]].replace("-","_")
				row_name = row_header[idx1[i]]
				box      = row_name.split(" ")[0].split("x")[1]
				# axm.text(x.shape[1]-0.2, i-0.1, '  '+spec_labels[strain], color="k", fontsize=20)
				axm.text(x.shape[1]-0.2, i-0.1, '  '+row_name, color=box_colors[box], fontsize=24)
			new_row_header.append(row_header[idx1[i]])
		else:
			if len(row_header)<100: ### Don't visualize gene associations when more than 100 rows
				axm.text(x.shape[1]-0.2, i, '  '+row_header[i]) ### When not clustering rows
			new_row_header.append(row_header[i])
	# Columns
	for i in range(x.shape[1]):
		if column_method != None:
			# axm.text(i, -0.9, ' '+column_header[idx2[i]], rotation=270, verticalalignment="top") # rotation could also be degrees
			# new_column_header.append(column_header[idx2[i]])
			axm.text(i+0.2, -0.65, ' '+motif_methyl_underliner(column_header[idx2[i]]), rotation=45, verticalalignment="top", horizontalalignment="right", fontsize=14) # rotation could also be degrees
			new_column_header.append(column_header[idx2[i]])
		else: ### When not clustering columns
			axm.text(i, -0.9, ' '+column_header[i], rotation=270, verticalalignment="top", horizontalalignment="center")
			new_column_header.append(column_header[i])


	# Plot color legend
	axcb = fig.add_axes([axcb_x, axcb_y, axcb_w, axcb_h], frame_on=False)  # axes for colorbar
	cb = matplotlib.colorbar.ColorbarBase(axcb, cmap=cmap, orientation='horizontal')
	cb.set_ticks([0,0.5,1])
	cb.ax.set_title("Methylation score (norm)")
	
	if '/' in filename:
		dataset_name = string.split(filename,'/')[-1][:-4]
		root_dir = string.join(string.split(filename,'/')[:-1],'/')+'/'
	else:
		dataset_name = string.split(filename,'\\')[-1][:-4]
		root_dir = string.join(string.split(filename,'\\')[:-1],'\\')+'\\'
	filename = '%s_%s_%s.pdf' % (dataset_name,column_metric,row_metric)
	# cb.set_label("Motif methylated", fontsize=24, labelpad=-90)
	# cb.ax.tick_params(labelsize=24)
	exportFlatClusterData(filename, new_row_header,new_column_header,xt,ind1,ind2)

	# Draw highlighting box around our two rows of interest
	# axh_w = 0.95
	# axh_h = 0.05
	# axh_x = 0.04
	# axh_y = 0.35
	# axh   = fig.add_axes([axh_x, axh_y, axh_w, axh_h], frame_on=True)
	# axh.set_xticks([])
	# axh.set_yticks([])
	# axh.patch.set_facecolor('red')
	# axh.patch.set_alpha(0.1)

	### Render the graphic
	# if len(row_header)>50 or len(column_header)>50:
	# 	pylab.rcParams['font.size'] = 5
	# else:
	# 	pylab.rcParams['font.size'] = 8

	pylab.rcParams['font.size'] = 20

	pylab.savefig(filename)
	print 'Exporting:',filename
	filename = filename[:-3]+'png'
	pylab.savefig(filename, dpi=100) #,dpi=200
	pylab.show()

def getColorRange(x):
	""" Determines the range of colors, centered at zero, for normalizing cmap """
	vmax=x.max()
	vmin=x.min()
	if vmax<0 and vmin<0: direction = 'negative'
	elif vmax>0 and vmin>0: direction = 'positive'
	else: direction = 'both'
	if direction == 'both':
		vmax = max([vmax,abs(vmin)])
		vmin = -1*vmax
		return vmax,vmin
	else:
		return vmax,vmin
	
################# Export the flat cluster data #################

def exportFlatClusterData(filename, new_row_header,new_column_header,xt,ind1,ind2):
	""" Export the clustered results as a text file, only indicating the flat-clusters rather than the tree """
	
	filename = string.replace(filename,'.pdf','.txt')
	export_text = open(filename,'w')
	column_header = string.join(['UID','row_clusters-flat']+new_column_header,'\t')+'\n' ### format column-names for export
	export_text.write(column_header)
	column_clusters = string.join(['column_clusters-flat','']+ map(str, ind2),'\t')+'\n' ### format column-flat-clusters for export
	export_text.write(column_clusters)
	
	### The clusters, dendrogram and flat clusters are drawn bottom-up, so we need to reverse the order to match
	new_row_header = new_row_header[::-1]
	xt = xt[::-1]
	
	### Export each row in the clustered data matrix xt
	i=0
	for row in xt:
		export_text.write(string.join([new_row_header[i],str(ind1[i])]+map(str, row),'\t')+'\n')
		i+=1
	export_text.close()
	
	### Export as CDT file
	filename = string.replace(filename,'.txt','.cdt')
	export_cdt = open(filename,'w')
	column_header = string.join(['UNIQID','NAME','GWEIGHT']+new_column_header,'\t')+'\n' ### format column-names for export
	export_cdt.write(column_header)
	eweight = string.join(['EWEIGHT','','']+ ['1']*len(new_column_header),'\t')+'\n' ### format column-flat-clusters for export
	export_cdt.write(eweight)
	
	### Export each row in the clustered data matrix xt
	i=0
	for row in xt:
		export_cdt.write(string.join([new_row_header[i]]*2+['1']+map(str, row),'\t')+'\n')
		i+=1
	export_cdt.close()

################# Create Custom Color Gradients #################
#http://matplotlib.sourceforge.net/examples/pylab_examples/custom_cmap.html

def RedBlackSkyBlue():
	cdict = {'red':   ((0.0, 0.0, 0.0),
					   (0.5, 0.0, 0.1),
					   (1.0, 1.0, 1.0)),
	
			 'green': ((0.0, 0.0, 0.9),
					   (0.5, 0.1, 0.0),
					   (1.0, 0.0, 0.0)),
	
			 'blue':  ((0.0, 0.0, 1.0),
					   (0.5, 0.1, 0.0),
					   (1.0, 0.0, 0.0))
			}

	my_cmap = matplotlib.colors.LinearSegmentedColormap('my_colormap',cdict,256)
	return my_cmap

def RedBlackBlue():
	cdict = {'red':   ((0.0, 0.0, 0.0),
					   (0.5, 0.0, 0.1),
					   (1.0, 1.0, 1.0)),

			 'green': ((0.0, 0.0, 0.0),
					   (1.0, 0.0, 0.0)),
	
			 'blue':  ((0.0, 0.0, 1.0),
					   (0.5, 0.1, 0.0),
					   (1.0, 0.0, 0.0))
			}

	my_cmap = matplotlib.colors.LinearSegmentedColormap('my_colormap',cdict,256)
	return my_cmap

def RedBlackGreen():
	cdict = {'red':   ((0.0, 0.0, 0.0),
					   (0.5, 0.0, 0.1),
					   (1.0, 1.0, 1.0)),
	
			 'blue': ((0.0, 0.0, 0.0),
					   (1.0, 0.0, 0.0)),
	
			 'green':  ((0.0, 0.0, 1.0),
					   (0.5, 0.1, 0.0),
					   (1.0, 0.0, 0.0))
			}
	
	my_cmap = matplotlib.colors.LinearSegmentedColormap('my_colormap',cdict,256)
	return my_cmap

def YellowBlackBlue():
	cdict = {'red':   ((0.0, 0.0, 0.0),
					   (0.5, 0.0, 0.1),
					   (1.0, 1.0, 1.0)),
	
			 'green': ((0.0, 0.0, 0.8),
					   (0.5, 0.1, 0.0),
					   (1.0, 1.0, 1.0)),
	
			 'blue':  ((0.0, 0.0, 1.0),
					   (0.5, 0.1, 0.0),
					   (1.0, 0.0, 0.0))
			}
	### yellow is created by adding y = 1 to RedBlackSkyBlue green last tuple
	### modulate between blue and cyan using the last y var in the first green tuple
	my_cmap = matplotlib.colors.LinearSegmentedColormap('my_colormap',cdict,256)
	return my_cmap

################# General data import methods #################

def importData(filename):
	start_time = time.time()
	matrix=[]
	row_header=[]
	first_row=True

	if '/' in filename:
		dataset_name = string.split(filename,'/')[-1][:-4]
	else:
		dataset_name = string.split(filename,'\\')[-1][:-4]
		
	for line in open(filename,'rU').xreadlines():         
		t = string.split(line[:-1],'\t') ### remove end-of-line character - file is tab-delimited
		if first_row:
			column_header = t[1:]
			first_row=False
		else:
			if ' ' not in t and '' not in t: ### Occurs for rows with missing data
				s = map(float,t[1:])
				if (abs(max(s)-min(s)))>0:
					matrix.append(s)
					row_header.append(t[0])
			
	time_diff = str(round(time.time()-start_time,1))
	try:
		print '\n%d rows and %d columns imported for %s in %s seconds...' % (len(matrix),len(column_header),dataset_name,time_diff)
	except Exception:
		print 'No data in input file.'; force_error
	return np.array(matrix), column_header, row_header