import os,sys
import numpy as np
import subprocess
import glob
import math
import multiproc
import multiprocessing
import contig_motif_processor

control_ipds_fn       = "control/tmp/control_quiver_ipds.tmp"
control_ipds_N_fn     = "control/tmp/control_quiver_ipds_N.tmp"
for line in open(control_ipds_fn, "r").xreadlines():
	break
n_total_cols = len(line.split("\t"))
nproc        = 40
minMotifIPD  = 1.7

def run_OS_command( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	return sts, stdOutErr

def chunks( l, n ):
	"""
	Yield successive n-sized chunks from l.
	"""
	for i in xrange(0, len(l), n):
		yield l[i:i+n]


def load_float_into_array(fn):
	return np.loadtxt(fn, dtype="float")

def load_int_into_array(fn):
	return np.loadtxt(fn, dtype="int")

def split_file_by_columns( matrix_fn, n_total_cols, nproc ):
	chunksize       = int(math.ceil(float( n_total_cols/nproc )))
	col_chunks      = chunks(range(n_total_cols), chunksize)
	chunk_id_fn_map = {}
	for i,chunk in enumerate(col_chunks):
		print "   writing %s.colchunk_%s..." % (matrix_fn, i)
		first          = chunk[0]+1
		last           = chunk[-1]+1
		out_fn         = "%s.colchunk_%s" % (matrix_fn, i)
		split_CMD      = "cut -d$'\t' -f%s-%s %s > %s" % (first, last, matrix_fn, out_fn)
		sts, stdOutErr = run_OS_command( split_CMD )
		chunk_id_fn_map[i] = out_fn
	return chunk_id_fn_map

print "Writing control IPD chunks..."
control_ipds_chunk_id_fn_map   = split_file_by_columns(control_ipds_fn, n_total_cols, nproc)
paths = control_ipds_chunk_id_fn_map.values()
paths.sort()
p     = multiprocessing.Pool()
try:
	results = p.map(load_float_into_array, paths)
	p.close()
	p.join()
except KeyboardInterrupt:
	p.terminate()
control_ipds_chunk_id_arr_map  = {}
for i,result in enumerate(results):
	control_ipds_chunk_id_arr_map[i] = result

print "Writing control IPD N chunks..."
control_ipds_N_chunk_id_fn_map  = split_file_by_columns(control_ipds_N_fn, n_total_cols, nproc)
paths = control_ipds_N_chunk_id_fn_map.values()
paths.sort()
p     = multiprocessing.Pool()
try:
	results = p.map(load_int_into_array, paths)
	p.close()
	p.join()
except KeyboardInterrupt:
	p.terminate()
control_ipds_N_chunk_id_arr_map  = {}
for i,result in enumerate(results):
	control_ipds_N_chunk_id_arr_map[i] = result

contig_labels_fns = glob.glob( os.path.join("tmp", "*_labels.tmp") )
contigs           = map(lambda x: os.path.basename(x).split("_labels.tmp")[0], contig_labels_fns)
for contig in contigs:
	print "...contig: %s" % contig
	
	print "    Creating tasks..."
	tasks   = multiprocessing.JoinableQueue()
	results = multiprocessing.Queue()
	num_consumers = nproc
	print "    Starting consumers..."
	consumers     = [ multiproc.Consumer(tasks, results) for i in xrange(num_consumers) ]
	for w in consumers:
		w.start()

	contig_ipds_fn             = os.path.join( "tmp", "%s_ipds.tmp"       % contig)
	contig_ipds_kmers          = os.path.join( "tmp", "%s_ipds_kmers.tmp" % contig)
	contig_ipds_N              = os.path.join( "tmp", "%s_ipds_N.tmp"     % contig)
	ipds_chunk_id_fn_map       = split_file_by_columns(contig_ipds_fn,    n_total_cols, nproc)
	ipds_kmers_chunk_id_fn_map = split_file_by_columns(contig_ipds_kmers, n_total_cols, nproc)
	ipds_N_chunk_id_fn_map     = split_file_by_columns(contig_ipds_N,     n_total_cols, nproc)
	for chunk_id in ipds_chunk_id_fn_map.keys():
		ipds   = np.loadtxt(ipds_chunk_id_fn_map[chunk_id],       dtype=float)
		kmers  = np.loadtxt(ipds_kmers_chunk_id_fn_map[chunk_id], dtype=str)
		ipds_N = np.loadtxt(ipds_N_chunk_id_fn_map[chunk_id],     dtype=int)
		tasks.put(contig_motif_processor.motifs_scanner( ipds,                     \
														 kmers,                    \
														 ipds_N,                   \
														 control_ipds_chunk_id_arr_map[chunk_id],   \
														 control_ipds_N_chunk_id_arr_map[chunk_id], \
														 minMotifIPD ))

	# Add a poison pill for each consumer
	for i in xrange(num_consumers):
		tasks.put(None)
	# Wait for all of the tasks to finish
	tasks.join()
	
	# Start printing results
	print "     Combining results data from all chunks..."
	num_jobs         = nproc
	parallel_results = []
	while num_jobs:
		result = results.get()
		parallel_results.append(result)
		num_jobs -= 1
		# print "...%s/%s" % ((nproc-num_jobs),nproc)
