import numpy as np
import math
import re
from collections import Counter, OrderedDict, defaultdict

class ipd_entry:
	def __init__( self, tup ):
		"""
		"""
		self.call    = tup[0]
		self.ipd     = tup[1]

class subread:
	def __init__( self, sub, leftAnchor=10, rightAnchor=10 ):
		"""
		"""
		self.leftAnchor  = leftAnchor
		self.rightAnchor = rightAnchor
		self.load_entries( sub )
		
		self.subread_normalize()

	def load_entries( self, sub ):
		"""
		"""
		self.entries    = {}

		# Pull out alignment-level information
		base_calls = np.array(list(sub.basecalls()))
		# fps        = sub.movieInfo[2]
		fps        = 75.0
		ipds       = np.array(sub.IPD()) / fps
		
		# minInsertionQV    = 
		# minDeletionQV     = 
		# minSubstitutionQV = 
		# minMergeQV        = 
		# minQV             = 

		# # Don't consider bases with bad QVs
		# error_mk = []
		# for base_call in base_calls:
		# 	if base_call == "-":
		# 		error_mk.append(1)
		# 	else:
		# 		error_mk.append(0)
		# error_mk   = np.array(error_mk)
		# base_calls = np.array(base_calls)[error_mk==0]
		# ipds       = ipds[error_mk==0]

		# Shift the IPDs backwards one position so that they are associated with their basecall
		ipds_shifted = list(ipds)[1:]
		ipds_shifted.append(0)
		replaced_ipds = []
		for ipd in ipds_shifted:
			replaced_ipds.append(ipd)
		ipds = np.array(replaced_ipds)

		# Attach these IPD entries to the molecule object
		for read_pos, tup in enumerate(zip(base_calls, ipds)):
			self.entries[ read_pos ] = ipd_entry(tup)

	def subread_normalize( self ):
		"""
		Every IPD entry needs to be normalized by the mean IPD of its subread.
		"""
		if len(self.entries) == 0:
			# Nothing to do here.
			return self.entries

		# First populate list of all IPDs per subread. Will use to get normalization factor.
		subread_vals = []
		for entry in self.entries.values():
			subread_vals.append(entry.ipd)

		rawIPDs = np.array(map(lambda x: math.log(x + 0.001), subread_vals))
		nfs     = rawIPDs.mean()

		entries_to_del = []
		for read_pos, entry in self.entries.iteritems():
			if nfs == 0:
				entries_to_del.append(read_pos)
				continue

			newIPD = math.log(entry.ipd + 0.001) - nfs
			
			if np.isnan(newIPD) or np.isnan(entry.ipd):
				raise Exception("NAN detected!")
			entry.ipd = newIPD

		for read_pos in entries_to_del:
			del self.entries[ read_pos ]

	def zip_bases_and_IPDs( self ):
		"""
		Reassemble the read and IPD values using the subread normalized IPDs
		"""
		od        = OrderedDict(sorted(self.entries.items()))
		read      = []
		self.ipds = []
		for read_pos, entry in od.items():
			read.append(entry.call)
			self.ipds.append(entry.ipd)
		self.read_str = "".join(read)

	def scan_motifs( self, motifs, min_IPD, read_barcodes, j, bases ):
		"""
		For each motif, find all occurrences in read.
		"""
		motif_counter = Counter()
		for motif in motifs:
			if motif.find("N") > -1:
					# There are N's in this motif; adjust accordingly!
					motif = motif.replace("N", "[ACGT]")

			matches_iter = re.finditer(motif, self.read_str)
			matches_list = []
			for match in matches_iter:
				matches_list.append(match)

			motif      = motif.replace("[ACGT]", "N")
			motif_ipds = []
			for match in matches_list:
				# For each motif occurrence, add the IPDs (list of size kmer) to 'motif_ipds'
				motif_counter[motif] += 1
				motif_start           = match.span()[0]
				motif_end             = match.span()[1]
				motif_ipds.append( self.ipds[motif_start:motif_end] )
			means = []
			for col in range(len(motif)):
				if len(motif_ipds) > 0:
					means.append( np.mean(map(lambda x: x[col], motif_ipds)) )
				else:
					means.append(0)

			# Only include motifs that contain at least one of the target bases
			for base in bases:
				indexes = [m.start() for m in re.finditer(base, motif)]
				for index in indexes:
					# If more than one target base, specify which one maps to the IPD score we're storing
					motif_string = "%s-%s" % (motif, index)
					IPD          = means[index]
					N_sites      = motif_counter[motif]
					if IPD > min_IPD:
						read_barcodes[j][motif_string] = {"IPD": IPD, "N": N_sites}
					else:
						read_barcodes[j][motif_string] = {"IPD": 0, "N": N_sites}
		return read_barcodes

	def write_to_fasta( self, f, name, width=70 ):
		"""
		Write subread to fasta file entry.
		"""
		def chunkstring(string, length):
			return (string[0+i:length+i] for i in range(0, len(string), length))
		
		f.write(">%s\n" % name)
		for line in chunkstring(self.read_str, width):
			f.write("%s\n" % line)
