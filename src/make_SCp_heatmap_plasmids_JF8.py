import os,sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import cm as cm
import scipy.cluster.hierarchy as hier
import scipy.spatial.distance as dist
import collections
import pub_figs
import hierarchical_cluster_plasmids

fn     = sys.argv[1]
motifs = np.loadtxt(sys.argv[2], dtype="str")
A      = np.loadtxt(fn, dtype="str")

########### JF8 plasmids ############
plasmids = ["unitig_82", \
			"unitig_87", \
			"unitig_73", \
			"unitig_96"]

max_contigs  = 3
spec_contigs = collections.defaultdict(list)
rows         = 0
for i in range(A.shape[0]):
	spec =     A[i,0]
	if spec=="B_theta":
		spec = "B_thetaiotaomicron"
	name =     A[i,1]
	if name in plasmids:
		spec = r"\textbf{PLASMID}"
		if name=="unitig_87":
			spec += r" (\textit{C. bolteae})"
		elif name=="unitig_82":
			spec += r" (\textit{B. thetaiotaomicron})"
		elif name=="unitig_73":
			spec += r" (\textit{B. thetaiotaomicron})"
		elif name=="unitig_96":
			spec += r" (\textit{C. aerofaciens})"
	else:
		spec = r"\textit{%s}" % spec.replace("_", ". ")
	leng = int(A[i,2])
	ipds =     A[i,3:].astype(np.float)
	if len(spec_contigs[spec])<max_contigs or name in plasmids:
		spec_contigs[spec].append( (spec, name, leng, ipds) )
		rows += 1

cols       = len(ipds)
B          = np.zeros([rows, cols])
i          = 0
motifs_str = "\t".join(motifs)
clust_mat_fn = fn+"_forClust"
f          = open(clust_mat_fn, "w")
f.write("species\t%s\n" % motifs_str)
for spec,contig_list in spec_contigs.iteritems():
	for contig_entry in contig_list:
		B[i,:] = contig_entry[3]
		i     += 1
		# name_str = "%s_%s" % (contig_entry[0].replace("_",""), contig_entry[1].replace("_",""))
		name_str = "%s: %s" % (contig_entry[0], contig_entry[1])
		name_str = name_str.replace("_","\_")
		ipds_str = "\t".join(contig_entry[3].astype(np.str))
		f.write("%s\t%s\n" % (name_str, ipds_str))
minPos = 0
maxPos = B.shape[1]
f.close()

hierarchical_cluster_plasmids.run(clust_mat_fn)

# heatmap_name = "%s.heatmap.png" % fn
# fig = pub_figs.init_fig()
# plt.pcolormesh(B, cmap=cm.jet)
# plt.colorbar()
# tick_lcs = np.arange(0, cols, cols/10)
# tick_lbs = tick_lcs + minPos
# plt.xticks(tick_lcs, tick_lbs)
# plt.savefig(heatmap_name)
# plt.close()