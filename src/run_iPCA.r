args <- commandArgs(TRUE)
fileIn  = args[1]
fileOut = args[2]
num_PCA = as.numeric(args[3])
thresh  = as.numeric(args[4])
source('~/gitRepo/eBinning/src/f_iPCA_nPCs.r')
iPCA(fileIn,fileOut,num_PCA,thresh) 