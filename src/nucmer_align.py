import os,sys
import subprocess
import logging
import optparse
from collections import defaultdict

class nuc_align:
	def __init__( self, align ):
		self.S1            = int(align.split()[0])
		self.E1            = int(align.split()[1])
		self.S2            = int(align.split()[3])
		self.E2            = int(align.split()[4])
		self.len1          = int(align.split()[6])
		self.len2          = int(align.split()[7])
		self.pct_idy       = float(align.split()[9])
		self.contig_len    = int(align.split()[11])
		self.LR_len        = int(align.split()[12])
		self.contig_cov    = float(align.split()[14])
		self.LR_cov        = float(align.split()[15])
		self.contig_strand = int(align.split()[18])
		self.contig_name   =  align.split()[19]
		self.LR_name       =  align.split()[20]

class aligner:
	def __init__( self ):
		"""
		Parse the options and arguments, then instantiate the logger. 
		"""
		self.__parseArgs( )
		self.__initLog( )
	
	def run( self ):
		nucmer_CMD  = "nucmer --maxgap 400 --minmatch 10 --breaklen 400 %s %s" % (self.ref, self.query)
		p         = subprocess.Popen(nucmer_CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		stdOutErr = p.communicate()
		sts       = p.returncode
		if sts != 0:
			raise Exception("Failed system command: %s" % nucmer_CMD)
		# print stdOutErr[1]

		show_coords_CMD  = "show-coords -L 1000 -l -c -d -r out.delta"
		p                = subprocess.Popen(show_coords_CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		stdOutErr        = p.communicate()
		sts              = p.returncode
		if sts != 0:
			raise Exception("Failed system command: %s" % show_coords_CMD)
		aligns = stdOutErr[0]

		header = aligns.split("\n")[3]
		aligns = aligns.split("\n")[5:-1]

		LR_contig_map = defaultdict(list)
		aln_stats     = {}
		for align in aligns:
			print align
			aln = nuc_align(align)
			if aln.LR_cov > 90 and aln.pct_idy > 80:
				LR_contig_map[aln.contig_name].append(aln.LR_name)
				aln_stats[ (aln.contig_name, aln.LR_name) ] = {"pct_idy":    aln.pct_idy, \
															   "LR_len":     aln.LR_len, \
															   "contig_cov": aln.contig_cov}
		fn = "contig_LR.maps"
		f  = open(fn,"w")
		print ""
		for contig,LRs in LR_contig_map.iteritems():
			print contig, LRs
			f.write("%s\t%s\n" % (contig, ",".join(LRs)))
		f.close()

	def __parseArgs( self ):
		"""Handle command line argument parsing"""

		usage = """%prog [--help] [options] <reference> <bax.h5_fofn>

		USAGE INSTRUCTIONS
		"""

		parser = optparse.OptionParser( usage=usage, description=__doc__ )

		parser.add_option( "-d", "--debug", action="store_true", help="Increase verbosity of logging" )
		parser.add_option( "-i", "--info", action="store_true", help="Add basic logging" )
		parser.add_option( "--logFile", type="str", help="Write logging to file [log.out]" )
		parser.set_defaults( logFile="log.out",                \
							 debug=False,                      \
							 info=False)

		self.opts, args = parser.parse_args( )

		if len(args) != 2:
			parser.error( "Expected 2 arguments." )

		self.ref   = args[0]
		self.query = args[1]

	def __initLog( self ):
		"""Sets up logging based on command line arguments. Allows for three levels of logging:
		logging.error( ): always emitted
		logging.info( ) : emitted with --info or --debug
		logging.debug( ): only with --debug"""

		if os.path.exists(self.opts.logFile):
			os.remove(self.opts.logFile)

		logLevel = logging.DEBUG if self.opts.debug \
					else logging.INFO if self.opts.info \
					else logging.ERROR

		self.logger = logging.getLogger("")
		self.logger.setLevel(logLevel)
		
		# create file handler which logs even debug messages
		fh = logging.FileHandler(self.opts.logFile)
		fh.setLevel(logLevel)
		
		# create console handler with a higher log level
		ch = logging.StreamHandler()
		ch.setLevel(logLevel)
		
		# create formatter and add it to the handlers
		logFormat = "%(asctime)s [%(levelname)s] %(message)s"
		formatter = logging.Formatter(logFormat)
		ch.setFormatter(formatter)
		fh.setFormatter(formatter)
		
		# add the handlers to logger
		self.logger.addHandler(ch)
		self.logger.addHandler(fh)

if __name__=="__main__":
	app     = aligner()
	results = app.run()
