import os,sys
import numpy as np
import subprocess
import glob
import math
import multiproc
import multiprocessing
from itertools import izip
import re

control_ipds_fn   = "/hpc/users/beaulj01/projects/ebinning/control/control_quiver_ipds.tmp"
control_ipds_N_fn = "/hpc/users/beaulj01/projects/ebinning/control/control_quiver_ipds_N.tmp"
nproc             = 24
minMotifIPD       = 1.7

def run_OS_command( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	return sts, stdOutErr

def transpose( fn ):
	trans_script   = "/hpc/users/beaulj01/gitRepo/eBinning/src/transpose.awk"
	trans_CMD      = "awk -f %s %s > %s.trans" % (trans_script, fn, fn)
	print trans_CMD
	sts, stdOutErr = run_OS_command(trans_CMD)

if os.path.exists(control_ipds_fn+".trans") and os.path.exists(control_ipds_N_fn+".trans"):
	pass
else:
	print "transposing control...."
	files = [control_ipds_fn, control_ipds_N_fn]
	p = multiprocessing.Pool(processes=len(files))
	try:
		results = p.map(transpose, files)
		p.close()
		p.join()
	except KeyboardInterrupt:
		p.terminate()
	print "Done transposing control."

def transpose_matrix( contig ):
	print "Transposing", contig
	contig_ipds_fn       = os.path.join( "tmp", "%s_ipds.tmp"       % contig)
	contig_ipds_kmers_fn = os.path.join( "tmp", "%s_ipds_kmers.tmp" % contig)
	contig_ipds_N_fn     = os.path.join( "tmp", "%s_ipds_N.tmp"     % contig)
	contig_ipds          = np.loadtxt(contig_ipds_fn,       dtype="float")
	contig_ipds_kmers    = np.loadtxt(contig_ipds_kmers_fn, dtype="str")
	contig_ipds_N        = np.loadtxt(contig_ipds_N_fn,     dtype="int")
	if len(contig_ipds.shape)==1:
		f = open(contig_ipds_fn+".trans", "w")
		for ipd in contig_ipds:
			f.write("%.4f\n" % ipd)
		f.close()
		f = open(contig_ipds_N_fn+".trans", "w")
		for N in contig_ipds_N:
			f.write("%s\n" % N)
		f.close()
	elif len(contig_ipds.shape)==2:
		contig_ipds    = contig_ipds.transpose()
		contig_ipds_N  = contig_ipds_N.transpose()
		np.savetxt(contig_ipds_fn+".trans",   contig_ipds,   fmt="%.4f", delimiter="\t")
		np.savetxt(contig_ipds_N_fn+".trans", contig_ipds_N, fmt="%s",   delimiter="\t")
	return None

def stream_case_control_files( tup ):
	control_ipds_fn      = tup[0]
	control_ipds_N_fn    = tup[1]
	contig               = tup[2]
	contig_motifs        = {}
	keeper_motifs        = set()
	contig_ipds_fn       = os.path.join( "tmp", "%s_ipds.tmp"       % contig)
	contig_ipds_kmers_fn = os.path.join( "tmp", "%s_ipds_kmers.tmp" % contig)
	contig_ipds_N_fn     = os.path.join( "tmp", "%s_ipds_N.tmp"     % contig)
	kmers = np.loadtxt(contig_ipds_kmers_fn, dtype="str")
	files = [control_ipds_fn+".trans", control_ipds_N_fn+".trans", contig_ipds_fn+".trans", contig_ipds_N_fn+".trans"]
	for i,(l1,l2,l3,l4) in enumerate(izip( open(files[0]), open(files[1]), open(files[2]), open(files[3]))):
		if i%10000==0:
			print i
		motif        = kmers[i]
		case_read_Ns = map(lambda x: int(x), l4.split())
		if np.sum(case_read_Ns)>=20:
			control_read_Ns = map(lambda x: int(x), l2.split())
			if np.sum(control_read_Ns)>=20:
				control_read_means   = map(lambda x: float(x), l1.split())
				case_read_means      = map(lambda x: float(x), l3.split())
				case_mean            = np.dot(case_read_means, case_read_Ns) / np.sum(case_read_Ns)
				control_mean         = np.dot(control_read_means, control_read_Ns) / np.sum(control_read_Ns)
				score                = case_mean - control_mean
				contig_motifs[motif] = score
			else:
				score = 0.0
		else:
			score = 0.0

	# Keep only the shortest version of the high scoring motifs (reduces redundancy)
	highscore_motifs = dict( [(motif,SCp) for motif,SCp in contig_motifs.items() if SCp>=minMotifIPD] )
	if len(highscore_motifs)>0:
		shortest_contiguous = min([len(m.split("-")[0]) for m in highscore_motifs.keys()])
		shortest_motifs     = [m for m in highscore_motifs.keys() if len(m.split("-")[0])==shortest_contiguous]
		to_del = set()
		for shorty in shortest_motifs:
			shorty_str =     shorty.split("-")[0]
			shorty_idx = int(shorty.split("-")[1])
			for motif in highscore_motifs.keys():
				if motif!=shorty:
					motif_str =     motif.split("-")[0]
					motif_idx = int(motif.split("-")[1])
					match = re.search(shorty_str, motif_str)
					if match != None:
						if (shorty_idx + match.start()) == motif_idx:
							to_del.add( motif )
		for motif in highscore_motifs.keys():
			if motif not in to_del:
				keeper_motifs.add(motif)
	print contig, keeper_motifs
	return keeper_motifs


contig_labels_fns = glob.glob( os.path.join("tmp", "*_labels.tmp") )
contigs           = map(lambda x: os.path.basename(x).split("_labels.tmp")[0], contig_labels_fns)



contigs = ["unitig_43"]



p     = multiprocessing.Pool(processes=nproc)
try:
	results = p.map(transpose_matrix, contigs)
	p.close()
	p.join()
except KeyboardInterrupt:
	p.terminate()

print "Streaming through %s contigs..." % len(contigs)
args = [(control_ipds_fn, control_ipds_N_fn, contig) for contig in contigs]
p    = multiprocessing.Pool(processes=nproc)
try:
	results = p.map(stream_case_control_files, args)
	p.close()
	p.join()
except KeyboardInterrupt:
	p.terminate()

keeper_motifs = set.union(*results)
print keeper_motifs