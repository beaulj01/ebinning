import os,sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib
from matplotlib import pyplot as plt
from collections import defaultdict, Counter
import operator
import bisect
import cmph5_read
import random
import pub_figs
import pickle

font_size = 18
pub_figs.setup_math_fonts(font_size=font_size, font="Computer Modern Sans serif")

dists_fn     = sys.argv[1]
case_ipds    = []
control_ipds = []
for line in open(dists_fn, "r").xreadlines():
	line        = line.strip()
	contig_type = line.split()[0]
	if contig_type=="case":
		case_ipds.append( float(line.split()[1]) )
	elif contig_type=="control":
		control_ipds.append( float(line.split()[1]) )

ROC_vals_pkl = "ROC_values.pkl"
N_samples    = 10000
N_sites_list = [2,4,6,8]
results         = defaultdict(list)
stats_to_return = {}
# for N_sites in N_sites_list:
# 	hit_counter     = Counter()

# 	SCp_case_vals    = []
# 	SCp_control_vals = []
# 	for i in range(N_samples):
# 		SCp_case    = np.mean(np.random.choice(case_ipds,    N_sites, replace=False))
# 		SCp_control = np.mean(np.random.choice(control_ipds, N_sites, replace=False))
		
# 		SCp_case_vals.append(SCp_case)
# 		SCp_control_vals.append(SCp_control)

# 	case_sorted    = sorted(SCp_case_vals)
# 	control_sorted = sorted(SCp_control_vals)
# 	FDRs = []
# 	already_printed = False
# 	for i,val in enumerate(case_sorted):
# 		hit_counter[N_sites] += 1

# 		# Find number of p-values larger than this value
# 		N_called_exps = len(case_sorted) - i
# 		N_exps        = len(case_sorted)
# 		exp_frac      = float(N_called_exps) / N_exps

# 		# Find closest entry in sorted control p-value list
# 		N_called_controls = len(control_sorted) - bisect.bisect_left(control_sorted, val)
# 		N_controls        = len(control_sorted)
# 		FPR               = float(N_called_controls) / N_controls

# 		FDR  = FPR / exp_frac
# 		FDRs.append(FDR)
# 		sens = float(N_called_exps) / len(case_sorted)
# 		incr = len(case_sorted)/1000
# 		if i>0 and i%incr==0:
# 			print "methyl: N_sites>=%s, i=%s, val=%s, FDR=%.4f, sensitivity=%.4f, FPR=%.4f, N_called_control=%s, exp_frac=%.4f, N_called_exps=%s" % (N_sites, \
# 																												   i, \
# 																												   val, \
# 																												   FDR, \
# 																												   sens, \
# 																												   FPR, \
# 																												   N_called_controls, \
# 																												   exp_frac, \
# 																												   N_called_exps)
# 			results[N_sites].append( (FPR, sens, val, FDR) )
			
# 		stats = "methyl: N_sites=%s item/sorted_total=%s/%s FDR=%.4f pct_called=%.4f ipdRatio>=%s" % (N_sites, hit_counter[N_sites], len(case_sorted), FDR, sens, val)
# 		string = "%s/%s" % (hit_counter[N_sites], len(case_sorted))
# 		stats_to_return[N_sites] = {"N_sites":N_sites,  \
# 								 "item/sorted_total":string, \
# 								 "FDR":FDR, \
# 								 "pct_called":sens, \
# 								 "ipdRatio":val}
# 		already_printed = True
# pickle.dump(results, open(ROC_vals_pkl, "wb"))


adjust    = 0.15
fig       = pub_figs.init_fig(x=10,y=8)
ax        = fig.add_subplot(111)
sq_axes   = pub_figs.make_square_axes( ax.axis() )
ax.axis(sq_axes) 
colors    = ["b", "r", "k", "c"]

results     = pickle.load(open(ROC_vals_pkl, "rb"))

ordered_results = sorted(results.iteritems(), key=operator.itemgetter(0))
for j,(mode, vals) in enumerate(ordered_results):
	X   = map(lambda x: x[0], vals)
	Y   = map(lambda x: x[1], vals)
	# gte = u"\u2265"
	lab = "%s IPDs" % mode
	ax.plot(X, Y, color=colors[j], label=lab, linewidth=4)
ax.set_xlim([0, 1])
ax.set_ylim([0, 1])
# plt.plot([0, 1], [0, 1], 'r--', linewidth=4)
legend = plt.legend(loc=4, fontsize=font_size, labelspacing=0.1, borderaxespad=0.03)
legend.draw_frame(False)
locs, labels = plt.xticks()
# plt.setp(labels, rotation="vertical")
ax.set_xlabel("FPR")
ax.set_ylabel("Sensitivity")
ax.get_xaxis().set_tick_params(length=20)
ax.get_yaxis().set_tick_params(length=20)
ax.tick_params(axis='x', pad=15)
ax.tick_params(axis='y', pad=15)
ax.xaxis.grid(color='gray', linestyle='dashed')
ax.yaxis.grid(color='gray', linestyle='dashed')
pub_figs.change_font_size(ax, font_size=font_size, bottom_adjust=adjust, left_adjust=adjust*1.3)
pub_figs.remove_top_right_axes(ax)
fn = "%s.ROC.png" % dists_fn
if os.path.exists(fn): os.remove(fn)
plt.savefig(fn, dpi=300)

fig      = pub_figs.init_fig()
ax       = fig.add_subplot(111)
sq_axes  = pub_figs.make_square_axes( ax.axis() )
ax.axis(sq_axes) 
colors   = ["b", "r", "k", "c"]
ordered_results = sorted(results.iteritems(), key=operator.itemgetter(0))
for j,(mode, vals) in enumerate(ordered_results):
	X   = map(lambda x: x[0], vals)
	Y   = map(lambda x: x[1], vals)
	# gte = u"\u2265"
	lab = mode
	ax.plot(X, Y, color=colors[j], label=lab, linewidth=5)
ax.set_xlim([0, 0.301])
ax.set_ylim([0.7, 1])
locs, labels = plt.xticks()
plt.setp(labels, rotation="vertical")
pub_figs.change_font_size(ax, font_size=font_size*1.4, bottom_adjust=adjust*2, left_adjust=adjust*2)
pub_figs.remove_top_right_axes(ax)
ax.get_xaxis().set_tick_params(length=20)
ax.get_yaxis().set_tick_params(length=20)
ax.tick_params(axis='x', pad=15)
ax.tick_params(axis='y', pad=15)
ax.set_xticks([0.0, 0.1, 0.2, 0.3])
ax.set_yticks([0.7, 0.8, 0.9, 1.0])
# ax.set_xticklabels()
# ax.set_yticklabels()
ax.xaxis.grid(color='gray', linestyle='dashed')
ax.yaxis.grid(color='gray', linestyle='dashed')
ax.set_axisbelow(True)
fn = "%s.ROC.zoom.png" % dists_fn
if os.path.exists(fn): os.remove(fn)
plt.savefig(fn, dpi=300)