import os,sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import cm as cm
import scipy.cluster.hierarchy as hier
import scipy.spatial.distance as dist
import collections
import pub_figs
import hierarchical_cluster_ten_spec_mix

fn      = "ten_mix.dists_and_motifs"
ten_mix = np.loadtxt(fn, dtype="str", delimiter="\t")

all_motifs = set()
org_motifs = {}
for i in range(ten_mix.shape[0]):
	dist   = ten_mix[i,0]
	names  = ten_mix[i,2]
	motifs = set(ten_mix[i,4].split(" "))
	map(lambda motif: all_motifs.add(motif), motifs)
	n_motifs = len(all_motifs)
	org_motifs[names] = motifs

all_motifs = list(all_motifs)
all_motifs.sort()
all_motifs = np.array(all_motifs)

motif_mat = np.zeros([ten_mix.shape[0], len(all_motifs)])
for i in range(ten_mix.shape[0]):
	names      = ten_mix[i,2]
	motifs     = set(ten_mix[i,4].split(" "))
	org_array  = np.zeros(n_motifs)
	for j,motif in enumerate(all_motifs):
		if motif in motifs:
			org_array[j]   = 1
			motif_mat[i,j] = 1

clust_mat_fn = fn+".forClust"
f            = open(clust_mat_fn, "w")
namess       = {}
f.write("names\t%s\n" % "\t".join(map(lambda x: str(x), all_motifs)))
for i in range(motif_mat.shape[0]):
	names         = ten_mix[i,2]
	motifs        = motif_mat[i,:]
	motifs_str    = "\t".join(map(lambda x: str(x), motifs))
	namess[names] = motifs
	name_str      = names.replace("_","-")
	f.write("%s\t%s\n" % (name_str, motifs_str))
f.close()

hierarchical_cluster_ten_spec_mix.run(clust_mat_fn)