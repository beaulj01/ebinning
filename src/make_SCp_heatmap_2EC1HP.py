import os,sys
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import cm as cm
import scipy.cluster.hierarchy as hier
import scipy.spatial.distance as dist
import collections
import pub_figs
from Bio import SeqIO,SeqUtils
import numpy as np
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
import hierarchical_cluster_2EC1HP

labels_fn   = "contigs.labels"
SCp_fn      = "catted_contigs.cdiff_control.SCp"
SCp_N_fn    = "catted_contigs.cdiff_control.SCp_N"
motifs_fn   = "ordered_motifs.cdiff_control.txt"
fasta_fn    = "pHel3.fa"
labels      = np.loadtxt(labels_fn, dtype="str", delimiter="\t")
SCp_scores  = np.loadtxt(SCp_fn,    dtype="float", delimiter="\t")
SCp_Ns      = np.loadtxt(SCp_N_fn,  dtype="int", delimiter="\t")
motifs      = np.loadtxt(motifs_fn, dtype="str", delimiter="\t")

minsites    = 3
minIPDs     = 3

n_sites = {}
for motif in motifs:
	motif_str = motif.split("-")[0]
	motif_idx = motif.split("-")[1]
	for seq_record in SeqIO.parse(fasta_fn, "fasta"):
		seq_str      = str(seq_record.seq.upper())
		for_results  = SeqUtils.nt_search(seq_str, motif_str)

		rc_motif_str = Seq(motif_str, IUPAC.unambiguous_dna).reverse_complement()
		rev_results  = SeqUtils.nt_search(seq_str, rc_motif_str)

		nfor_sites = len(for_results[1:])
		nrev_sites = len(rev_results[1:])
		n_sites[motif] = nfor_sites+nrev_sites
		# print motif, for_results[1:], rev_results[1:]

# Prune out motif columns where there are not SCp values for any contigs
# (e.i. where there are 0.0 values)
col_idx = []
for j in range(SCp_scores.shape[1]):
	if any(SCp_scores[:,j]==0.0) or n_sites[motifs[j]]<minsites or any(SCp_Ns[:,j]<minIPDs):
		pass
	else:
		col_idx.append(j)

col_idx = np.array(col_idx)

clust_mat_fn = SCp_fn+".forClust"
f            = open(clust_mat_fn, "w")
f.write("names\t%s\n" % "\t".join(map(lambda x: str(x), motifs[col_idx])))
for i in range(SCp_scores.shape[0]):
	names    = labels[i]
	SCp      = SCp_scores[i,col_idx]
	SCp_str  = "\t".join(map(lambda x: str(x), SCp))
	name_str = names.replace("_","-")
	f.write("%s\t%s\n" % (name_str, SCp_str))
f.close()

hierarchical_cluster_2EC1HP.run(clust_mat_fn)