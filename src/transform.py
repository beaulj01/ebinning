import sys,os
from sklearn.decomposition import PCA
import pandas as p
import numpy as np
import matplotlib.pyplot as plt

eBar_file = sys.argv[1]
pca_components = 8
FLOAT_FORMAT = '%1.8e'
length_threshold = 500

def perform_pca(d,nc):
	pca_object = PCA(n_components=nc).fit(d)
	return pca_object.transform(d), pca_object

def write_pca(transform, threshold, index):
	transform_df  = p.DataFrame(transform, index=index)
	PCA_FILE_BASE = "PCA_transformed_data_gt{0}.csv"
	transform_df.to_csv(
		PCA_FILE_BASE.format(threshold),
		float_format=FLOAT_FORMAT,
		index_label="motif_id"
		)
	print 'Wrote PCA transformed file.'

def write_pca_components(components, threshold):
	PCA_COMPONENTS_FILE_BASE = "PCA_components_data_gt{0}.csv"
	np.savetxt(
		PCA_COMPONENTS_FILE_BASE.format(threshold),
		components,
		fmt=FLOAT_FORMAT,
		delimiter=","
	)
	print 'Wrote PCA components file.'

joined = p.read_table(eBar_file, header=0, index_col=0)

#PCA on the contigs that have kmer count greater than length_threshold
transform_filter, pca = perform_pca(
	joined,
	pca_components
	)

print 'Performed PCA, resulted in %s dimensions' % transform_filter.shape[1]

# Output.write_original_data(
# 	joined,
# 	args.length_threshold
# 	)

write_pca(
	transform_filter,
	length_threshold,
	joined.index,
	)

write_pca_components(
	pca.components_,
	length_threshold
	)

print 'PCA transformed data.'
