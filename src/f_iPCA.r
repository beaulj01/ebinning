library(scales)
iPCA =  function(fileIn,fileOut,plotOut,threshold=1e-17)
{	
	
	data = read.table(fileIn,sep='\t',header=F)
	# label = as.character(data[,1])
	avg_thres = threshold*nrow(data)
	
	X1  = as.matrix(data[,-1])
	A   = t(X1)%*%X1
	r1  = rnorm(ncol(X1))
	rp1 = 0
	while( sum(abs(r1-rp1)) > avg_thres  )
	{
		rp1 = r1
		s   = A%*%matrix(r1)
		r1  = s/sqrt(sum(s^2))
		cat('difference1= ',sum(abs( r1-rp1 )),'\n')
	}

	pc1 = X1%*%matrix(r1)
	X2  = apply(X1,2,function(x){ residuals(lm(x~pc1)) } ) 
	B   = t(X2)%*%X2
	r2  = rnorm(ncol(X2))
	rp2 = 0
	while( sum(abs(r2-rp2)) > avg_thres )
	{
		rp2 = r2
		s   = B%*%matrix(r2)
		r2  = s/sqrt(sum(s^2))
		cat('difference2= ',sum(abs( r2-rp2 )),'\n')
	}
	pc2 = X2%*%matrix(r2)

	# pdf(plotOut)
	# plot(pc1,pc2,main='Barcode',xlab='pc1',ylab='pc2',col='white')
	# uni_label = unique(label)
	# #uni_label = c("ecoli","caero","rgnavus","cbolteae","bovatus","btheta")
	# #uni_label = c("bovatus","btheta","bcaccae")
	# for(i in 1:length(uni_label))
	# {
	# 	range = which(label==uni_label[i])
	# 	points(pc1[range],pc2[range],col=alpha(i,0.2),pch=i)
	# }

	# legend( min(pc1), max(pc2)  , c( uni_label ), col = c( 1:length(uni_label) ), pch = c( 1:length(uni_label) ) )
	# dev.off()

	data = cbind(pc1,pc2)
	write.table(data, fileOut, sep="\t", quote=FALSE, row.names = FALSE, col.names = FALSE)
	# write.table(pc1, "PCA1.out", sep="\t", row.names = FALSE, col.names = FALSE)
	# write.table(pc2, "PCA2.out", sep="\t", row.names = FALSE, col.names = FALSE)
}



iPCA_permute =  function(fileIn,fileOut,keep_label,threshold=1e-17)
{	
	
	data = read.table(fileIn,sep='\t',header=F)
	label = as.character(data[,1])
	avg_thres = threshold*nrow(data)
	
	X1 = as.matrix(data[,-1])
	keep_range = which(label%in%keep_label)
	X1[ -keep_range , ] =  t( apply( X1[ -keep_range , ] , 1, function(x) { x[ sample( 1:length(x),length(x) ) ] } ) )
	
	A = t(X1)%*%X1
	r1 = rnorm(ncol(X1))
	rp1 = 0
	while( sum(abs(r1-rp1)) > avg_thres  )
	{
		rp1 = r1
		s = A%*%matrix(r1)
		r1 = s/sqrt(sum(s^2))
		cat('difference1= ',sum(abs( r1-rp1 )),'\n')
	}

	pc1 = X1%*%matrix(r1)
	X2 = apply(X1,2,function(x){ residuals(lm(x~pc1)) } ) 
	B = t(X2)%*%X2
	r2 = rnorm(ncol(X2))
	rp2 = 0
	while( sum(abs(r2-rp2)) > avg_thres )
	{
		rp2 = r2
		s = B%*%matrix(r2)
		r2 = s/sqrt(sum(s^2))
		cat('difference2= ',sum(abs( r2-rp2 )),'\n')
	}
	pc2 = X2%*%matrix(r2)

	pdf( paste(gsub('.pdf','',fileOut),'permute.pdf',sep='_') )
	plot(pc1,pc2,main='Barcode',xlab='pc1',ylab='pc2',col='white')
	uni_label = unique(label)
	for(i in 1:length(uni_label))
	{
		range = which(label==uni_label[i])
		points(pc1[range],pc2[range],col=i,pch=i)
	}

	legend( min(pc1), max(pc2)  , c( uni_label ), col = c( 1:length(uni_label) ), pch = c( 1:length(uni_label) ) )
	dev.off()

	#cbind(pc1,pc2)
}

