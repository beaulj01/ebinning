import os,sys
from collections import Counter,defaultdict
import math
import numpy as np
from pbcore.io.BasH5IO import BasH5Reader
import re
import random
import multiproc
import ebarcode_setup
import baxh5_read
import optparse
import logging
import multiprocessing
from operator import itemgetter

class eBinner:
	def __init__( self ):
		"""
		Parse the options and arguments, then instantiate the logger. 
		"""
		self.__parseArgs( )
		self.__initLog( )

	def run( self ):
		if self.opts.motifs_file != None:
			self.motifs, self.bi_motifs = ebarcode_setup.motifs_from_file( self.opts )
		else:
			self.motifs, self.bi_motifs = ebarcode_setup.build_motif_dict( self.opts )

		baxh5_files      = []
		baxh5_labels     = {}
		for line in open(self.opts.baxh5_files).xreadlines():
			line            = line.strip("\n")
			baxh5_fn        = line.split()[0]
			try:
				label           = line.split()[1]
			except IndexError:
				raise Exception("Please add a label to each bax.h5 fille (in case its a mock mixture)")
			baxh5_labels[baxh5_fn] = label
			baxh5_files.append(baxh5_fn)
		self.opts.baxh5_labels = baxh5_labels

		all_baxh5_bar_fns         = []
		all_baxh5_read_fns        = []
		all_baxh5_labels_fns      = []
		all_baxh5_motif_count_fns = []
		all_baxh5_kmers_fns       = []
		for i,baxh5_file in enumerate(baxh5_files):
			logging.info("Loading data from %s..." % baxh5_file)
			results                       = self.launch_parallel_subread_loading( baxh5_file )
			eBarcode_files,readname_files,label_files, counts_files, kmers_files = zip(*results)

			eBarcode_fn  = baxh5_file.replace("bax.h5", "eBarcode")
			baxh5_read.cat_list_of_files(eBarcode_files, eBarcode_fn)
			all_baxh5_bar_fns.append(eBarcode_fn)

			readnames_fn = baxh5_file.replace("bax.h5", "reads")
			baxh5_read.cat_list_of_files(readname_files, readnames_fn)
			all_baxh5_read_fns.append(readnames_fn)

			labels_fn = baxh5_file.replace("bax.h5", "labels")
			baxh5_read.cat_list_of_files(label_files, labels_fn)
			all_baxh5_labels_fns.append(labels_fn)
			
			motif_count_fn = baxh5_file.replace("bax.h5", "motif_counts")
			baxh5_read.cat_list_of_files(counts_files, motif_count_fn)
			all_baxh5_motif_count_fns.append(motif_count_fn)

			kmers_fn = baxh5_file.replace("bax.h5", "kmers")
			baxh5_read.cat_list_of_files(kmers_files, kmers_fn)
			all_baxh5_kmers_fns.append(kmers_fn)

		combined_baxh5_bar_fn          = "binning.barcode.tmp"
		baxh5_read.cat_list_of_files( all_baxh5_bar_fns, combined_baxh5_bar_fn)

		combined_baxh5_read_fn         = "binning.readnames.tmp"
		baxh5_read.cat_list_of_files( all_baxh5_read_fns, combined_baxh5_read_fn)

		combined_baxh5_labels_fn       = "binning.labels.tmp"
		baxh5_read.cat_list_of_files( all_baxh5_labels_fns, combined_baxh5_labels_fn)

		combined_baxh5_motif_counts_fn = "binning.motif_counts.tmp"
		baxh5_read.cat_list_of_files( all_baxh5_motif_count_fns, combined_baxh5_motif_counts_fn)

		combined_baxh5_kmers_fn        = "binning.kmer_columns.tmp"
		baxh5_read.cat_list_of_files( all_baxh5_kmers_fns, combined_baxh5_kmers_fn)

		logging.info("Final cleanup...")
		# Remove duplicate read entries that sometimes arise from splitting for multiproc
		seen        = set()
		skip_idx    = set()
		f_readnames = open("binning.readnames", "w")
		for j,line in enumerate(open(combined_baxh5_read_fn).xreadlines()):
			readname = line.strip()
			if readname in seen:
				skip_idx.add(j)
			else:
				seen.add(readname)
				f_readnames.write(readname+"\n")

		f_barcode      = open("binning.barcode", "w")
		f_labels       = open("binning.labels", "w")
		f_motif_counts = open("binning.motif_counts", "w")
		f_kmers        = open("binning.kmer_columns", "w")
		for j,line in enumerate(open(combined_baxh5_bar_fn, "r").xreadlines()):
			if j not in skip_idx:
				f_barcode.write(line)

		for j,line in enumerate(open(combined_baxh5_labels_fn, "r").xreadlines()):
			if j not in skip_idx:
				f_labels.write(line)

		for j,line in enumerate(open(combined_baxh5_motif_counts_fn, "r").xreadlines()):
			if j not in skip_idx:
				f_motif_counts.write(line)

		for j,line in enumerate(open(combined_baxh5_kmers_fn, "r").xreadlines()):
			if j not in skip_idx:
				f_kmers.write(line)
				break

		f_readnames.close()
		f_barcode.close()
		f_labels.close()
		f_motif_counts.close()
		f_kmers.close()

		os.remove(combined_baxh5_bar_fn)
		os.remove(combined_baxh5_read_fn)
		os.remove(combined_baxh5_labels_fn)
		os.remove(combined_baxh5_motif_counts_fn)
		os.remove(combined_baxh5_kmers_fn)
		logging.info("Done.")

	def launch_parallel_subread_loading( self, baxh5_file ):
		"""
		"""
		logging.debug("Creating tasks...")
		tasks   = multiprocessing.JoinableQueue()
		results = multiprocessing.Queue()
		logging.debug("Done.")

		num_consumers = self.opts.procs

		logging.debug("Starting consumers...")
		consumers     = [ multiproc.Consumer(tasks, results) for i in xrange(num_consumers) ]
		for w in consumers:
			w.start()
		logging.debug("Done.")

		# Enqueue jobs
		num_jobs       = self.opts.procs
		N_target_reads = {}
		reads_left     = self.opts.N_reads
		procs_left     = self.opts.procs
		
		for job in range(num_jobs):
			N_target_reads[job] = int(math.ceil(float(reads_left)/procs_left))
			reads_left -= N_target_reads[job]
			procs_left -= 1
		
		def chunks(l, n):
			"""
			Yield successive n-sized chunks from l.
			"""
			for i in xrange(0, len(l), n):
				yield l[i:i+n]

		logging.debug("Partitioning %s into %s chunks for analysis..." % (baxh5_file, num_jobs))
		reader         = BasH5Reader(baxh5_file)
		zmws           = [r.holeNumber for r in reader]
		reader.close()
		chunksize      = int(math.ceil(float( len(zmws)/self.opts.procs )))
		zmw_chunks     = list(chunks( zmws, chunksize ))
		for chunk_id in range(self.opts.procs):
			n   = N_target_reads[chunk_id]
			idx = zmw_chunks[chunk_id]
			tasks.put(baxh5_read.subread_motif_processor( baxh5_file, chunk_id, idx, n, self.motifs, self.bi_motifs, self.opts) )
		logging.debug("Done (%s)." % chunk_id)
		
		# Add a poison pill for each consumer
		for i in xrange(num_consumers):
			tasks.put(None)

		# Wait for all of the tasks to finish
		tasks.join()
		
		# Start printing results
		logging.info("Combining results data from all chunks...")
		parallel_results = []
		while num_jobs:
			result = results.get()
			parallel_results.append(result)
			num_jobs -= 1
			logging.info("...%s/%s" % ((self.opts.procs-num_jobs),self.opts.procs))
		logging.info("Done.")
		return parallel_results

	def __parseArgs( self ):
		"""Handle command line argument parsing"""

		usage = """%prog [--help] [options]

		USAGE INSTRUCTIONS
		"""

		parser = optparse.OptionParser( usage=usage, description=__doc__ )

		parser.add_option( "-d", "--debug", action="store_true", help="Increase verbosity of logging" )
		parser.add_option( "-i", "--info", action="store_true", help="Add basic logging" )
		parser.add_option( "--logFile", type="str", help="Write logging to file [log.out]" )
		parser.add_option( "--readlength_min", type="int", help="Minimum read length to include for analysis [0]" )
		parser.add_option( "--readlength_max", type="int", help="Maximum read length to include for analysis [10000000]" )
		parser.add_option( "--subreadlength_min", type="int", help="Minimum subread length to include for analysis [0]" )
		parser.add_option( "--min_IPD", type="float", help="If motif IPD is < min_IPD, set to zero [1.0]" )
		parser.add_option( "--min_pct", type="float", help="Remove motifs if they are significant in < min_pct of all reads [5.0]" )
		parser.add_option( "--min_sites", type="int", help="Only use reads with >= N motif sites [5]" )
		parser.add_option( "--max_kmer", type="int", help="Maximum motif size to scan (contiguous motifs) [6]" )
		parser.add_option( "--mod_bases", type="str", help="String containing bases to query for mods ['A']" )
		parser.add_option( "--use_comp", action="store_true", help="Use sequence composition (kmer frequencies) to bin reads [False]" )
		parser.add_option( "--comp_kmer", type="int", help="Kmer size to use for sequence composition measurements [5]" )
		parser.add_option( "--no_SMp", action="store_true", help="Don't use IPDs (SMp scores) to bin reads [False]" )
		parser.add_option( "--minReadScore", type="float", help="Min read score (predicted accuracy) of read [0.8]" )
		parser.add_option( "--maxPausiness", type="float", help="Max pausiness of read of read [0.1]" )
		parser.add_option( "--bipartite", action="store_true", help="Search bipartite motifs also [False]" )
		parser.add_option( "--get_baxh5_stats", action="store_true", help="Generate read stats and exit [False]" )
		parser.add_option( "--motifs_file", type="str", help="File containing specific motifs to include [None]" )
		parser.add_option( "--skip_motifs", type="str", help="File containing specific motifs to SKIP [None]" )
		parser.add_option( "--procs", type="int", help="Number of cores to use [8]" )
		parser.add_option( "--N_reads", type="int", help="Number of qualifying reads to include in analysis [1000000000]" )
		parser.add_option( "--minQV", type="int", help="Basecalls must have QV greater than this to be included in analysis [0]" )
		parser.add_option( "--baxh5_files", type="str", help="File listing the bax.h5 files to extract [None]" )
		parser.set_defaults( logFile="log.out",       \
							 info=False,              \
							 debug=False,             \
							 readlength_min=0,        \
							 readlength_max=10000000, \
							 subreadlength_min=0,     \
							 comp_kmer=5,             \
							 min_IPD=1.0,             \
							 min_pct=5.0,             \
							 min_sites=5,             \
							 max_kmer=4,              \
							 mod_bases="A",           \
							 use_comp=False,          \
							 no_SMp=False,            \
							 minReadScore=0.8,        \
							 maxPausiness=0.1,        \
							 get_baxh5_stats=False,   \
							 bipartite=False,         \
							 motifs_file=None,        \
							 skip_motifs=None,        \
							 procs=8,                 \
							 N_reads=1000000000,      \
							 minQV=0,                 \
							 baxh5_files=None)

		self.opts, args = parser.parse_args( )

		if len(args) != 0:
			parser.error( "Expected 0 arguments." )		

	def __initLog( self ):
		"""Sets up logging based on command line arguments. Allows for three levels of logging:
		logging.error( ): always emitted
		logging.info( ) : emitted with --info or --debug
		logging.debug( ): only with --debug"""

		if os.path.exists(self.opts.logFile):
			os.remove(self.opts.logFile)

		logLevel = logging.DEBUG if self.opts.debug \
					else logging.INFO if self.opts.info \
					else logging.ERROR

		self.logger = logging.getLogger("")
		self.logger.setLevel(logLevel)
		
		# create file handler which logs even debug messages
		fh = logging.FileHandler(self.opts.logFile)
		fh.setLevel(logLevel)
		
		# create console handler with a higher log level
		ch = logging.StreamHandler()
		ch.setLevel(logLevel)
		
		# create formatter and add it to the handlers
		logFormat = "%(asctime)s [%(levelname)s] %(message)s"
		formatter = logging.Formatter(logFormat)
		ch.setFormatter(formatter)
		fh.setFormatter(formatter)
		
		# add the handlers to logger
		self.logger.addHandler(ch)
		self.logger.addHandler(fh)

if __name__=="__main__":
	app     = eBinner()
	results = app.run()