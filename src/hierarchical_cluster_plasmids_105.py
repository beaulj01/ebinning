import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as pylab
import string
import scipy
import scipy.cluster.hierarchy as sch
import scipy.spatial.distance as dist
import fastcluster
import time
import pub_figs
import numpy as np
from collections import Counter
import logging

font_size = 20
style     = "bold"
fontProperties = pub_figs.setup_math_fonts(font_size)

def run( barcodes_fn ):
	################  Default Methods ################
	row_method     = 'ward'
	row_metric     = 'euclidean' #cosine
	column_method  = 'ward'
	column_metric  = 'euclidean'
	color_gradient = 'Greys'

	""" Running with cosine or other distance metrics can often produce negative Z scores
		during clustering, so adjustments to the clustering may be required.
		
	see: http://docs.scipy.org/doc/scipy/reference/cluster.hierarchy.html
	see: http://docs.scipy.org/doc/scipy/reference/spatial.distance.htm  
	color_gradient = red_white_blue|red_black_sky|red_black_blue|red_black_green|yellow_black_blue|green_white_purple'
	"""
	matrix, column_header, row_header = importData(barcodes_fn)

	# Cap the max values in the score matrix
	matrix = np.clip(matrix, a_min=-5, a_max=2.2)

	if len(matrix)>0:
		try:
			heatmap(matrix, row_header, column_header, row_method, column_method, row_metric, column_metric, color_gradient, barcodes_fn)
		except Exception:
			print 'Error using %s ... trying euclidean instead' % row_metric
			row_metric = 'euclidean'
			try:
				heatmap(matrix, row_header, column_header, row_method, column_method, row_metric, column_metric, color_gradient, barcodes_fn)
			except IOError:
				print 'Error with clustering encountered'

def heatmap(x, row_header, column_header, row_method,
			column_method, row_metric, column_metric,
			color_gradient, filename):
	
	print "\nPerforming hiearchical clustering using %s for columns and %s for rows" % (column_metric,row_metric)
		
	"""
	This below code is based in large part on the protype methods:
	http://old.nabble.com/How-to-plot-heatmap-with-matplotlib--td32534593.html
	http://stackoverflow.com/questions/7664826/how-to-get-flat-clustering-corresponding-to-color-clusters-in-the-dendrogram-cre

	x is an m by n ndarray, m observations, n genes
	"""
	
	### Define the color gradient to use based on the provided name
	n = len(x[0]); m = len(x)
	if color_gradient == 'red_white_blue':
		cmap=pylab.cm.bwr
	if color_gradient == 'red_black_sky':
		cmap=RedBlackSkyBlue()
	if color_gradient == 'red_black_blue':
		cmap=RedBlackBlue()
	if color_gradient == 'red_black_green':
		cmap=RedBlackGreen()
	if color_gradient == 'yellow_black_blue':
		cmap=YellowBlackBlue()
	if color_gradient == 'seismic':
		cmap=pylab.cm.seismic
	if color_gradient == 'green_white_purple':
		cmap=pylab.cm.PiYG_r
	if color_gradient == 'coolwarm':
		cmap=pylab.cm.coolwarm
	if color_gradient == "Greys":
		cmap=pylab.cm.Greys
	if color_gradient == "RdYlBu":
		cmap=pylab.cm.RdYlBu

	### Scale the max and min colors so that 0 is white/black
	vmin = x.min()
	vmax = x.max()
	vmax = max([vmax,abs(vmin)])
	# vmin = vmax*-1
	vmin = 0
	norm = matplotlib.colors.Normalize(vmin/2, vmax/2) ### adjust the max and min to scale these colors

	### Scale the Matplotlib window size
	default_window_hight = 20
	# default_window_width = 50
	default_window_width = 20
	fig = pylab.figure(figsize=(default_window_width,default_window_hight)) ### could use m,n to scale here
	color_bar_w = 0.015 ### Sufficient size to show
		
	# ## calculate positions for all elements
	# # ax1, placement of dendrogram 1, on the left of the heatmap
	# #if row_method != None: w1 = 
	# [ax1_x, ax1_y, ax1_w, ax1_h] = [0.05,0.22,0.2,0.6]   ### The second value controls the position of the matrix relative to the bottom of the view
	# width_between_ax1_axr = 0.004
	# height_between_ax1_axc = 0.004 ### distance between the top color bar axis and the matrix
	
	# # axr, placement of row side colorbar
	# [axr_x, axr_y, axr_w, axr_h] = [0.31,0.1,color_bar_w,0.6] ### second to last controls the width of the side color bar - 0.015 when showing
	# axr_x = ax1_x + ax1_w + width_between_ax1_axr
	# axr_y = ax1_y; axr_h = ax1_h
	# width_between_axr_axm = 0.004

	# # axc, placement of column side colorbar
	# [axc_x, axc_y, axc_w, axc_h] = [0.4,0.63,0.5,color_bar_w] ### last one controls the hight of the top color bar - 0.015 when showing
	# axc_x = axr_x + axr_w + width_between_axr_axm
	# axc_y = ax1_y + ax1_h + height_between_ax1_axc
	# height_between_axc_ax2 = 0.004

	# # axm, placement of heatmap for the data matrix
	# [axm_x, axm_y, axm_w, axm_h] = [0.4,0.9,2.5,0.5]
	# axm_x = axr_x + axr_w + width_between_axr_axm
	# axm_y = ax1_y
	# axm_h = ax1_h
	# # axm_w = axc_w
	# axm_w = 0.15

	# # ax2, placement of dendrogram 2, on the top of the heatmap
	# # [ax2_x, ax2_y, ax2_w, ax2_h] = [0.3,0.72,0.6,0.15] ### last one controls hight of the dendrogram
	# [ax2_x, ax2_y, ax2_w, ax2_h] = [0.3,0.72,0.6,0.001] ### last one controls hight of the dendrogram
	# ax2_x = axr_x + axr_w + width_between_axr_axm
	# # ax2_y = ax1_y + ax1_h + height_between_ax1_axc + axc_h + height_between_axc_ax2
	# ax2_y = 0.99
	# ax2_w = axc_w

	# # axcb - placement of the color legend
	# # [axcb_x, axcb_y, axcb_w, axcb_h] = [0.07,0.88,0.18,0.09]
	# [axcb_x, axcb_y, axcb_w, axcb_h] = [0.28,0.86,0.18,0.09]

	#######################
	# CUSTOMIZED LAYOUT
	color_bar_w = 0.0

	# ax1, placement of dendrogram 1, on the left of the heatmap
	ax1_x = 0.05
	ax1_y = 0.18
	ax1_w = 0.03
	ax1_h = 0.3
	width_between_ax1_axr  = 0.004
	height_between_ax1_axc = 0.004 ### distance between the top color bar axis and the matrix
	
	# axr, placement of row side colorbar
	axr_w = color_bar_w ### controls the width of the side color bar - 0.015 when showing
	axr_x = ax1_x + ax1_w + width_between_ax1_axr
	axr_y = ax1_y
	axr_h = ax1_h
	width_between_axr_axm = 0.0

	# axm, placement of heatmap for the data matrix
	[axm_w, axm_h] = [2.5,0.5]
	axm_x = axr_x + axr_w + width_between_axr_axm
	axm_y = ax1_y
	axm_h = ax1_h
	axm_w = 0.25

	# axc, placement of column side colorbar
	axc_x = axr_x + axr_w + width_between_axr_axm
	axc_y = ax1_y + ax1_h + height_between_ax1_axc
	axc_w = axm_w
	axc_h = color_bar_w
	height_between_axc_ax2 = 0.004

	# ax2, placement of dendrogram 2, on the top of the heatmap
	ax2_h = 0.03 ### controls hight of the dendrogram
	ax2_x = axr_x + axr_w + width_between_axr_axm
	ax2_y = ax1_y + ax1_h + height_between_ax1_axc + axc_h + height_between_axc_ax2
	ax2_w = axc_w

	# axcb, placement of the color legend
	axcb_x = 0.05
	axcb_y = 0.88
	axcb_w = 0.30
	axcb_h = 0.04
	#######################

	# Compute and plot top dendrogram
	if column_method != None:
		start_time = time.time()
		d2   = dist.pdist(x.T)
		D2   = dist.squareform(d2)
		ax2  = fig.add_axes([ax2_x, ax2_y, ax2_w, ax2_h], frame_on=True)
		pylab.axis('off')
		# Y2   = sch.linkage(D2, method=column_method, metric=column_metric) ### array-clustering metric - 'average', 'single', 'centroid', 'complete'
		Y2   = fastcluster.linkage(D2, method=column_method, metric=column_metric, preserve_input=False)
		Z2   = sch.dendrogram(Y2, color_threshold=0.0, above_threshold_color="k")
		ind2 = sch.fcluster(Y2,0.3*max(Y2[:,2]),'distance') ### This is the default behavior of dendrogram
		# ax2.set_xticks([]) ### Hides ticks
		# ax2.set_yticks([])
		time_diff = str(round(time.time()-start_time,1))
		logging.info('Column clustering completed in %s seconds' % time_diff)
	else:
		ind2 = ['NA']*len(column_header) ### Used for exporting the flat cluster data
		
	axm  = fig.add_axes([axm_x, axm_y, axm_w, axm_h])  # axes for the data matrix
	axm.axis("off")
	xt   = x

	idx2 = Z2['leaves'] ### apply the clustering for the array-dendrograms to the actual matrix data
	ind2 = ind2[idx2] ### reorder the flat cluster to match the order of the leaves the dendrogram
	xt   = xt[:,idx2]

	# Compute and plot left dendrogram.
	if row_method != None:
		start_time = time.time()
		d1   = dist.pdist(x)
		D1   = dist.squareform(d1)  # full matrix
		ax1  = fig.add_axes([ax1_x, ax1_y, ax1_w, ax1_h], frame_on=False) # frame_on may be False
		Y1   = sch.linkage(D1, method=row_method, metric=row_metric) ### gene-clustering metric - 'average', 'single', 'centroid', 'complete'
		Z1   = sch.dendrogram(Y1, orientation='left', color_threshold=0.0, above_threshold_color="k")
		ind1 = sch.fcluster(Y1,0.4*max(Y1[:,2]),'distance') ### This is the default behavior of dendrogram
		ax1.set_xticks([]) ### Hides ticks
		ax1.set_yticks([])
		time_diff = str(round(time.time()-start_time,1))
		logging.info('Row clustering completed in %s seconds' % time_diff)
	else:
		ind1 = ['NA']*len(row_header) ### Used for exporting the flat cluster data
		
	# Plot distance matrix.
	# axm = fig.add_axes([axm_x, axm_y, axm_w, axm_h])  # axes for the data matrix
	# xt = x
	# if column_method != None:
		# idx2 = Z2['leaves'] ### apply the clustering for the array-dendrograms to the actual matrix data
		# xt   = xt[:,idx2]
		# ind2 = ind2[:,idx2] ### reorder the flat cluster to match the order of the leaves the dendrogram
		# ind2 = ind2[idx2] ### reorder the flat cluster to match the order of the leaves the dendrogram
	if row_method != None:
		idx1 = Z1['leaves'] ### apply the clustering for the gene-dendrograms to the actual matrix data
		xt   = xt[idx1,:]   # xt is transformed x
		# ind1 = ind1[idx1,:] ### reorder the flat cluster to match the order of the leaves the dendrogram
		ind1 = ind1[idx1] ### reorder the flat cluster to match the order of the leaves the dendrogram
	### taken from http://stackoverflow.com/questions/2982929/plotting-results-of-hierarchical-clustering-ontop-of-a-matrix-of-data-in-python/3011894#3011894
	# im = axm.matshow(xt, aspect='auto', origin='lower', cmap=cmap, norm=norm) ### norm=norm added to scale coloring of expression with zero = white or black
	im = axm.matshow(xt, aspect='auto', origin='lower', cmap=cmap) ### norm=norm added to scale coloring of expression with zero = white or black
	axm.set_xticks([]) ### Hides x-ticks
	axm.set_yticks([])

	# Add text
	new_row_header=[]
	new_column_header=[]
	spec_colors = {"B_caccae":     "k", \
				   "B_ovatus":     "indigo", \
				   "B_vulgatus":   "darkcyan", \
				   "C_aerofaciens":"green", \
				   "R_gnavus":     "r", \
				   "B_theta":      "dodgerblue", \
				   "E_coli":       "gold", \
				   "C_bolteae":    "lime"}
	


	for i in range(x.shape[0]):
		if row_method != None:
			if len(row_header)<100: ### Don't visualize gene associations when more than 100 rows
				# row_name    = row_header[idx1[i]].replace("-","_")
				# spec_name   = "_".join(row_name.split("_")[:2])
				row_name    = row_header[idx1[i]]
				contig      = "-".join(row_name.split("-")[-2:])
				label       = "-".join(row_name.split("-")[:-2])
				# axm.text(x.shape[1]-0.2, i-0.3, '  '+spec_labels[spec_name]+": "+contig_name, color=spec_colors[spec_name], fontsize=24)
				# axm.text(x.shape[1]-0.2, i-0.3, '  '+label+": "+contig, color='k', fontsize=18)
				axm.text(x.shape[1]-0.2, i-0.3, "  "+contig, color="k", fontsize=18)
				# axm.text(x.shape[1]-0.2, i-0.3, "  "+contig, color=c, fontsize=18)
			new_row_header.append(row_header[idx1[i]])
		else:
			if len(row_header)<100: ### Don't visualize gene associations when more than 100 rows
				axm.text(x.shape[1]-0.2, i, '  '+row_header[i]) ### When not clustering rows
			new_row_header.append(row_header[i])
	for i in range(x.shape[1]):
		if column_method != None:
			# axm.text(i, -0.9, ' '+column_header[idx2[i]], rotation=270, verticalalignment="top") # rotation could also be degrees
			# new_column_header.append(column_header[idx2[i]])
			axm.text(i+0.2, -0.65, ' '+column_header[idx2[i]], rotation=45, verticalalignment="top", horizontalalignment="right", fontsize=18) # rotation could also be degrees
			new_column_header.append(column_header[idx2[i]])
		else: ### When not clustering columns
			axm.text(i, -0.9, ' '+column_header[i], rotation=45, verticalalignment="center", horizontalalignment="right")
			new_column_header.append(column_header[i])

	# Plot colside colors
	# axc --> axes for column side colorbar
	# if column_method != None:
	# 	axc = fig.add_axes([axc_x, axc_y, axc_w, axc_h])  # axes for column side colorbar
	# 	cmap_c = matplotlib.colors.ListedColormap(['r', 'g', 'b', 'y', 'w', 'k', 'm'])
	# 	dc = np.array(ind2, dtype=int)
	# 	dc.shape = (1,len(ind2)) 
	# 	im_c = axc.matshow(dc, aspect='auto', origin='lower', cmap=cmap_c)
	# 	axc.set_xticks([]) ### Hides ticks
	# 	axc.set_yticks([])
	
	# Plot rowside colors
	# axr --> axes for row side colorbar
	# if row_method != None:
	# 	axr = fig.add_axes([axr_x, axr_y, axr_w, axr_h])  # axes for column side colorbar
	# 	dr = np.array(ind1, dtype=int)
	# 	dr.shape = (len(ind1),1)
	# 	#print ind1, len(ind1)
	# 	cmap_r = matplotlib.colors.ListedColormap(['r', 'g', 'b', 'y', 'w', 'k', 'm'])
	# 	im_r = axr.matshow(dr, aspect='auto', origin='lower', cmap=cmap_r)
	# 	axr.set_xticks([]) ### Hides ticks
	# 	axr.set_yticks([])

	# Plot color legend
	axcb = fig.add_axes([axcb_x, axcb_y, axcb_w, axcb_h], frame_on=False)  # axes for colorbar
	norm = matplotlib .colors.Normalize(vmin=-1, vmax=vmax)
	cb = matplotlib.colorbar.ColorbarBase(axcb, norm=norm, ticks=[-1.0, 0.0, 1.0, vmax], cmap=cmap, orientation='horizontal')
	# axcb.set_title("colorkey")
	
	if '/' in filename:
		dataset_name = string.split(filename,'/')[-1][:-4]
		root_dir = string.join(string.split(filename,'/')[:-1],'/')+'/'
	else:
		dataset_name = string.split(filename,'\\')[-1][:-4]
		root_dir = string.join(string.split(filename,'\\')[:-1],'\\')+'\\'
	filename = '%s_%s_%s.pdf' % (dataset_name,column_metric,row_metric)
	cb.set_label("Methylation score", fontsize=18)
	cb.ax.tick_params(labelsize=18)
	exportFlatClusterData(filename, new_row_header,new_column_header,xt,ind1,ind2)

	### Render the graphic
	# if len(row_header)>50 or len(column_header)>50:
	# 	pylab.rcParams['font.size'] = 5
	# else:
	# 	pylab.rcParams['font.size'] = 8

	pylab.rcParams['font.size'] = 20

	pylab.savefig(filename)
	print 'Exporting:',filename
	filename = filename[:-3]+'png'
	pylab.savefig(filename, dpi=100) #,dpi=200
	pylab.show()

def getColorRange(x):
	""" Determines the range of colors, centered at zero, for normalizing cmap """
	vmax=x.max()
	vmin=x.min()
	if vmax<0 and vmin<0: direction = 'negative'
	elif vmax>0 and vmin>0: direction = 'positive'
	else: direction = 'both'
	if direction == 'both':
		vmax = max([vmax,abs(vmin)])
		vmin = -1*vmax
		return vmax,vmin
	else:
		return vmax,vmin
	
################# Export the flat cluster data #################

def exportFlatClusterData(filename, new_row_header,new_column_header,xt,ind1,ind2):
	""" Export the clustered results as a text file, only indicating the flat-clusters rather than the tree """
	
	filename = string.replace(filename,'.pdf','.txt')
	export_text = open(filename,'w')
	column_header = string.join(['UID','row_clusters-flat']+new_column_header,'\t')+'\n' ### format column-names for export
	export_text.write(column_header)
	column_clusters = string.join(['column_clusters-flat','']+ map(str, ind2),'\t')+'\n' ### format column-flat-clusters for export
	export_text.write(column_clusters)
	
	### The clusters, dendrogram and flat clusters are drawn bottom-up, so we need to reverse the order to match
	new_row_header = new_row_header[::-1]
	xt = xt[::-1]
	
	### Export each row in the clustered data matrix xt
	i=0
	for row in xt:
		export_text.write(string.join([new_row_header[i],str(ind1[i])]+map(str, row),'\t')+'\n')
		i+=1
	export_text.close()
	
	### Export as CDT file
	filename = string.replace(filename,'.txt','.cdt')
	export_cdt = open(filename,'w')
	column_header = string.join(['UNIQID','NAME','GWEIGHT']+new_column_header,'\t')+'\n' ### format column-names for export
	export_cdt.write(column_header)
	eweight = string.join(['EWEIGHT','','']+ ['1']*len(new_column_header),'\t')+'\n' ### format column-flat-clusters for export
	export_cdt.write(eweight)
	
	### Export each row in the clustered data matrix xt
	i=0
	for row in xt:
		export_cdt.write(string.join([new_row_header[i]]*2+['1']+map(str, row),'\t')+'\n')
		i+=1
	export_cdt.close()

################# Create Custom Color Gradients #################
#http://matplotlib.sourceforge.net/examples/pylab_examples/custom_cmap.html

def RedBlackSkyBlue():
	cdict = {'red':   ((0.0, 0.0, 0.0),
					   (0.5, 0.0, 0.1),
					   (1.0, 1.0, 1.0)),
	
			 'green': ((0.0, 0.0, 0.9),
					   (0.5, 0.1, 0.0),
					   (1.0, 0.0, 0.0)),
	
			 'blue':  ((0.0, 0.0, 1.0),
					   (0.5, 0.1, 0.0),
					   (1.0, 0.0, 0.0))
			}

	my_cmap = matplotlib.colors.LinearSegmentedColormap('my_colormap',cdict,256)
	return my_cmap

def RedBlackBlue():
	cdict = {'red':   ((0.0, 0.0, 0.0),
					   (0.5, 0.0, 0.1),
					   (1.0, 1.0, 1.0)),

			 'green': ((0.0, 0.0, 0.0),
					   (1.0, 0.0, 0.0)),
	
			 'blue':  ((0.0, 0.0, 1.0),
					   (0.5, 0.1, 0.0),
					   (1.0, 0.0, 0.0))
			}

	my_cmap = matplotlib.colors.LinearSegmentedColormap('my_colormap',cdict,256)
	return my_cmap

def RedBlackGreen():
	cdict = {'red':   ((0.0, 0.0, 0.0),
					   (0.5, 0.0, 0.1),
					   (1.0, 1.0, 1.0)),
	
			 'blue': ((0.0, 0.0, 0.0),
					   (1.0, 0.0, 0.0)),
	
			 'green':  ((0.0, 0.0, 1.0),
					   (0.5, 0.1, 0.0),
					   (1.0, 0.0, 0.0))
			}
	
	my_cmap = matplotlib.colors.LinearSegmentedColormap('my_colormap',cdict,256)
	return my_cmap

def YellowBlackBlue():
	cdict = {'red':   ((0.0, 0.0, 0.0),
					   (0.5, 0.0, 0.1),
					   (1.0, 1.0, 1.0)),
	
			 'green': ((0.0, 0.0, 0.8),
					   (0.5, 0.1, 0.0),
					   (1.0, 1.0, 1.0)),
	
			 'blue':  ((0.0, 0.0, 1.0),
					   (0.5, 0.1, 0.0),
					   (1.0, 0.0, 0.0))
			}
	### yellow is created by adding y = 1 to RedBlackSkyBlue green last tuple
	### modulate between blue and cyan using the last y var in the first green tuple
	my_cmap = matplotlib.colors.LinearSegmentedColormap('my_colormap',cdict,256)
	return my_cmap

################# General data import methods #################

def importData(filename):
	start_time = time.time()
	matrix=[]
	row_header=[]
	first_row=True

	if '/' in filename:
		dataset_name = string.split(filename,'/')[-1][:-4]
	else:
		dataset_name = string.split(filename,'\\')[-1][:-4]
		
	for line in open(filename,'rU').xreadlines():         
		t = string.split(line[:-1],'\t') ### remove end-of-line character - file is tab-delimited
		if first_row:
			column_header = t[1:]
			first_row=False
		else:
			if ' ' not in t and '' not in t: ### Occurs for rows with missing data
				s = map(float,t[1:])
				if (abs(max(s)-min(s)))>0:
					matrix.append(s)
					row_header.append(t[0])
			
	time_diff = str(round(time.time()-start_time,1))
	try:
		print '\n%d rows and %d columns imported for %s in %s seconds...' % (len(matrix),len(column_header),dataset_name,time_diff)
	except Exception:
		print 'No data in input file.'; force_error
	return np.array(matrix), column_header, row_header