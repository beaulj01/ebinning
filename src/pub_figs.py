import matplotlib.pyplot as plt
import matplotlib.font_manager as font_manager
from matplotlib import rc,rcParams
import re

def setup_math_fonts(font_size, font='Helvetica'):
	"""Set the font that will be used for all labels & legend text."""
	fontProperties = {'family':'sans-serif','sans-serif':[font],'weight' : 'normal', 'size' : font_size}
	rc('font',**fontProperties)
	rc('text', usetex=True)
	rc('text.latex', preamble=r'\usepackage{arev}')
	rcParams['mathtext.fontset'] = 'custom'
	rcParams['mathtext.rm']      = 'Bitstream Vera Sans'
	rcParams['mathtext.it']      = 'Bitstream Vera Sans:italic'
	rcParams['mathtext.bf']      = 'Bitstream Vera Sans:bold'
	return fontProperties

def init_fig(x=8, y=8):
	FIG_SIZE = (x,y)
	return plt.figure(figsize=FIG_SIZE)

def tex_escape(text):
	"""
		:param text: a plain text message
		:return: the message escaped to appear correctly in LaTeX
	"""
	conv = {
		'&': r'\&',
		'%': r'\%',
		'$': r'\$',
		'#': r'\#',
		'_': r'\_',
		'{': r'\{',
		'}': r'\}',
		'~': r'\textasciitilde{}',
		'^': r'\^{}',
		'\\': r'\textbackslash{}',
		'<': r'\textless',
		'>': r'\textgreater',
	}
	regex = re.compile('|'.join(re.escape(unicode(key)) for key in sorted(conv.keys(), key = lambda item: - len(item))))
	return regex.sub(lambda match: conv[match.group()], text)

def make_square_axes( ax ):
	"""Read in existing ax limits, and return modified ax such that they are the same in x and y."""
	all_min = min(ax)
	all_max = max(ax)
	return (all_min, all_max, all_min, all_max)

def change_font_size( ax, font_size=20, bottom_adjust=0.15, left_adjust=0, hspace=0.5, wspace=0.2 ):
	for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] + ax.get_xticklabels() + ax.get_yticklabels()):
		item.set_fontsize(font_size)
	plt.gcf().subplots_adjust(bottom=bottom_adjust, left=left_adjust, hspace=hspace, wspace=wspace)

def remove_top_right_axes( ax ):
	ax.spines["right"].set_visible(False)
	ax.spines["top"].set_visible(False)
	ax.tick_params(axis='both', direction='in')
	ax.get_xaxis().tick_bottom()
	ax.get_yaxis().tick_left()

def remove_axes_border_and_spines( ax ):
	ax.spines["left"].set_visible(False)
	ax.spines["right"].set_visible(False)
	ax.spines["top"].set_visible(False)
	ax.spines["bottom"].set_visible(False)
	# ax.tick_params(axis='both', direction='out')
	# ax.get_xaxis().tick_bottom()
	# ax.get_yaxis().tick_left()

def remove_top_left_axes( ax ):
	ax.spines["left"].set_visible(False)
	ax.spines["top"].set_visible(False)
	ax.tick_params(axis='both', direction='out')
	ax.get_xaxis().tick_bottom()
	ax.get_yaxis().tick_right()

def format_pb20_assembly_axes():
	setup_math_fonts(18)
	fig = plt.figure(figsize=[15,10])
	ax  = fig.add_axes([0.15, 0.4, 0.8, 0.4])
	return ax,fig